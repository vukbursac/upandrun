﻿using ProtoBuf;
using System;

namespace Runtime.Models.Response
{
    [ProtoContract]
    public class LambdaResponse
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        public string Name { get; set; }

        public LambdaResponse()
        {
        }

        public LambdaResponse(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}