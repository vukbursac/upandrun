﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Runtime.Models.Request
{
    [ProtoContract]
    public class RunLambdaRequest
    {
        [ProtoMember(1)]
        public Guid UserId { get; set; }

        [ProtoMember(2)]
        public Guid LambdaId { get; set; }

        [ProtoMember(3)]
        public List<string> LambdaParams { get; set; }

        [ProtoMember(4)]
        public List<byte[]> Files { get; set; }

        [ProtoMember(5)]
        public List<string> FileNames { get; set; }

        public RunLambdaRequest()
        {
        }

        public RunLambdaRequest(Guid userId, Guid lambdaId, List<string> lambdaParams)
        {
            UserId = userId;
            LambdaId = lambdaId;
            LambdaParams = lambdaParams;
        }

        public RunLambdaRequest(Guid userId, Guid lambdaId, List<string> lambdaParams, List<string> fileNamesList, List<byte[]> filesList)
        {
            UserId = userId;
            LambdaId = lambdaId;
            LambdaParams = lambdaParams;
            FileNames = fileNamesList;
            Files = filesList;
        }
    }
}