﻿using System;
using ProtoBuf;

namespace Runtime.Models
{
    [ProtoContract]
    public class CreateLambdaRequest
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public Guid UserId { get; set; }

        [ProtoMember(3)]
        public byte[] LambdaFile { get; set; }

        public CreateLambdaRequest()
        {
        }
    }
}