﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Runtime.Models.Request
{
    [ProtoContract]
    public class RunLambdaOnfileRequest
    {
        [ProtoMember(1)]
        public Guid UserId { get; set; }

        [ProtoMember(2)]
        public Guid LambdaId { get; set; }

        [ProtoMember(3)]
        public List<byte[]> Files { get; set; }

        [ProtoMember(4)]
        public List<string> FileNames { get; set; }

        public RunLambdaOnfileRequest()
        {
        }
    }
}