﻿using System;

namespace UpAndRun.Storage.Helpers
{
    public static class GuidValidator
    {
        public static bool IsGuid(string id)
        {
            return Guid.TryParse(id, result: out Guid fileId);
        }
    }
}
