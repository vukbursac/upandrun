﻿using System;
using System.Collections.Generic;

namespace UpAndRun.Storage.Data.Entities
{
    public class CrateEntity : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Guid> SubCratesIds { get; set; } = new List<Guid>();
        public ICollection<Guid> FileIds { get; set; } = new List<Guid>();
        public Guid UserId { get; set; }
        public ICollection<string> BigFileIds { get; set; } = new List<string>();

        public CrateEntity()
        {
        }

        public CrateEntity(string name)
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
            Name = name;
        }
    }
}