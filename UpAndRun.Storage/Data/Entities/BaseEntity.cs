﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using UpAndRun.Storage.Data.Interfaces;

namespace UpAndRun.Storage.Data.Entities
{
    public abstract class BaseEntity : IBaseEntity
    {
        [BsonId]
        public Guid Id { get; set; }

        public DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? ParentCrateId { get; set; }
    }
}