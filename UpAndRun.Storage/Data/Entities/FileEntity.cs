﻿using System;

namespace UpAndRun.Storage.Data.Entities
{
    public class FileEntity : BaseEntity
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public int Size { get; set; }

        public byte[] FileObject { get; set; }

        public FileEntity()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }

        public FileEntity(string fileName, string fileType, string description, int size, byte[] fileObject)
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
            FileName = fileName;
            FileType = fileType;
            Description = description;
            Size = size;
            FileObject = fileObject;
        }

        public FileEntity(string fileName, string fileType, int size, byte[] fileObject)
        {
            FileName = fileName;
            FileType = fileType;
            Size = size;
            FileObject = fileObject;
        }
    }
}