﻿using System;

namespace UpAndRun.Storage.Data.Entities
{
    public class BigFileEntity
    {
        private const int MaxFileSize = 524288000;
        public string Id { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public int Size { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? ParentCrateId { get; set; }

        public byte[] FileObject { get; set; }

        public BigFileEntity()
        {
            CreationDate = DateTime.Now;
        }
    }
}