﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Storage.Data.Entities;
using UpAndRun.Storage.Data.Interfaces;
using UpAndRun.Storage.Database;

namespace UpAndRun.Storage.Data.Repositories
{
    public class CrateRepository : MongoRepository<CrateEntity>, ICrateRepository
    {
        private readonly IMongoCollection<CrateEntity> crateCollection;
        private readonly FilterDefinitionBuilder<CrateEntity> filterBuilder = Builders<CrateEntity>.Filter;
        private readonly UpdateDefinitionBuilder<CrateEntity> updateDefinitionBuilder = Builders<CrateEntity>.Update;

        public CrateRepository(IOptions<DatabaseSettings> databaseSettingsOptions, IMongoDatabase database) : base(database, databaseSettingsOptions?.Value.CratesCollectionName)
        {
            var databaseSettings = databaseSettingsOptions?.Value ?? throw new ArgumentNullException(nameof(databaseSettingsOptions));
            crateCollection = database.GetCollection<CrateEntity>(databaseSettings.CratesCollectionName);
        }

        public async Task<IEnumerable<CrateEntity>> GetAll(Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.UserId, userId), filterBuilder.Eq(e => e.IsDeleted, false));
            return await crateCollection.Find(filter).ToListAsync();
        }

        public async Task<CrateEntity> GetById(Guid id, Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.Id, id), filterBuilder.Eq(e => e.IsDeleted, false), filterBuilder.Eq(e => e.UserId, userId));
            return await crateCollection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task AddChildCrateId(Guid parentEntityId, Guid subCrateId)
        {
            var parentEntity = await GetByIdAsync(parentEntityId);

            if (parentEntity == null || parentEntity.SubCratesIds.Contains(subCrateId))
            {
                return;
            }
            parentEntity.SubCratesIds.Add(subCrateId);

            await UpdateSubcrateIds(parentEntity);
        }

        public async Task RemoveChildCrateId(Guid parentEntityId, Guid subCrateId)
        {
            var parentEntity = await GetByIdAsync(parentEntityId);
            if (parentEntity == null || !parentEntity.SubCratesIds.Contains(subCrateId))
            {
                return;
            }
            parentEntity.SubCratesIds.Remove(subCrateId);

            await UpdateSubcrateIds(parentEntity);
        }

        public async Task UpdateFileListAsync(CrateEntity parentEntity, Guid fileId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.Eq(e => e.Id, parentEntity.Id);

            if (!parentEntity.FileIds.Contains(fileId))
            {
                parentEntity.FileIds.Add(fileId);
                UpdateDefinition<CrateEntity> fileIdsUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.FileIds, parentEntity.FileIds);
                await crateCollection.UpdateOneAsync(filter, fileIdsUpdateDefinition);
            }
        }

        public async Task RemoveFileListAsync(CrateEntity parentEntity, Guid fileId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.Eq(e => e.Id, parentEntity.Id);

            if (parentEntity.FileIds.Contains(fileId))
            {
                parentEntity.FileIds.Remove(fileId);
                UpdateDefinition<CrateEntity> fileIdsUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.FileIds, parentEntity.FileIds);
                await crateCollection.UpdateOneAsync(filter, fileIdsUpdateDefinition);
            }
        }

        public async Task MoveCrate(CrateEntity crate, Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(existingCrate => existingCrate.Id, crate.Id), filterBuilder.Eq(existingCrate => existingCrate.IsDeleted, false));
            UpdateDefinition<CrateEntity> cratesUpdateDefinition = updateDefinitionBuilder.Set(existingCrate => existingCrate.ParentCrateId, crate.ParentCrateId);
            await crateCollection.UpdateOneAsync(filter, cratesUpdateDefinition);
        }

        public async Task UpdateCrateName(CrateEntity renamedCrate, Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.UserId, userId), filterBuilder.Eq(e => e.Id, renamedCrate.Id));
            UpdateDefinition<CrateEntity> cratesUpdateDefinition = updateDefinitionBuilder.Set(existingCrate => existingCrate.Name, renamedCrate.Name);
            await crateCollection.UpdateOneAsync(filter, cratesUpdateDefinition);
        }

        public async Task UpdateIsDeletedStatus(CrateEntity entity, Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.UserId, userId), filterBuilder.Eq(e => e.Id, entity.Id));
            FilterDefinition<CrateEntity> filterForChildBuckets = filterBuilder.Eq(e => e.ParentCrateId, entity.Id);

            UpdateDefinition<CrateEntity> isDeletedUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.IsDeleted, true);

            await crateCollection.UpdateManyAsync(filterForChildBuckets, isDeletedUpdateDefinition);
            await crateCollection.UpdateOneAsync(filter, isDeletedUpdateDefinition);
        }

        public async Task DeleteCrate(CrateEntity entity, Guid userId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.UserId, userId), filterBuilder.Eq(e => e.Id, entity.Id));

            await crateCollection.DeleteOneAsync(filter);
        }

        private async Task UpdateSubcrateIds(CrateEntity parentEntity)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.Eq(e => e.Id, parentEntity.Id);
            UpdateDefinition<CrateEntity> subBucketsUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.SubCratesIds, parentEntity.SubCratesIds);
            await crateCollection.UpdateOneAsync(filter, subBucketsUpdateDefinition);
        }

        public async Task UpdateBigFileListAsync(CrateEntity parentEntity, string fileId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.Eq(e => e.Id, parentEntity.Id);

            if (!parentEntity.BigFileIds.Contains(fileId))
            {
                parentEntity.BigFileIds.Add(fileId);
                UpdateDefinition<CrateEntity> fileIdsUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.BigFileIds, parentEntity.BigFileIds);
                await crateCollection.UpdateOneAsync(filter, fileIdsUpdateDefinition);
            }
        }

        public async Task RemoveBigFileListAsync(CrateEntity parentEntity, string bigFileId)
        {
            FilterDefinition<CrateEntity> filter = filterBuilder.Eq(e => e.Id, parentEntity.Id);

            if (parentEntity.BigFileIds.Contains(bigFileId))
            {
                parentEntity.BigFileIds.Remove(bigFileId);
                UpdateDefinition<CrateEntity> fileIdsUpdateDefinition = updateDefinitionBuilder.Set(entity => entity.BigFileIds, parentEntity.BigFileIds);
                await crateCollection.UpdateOneAsync(filter, fileIdsUpdateDefinition);
            }
        }
    }
}