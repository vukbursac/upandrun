﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UpAndRun.Storage.Data.Interfaces;

namespace UpAndRun.Storage.Data.Repositories
{
    public class MongoRepository<TDocument> : IMongoRepository<TDocument> where TDocument : IBaseEntity
    {
        private readonly IMongoCollection<TDocument> collection;
        private readonly FilterDefinitionBuilder<TDocument> filterBuilder = Builders<TDocument>.Filter;

        public MongoRepository(IMongoDatabase database, string collectionName)
        {
            collection = database.GetCollection<TDocument>(collectionName);
        }

        public async Task CreateAsync(TDocument entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            await collection.InsertOneAsync(entity);
        }

        public async Task<IEnumerable<TDocument>> GetAllAsync()
        {
            return await collection.Find(filterBuilder.Eq(e => e.IsDeleted, false)).ToListAsync();
        }

        public async Task<IEnumerable<TDocument>> GetAllFilteredAsync(Expression<Func<TDocument, bool>> filter)
        {
            return await collection.Find(filter).ToListAsync();
        }

        public async Task<TDocument> GetByIdAsync(Guid id)
        {
            FilterDefinition<TDocument> filter = filterBuilder.And(filterBuilder.Eq(e => e.Id, id), filterBuilder.Eq(e => e.IsDeleted, false));
            return await collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<TDocument> GetFilteredAsync(Expression<Func<TDocument, bool>> filter)
        {
            return await collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            FilterDefinition<TDocument> filter = filterBuilder.Eq(e => e.Id, id);
            await collection.DeleteOneAsync(filter);
        }

        public async Task UpdateAsync(TDocument entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            FilterDefinition<TDocument> filter = filterBuilder.Eq(e => e.Id, entity.Id);
            await collection.ReplaceOneAsync(filter, entity);
        }
    }
}