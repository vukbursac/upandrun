﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using Storage.Models.Requests;
using UpAndRun.Storage.Data.Entities;
using UpAndRun.Storage.Data.Interfaces;
using UpAndRun.Storage.Database;

namespace UpAndRun.Storage.Data.Repositories
{
    public class FileRepository : MongoRepository<FileEntity>, IFileRepository
    {
        private readonly IMongoCollection<FileEntity> filesCollection;
        private readonly FilterDefinitionBuilder<FileEntity> filterBuilder = Builders<FileEntity>.Filter;
        private readonly UpdateDefinitionBuilder<FileEntity> updateBuilder = Builders<FileEntity>.Update;
        private readonly GridFSBucket filesBucket;
        private readonly FilterDefinitionBuilder<GridFSFileInfo> filterBigFilesBuilder = Builders<GridFSFileInfo>.Filter;
        private readonly UpdateDefinitionBuilder<GridFSFileInfo> updateBigFilesBuilder = Builders<GridFSFileInfo>.Update;
        private readonly IMongoCollection<GridFSFileInfo> gridFsFilesCollection;

        public FileRepository(IOptions<DatabaseSettings> databaseSettingsOptions, IMongoDatabase database) : base(database, databaseSettingsOptions?.Value.FilesCollectionName)
        {
            var databaseSettings = databaseSettingsOptions?.Value ?? throw new ArgumentNullException(nameof(databaseSettingsOptions));
            filesCollection = database.GetCollection<FileEntity>(databaseSettings.FilesCollectionName);
            filesBucket = new GridFSBucket(database);
            gridFsFilesCollection = database.GetCollection<GridFSFileInfo>("fs.files");
        }

        public async Task<Result<ICollection<BigFileEntity>>> GetAllBigFilesInParentCrateAsync(Guid parentCrateId)
        {
            var binGuid = new BsonBinaryData(parentCrateId, GuidRepresentation.Standard);
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq(e => e.Metadata["ParentCrateId"], binGuid),
                filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));
            var results = await filesBucket.FindAsync(filter);

            List<GridFSFileInfo> fileInfoList = new List<GridFSFileInfo>();

            await results.ForEachAsync(e => fileInfoList.Add(e));

            ICollection<BigFileEntity> fileEntities = new List<BigFileEntity>();

            foreach (var fileInfo in fileInfoList)
            {
                var fileEntity = new BigFileEntity
                {
                    FileName = fileInfo.Filename,
                    FileType = fileInfo.Metadata["FileType"].ToString(),
                    Description = fileInfo.Metadata["Description"].ToString(),
                    Size = fileInfo.Metadata["Size"].ToInt32(),
                    ParentCrateId = (Guid?)fileInfo.Metadata["ParentCrateId"],
                    IsDeleted = fileInfo.Metadata["IsDeleted"].AsBoolean,
                    Id = fileInfo.Id.ToString()
                };
                fileEntities.Add(fileEntity);
            }

            return Result.Success(fileEntities);
        }

        public async Task<string> UploadBigFileAsync(BigFileEntity bigFileEntity)
        {
            var fileMetadata = new FileMetadata
            {
                FileName = bigFileEntity.FileName,
                FileType = bigFileEntity.FileType,
                Description = bigFileEntity.Description,
                Size = bigFileEntity.Size,
                ParentCrateId = bigFileEntity.ParentCrateId.Value,
                IsDeleted = bigFileEntity.IsDeleted,
            };

            GridFSFileInfo fileInfo = new GridFSFileInfo(bigFileEntity.ToBsonDocument());
            var id = await filesBucket.UploadFromBytesAsync(bigFileEntity.FileName, bigFileEntity.FileObject,
                new GridFSUploadOptions
                {
                    Metadata = fileMetadata.ToBsonDocument()
                });
            return id.ToString();
        }

        public async Task<Result<BigFileEntity>> GetBigFileByIdAsync(ObjectId id)
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", id), filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));

            try
            {
                var results = await filesBucket.FindAsync(filter);
                var file = await results.FirstOrDefaultAsync();
                if (file == null)
                {
                    return null;
                }
                var fileMetadata = file.Metadata;
                var bigFileEntity = new BigFileEntity
                {
                    FileName = file.Filename,
                    FileType = fileMetadata["FileType"].ToString(),
                    Description = fileMetadata["Description"].ToString(),
                    Size = fileMetadata["Size"].ToInt32(),
                    ParentCrateId = (Guid?)fileMetadata["ParentCrateId"],
                    IsDeleted = fileMetadata["IsDeleted"].AsBoolean,
                    Id = file.Id.ToString()
                };
                return Result.Success(bigFileEntity);
            }
            catch (Exception)
            {
                return Result.Failure<BigFileEntity>("File not found.");
            }
        }

        public async Task<Result<BigFileEntity>> GetBigFileByIdForDownloadAsync(ObjectId id)
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", id), filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));
            var results = await filesBucket.FindAsync(filter);
            var file = await results.FirstOrDefaultAsync();
            var fileMetadata = file.Metadata;

            var fileObject = await filesBucket.DownloadAsBytesAsync(id);

            if (fileObject == null)
            {
                return Result.Failure<BigFileEntity>("File not found.");
            }

            var bigFileEntity = new BigFileEntity
            {
                FileName = fileMetadata["FileName"].ToString(),
                FileType = fileMetadata["FileType"].ToString(),
                Description = fileMetadata["Description"].ToString(),
                Size = fileMetadata["Size"].ToInt32(),
                ParentCrateId = (Guid?)fileMetadata["ParentCrateId"],
                IsDeleted = fileMetadata["IsDeleted"].AsBoolean,
                FileObject = fileObject
            };
            return Result.Success(bigFileEntity);
        }

        public async Task<IEnumerable<BigFileEntity>> GetAllBigFilesAsync()
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false);
            var results = await filesBucket.FindAsync(filter);
            var documentList = await results.ToListAsync();
            List<BigFileEntity> fileEntities = new List<BigFileEntity>(documentList.Count);
            foreach (var document in documentList)
            {
                var fileMetadata = document.Metadata;
                var bigFileEntity = new BigFileEntity
                {
                    FileName = fileMetadata["FileName"].ToString(),
                    FileType = fileMetadata["FileType"].ToString(),
                    Description = fileMetadata["Description"].ToString(),
                    Size = fileMetadata["Size"].ToInt32(),
                    ParentCrateId = (Guid?)fileMetadata["ParentCrateId"],
                    IsDeleted = fileMetadata["IsDeleted"].AsBoolean,
                    CreationDate = document.UploadDateTime,
                    Id = document.Id.ToString()
                };
                fileEntities.Add(bigFileEntity);
            }

            return fileEntities;
        }

        public async Task<Result> DeleteBigFileAsync(ObjectId id)
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", id), filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));

            try
            {
                await filesBucket.DeleteAsync(id);
            }
            catch (GridFSFileNotFoundException)
            {
                return Result.Failure("File not found.");
            }
            return Result.Success();
        }

        public async Task<Result> SoftDeleteBigFileAsync(ObjectId id)
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", id), filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));

            try
            {
                var results = await filesBucket.FindAsync(filter);
                var file = await results.FirstOrDefaultAsync();
            }
            catch (GridFSFileNotFoundException)
            {
                return Result.Failure("File not found.");
            }

            var update = updateBigFilesBuilder.Set("metadata.IsDeleted", true);
            gridFsFilesCollection.UpdateOne(filter, update);
            return Result.Success();
        }

        public async Task UpdateFileAsync(Guid id, UpdateFileRequest file)
        {
            if (file == null)
            {
                return;
            }
            FilterDefinition<FileEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.Id, id), filterBuilder.Eq(e => e.IsDeleted, false));
            UpdateDefinition<FileEntity> update;
            if (file.FileName == null)
            {
                update = updateBuilder.Set(e => e.Description, file.Description);
            }
            else if (file.Description == null)
            {
                update = updateBuilder.Set(e => e.FileName, file.FileName);
            }
            else
            {
                update = updateBuilder.Set(e => e.FileName, file.FileName).Set(e => e.Description, file.Description);
            }

            await filesCollection.UpdateOneAsync(filter, update);
        }

        public async Task UpdateFileParentCrateIdAsync(Guid fileId, Guid newParentCrateId)
        {
            if (fileId == new Guid() || newParentCrateId == new Guid())
            {
                return;
            }
            FilterDefinition<FileEntity> filter = filterBuilder.And(filterBuilder.Eq(e => e.Id, fileId), filterBuilder.Eq(e => e.IsDeleted, false));
            UpdateDefinition<FileEntity> update;

            update = updateBuilder.Set(e => e.ParentCrateId, newParentCrateId);

            await filesCollection.UpdateOneAsync(filter, update);
        }

        public async Task SoftDeleteAsync(Guid id)
        {
            FilterDefinition<FileEntity> filter = filterBuilder.Eq(e => e.Id, id);
            var update = updateBuilder.Set(e => e.IsDeleted, true);
            await filesCollection.UpdateOneAsync(filter, update);
        }

        public async Task<Result> UpdateBigFileAsync(ObjectId fileId, UpdateFileRequest updateFileRequest)
        {
            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", fileId),
                filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));

            try
            {
                var results = await filesBucket.FindAsync(filter);
                var file = await results.FirstOrDefaultAsync();
            }
            catch (GridFSFileNotFoundException)
            {
                return Result.Failure("File not found");
            }

            if (updateFileRequest.FileName == null)
            {
                var update = updateBigFilesBuilder.Set("metadata.Description", updateFileRequest.Description);
                gridFsFilesCollection.UpdateOne(filter, update);
            }
            else if (updateFileRequest.Description == null)
            {
                await filesBucket.RenameAsync(fileId, updateFileRequest.FileName);
                var update = updateBigFilesBuilder.Set("metadata.FileName", updateFileRequest.FileName);
                gridFsFilesCollection.UpdateOne(filter, update);
            }
            else
            {
                var update1 = updateBigFilesBuilder.Set("metadata.Description", updateFileRequest.Description);
                gridFsFilesCollection.UpdateOne(filter, update1);
                await filesBucket.RenameAsync(fileId, updateFileRequest.FileName);
                var update2 = updateBigFilesBuilder.Set("metadata.FileName", updateFileRequest.FileName);
                gridFsFilesCollection.UpdateOne(filter, update2);
            }

            return Result.Success();
        }

        public async Task<Result> UpdateBigFileParentCrateIdAsync(MoveBigFileRequest moveBigFileRequest)
        {
            var parsedId = ObjectId.Parse(moveBigFileRequest.FileId);

            FilterDefinition<GridFSFileInfo> filter = filterBigFilesBuilder.And(filterBigFilesBuilder.Eq("_id", parsedId),
                filterBigFilesBuilder.Eq(e => e.Metadata["IsDeleted"], false));

            try
            {
                var results = await filesBucket.FindAsync(filter);
                var file = await results.FirstOrDefaultAsync();
                var update = updateBigFilesBuilder.Set("metadata.ParentCrateId", moveBigFileRequest.NewParentCrateId);
                gridFsFilesCollection.UpdateOne(filter, update);
            }
            catch (Exception)
            {
                return Result.Failure("File not found.");
            }

            return Result.Success();
        }
    }
}