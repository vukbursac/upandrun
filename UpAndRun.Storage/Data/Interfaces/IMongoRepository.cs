﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace UpAndRun.Storage.Data.Interfaces
{
    public interface IMongoRepository<TDocument> where TDocument : IBaseEntity
    {
        Task CreateAsync(TDocument entity);

        Task<IEnumerable<TDocument>> GetAllAsync();

        Task<IEnumerable<TDocument>> GetAllFilteredAsync(Expression<Func<TDocument, bool>> filter);

        Task<TDocument> GetByIdAsync(Guid id);

        Task<TDocument> GetFilteredAsync(Expression<Func<TDocument, bool>> filter);

        Task RemoveAsync(Guid id);

        Task UpdateAsync(TDocument entity);
    }
}