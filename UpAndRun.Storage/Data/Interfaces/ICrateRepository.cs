﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Storage.Data.Entities;

namespace UpAndRun.Storage.Data.Interfaces
{
    public interface ICrateRepository : IMongoRepository<CrateEntity>
    {
        Task<IEnumerable<CrateEntity>> GetAll(Guid userId);

        Task<CrateEntity> GetById(Guid id, Guid userId);

        Task AddChildCrateId(Guid parentEntityId, Guid subBucketId);

        Task RemoveChildCrateId(Guid parentEntityId, Guid subBucketId);

        Task MoveCrate(CrateEntity entity, Guid userId);

        Task DeleteCrate(CrateEntity entity, Guid userId);

        Task UpdateIsDeletedStatus(CrateEntity entity, Guid userId);

        Task UpdateFileListAsync(CrateEntity parentEntity, Guid fileId);

        Task RemoveFileListAsync(CrateEntity parentEntity, Guid fileId);

        Task UpdateBigFileListAsync(CrateEntity parentEntity, string fileId);

        Task RemoveBigFileListAsync(CrateEntity parentEntity, string bigFileId);
    }
}