﻿using System;

namespace UpAndRun.Storage.Data.Interfaces
{
    public interface IBaseEntity
    {
        public Guid Id { get; set; }

        public DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? ParentCrateId { get; set; }
    }
}