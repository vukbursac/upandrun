﻿using CSharpFunctionalExtensions;
using MongoDB.Bson;
using Storage.Models.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Storage.Data.Entities;

namespace UpAndRun.Storage.Data.Interfaces
{
    public interface IFileRepository : IMongoRepository<FileEntity>
    {
        Task UpdateFileAsync(Guid id, UpdateFileRequest file);

        Task UpdateFileParentCrateIdAsync(Guid fileId, Guid newParentCrateId);

        Task SoftDeleteAsync(Guid id);

        Task<string> UploadBigFileAsync(BigFileEntity fileEntity);

        Task<Result<BigFileEntity>> GetBigFileByIdAsync(ObjectId id);

        Task<IEnumerable<BigFileEntity>> GetAllBigFilesAsync();

        Task<Result<BigFileEntity>> GetBigFileByIdForDownloadAsync(ObjectId id);

        Task<Result> DeleteBigFileAsync(ObjectId id);

        Task<Result> UpdateBigFileAsync(ObjectId fileId, UpdateFileRequest updateBigFileRequest);

        Task<Result> UpdateBigFileParentCrateIdAsync(MoveBigFileRequest moveBigFileRequest);

        Task<Result<ICollection<BigFileEntity>>> GetAllBigFilesInParentCrateAsync(Guid parentCrateId);

        Task<Result> SoftDeleteBigFileAsync(ObjectId id);
    }
}