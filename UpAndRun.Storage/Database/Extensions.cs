﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using UpAndRun.Storage.Data.Interfaces;
using UpAndRun.Storage.Data.Repositories;

namespace UpAndRun.Storage.Database
{
    public static class Extensions
    {
        public static IServiceCollection AddMongo(this IServiceCollection services)
        {
            services.AddSingleton(a =>
            {
                var configuration = a.GetService<IConfiguration>();
                var mongoDbSettings = configuration.GetSection(nameof(DatabaseSettings)).Get<DatabaseSettings>();
                var mongoClient = new MongoClient(mongoDbSettings.ConnectionString);
                return mongoClient.GetDatabase(mongoDbSettings.DatabaseName);
            });

            return services;
        }

        public static IServiceCollection AddMongoRepository<TDocument>(this IServiceCollection services, string collectionName) where TDocument : IBaseEntity
        {
            services.AddSingleton<IMongoRepository<TDocument>>(a =>
            {
                var database = a.GetService<IMongoDatabase>();
                return new MongoRepository<TDocument>(database, collectionName);
            });
            return services;
        }
    }
}