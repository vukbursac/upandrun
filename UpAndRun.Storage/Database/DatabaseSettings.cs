﻿namespace UpAndRun.Storage.Database
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string CratesCollectionName { get; set; }
        public string FilesCollectionName { get; set; }
    }
}