﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using UpAndRun.Storage.Business.Interfaces;
using UpAndRun.Storage.Business.Services;
using UpAndRun.Storage.Data.Entities;
using UpAndRun.Storage.Data.Interfaces;
using UpAndRun.Storage.Data.Repositories;
using UpAndRun.Storage.Database;

namespace UpAndRun.Storage
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("-------------------------------Storage service-----------------------------------");

            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var serviceProvider = new ServiceCollection()
            .Configure<DatabaseSettings>(configuration.GetSection(nameof(DatabaseSettings)))
             .AddMongo().AddMongoRepository<CrateEntity>("Crates")
            .AddMongo().AddMongoRepository<FileEntity>("Files")
            .AddSingleton<ICrateService, CrateService>()
            .AddSingleton<IFileService, FileService>()
            .AddSingleton<ICrateRepository, CrateRepository>()
            .AddSingleton<IFileRepository, FileRepository>()
            .AddSingleton<IStorageMessagingService, StorageMessagingService>()
            .AddSingleton<IConfiguration>(configuration)
            .AddAutoMapper(Assembly.GetExecutingAssembly())

            .BuildServiceProvider();

            var messagingService = serviceProvider.GetService<IStorageMessagingService>();
            messagingService.Start();
        }
    }
}