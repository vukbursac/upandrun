﻿using NetMQ;
using UpAndRun.Storage.Business.Interfaces;
using URCommunicationProtocolLibrary;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;
using Utility.Authentication;
using Utility.Constants;

namespace UpAndRun.Storage.Business.Services
{
    public class StorageMessagingService : BaseService, IStorageMessagingService
    {
        private readonly ICrateService _crateService;
        private readonly IFileService _fileService;

        public StorageMessagingService(ICrateService crateService, IFileService fileService)
        {
            _crateService = crateService;
            _fileService = fileService;

            Subscribe("AddCrateRequestMessage", HandleRequestForAddingCrate);
            Subscribe("UploadFileRequestMessage", HandleRequestForUploadingFile);
            Subscribe("GetAllCratesRequestMessage", HandleRequestForGettingAllCrates);
            Subscribe("UpdateFileRequestMessage", HandleRequestForUpdatingFile);
            Subscribe("MoveFileRequestMessage", HandleRequestForMovingFile);
            Subscribe("GetAllFilesRequestMessage", HandleRequestForGettingAllFiles);
            Subscribe("GetFileByIdRequestMessage", HandleRequestForGettingFileById);
            Subscribe("SoftDeleteFileRequestMessage", HandleRequestForSoftDeletingFile);
            Subscribe("DeleteFileRequestMessage", HandleRequestForDeletingFile);
            Subscribe(MessageConstants.DownloadFileRequestTopicName, HandleRequestForDownloadingFile);
            Subscribe(MessageConstants.GetCrateByIdRequestTopicName, HandleRequestForGettingCrateById);
            Subscribe(MessageConstants.SoftDeleteCrateRequestTopicName, HandleRequestForSoftDeletingCrate);
            Subscribe(MessageConstants.RenameCrateRequestTopicName, HandleRequestForRenamingCrate);
            Subscribe(MessageConstants.DeleteCrateRequestTopicName, HandleRequestForDeletingCrate);
            Subscribe(MessageConstants.MoveCrateRequestTopicName, HandleRequestForMovingCrate);
        }

        private async void HandleRequestForDeletingCrate(NetMQMessage msg)
        {
            var message = new DeleteCrateRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new DeleteCrateResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _crateService.Delete(message.CrateId, message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForSoftDeletingCrate(NetMQMessage msg)
        {
            var message = new SoftDeleteCrateRequestMessage(msg);
            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new SoftDeleteCrateResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!", CrateName = string.Empty });
                return;
            }
            var response = await _crateService.UpdateIsDeletedStatus(message.CrateId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForMovingCrate(NetMQMessage msg)
        {
            var message = new MoveCrateRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new MoveCrateResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _crateService.MoveCrate(message.MoveCrateRequest, message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForGettingCrateById(NetMQMessage msg)
        {
            var message = new GetCrateByIdRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new GetCrateByIdResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _crateService.GetById(message.CrateId, message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForAddingCrate(NetMQMessage msg)
        {
            var message = new AddCrateRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new AddCrateResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _crateService.Create(message.AddCrateRequest, message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForRenamingCrate(NetMQMessage msg)
        {
            var message = new RenameCrateRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new RenameCrateResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }

            var response = await _crateService.RenameCrate(message.UpdateCrateRequest, message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForGettingAllCrates(NetMQMessage msg)
        {
            var message = new GetAllCratesRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new GetAllCratesResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _crateService.GetAll(message.UserId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForUploadingFile(NetMQMessage msg)
        {
            var message = new UploadFileRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new UploadFileResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }

            var response = await _fileService.Create(message.UploadFileRequest);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForDownloadingFile(NetMQMessage msg)
        {
            var message = new DownloadFileRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new DownloadFileResponseMessage { Id = message.Id, Message = "Unauthorized!" });
                return;
            }
            var response = await _fileService.GetByIdForDownload(message.FileId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForUpdatingFile(NetMQMessage msg)
        {
            var message = new UpdateFileRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new UpdateFileResponseMessage { Id = message.Id, Message = "Unauthorized!" });
                return;
            }
            var response = await _fileService.Update(message.UpdateFileRequest.FileId, message.UpdateFileRequest);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForMovingFile(NetMQMessage msg)
        {
            var message = new MoveFileRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new MoveFileResponseMessage { Id = message.Id, Message = "Unauthorized!" });
                return;
            }
            var response = await _fileService.Move(message.MoveFileRequest.FileId, message.MoveFileRequest.ParentCrateId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForGettingAllFiles(NetMQMessage msg)
        {
            var message = new GetAllFilesRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new GetAllFilesResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _fileService.GetAll();
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForSoftDeletingFile(NetMQMessage msg)
        {
            var message = new SoftDeleteRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new SoftDeleteResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!", FileName = string.Empty });
                return;
            }
            var response = await _fileService.SoftDelete(message.FileId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForDeletingFile(NetMQMessage msg)
        {
            var message = new DeleteFileRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new DeleteFileResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!", FileName = string.Empty });
                return;
            }
            var response = await _fileService.Delete(message.FileId);
            response.Id = message.Id;

            Send(response);
        }

        public async void HandleRequestForGettingFileById(NetMQMessage msg)
        {
            var message = new GetFileByIdRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new GetFileByIdResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _fileService.GetById(message.FileId);
            response.Id = message.Id;

            Send(response);
        }
    }
}