﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MongoDB.Bson;
using Storage.Models.Requests;
using Storage.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UpAndRun.Storage.Business.Interfaces;
using UpAndRun.Storage.Data.Entities;
using UpAndRun.Storage.Data.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Storage.Business.Services
{
    public class CrateService : ICrateService
    {
        private readonly ICrateRepository _crateRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IMapper _mapper;

        public CrateService(ICrateRepository crateRepository, IFileRepository fileRepository, IMapper mapper)
        {
            _crateRepository = crateRepository;
            _fileRepository = fileRepository;
            _mapper = mapper;
        }

        public async Task<GetAllCratesResponseMessage> GetAll(Guid userId)
        {
            var allCrates = await _crateRepository.GetAll(userId);
            var mappedCrates = _mapper.Map<IEnumerable<CrateResponse>>(allCrates);
            if (allCrates == null)
            {
                return new GetAllCratesResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "No crates added."
                };
            }
            return new GetAllCratesResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                Crates = mappedCrates
            };
        }

        public async Task<GetCrateByIdResponseMessage> GetById(Guid id, Guid userId)
        {
            var crate = await _crateRepository.GetById(id, userId);
            if (crate == null)
            {
                return new GetCrateByIdResponseMessage() { ErrorMessage = $"Crate with id - {id} does not exist." };
            }
            var crateResponse = _mapper.Map<CrateResponse>(crate);

            return new GetCrateByIdResponseMessage() { IsSuccess = true, ErrorMessage = string.Empty, CrateResponse = crateResponse };
        }

        public async Task<AddCrateResponseMessage> Create(AddCrateRequest crateRequest, Guid userId)
        {
            if (string.IsNullOrWhiteSpace(crateRequest.Name))
            {
                return new AddCrateResponseMessage { IsSuccess = false, ErrorMessage = "Missing crate name. Crate not created." };
            }

            var hasDuplicateCrateNames = await CrateWithDuplicateNameExists(crateRequest.ParentCrateId, crateRequest.Name, userId);

            if (hasDuplicateCrateNames)
            {
                return new AddCrateResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Crate name already exists. Crate not created.",
                    CrateResponse = new CrateResponse()
                };
            }

            var newCrate = new CrateEntity
            {
                Id = Guid.NewGuid(),
                Name = crateRequest.Name,
                ParentCrateId = crateRequest.ParentCrateId,
                CreationDate = DateTime.Now,
                UserId = userId
            };

            if (crateRequest.ParentCrateId != null)
            {
                var parentCrate = await _crateRepository.GetByIdAsync(crateRequest.ParentCrateId.Value);

                if (parentCrate == null)
                {
                    return new AddCrateResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Parent crate does not exist.",
                        CrateResponse = new CrateResponse()
                    };
                }
                await _crateRepository.AddChildCrateId(newCrate.ParentCrateId.Value, newCrate.Id);
            }
            await _crateRepository.CreateAsync(newCrate);

            var crateResponse = _mapper.Map<CrateResponse>(newCrate);

            return new AddCrateResponseMessage { IsSuccess = true, ErrorMessage = "", CrateResponse = crateResponse };
        }

        public async Task<SoftDeleteCrateResponseMessage> UpdateIsDeletedStatus(Guid id)
        {
            var entity = await _crateRepository.GetByIdAsync(id);

            if (entity == null || entity.IsDeleted == true)
            {
                return new SoftDeleteCrateResponseMessage { ErrorMessage = $"Crate with id {id} does not exist." };
            }

            var crateFiles = await _fileRepository.GetAllFilteredAsync(entity => entity.ParentCrateId == id
            && !entity.IsDeleted);

            crateFiles?.Select(async file => await _fileRepository.SoftDeleteAsync(file.Id));

            await _crateRepository.UpdateIsDeletedStatus(entity, entity.UserId);

            var resultBigFiles = await _fileRepository.GetAllBigFilesInParentCrateAsync(id);
            var crateBigFiles = resultBigFiles.Value;
            crateBigFiles.ToList().ForEach(async file => await _fileRepository.SoftDeleteBigFileAsync(ObjectId.Parse(file.Id)));

            return new SoftDeleteCrateResponseMessage { IsSuccess = true, ErrorMessage = "", CrateName = entity.Name };
        }

        public async Task<RenameCrateResponseMessage> RenameCrate(RenameCrateRequest crateUpdateData, Guid userId)
        {
            var existingCrate = await _crateRepository.GetByIdAsync(crateUpdateData.Id);

            if (existingCrate == null)
            {
                return new RenameCrateResponseMessage { IsSuccess = false, ErrorMessage = $"Crate with id {crateUpdateData.Id} does not exist." };
            }

            var hasDuplicateCrateNames = await CrateWithDuplicateNameExists(existingCrate.ParentCrateId, crateUpdateData.Name, userId);

            if (hasDuplicateCrateNames)
            {
                return new RenameCrateResponseMessage { IsSuccess = false, ErrorMessage = $"Crate with name {crateUpdateData.Name} already exists." };
            }

            if (!string.IsNullOrEmpty(crateUpdateData.Name) && !existingCrate.Name.Equals(crateUpdateData.Name))
            {
                existingCrate.Name = crateUpdateData.Name;
            }

            await _crateRepository.UpdateAsync(existingCrate);

            var updatedCrate = await _crateRepository.GetByIdAsync(existingCrate.Id);
            var crateResponse = _mapper.Map<CrateResponse>(updatedCrate);

            return new RenameCrateResponseMessage { IsSuccess = true, ErrorMessage = string.Empty, CrateResponse = crateResponse };
        }

        public async Task<MoveCrateResponseMessage> MoveCrate(MoveCrateRequest crateUpdateData, Guid userId)
        {
            var existingCrate = await _crateRepository.GetByIdAsync(crateUpdateData.Id);

            if (existingCrate == null)
            {
                return new MoveCrateResponseMessage { IsSuccess = false, ErrorMessage = $"Crate with id {crateUpdateData.Id} does not exist." };
            }

            var hasDuplicateCrateNames = await CrateWithDuplicateNameExists(crateUpdateData.ParentCrateId, existingCrate.Name, userId);

            if (hasDuplicateCrateNames)
            {
                return new MoveCrateResponseMessage { IsSuccess = false, ErrorMessage = $"Crate with name {existingCrate.Name} already exists." };
            }

            if (crateUpdateData.ParentCrateId != null && crateUpdateData.ParentCrateId != existingCrate.ParentCrateId)
            {
                //remove child id on old parent
                if (existingCrate.ParentCrateId != null)
                {
                    await _crateRepository.RemoveChildCrateId(existingCrate.ParentCrateId.Value, crateUpdateData.Id);
                }
                var parentCrate = await _crateRepository.GetByIdAsync(crateUpdateData.ParentCrateId);

                if (parentCrate == null)
                {
                    new MoveCrateResponseMessage { ErrorMessage = "Target parent crate does not exist." };
                }

                existingCrate.ParentCrateId = crateUpdateData.ParentCrateId;

                //add child id on new parent
                if (existingCrate.ParentCrateId != null)
                {
                    await _crateRepository.AddChildCrateId(existingCrate.ParentCrateId.Value, crateUpdateData.Id);
                }
            }

            await _crateRepository.MoveCrate(existingCrate, userId);

            var updatedCrate = await _crateRepository.GetByIdAsync(existingCrate.Id);
            var crateResponse = _mapper.Map<CrateResponse>(updatedCrate);

            return new MoveCrateResponseMessage { IsSuccess = true, ErrorMessage = string.Empty, CrateResponse = crateResponse };
        }

        public async Task<DeleteCrateResponseMessage> Delete(Guid id, Guid userId)
        {
            var crateToDelete = await _crateRepository.GetById(id, userId);
            if (crateToDelete == null)
            {
                return new DeleteCrateResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = $"Crate with id {id} does not exist."
                };
            }
            await DeleteCrateAndFiles(crateToDelete, userId);

            await ChildCratesCascadeDelete(id, userId);

            return new DeleteCrateResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                CrateName = crateToDelete.Name
            };
        }

        private async Task ChildCratesCascadeDelete(Guid id, Guid userId)
        {
            var childCrates = await _crateRepository.GetAllFilteredAsync(entity => entity.ParentCrateId == id);

            foreach (var crate in childCrates)
            {
                await DeleteCrateAndFiles(crate, userId);
                await ChildCratesCascadeDelete(crate.Id, userId);
            }
        }

        private async Task DeleteCrateAndFiles(CrateEntity crateToDelete, Guid userId)
        {
            var crateFiles = await _fileRepository.GetAllFilteredAsync(entity => entity.ParentCrateId == crateToDelete.Id);

            crateFiles?.ToList().ForEach(async file => await _fileRepository.RemoveAsync(file.Id));

            var resultBigFiles = await _fileRepository.GetAllBigFilesInParentCrateAsync(crateToDelete.Id);

            if (resultBigFiles.IsSuccess)
            {
                var crateBigFiles = resultBigFiles.Value;
                crateBigFiles.ToList().ForEach(async file => await _fileRepository.DeleteBigFileAsync(ObjectId.Parse(file.Id)));
            }

            await _crateRepository.DeleteCrate(crateToDelete, userId);
        }

        private async Task<bool> CrateWithDuplicateNameExists(Guid? parentCrateId, string desiredName, Guid userId)
        {
            if (parentCrateId != null && parentCrateId != Guid.Empty)
            {
                var duplicateCrates = await _crateRepository.GetAllFilteredAsync(entity => entity.ParentCrateId == parentCrateId
                && !entity.IsDeleted && entity.Name.Equals(desiredName) && entity.UserId.Equals(userId));

                return duplicateCrates.Count() > 0;
            }

            var duplicateCratesAtRoot = await _crateRepository.GetAllFilteredAsync(entity => entity.ParentCrateId == null
                && !entity.IsDeleted && entity.Name.Equals(desiredName) && entity.UserId.Equals(userId));

            return duplicateCratesAtRoot.Count() > 0;
        }
    }
}