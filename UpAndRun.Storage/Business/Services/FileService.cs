﻿using AutoMapper;
using MongoDB.Bson;
using Storage.Models.Requests;
using Storage.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UpAndRun.Storage.Business.Interfaces;
using UpAndRun.Storage.Data.Entities;
using UpAndRun.Storage.Data.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Storage.Business.Services
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        private readonly ICrateRepository _crateRepository;
        private readonly IMapper _mapper;

        public FileService(IFileRepository fileRepository, ICrateRepository crateRepository, IMapper mapper)
        {
            _fileRepository = fileRepository;
            _crateRepository = crateRepository;
            _mapper = mapper;
        }

        public async Task<UploadFileResponseMessage> Create(UploadFileRequest uploadFileRequest)
        {
            if (uploadFileRequest == null)
            {
                return new UploadFileResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "File not provided.",
                    UploadFileResponse = new UploadFileResponse(),
                    UploadBigFileResponse = new UploadBigFileResponse()
                };
            }

            if (string.IsNullOrWhiteSpace(uploadFileRequest.FileName))
            {
                return new UploadFileResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "File name must contain at least one character.",
                    UploadFileResponse = new UploadFileResponse(),
                    UploadBigFileResponse = new UploadBigFileResponse()
                };
            }

            if (uploadFileRequest.ParentCrateId == new Guid())
            {
                return new UploadFileResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Crate should be created prior to file upload.",
                    UploadFileResponse = new UploadFileResponse(),
                    UploadBigFileResponse = new UploadBigFileResponse()
                };
            }

            var parentCrate = await _crateRepository.GetByIdAsync(uploadFileRequest.ParentCrateId);

            if (parentCrate == null)
            {
                return new UploadFileResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Crate should be created prior to file upload.",
                    UploadFileResponse = new UploadFileResponse(),
                    UploadBigFileResponse = new UploadBigFileResponse()
                };
            }

            if (uploadFileRequest.Size < 16777216)
            {
                if (parentCrate.FileIds.Any())
                {
                    var fileList = new List<string>();

                    foreach (var fileId in parentCrate.FileIds)
                    {
                        var file = await _fileRepository.GetByIdAsync(fileId);
                        fileList.Add(file.FileName);
                    };

                    if (fileList.Contains(uploadFileRequest.FileName))
                    {
                        return new UploadFileResponseMessage
                        {
                            IsSuccess = false,
                            ErrorMessage = "Cannot upload two files with the same name in provided crate.",
                            UploadFileResponse = new UploadFileResponse(),
                            UploadBigFileResponse = new UploadBigFileResponse()
                        };
                    }

                    var newFile = _mapper.Map<FileEntity>(uploadFileRequest);

                    await _fileRepository.CreateAsync(newFile);
                    await _crateRepository.UpdateFileListAsync(parentCrate, newFile.Id);

                    var uploadFileResponse = new UploadFileResponse
                    {
                        Id = newFile.Id,
                        FileType = newFile.FileType,
                        CreationDate = newFile.CreationDate,
                        FileName = newFile.FileName,
                        Description = newFile.Description,
                        ParentCrateId = newFile.ParentCrateId.Value,
                        Size = newFile.Size
                    };

                    return new UploadFileResponseMessage
                    {
                        IsSuccess = true,
                        ErrorMessage = "",
                        UploadFileResponse = uploadFileResponse,
                        UploadBigFileResponse = new UploadBigFileResponse()
                    };
                }
            }

            if (parentCrate.BigFileIds.Count != 0)
            {
                var bigFileList = new List<string>();

                foreach (var idOfFile in parentCrate.BigFileIds)
                {
                    ObjectId largeFileId;
                    ObjectId.TryParse(idOfFile, out largeFileId);
                    var result = await _fileRepository.GetBigFileByIdAsync(largeFileId);

                    bigFileList.Add(result.Value.FileName);

                    if (bigFileList.Contains(uploadFileRequest.FileName))
                    {
                        return new UploadFileResponseMessage
                        {
                            IsSuccess = false,
                            ErrorMessage = "Cannot upload two files with the same name in provided crate.",
                            UploadFileResponse = new UploadFileResponse(),
                            UploadBigFileResponse = new UploadBigFileResponse()
                        };
                    }
                }
            }

            var newBigFile = _mapper.Map<BigFileEntity>(uploadFileRequest);

            var bigFileId = await _fileRepository.UploadBigFileAsync(newBigFile);
            await _crateRepository.UpdateBigFileListAsync(parentCrate, bigFileId);

            var uploadBigFileResponse = new UploadBigFileResponse
            {
                Id = bigFileId,
                FileType = newBigFile.FileType,
                CreationDate = newBigFile.CreationDate,
                FileName = newBigFile.FileName,
                Description = newBigFile.Description,
                ParentCrateId = newBigFile.ParentCrateId.Value,
                Size = newBigFile.Size
            };

            return new UploadFileResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                UploadFileResponse = new UploadFileResponse(),
                UploadBigFileResponse = uploadBigFileResponse
            };
        }

        public async Task<SoftDeleteResponseMessage> SoftDelete(string id)
        {
            Guid fileId;
            ObjectId bigFileId;
            if (Guid.TryParse(id, out fileId))
            {
                var fileToDelete = await _fileRepository.GetByIdAsync(fileId);
                if (fileToDelete == null)
                {
                    return new SoftDeleteResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileName = string.Empty
                    };
                }
                await _fileRepository.SoftDeleteAsync(fileId);

                var parentCrateId = fileToDelete.ParentCrateId;

                var crate = await _crateRepository.GetByIdAsync(parentCrateId.Value);

                if (crate == null)
                {
                    return new SoftDeleteResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Crate with provided id does not exist."
                    };
                }
                await _crateRepository.RemoveFileListAsync(crate, fileId);

                return new SoftDeleteResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = $"File {fileToDelete.FileName} deleted.",
                    FileName = fileToDelete.FileName
                };
            }
            else if (ObjectId.TryParse(id, out bigFileId))
            {
                var result = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var bigFileEntity = result.Value;

                if (bigFileEntity == null)
                {
                    return new SoftDeleteResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileName = string.Empty
                    };
                }

                await _fileRepository.SoftDeleteBigFileAsync(bigFileId);

                var parentCrateId = bigFileEntity.ParentCrateId;

                var crate = await _crateRepository.GetByIdAsync(parentCrateId.Value);

                if (crate == null)
                {
                    return new SoftDeleteResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Crate with provided id does not exist.",
                        FileName = string.Empty
                    };
                }
                await _crateRepository.RemoveBigFileListAsync(crate, id);

                return new SoftDeleteResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = $"File {bigFileEntity.FileName} deleted.",
                    FileName = bigFileEntity.FileName
                };
            }
            else
            {
                return new SoftDeleteResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Invalid format of provided id.",
                    FileName = string.Empty
                };
            }
        }

        public async Task<DeleteFileResponseMessage> Delete(string id)
        {
            Guid fileId;
            ObjectId bigFileId;
            if (Guid.TryParse(id, out fileId))
            {
                var fileToDelete = await _fileRepository.GetByIdAsync(fileId);
                if (fileToDelete == null)
                {
                    return new DeleteFileResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileName = string.Empty
                    };
                }
                await _fileRepository.RemoveAsync(fileId);

                var parentCrateId = fileToDelete.ParentCrateId;

                var crate = await _crateRepository.GetByIdAsync(parentCrateId.Value);

                if (crate == null)
                {
                    return new DeleteFileResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Crate with provided id does not exist."
                    };
                }
                await _crateRepository.RemoveFileListAsync(crate, fileId);

                return new DeleteFileResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = $"File {fileToDelete.FileName} deleted.",
                    FileName = fileToDelete.FileName
                };
            }
            else if (ObjectId.TryParse(id, out bigFileId))
            {
                var result = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var bigFileEntity = result.Value;

                if (bigFileEntity == null)
                {
                    return new DeleteFileResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileName = string.Empty
                    };
                }

                await _fileRepository.DeleteBigFileAsync(bigFileId);

                var parentCrateId = bigFileEntity.ParentCrateId;

                var crate = await _crateRepository.GetByIdAsync(parentCrateId.Value);

                if (crate == null)
                {
                    return new DeleteFileResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Crate with provided id does not exist."
                    };
                }
                await _crateRepository.RemoveBigFileListAsync(crate, id);

                return new DeleteFileResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = $"File {bigFileEntity.FileName} deleted.",
                    FileName = bigFileEntity.FileName
                };
            }
            else
            {
                return new DeleteFileResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Invalid format of provided id.",
                    FileName = string.Empty
                };
            }
        }

        public async Task<GetAllFilesResponseMessage> GetAll()
        {
            var allFiles = await _fileRepository.GetAllAsync();
            var allBigFiles = await _fileRepository.GetAllBigFilesAsync();

            if (allFiles?.Any() != true && allBigFiles?.Any() != true)
            {
                return new GetAllFilesResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "No files added.",
                };
            }

            var mappedFiles = _mapper.Map<IEnumerable<FileResponse>>(allFiles);
            var mappedBigFiles = _mapper.Map<IEnumerable<BigFileResponse>>(allBigFiles);
            return new GetAllFilesResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                Files = mappedFiles,
                BigFiles = mappedBigFiles
            };
        }

        public async Task<GetFileByIdResponseMessage> GetById(string id)
        {
            Guid fileId;
            ObjectId bigFileId;
            if (Guid.TryParse(id, out fileId))
            {
                var fileEntity = await _fileRepository.GetByIdAsync(fileId);
                if (fileEntity == null)
                {
                    return new GetFileByIdResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileResponse = new FileResponse(),
                        BigFileResponse = new BigFileResponse()
                    };
                }
                var mappedObject = _mapper.Map<FileResponse>(fileEntity);

                return new GetFileByIdResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = "",
                    FileResponse = mappedObject,
                    BigFileResponse = new BigFileResponse()
                };
            }
            else if (ObjectId.TryParse(id, out bigFileId))
            {
                var result = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var bigFileEntity = result.Value;

                if (bigFileEntity == null)
                {
                    return new GetFileByIdResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "File not found.",
                        FileResponse = new FileResponse(),
                        BigFileResponse = new BigFileResponse()
                    };
                }
                var mappedObject = _mapper.Map<BigFileResponse>(bigFileEntity);

                return new GetFileByIdResponseMessage
                {
                    IsSuccess = true,
                    ErrorMessage = "",
                    FileResponse = new FileResponse(),
                    BigFileResponse = mappedObject
                };
            }
            else
            {
                return new GetFileByIdResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Invalid format of provided id.",
                    FileResponse = new FileResponse(),
                    BigFileResponse = new BigFileResponse()
                };
            }
        }

        public async Task<DownloadFileResponseMessage> GetByIdForDownload(string id)
        {
            DownloadFileResponse downloadFileResponse = null;

            if (Guid.TryParse(id, out Guid fileId))
            {
                var fileEntity = await _fileRepository.GetByIdAsync(fileId);

                if (fileEntity != null)
                {
                    downloadFileResponse = _mapper.Map<DownloadFileResponse>(fileEntity);
                }
            }
            else if (ObjectId.TryParse(id, out ObjectId bigFileId))
            {
                var result = await _fileRepository.GetBigFileByIdForDownloadAsync(bigFileId);

                if (result.IsSuccess && result.Value != null)
                {
                    downloadFileResponse = _mapper.Map<DownloadFileResponse>(result.Value);
                }
            }

            if (downloadFileResponse == null)
            {
                return new DownloadFileResponseMessage
                {
                    IsSuccess = false,
                    Message = "File not found.",
                    Response = new DownloadFileResponse()
                };
            }

            return new DownloadFileResponseMessage
            {
                IsSuccess = true,
                Message = string.Empty,
                Response = downloadFileResponse
            };
        }

        public async Task<UpdateFileResponseMessage> Update(string id, UpdateFileRequest file)
        {
            if (file == null)
            {
                return new UpdateFileResponseMessage
                {
                    IsSuccess = false,
                    Message = "Update data not provided."
                };
            }

            if (string.IsNullOrWhiteSpace(file.FileName))
            {
                return new UpdateFileResponseMessage
                {
                    IsSuccess = false,
                    Message = "File name must contain at least one character."
                };
            }

            Guid fileId;
            ObjectId bigFileId;

            if (Guid.TryParse(id, out fileId))
            {
                var fileToUpdate = await _fileRepository.GetByIdAsync(fileId);

                if (fileToUpdate == null)
                {
                    return new UpdateFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = $"File with id {id} does not exist.",
                        UpdateFileResponse = new FileResponse(),
                        UpdateBigFileResponse = new BigFileResponse()
                    };
                }

                await _fileRepository.UpdateFileAsync(fileId, file);
                var updatedFile = await _fileRepository.GetByIdAsync(fileId);

                var updateFileResponse = _mapper.Map<FileResponse>(updatedFile);

                return new UpdateFileResponseMessage
                {
                    FileName = updatedFile.FileName,
                    IsSuccess = true,
                    Message = $"File with id {fileToUpdate.Id} successfully updated.",
                    UpdateFileResponse = updateFileResponse,
                    UpdateBigFileResponse = new BigFileResponse()
                };
            }
            else if (ObjectId.TryParse(id, out bigFileId))

            {
                var result = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var bigFileEntity = result.Value;

                if (bigFileEntity == null)
                {
                    return new UpdateFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = "File not found.",
                        UpdateFileResponse = new FileResponse(),
                        UpdateBigFileResponse = new BigFileResponse()
                    };
                }
                await _fileRepository.UpdateBigFileAsync(bigFileId, file);
                var result1 = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var updatedFile = result1.Value;
                var updateBigFileResponse = _mapper.Map<BigFileResponse>(updatedFile);

                return new UpdateFileResponseMessage
                {
                    IsSuccess = true,
                    Message = "",
                    UpdateFileResponse = new FileResponse(),
                    UpdateBigFileResponse = updateBigFileResponse
                };
            }
            else
            {
                return new UpdateFileResponseMessage
                {
                    IsSuccess = false,
                    Message = "Invalid id format.",
                    UpdateFileResponse = new FileResponse(),
                    UpdateBigFileResponse = new BigFileResponse()
                };
            }
        }

        public async Task<MoveFileResponseMessage> Move(string id, Guid newParentCrateId)
        {
            Guid fileId;
            ObjectId bigFileId;

            if (Guid.TryParse(id, out fileId))
            {
                if (fileId == new Guid() || newParentCrateId == new Guid())
                {
                    return new MoveFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = $"Update data not provided.",
                        FileName = string.Empty,
                        FileId = string.Empty
                    };
                }
                var fileToUpdate = await _fileRepository.GetByIdAsync(fileId);

                if (fileToUpdate == null)
                {
                    return new MoveFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = $"File with id {fileId} does not exist.",
                        FileName = string.Empty,
                        FileId = string.Empty
                    };
                }

                var crate = await _crateRepository.GetByIdAsync(fileToUpdate.ParentCrateId.Value);
                var newCrate = await _crateRepository.GetByIdAsync(newParentCrateId);

                var fileNameList = new List<string>(newCrate.FileIds.Count);
                foreach (var idOfFile in newCrate.BigFileIds)
                {
                    var bigFileResult = await _fileRepository.GetBigFileByIdAsync(ObjectId.Parse(idOfFile));
                    fileNameList.Add(bigFileResult.Value.FileName);
                }

                if (fileNameList.Contains(fileToUpdate.FileName))
                {
                    return new MoveFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = $"File {fileToUpdate.FileName} already exist in destination crate.",
                        FileName = string.Empty,
                        FileId = string.Empty
                    };
                }

                await _fileRepository.UpdateFileParentCrateIdAsync(fileId, newParentCrateId);
                await _crateRepository.RemoveFileListAsync(crate, fileId);
                await _crateRepository.UpdateFileListAsync(newCrate, fileId);

                var updatedFile = await _fileRepository.GetByIdAsync(fileId);

                return new MoveFileResponseMessage
                {
                    FileId = updatedFile.Id.ToString(),
                    FileName = updatedFile.FileName,
                    IsSuccess = true,
                    Message = $"File {fileToUpdate.FileName} successfully moved."
                };
            }
            else if (ObjectId.TryParse(id, out bigFileId))
            {
                var result = await _fileRepository.GetBigFileByIdAsync(bigFileId);
                var bigFileEntity = result.Value;

                if (bigFileEntity == null)
                {
                    return new MoveFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = "File not found.",
                        FileName = string.Empty,
                        FileId = string.Empty
                    };
                }

                var crate = await _crateRepository.GetByIdAsync(bigFileEntity.ParentCrateId.Value);
                var newCrate = await _crateRepository.GetByIdAsync(newParentCrateId);

                var fileNameList = new List<string>(newCrate.BigFileIds.Count);
                foreach (var idOfFile in newCrate.BigFileIds)
                {
                    var bigFileResult = await _fileRepository.GetBigFileByIdAsync(ObjectId.Parse(idOfFile));
                    fileNameList.Add(bigFileResult.Value.FileName);
                }

                if (fileNameList.Contains(bigFileEntity.FileName))
                {
                    return new MoveFileResponseMessage
                    {
                        IsSuccess = false,
                        Message = $"File {bigFileEntity.FileName} already exist in destination crate.",
                        FileName = string.Empty,
                        FileId = string.Empty
                    };
                }

                var moveBigFileRequest = new MoveBigFileRequest
                {
                    FileId = id,
                    NewParentCrateId = newParentCrateId
                };

                await _fileRepository.UpdateBigFileParentCrateIdAsync(moveBigFileRequest);
                await _crateRepository.RemoveBigFileListAsync(crate, id);
                await _crateRepository.UpdateBigFileListAsync(newCrate, id);

                return new MoveFileResponseMessage
                {
                    IsSuccess = true,
                    Message = $"File {bigFileEntity.FileName} successfully moved.",
                    FileName = bigFileEntity.FileName,
                    FileId = bigFileEntity.Id
                };
            }
            else
            {
                return new MoveFileResponseMessage
                {
                    IsSuccess = false,
                    Message = "Invalid id format.",
                    FileName = string.Empty,
                    FileId = string.Empty
                };
            }
        }
    }
}