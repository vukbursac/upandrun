﻿using NetMQ;
using URCommunicationProtocolLibrary;

namespace UpAndRun.Storage.Business.Interfaces
{
    public interface IStorageMessagingService : IBaseService
    {
        void HandleRequestForAddingCrate(NetMQMessage msg);

        void HandleRequestForRenamingCrate(NetMQMessage msg);

        void HandleRequestForGettingAllCrates(NetMQMessage msg);

        void HandleRequestForSoftDeletingCrate(NetMQMessage msg);

        void HandleRequestForGettingAllFiles(NetMQMessage msg);

        void HandleRequestForGettingFileById(NetMQMessage msg);

        void HandleRequestForUploadingFile(NetMQMessage msg);

        void HandleRequestForUpdatingFile(NetMQMessage msg);

        void HandleRequestForMovingFile(NetMQMessage msg);

        void HandleRequestForSoftDeletingFile(NetMQMessage msg);

        void HandleRequestForDeletingFile(NetMQMessage msg);

        void HandleRequestForDownloadingFile(NetMQMessage msg);

        void HandleRequestForMovingCrate(NetMQMessage msg);
    }
}