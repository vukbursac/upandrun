﻿using Storage.Models.Requests;
using System;
using System.Threading.Tasks;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Storage.Business.Interfaces
{
    public interface ICrateService
    {
        Task<AddCrateResponseMessage> Create(AddCrateRequest crate, Guid id);

        Task<GetCrateByIdResponseMessage> GetById(Guid id, Guid userId);

        Task<GetAllCratesResponseMessage> GetAll(Guid userId);

        Task<RenameCrateResponseMessage> RenameCrate(RenameCrateRequest crateUpdateData, Guid userId);

        Task<MoveCrateResponseMessage> MoveCrate(MoveCrateRequest crateUpdateData, Guid userId);

        Task<SoftDeleteCrateResponseMessage> UpdateIsDeletedStatus(Guid id);

        Task<DeleteCrateResponseMessage> Delete(Guid id, Guid userId);
    }
}