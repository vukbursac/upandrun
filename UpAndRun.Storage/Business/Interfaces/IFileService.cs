﻿using Storage.Models.Requests;
using System;
using System.Threading.Tasks;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Storage.Business.Interfaces
{
    public interface IFileService
    {
        Task<UploadFileResponseMessage> Create(UploadFileRequest uploadFileRequest);

        Task<GetFileByIdResponseMessage> GetById(string id);

        Task<GetAllFilesResponseMessage> GetAll();

        Task<UpdateFileResponseMessage> Update(string idOfFileToBeUpdated, UpdateFileRequest updateFileRequest);

        Task<MoveFileResponseMessage> Move(string fileId, Guid newParentCrateId);

        Task<SoftDeleteResponseMessage> SoftDelete(string id);

        Task<DeleteFileResponseMessage> Delete(string id);

        Task<DownloadFileResponseMessage> GetByIdForDownload(string id);
    }
}