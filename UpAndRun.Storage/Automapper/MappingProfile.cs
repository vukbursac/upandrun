﻿using AutoMapper;
using Storage.Models.Requests;
using Storage.Models.Responses;
using UpAndRun.Storage.Data.Entities;

namespace Utility.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BaseEntity, BaseEntityResponse>()
                .ForMember(dest => dest.Id, conf => conf.MapFrom(src => src.Id))
                .ForMember(dest => dest.ParentCrateId, conf => conf.MapFrom(src => src.ParentCrateId))
            //.ForMember(dest => dest.BigFileId, conf => conf.MapFrom(src => src.BigFileId))

            .Include<CrateEntity, CrateResponse>()
            .Include<FileEntity, FileResponse>();

            CreateMap<CrateEntity, CrateResponse>();
            CreateMap<FileEntity, FileResponse>();
            CreateMap<FileEntity, DownloadFileResponse>();
            CreateMap<BigFileEntity, DownloadFileResponse>();
            CreateMap<UploadFileRequest, FileEntity>();
            CreateMap<UploadFileRequest, BigFileEntity>();
            CreateMap<BigFileEntity, BigFileResponse>();
            CreateMap<BigFileEntity, DownloadBigFileResponse>();
        }
    }
}