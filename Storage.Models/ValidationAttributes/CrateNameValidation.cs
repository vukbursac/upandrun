﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Storage.Models.ValidationAttributes
{
    public class CrateNameValidation : ValidationAttribute
    {
        public const string DefaultErrorMessage = "A crate name can't be empty or contain any of the following characters: < > : / |  \\ \" ? *";

        public CrateNameValidation() : base(DefaultErrorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            Regex regex = new Regex("^([a-zA-Z0-9][^*/><?\"|:]*)$");

            return regex.IsMatch(value?.ToString() ?? string.Empty);
        }
    }
}