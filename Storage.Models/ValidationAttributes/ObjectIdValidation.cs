﻿using System.ComponentModel.DataAnnotations;
using Storage.Models.ValidationAttributes.Helpers;

namespace Storage.Models.ValidationAttributes
{
    public class ObjectIdValidation : ValidationAttribute
    {
        public ObjectIdValidation() : base(ObjectIdValidationHelper.DefaultErrorMessage)
        { }

        public override bool IsValid(object value)
        {
            return FileNameValidationHelper.IsValid(value.ToString());
        }
    }
}