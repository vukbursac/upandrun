﻿using System.Text.RegularExpressions;

namespace Storage.Models.ValidationAttributes.Helpers
{
    public static class ObjectIdValidationHelper
    {
        public const string DefaultErrorMessage = "Object id cannot be empty or in wrong format.";

        public static bool IsValid(string objectId)
        {
            Regex regex = new Regex("^[0-9a-fA-F]{24}$");

            if (string.IsNullOrWhiteSpace(objectId))
            {
                return false;
            }

            return regex.IsMatch(objectId);
        }
    }
}