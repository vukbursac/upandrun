﻿using System.Text.RegularExpressions;

namespace Storage.Models.ValidationAttributes.Helpers
{
    public static class FileNameValidationHelper
    {
        public const string DefaultErrorMessage = @"A file name can't be empty or contain any of the following characters: < > : / | \ ? *";

        public static bool IsValid(string fileName)
        {
            Regex regex = new Regex("^([a-zA-Z0-9][^*/><?\"|:]*)$");

            return regex.IsMatch(fileName);
        }
    }
}