﻿using System.ComponentModel.DataAnnotations;
using Storage.Models.ValidationAttributes.Helpers;

namespace Storage.Models.ValidationAttributes
{
    public class FileNameValidation : ValidationAttribute
    {
        public FileNameValidation() : base(FileNameValidationHelper.DefaultErrorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            return FileNameValidationHelper.IsValid(value.ToString());
        }
    }
}