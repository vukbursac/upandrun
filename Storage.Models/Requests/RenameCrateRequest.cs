﻿using ProtoBuf;
using Storage.Models.ValidationAttributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class RenameCrateRequest
    {
        [ProtoMember(1)]
        [Required]
        [NotEmptyAttribute]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        [CrateNameValidation]
        public string Name { get; set; }
    }
}