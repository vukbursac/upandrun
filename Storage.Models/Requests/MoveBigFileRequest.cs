﻿using System;
using ProtoBuf;
using Storage.Models.ValidationAttributes;
using Storage.Models.ValidationAttributes.Helpers;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class MoveBigFileRequest
    {
        [ProtoMember(1)]
        [ObjectIdValidation]
        public string FileId { get; set; }

        [ProtoMember(2)]
        public Guid NewParentCrateId { get; set; }

        public MoveBigFileRequest()
        {
        }
    }
}