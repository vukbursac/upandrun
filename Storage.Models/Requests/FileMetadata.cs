﻿using System;

namespace Storage.Models.Requests
{
    public class FileMetadata
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Extension { get; set; }
        public string Description { get; set; }
        public int Size { get; set; }
        public Guid ParentCrateId { get; set; }
        public bool IsDeleted { get; set; }

        public FileMetadata()
        {
        }
    }
}