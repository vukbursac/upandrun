﻿using System;
using ProtoBuf;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class MoveFileRequest
    {
        [ProtoMember(1)]
        public string FileId { get; set; }

        [ProtoMember(2)]
        public Guid ParentCrateId { get; set; }

        public MoveFileRequest()
        {
        }
    }
}