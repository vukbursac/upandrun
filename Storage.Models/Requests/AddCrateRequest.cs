﻿using ProtoBuf;
using Storage.Models.ValidationAttributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class AddCrateRequest
    {
        [ProtoMember(1)]
        [CrateNameValidation]
        public string Name { get; set; }

        [ProtoMember(2)]
        public Guid? ParentCrateId { get; set; }
    }
}