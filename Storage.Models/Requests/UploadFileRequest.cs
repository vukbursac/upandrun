﻿using System;
using System.ComponentModel.DataAnnotations;
using ProtoBuf;
using Storage.Models.ValidationAttributes;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class UploadFileRequest
    {
        [ProtoMember(1)]
        [Required(ErrorMessage = "File name is required")]
        [FileNameValidation]
        public string FileName { get; set; }

        [ProtoMember(2)]
        public string FileType { get; set; }

        [ProtoMember(4)]
        public string Description { get; set; }

        [ProtoMember(5)]
        public int Size { get; set; }

        [ProtoMember(6)]
        [Required(ErrorMessage = "Parent crate id is required")]
        public Guid ParentCrateId { get; set; }

        [ProtoMember(7)]
        public byte[] FileObject { get; set; }
    }
}