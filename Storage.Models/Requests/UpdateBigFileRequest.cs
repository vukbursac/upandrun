﻿using ProtoBuf;
using Storage.Models.ValidationAttributes;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class UpdateBigFileRequest
    {
        [ProtoMember(1)]
        [ObjectIdValidation]
        public string BigFileId { get; set; }

        [ProtoMember(2)]
        [FileNameValidation]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string Description { get; set; }
    }
}