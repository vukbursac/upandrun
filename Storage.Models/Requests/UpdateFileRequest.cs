﻿using ProtoBuf;
using Storage.Models.ValidationAttributes;
using System;

namespace Storage.Models.Requests
{
    [ProtoContract]
    public class UpdateFileRequest
    {
        [ProtoMember(1)]
        public string FileId { get; set; }

        [ProtoMember(2)]
        [FileNameValidation]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string Description { get; set; }

        public UpdateFileRequest()
        {
        }
    }
}