﻿using ProtoBuf;

namespace Storage.Models.Responses
{
    [ProtoContract]
    public class DownloadFileResponse
    {
        [ProtoMember(2)]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string FileType { get; set; }

        [ProtoMember(4)]
        public byte[] FileObject { get; set; }

        public DownloadFileResponse()
        {
        }
    }
}