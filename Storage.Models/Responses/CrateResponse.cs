﻿using ProtoBuf;
using System;
using System.Collections.Generic;

namespace Storage.Models.Responses
{
    [ProtoContract]
    public class CrateResponse : BaseEntityResponse

    {
        [ProtoMember(2)]
        public string Name { get; set; }

        [ProtoMember(3)]
        public ICollection<Guid> SubCratesIds { get; set; } = new List<Guid>();

        [ProtoMember(4)]
        public ICollection<Guid> FileIds { get; set; } = new List<Guid>();

        [ProtoMember(5)]
        public ICollection<string> BigFileIds { get; set; } = new List<string>();

        public CrateResponse()
        {
        }

        public CrateResponse(string crateName, Guid parentCrateId)
        {
            Name = crateName;
            ParentCrateId = parentCrateId;
        }
    }
}