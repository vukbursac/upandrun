﻿using System;
using ProtoBuf;

namespace Storage.Models.Responses
{
    [ProtoContract]
    public class UploadFileResponse : BaseEntityResponse
    {
        [ProtoMember(2)]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string FileType { get; set; }

        [ProtoMember(4)]
        public string Description { get; set; }

        [ProtoMember(5)]
        public int Size { get; set; }

        public UploadFileResponse()
        {
        }

        public override bool Equals(object obj)
        {
            return obj is UploadFileResponse response &&
                   Id.Equals(response.Id) &&
                   ParentCrateId.Equals(response.ParentCrateId) &&
                   CreationDate == response.CreationDate &&
                   FileName == response.FileName &&
                   FileType == response.FileType &&
                   Description == response.Description &&
                   Size == response.Size;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, ParentCrateId, CreationDate, FileName, FileType, Description, Size);
        }
    }
}