﻿using ProtoBuf;
using System;

namespace Storage.Models.Responses
{
    [ProtoContract]
    [ProtoInclude(20, typeof(CrateResponse))]
    [ProtoInclude(21, typeof(FileResponse))]
    [ProtoInclude(22, typeof(UploadFileResponse))]
    [ProtoInclude(26, typeof(UpdateFileResponse))]
    public class BaseEntityResponse
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        public Guid ParentCrateId { get; set; }

        [ProtoMember(3)]
        public DateTime CreationDate { get; set; }

        public BaseEntityResponse()
        {
        }
    }
}