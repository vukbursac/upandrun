﻿using System;
using System.Collections.Generic;
using System.Text;
using ProtoBuf;

namespace Storage.Models.Responses
{
    [ProtoContract]
    public class UpdateBigFileResponse : BaseEntityResponse
    {
        [ProtoMember(1)]
        public new string Id { get; set; }

        [ProtoMember(2)]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string FileType { get; set; }

        [ProtoMember(4)]
        public string Description { get; set; }

        [ProtoMember(5)]
        public int Size { get; set; }

        public UpdateBigFileResponse()
        {
        }

        public override bool Equals(object obj)
        {
            return obj is UpdateBigFileResponse response &&
                   Id.Equals(response.Id) &&
                   ParentCrateId.Equals(response.ParentCrateId) &&
                   CreationDate == response.CreationDate &&
                   Id == response.Id &&
                   FileName == response.FileName &&
                   FileType == response.FileType &&
                   Description == response.Description &&
                   Size == response.Size;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, ParentCrateId, CreationDate, Id, FileName, FileType, Description, Size);
        }
    }
}