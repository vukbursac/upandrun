﻿using System;
using ProtoBuf;

namespace Storage.Models.Responses
{
    [ProtoContract]
    public class DownloadBigFileResponse
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string FileName { get; set; }

        [ProtoMember(3)]
        public string FileType { get; set; }

        [ProtoMember(4)]
        public string Description { get; set; }

        [ProtoMember(5)]
        public int Size { get; set; }

        [ProtoMember(6)]
        public byte[] FileObject { get; set; }

        [ProtoMember(7)]
        public Guid ParentCrateId { get; set; }

        [ProtoMember(8)]
        public DateTime CreationDate { get; set; }

        public DownloadBigFileResponse()
        {
            CreationDate = DateTime.Now;
        }
    }
}