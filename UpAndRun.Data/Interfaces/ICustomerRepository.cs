﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using UpAndRun.Models.Entities;

namespace UpAndRun.Data.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Maybe<Customer>> GetByEmail(string email);

        Task<Maybe<Customer>> GetByUsername(string email);
    }
}