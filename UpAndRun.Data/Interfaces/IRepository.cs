﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Identity;

namespace UpAndRun.Data.Interfaces
{
    public interface IRepository<T> where T : IdentityUser
    {
        Task<IEnumerable<T>> GetAll();

        Task<Maybe<T>> GetById(Guid id);

        Task<Maybe<T>> Create(T entity);

        void Update(T entity);
    }
}