﻿namespace UpAndRun.Data.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }

        void Commit();
    }
}