﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UpAndRun.Data.Context;

namespace UpAndRun.Data.Factories
{
   public class AppDBContextFactory : IDesignTimeDbContextFactory<AppDBContext>
    {
        private static string _connectionString;

        public AppDBContext CreateDbContext()
        {
            return CreateDbContext(null);
        }

        public AppDBContext CreateDbContext(string[] args)
        {
            if (string.IsNullOrEmpty(_connectionString))
            {
                LoadConnectionString();
            }

            var builder = new DbContextOptionsBuilder<AppDBContext>();
            builder.UseSqlServer(_connectionString);

            return new AppDBContext(builder.Options);
        }

        private static void LoadConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile(@"C:\\Users\\User\\source\\repos\\upandrun\\UpAndRun.Data\\appsettings.json", optional: false);

            var configuration = builder.Build();

            _connectionString = configuration.GetConnectionString("UpAndRunDatabase");
        }
    }
}
