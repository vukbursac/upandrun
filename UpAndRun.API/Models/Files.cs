﻿using System.Collections.Generic;
using Storage.Models.Responses;

namespace UpAndRun.Gateway.Models
{
    public class Files
    {
        public IEnumerable<FileResponse> FileList { get; set; }
        public IEnumerable<BigFileResponse> BigFileList { get; set; }

        public Files()
        {
        }
    }
}