﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Storage.Models.Requests;

namespace UpAndRun.Gateway.Helper
{
    public static class UploadFileHelper
    {
        public static async Task<UploadFileRequest> CreateUploadFileRequest(IFormFileCollection files, Guid parentCrateId)
        {
            var uploadFileRequest = new UploadFileRequest();

            foreach (var file in files)
            {
                if (file.Length >= 0)
                {
                    using var memoryStream = new MemoryStream();
                    await file.CopyToAsync(memoryStream);

                    uploadFileRequest = new UploadFileRequest
                    {
                        FileName = file.FileName,
                        FileType = file.ContentType,
                        Size = (int)file.Length,
                        ParentCrateId = parentCrateId,
                        FileObject = memoryStream.ToArray()
                    };
                }
            }
            return uploadFileRequest;
        }
    }
}