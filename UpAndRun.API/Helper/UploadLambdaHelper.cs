﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Runtime.Models;

namespace UpAndRun.Gateway.Helper
{
    public static class UploadLambdaHelper
    {
        public static async Task<CreateLambdaRequest> UploadLambdaFileRequest(IFormFileCollection files, Guid userId, string userName)
        {
            var createLambdaRequest = new CreateLambdaRequest();

            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    using var memoryStream = new MemoryStream();
                    await file.CopyToAsync(memoryStream);

                    createLambdaRequest = new CreateLambdaRequest
                    {
                        Name = $"{userName}/{file.FileName}".ToLower(),
                        LambdaFile = memoryStream.ToArray(),
                        UserId = userId
                    };
                }
            }
            return createLambdaRequest;
        }
    }
}