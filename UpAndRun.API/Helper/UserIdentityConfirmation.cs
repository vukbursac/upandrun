﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace UpAndRun.Gateway.Helper
{
    public static class UserIdentityConfirmation
    {
        public static string GetUserId(this HttpContext httpContext)
        {
            if (httpContext.User == null)
            {
                return string.Empty;
            }

            return httpContext.User.Claims.Single(claim => claim.Type == "id").Value;
        }
    }
}
