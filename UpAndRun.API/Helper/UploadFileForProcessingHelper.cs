﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Runtime.Models.Request;

namespace UpAndRun.Gateway.Helper
{
    public static class UploadFileForProcessingHelper
    {
        public static async Task<RunLambdaOnfileRequest> Getfiles(IFormFileCollection files, Guid userId, Guid lambdaId)
        {
            List<byte[]> filesForProcessing = new List<byte[]>();
            using var memoryStream = new MemoryStream();
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    await file.CopyToAsync(memoryStream);
                    filesForProcessing.Add(memoryStream.ToArray());
                }
            }
            // filesForProcessing = memoryStream.ToArray();
            var runLambdaRequest = new RunLambdaOnfileRequest
            {
                UserId = userId,
                LambdaId = lambdaId
                //Files = filesForProcessing
            };

            return runLambdaRequest;
        }
    }
}