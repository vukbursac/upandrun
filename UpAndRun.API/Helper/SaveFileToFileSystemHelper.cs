﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace UpAndRun.Gateway.Helper
{
    public static class SaveFileToFileSystemHelper
    {
        public async static void SaveFile(IFormFileCollection files, string rootPath)
        {
            foreach (IFormFile file in files)
            {
                if (file.Length > 0)
                {
                    string filePath = Path.Combine(rootPath, file.FileName);
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        fileStream.Position = 0;
                    }
                }
            }
        }
    }
}