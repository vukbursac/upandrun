﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Runtime.Models.Request;

namespace UpAndRun.Gateway.Helper
{
    public class RunLambdaHelper
    {
        public static async Task<RunLambdaRequest> RunLambdaRequest(IFormFileCollection files, Guid userId, Guid lambdaId)
        {
            var createLambdaRequest = new RunLambdaRequest();

            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    using var memoryStream = new MemoryStream();
                    await file.CopyToAsync(memoryStream);

                    createLambdaRequest = new RunLambdaRequest
                    {
                        UserId = userId,
                        LambdaId = lambdaId,
                        LambdaParams = new List<string>()
                    };
                }
            }
            return createLambdaRequest;
        }
    }
}