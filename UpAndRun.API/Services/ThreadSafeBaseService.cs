﻿using System.Collections.Concurrent;
using URCommunicationProtocolLibrary;

namespace UpAndRun.Gateway.Services
{
    public abstract class ThreadSafeBaseService : BaseService
    {
        private object _communicationLock = new object();

        public new void Send(IMessage message)
        {
            lock (_communicationLock)
            {
                base.Send(message);
            }
        }
    }
}
