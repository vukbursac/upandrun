﻿using Microsoft.AspNetCore.Http;
using NetMQ;
using Runtime.Models;
using Runtime.Models.Request;
using System;
using System.Collections.Generic;
using System.Threading;
using UpAndRun.Gateway.Interfaces;
using URCommunicationProtocolLibrary;
using URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels;
using Utility.Constants;

namespace UpAndRun.Gateway.Services
{
    public class RuntimeGatewayService : ThreadSafeBaseService, IRuntimeGatewayService
    {
        private readonly Dictionary<Guid, AutoResetEvent> _responseIds;
        private readonly Dictionary<Guid, CreateLambdaResponseMessage> _createLambdaResponses;
        private readonly Dictionary<Guid, RunLambdaResponseMessage> _runLambdaResponses;
        private readonly Dictionary<Guid, GetAllLambdasResponseMessage> _getAllLambdasResponses;
        private readonly Dictionary<Guid, DeleteLambdaResponseMessage> _deleteLambdaResponses;
        private IHttpContextAccessor _httpContextAccessor;

        public RuntimeGatewayService(IHttpContextAccessor httpContextAccessor)
        {
            _responseIds = new Dictionary<Guid, AutoResetEvent>();
            _createLambdaResponses = new Dictionary<Guid, CreateLambdaResponseMessage>();
            _runLambdaResponses = new Dictionary<Guid, RunLambdaResponseMessage>();
            _getAllLambdasResponses = new Dictionary<Guid, GetAllLambdasResponseMessage>();
            _deleteLambdaResponses = new Dictionary<Guid, DeleteLambdaResponseMessage>();
            _httpContextAccessor = httpContextAccessor;

            Subscribe(MessageConstants.CreateLambdaResponseTopicName, OnCreatedLambda);
            Subscribe(MessageConstants.GetAllLambdasResponseTopicName, OnGetAllLambdas);
            Subscribe(MessageConstants.DeleteLambdaResponseTopicName, OnDeleteLambda);
            Subscribe(MessageConstants.RunLambdaResponseTopicName, OnRunLambda);
        }

        public CreateLambdaResponseMessage SendCreateLambdaRequest(CreateLambdaRequest createLambdaRequest)
        {
            var message = new CreateLambdaRequestMessage(createLambdaRequest);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_createLambdaResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _createLambdaResponses[message.Id];

            _createLambdaResponses.Remove(message.Id);

            return response;
        }

        public DeleteLambdaResponseMessage SendDeleteLambdaRequest(Guid imageId)
        {
            var message = new DeleteLambdaRequestMessage(imageId);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_deleteLambdaResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _deleteLambdaResponses[message.Id];

            _deleteLambdaResponses.Remove(message.Id);

            return response;
        }

        public RunLambdaResponseMessage SendRunLambdaRequest(RunLambdaRequest createLambdaRequest)
        {
            var message = new RunLambdaRequestMessage(createLambdaRequest);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_runLambdaResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _runLambdaResponses[message.Id];

            _runLambdaResponses.Remove(message.Id);

            return response;
        }

        public void OnCreatedLambda(NetMQMessage message)
        {
            var createLambdaResponseMessage = new CreateLambdaResponseMessage(message);

            var id = createLambdaResponseMessage.Id;

            _createLambdaResponses.Add(id, createLambdaResponseMessage);

            if (!_responseIds.ContainsKey(id))
            {
                return;
            }

            _responseIds[id].Set();
            _responseIds.Remove(id);
        }

        private void OnDeleteLambda(NetMQMessage message)
        {
            var deleteLambdaResponseMessage = new DeleteLambdaResponseMessage(message);

            var id = deleteLambdaResponseMessage.Id;

            _deleteLambdaResponses.Add(id, deleteLambdaResponseMessage);

            if (!_responseIds.ContainsKey(id))
            {
                return;
            }

            _responseIds[id].Set();
            _responseIds.Remove(id);
        }

        public void OnRunLambda(NetMQMessage message)
        {
            var runLambdaResponseMessage = new RunLambdaResponseMessage(message);

            var id = runLambdaResponseMessage.Id;

            _runLambdaResponses.Add(id, runLambdaResponseMessage);

            if (!_responseIds.ContainsKey(id))
            {
                return;
            }

            _responseIds[id].Set();
            _responseIds.Remove(id);
        }

        public GetAllLambdasResponseMessage SendGetAllLambdasRequest(Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new GetAllLambdasRequestMessage(jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getAllLambdasResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getAllLambdasResponses[message.Id];

            _getAllLambdasResponses.Remove(message.Id);

            return response;
        }

        public void OnGetAllLambdas(NetMQMessage message)
        {
            var getAllLambdasResponseMessage = new GetAllLambdasResponseMessage(message);
            var id = getAllLambdasResponseMessage.Id;
            _getAllLambdasResponses.Add(id, getAllLambdasResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        private string TryGetJwt()
        {
            var jwt = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(jwt))
            {
                throw new UnauthorizedAccessException("Please provide jwt token");
            }
            return jwt;
        }
    }
}