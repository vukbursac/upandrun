﻿using Microsoft.AspNetCore.Http;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Threading;
using UpAndRun.Gateway.Interfaces;
using UpAndRun.Models.Requests;
using UpAndRun.Models.Responses;
using URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels;
using Utility.Constants;
using Utility.Protobuf;

namespace UpAndRun.Gateway.Services
{
    public class UserManagementGatewayService : ThreadSafeBaseService, IUserManagementGatewayService
    {
        private Dictionary<Guid, AutoResetEvent> _responseIds;
        private Dictionary<Guid, RegistrationResponse> _registrationResponses;
        private Dictionary<Guid, EmailVerificationResponseMessage> _emailConfirmationResponses;
        private Dictionary<Guid, LoginResponseMessage> _loginResponses;
        private Dictionary<Guid, GetByIdResponseMessage> _getByIdResponses;
        private Dictionary<Guid, DeleteResponseMessage> _deleteResponses;
        private Dictionary<Guid, UpdateResponseMessage> _updateResponses;
        private IHttpContextAccessor _httpContextAccessor;

        public UserManagementGatewayService(IHttpContextAccessor httpContextAccessor)
        {
            _responseIds = new Dictionary<Guid, AutoResetEvent>();
            _registrationResponses = new Dictionary<Guid, RegistrationResponse>();
            _loginResponses = new Dictionary<Guid, LoginResponseMessage>();
            _getByIdResponses = new Dictionary<Guid, GetByIdResponseMessage>();
            _deleteResponses = new Dictionary<Guid, DeleteResponseMessage>();
            _updateResponses = new Dictionary<Guid, UpdateResponseMessage>();
            _emailConfirmationResponses = new Dictionary<Guid, EmailVerificationResponseMessage>();

            Subscribe(MessageConstants.RegistrationResponseTopicName, OnRegistrationResponse);
            Subscribe(MessageConstants.LoginResponseTopicName, OnLoginResponse);
            Subscribe(MessageConstants.ResponseForEmailVerificationTopicName, OnEmailConfirmationResponse);
            Subscribe(MessageConstants.GetByIdResponseTopicName, OnGetByIdResponse);
            Subscribe(MessageConstants.DeleteResponseTopicName, OnDeletionResponse);
            Subscribe(MessageConstants.UpdateResponseTopicName, OnUpdateResponse);

            _httpContextAccessor = httpContextAccessor;
        }

        private void OnEmailConfirmationResponse(NetMQMessage message)
        {
            message.Pop();
            var id = Guid.Parse(message.Pop().ConvertToString());       
            var errorMessage = message.Pop().ConvertToString();
            var isSuccess = bool.Parse(message.Pop().ConvertToString());

            var response = new EmailVerificationResponseMessage(errorMessage,isSuccess);

            if (!_responseIds.ContainsKey(id))
            {
                return;
            }

            _emailConfirmationResponses.Add(id, response);

            _responseIds[id].Set();
            _responseIds.Remove(id);
        }

        public RegistrationResponse SendRequestForRegistration(RegistrationRequest registrationRequest)
        {
            var message = new RegistrationRequestMessage(registrationRequest);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_registrationResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _registrationResponses[message.Id];

            _registrationResponses.Remove(message.Id);

            return response;
        }

        public void OnRegistrationResponse(NetMQMessage message)
        {
            message.Pop();
            var id = Guid.Parse(message.Pop().ConvertToString());
            var response = Deserialization.Deserialize<RegistrationResponse>(message.Pop().ToByteArray());

            if (!_responseIds.ContainsKey(id))
            {
                return;
            }

            _registrationResponses.Add(id, response);

            _responseIds[id].Set();
            _responseIds.Remove(id);
        }
        
        public LoginResponseMessage SendRequestForLogin(LoginRequest loginRequest)
        {
            var message = new LoginRequestMessage(loginRequest);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_loginResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _loginResponses[message.Id];

            _loginResponses.Remove(message.Id);

            return response;
        }

        public void OnLoginResponse(NetMQMessage message)
        {
            message.Pop();
            var id = Guid.Parse(message.Pop().ConvertToString());
            var isSuccess = bool.Parse(message.Pop().ConvertToString());

            var errorMessage = message.Pop().ConvertToString();
            var response = Deserialization.Deserialize<LoginResponse>(message.Pop().ToByteArray());

            var loginResponseMessage = new LoginResponseMessage();
            if (!isSuccess)
            {
                loginResponseMessage.IsSuccess = false;
                loginResponseMessage.Id = id;
                loginResponseMessage.LoginResponse = response;
                loginResponseMessage.Message = errorMessage;
                _loginResponses.Add(id, loginResponseMessage);
            }

            if (isSuccess)
            {
                loginResponseMessage.IsSuccess = true;
                loginResponseMessage.Id = id;
                loginResponseMessage.LoginResponse = response;
                loginResponseMessage.Message = errorMessage;
                _loginResponses.Add(id, loginResponseMessage);
            }

            HandleResponseIdSetAndRemove(id, _responseIds);
        }
      
        public GetByIdResponseMessage SendGetByIdRequest(Guid id)
        {
            var jwt = TryGetJwt();

            var message = new GetByIdRequestMessage(id, jwt);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getByIdResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getByIdResponses[message.Id];

            _getByIdResponses.Remove(message.Id);

            return response;
        }

        public void OnGetByIdResponse(NetMQMessage message)
        {
            message.Pop();

            var getByIdMessage = new GetByIdResponseMessage
            {
                Id = Guid.Parse(message.Pop().ConvertToString()),
                IsSuccess = bool.Parse(message.Pop().ConvertToString()),
                Message = message.Pop().ConvertToString(),
                CustomerResponse = Deserialization.Deserialize<CustomerResponse>(message.Pop().ToByteArray())
            };

            _getByIdResponses.Add(getByIdMessage.Id, getByIdMessage);

            if (!_responseIds.ContainsKey(getByIdMessage.Id))
            {
                return;
            }

            _responseIds[getByIdMessage.Id].Set();
            _responseIds.Remove(getByIdMessage.Id);
        }

        public DeleteResponseMessage SendDeleteRequest(Guid id, bool hardDelete)
        {
            var jwt = TryGetJwt();
            var message = new DeleteRequestMessage(id, hardDelete, jwt);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_deleteResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _deleteResponses[message.Id];

            _deleteResponses.Remove(message.Id);

            return response;
        }

        public void OnDeletionResponse(NetMQMessage message)
        {
            message.Pop();

            var deleteResponse = new DeleteResponseMessage
            {
                Id = Guid.Parse(message.Pop().ConvertToString()),
                IsSuccess = bool.Parse(message.Pop().ConvertToString()),
                Message = message.Pop().ConvertToString(),
            };

            _deleteResponses.Add(deleteResponse.Id, deleteResponse);

            if (!_responseIds.ContainsKey(deleteResponse.Id))
            {
                return;
            }

            _responseIds[deleteResponse.Id].Set();
            _responseIds.Remove(deleteResponse.Id);
        }

        public UpdateResponseMessage SendUpdateRequest(UpdateRequest updateRequest, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new UpdateRequestMessage(updateRequest, userId, jwt);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_updateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _updateResponses[message.Id];

            _updateResponses.Remove(message.Id);

            return response;
        }

        private void OnUpdateResponse(NetMQMessage message)
        {
            var updateResponseMessage = new UpdateResponseMessage(message);

            if (!_responseIds.ContainsKey(updateResponseMessage.Id))
            {
                return;
            }

            _updateResponses.Add(updateResponseMessage.Id, updateResponseMessage);

            _responseIds[updateResponseMessage.Id].Set();
            _responseIds.Remove(updateResponseMessage.Id);
        }

        private string TryGetJwt()
        {
            var jwt = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(jwt))
            {
                throw new UnauthorizedAccessException("Please provide jwt token");
            }
            return jwt;
        }

        public EmailVerificationResponseMessage ConfirmEmail(string userId, string token)
        {
            var message = new EmailVerificationRequestMessage(token, userId);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_emailConfirmationResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _emailConfirmationResponses[message.Id];

            _emailConfirmationResponses.Remove(message.Id);

            return response;
        }
    }
}