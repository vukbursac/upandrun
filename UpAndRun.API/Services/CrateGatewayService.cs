﻿using Microsoft.AspNetCore.Http;
using NetMQ;
using Storage.Models.Requests;
using System;
using System.Collections.Generic;
using System.Threading;
using UpAndRun.Gateway.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;
using Utility.Constants;

namespace UpAndRun.Gateway.Services
{
    public class CrateGatewayService : ThreadSafeBaseService, ICrateGatewayService
    {
        private Dictionary<Guid, AutoResetEvent> _responseIds;
        private Dictionary<Guid, AddCrateResponseMessage> _crateAdditionResponses;
        private Dictionary<Guid, GetAllCratesResponseMessage> _getAllCratesResponses;
        private Dictionary<Guid, GetCrateByIdResponseMessage> _getCrateByIdResponses;
        private Dictionary<Guid, SoftDeleteCrateResponseMessage> _softDeleteCrateResponses;
        private Dictionary<Guid, RenameCrateResponseMessage> _renameCrateResponses;
        private Dictionary<Guid, MoveCrateResponseMessage> _moveCrateResponses;
        private Dictionary<Guid, DeleteCrateResponseMessage> _deleteCrateResponses;
        private IHttpContextAccessor _httpContextAccessor;

        public CrateGatewayService(IHttpContextAccessor httpContextAccessor)
        {
            _responseIds = new Dictionary<Guid, AutoResetEvent>();
            _crateAdditionResponses = new Dictionary<Guid, AddCrateResponseMessage>(); ;
            _getAllCratesResponses = new Dictionary<Guid, GetAllCratesResponseMessage>();
            _getCrateByIdResponses = new Dictionary<Guid, GetCrateByIdResponseMessage>();
            _softDeleteCrateResponses = new Dictionary<Guid, SoftDeleteCrateResponseMessage>();
            _renameCrateResponses = new Dictionary<Guid, RenameCrateResponseMessage>();
            _moveCrateResponses = new Dictionary<Guid, MoveCrateResponseMessage>();
            _deleteCrateResponses = new Dictionary<Guid, DeleteCrateResponseMessage>();

            Subscribe(MessageConstants.AddCrateResponseTopicName, OnCrateAddition);
            Subscribe(MessageConstants.GetAllCratesResponseTopicName, OnGetAllCrates);
            Subscribe(MessageConstants.GetCrateByIdResponseTopicName, OnSucessfulGetById);
            Subscribe(MessageConstants.SoftDeleteCrateResponseTopicName, OnSucessfulSofDelete);
            Subscribe(MessageConstants.RenameCrateResponseTopicName, OnRenameCrate);
            Subscribe(MessageConstants.DeleteCrateResponseTopicName, OnSuccesfulDelete);
            Subscribe(MessageConstants.MoveCrateResponseTopicName, OnMoveCrate);

            _httpContextAccessor = httpContextAccessor;
        }

        public void OnMoveCrate(NetMQMessage msg)
        {
            var moveCrateResponseMessage = new MoveCrateResponseMessage(msg);
            var id = moveCrateResponseMessage.Id;
            _moveCrateResponses.Add(id, moveCrateResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnSuccesfulDelete(NetMQMessage msg)
        {
            var deleteCrateResponseMessage = new DeleteCrateResponseMessage(msg);
            var id = deleteCrateResponseMessage.Id;
            _deleteCrateResponses.Add(id, deleteCrateResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnRenameCrate(NetMQMessage msg)
        {
            var renameCrateResponseMessage = new RenameCrateResponseMessage(msg);
            var id = renameCrateResponseMessage.Id;
            _renameCrateResponses.Add(id, renameCrateResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnSucessfulSofDelete(NetMQMessage msg)
        {
            var softDeleteCrateResponseMessage = new SoftDeleteCrateResponseMessage(msg);
            var id = softDeleteCrateResponseMessage.Id;
            _softDeleteCrateResponses.Add(id, softDeleteCrateResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public GetCrateByIdResponseMessage SendGetCrateByIdRequest(Guid id, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new GetCrateByIdRequestMessage(id, jwt, userId);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getCrateByIdResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getCrateByIdResponses[message.Id];

            _getCrateByIdResponses.Remove(message.Id);

            return response;
        }

        public void OnSucessfulGetById(NetMQMessage message)
        {
            var getCrateByIdResponseMessage = new GetCrateByIdResponseMessage(message);
            var id = getCrateByIdResponseMessage.Id;
            _getCrateByIdResponses.Add(id, getCrateByIdResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnCrateAddition(NetMQMessage message)
        {
            var addCrateResponseMessage = new AddCrateResponseMessage(message);

            var id = addCrateResponseMessage.Id;

            _crateAdditionResponses.Add(id, addCrateResponseMessage);

            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public GetAllCratesResponseMessage SendGetAllCratesRequest(Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new GetAllCratesRequestMessage(jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getAllCratesResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getAllCratesResponses[message.Id];

            _getAllCratesResponses.Remove(message.Id);

            return response;
        }

        public void OnGetAllCrates(NetMQMessage message)
        {
            var getAllCratesResponseMessage = new GetAllCratesResponseMessage(message);
            var id = getAllCratesResponseMessage.Id;
            _getAllCratesResponses.Add(id, getAllCratesResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public AddCrateResponseMessage SendAddCrateRequest(AddCrateRequest crateRequest, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new AddCrateRequestMessage(crateRequest, jwt, userId);
            Send(message);

            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_crateAdditionResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _crateAdditionResponses[message.Id];

            _crateAdditionResponses.Remove(message.Id);

            return response;
        }

        public RenameCrateResponseMessage SendRenameCrateRequest(RenameCrateRequest renameCrateRequest, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new RenameCrateRequestMessage(renameCrateRequest, jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_renameCrateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _renameCrateResponses[message.Id];

            _moveCrateResponses.Remove(message.Id);

            return response;
        }

        public MoveCrateResponseMessage SendMoveCrateRequest(MoveCrateRequest moveCrateRequest, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new MoveCrateRequestMessage(moveCrateRequest, jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_moveCrateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _moveCrateResponses[message.Id];

            _moveCrateResponses.Remove(message.Id);

            return response;
        }

        public SoftDeleteCrateResponseMessage SendSoftDeleteCrateRequest(Guid crateId, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new SoftDeleteCrateRequestMessage(crateId, jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_softDeleteCrateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _softDeleteCrateResponses[message.Id];

            _softDeleteCrateResponses.Remove(message.Id);

            return response;
        }

        public DeleteCrateResponseMessage SendDeleteCrateRequest(Guid id, Guid userId)
        {
            var jwt = TryGetJwt();
            var message = new DeleteCrateRequestMessage(id, jwt, userId);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_deleteCrateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _deleteCrateResponses[message.Id];

            _deleteCrateResponses.Remove(message.Id);

            return response;
        }

        private string TryGetJwt()
        {
            var jwt = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(jwt))
            {
                throw new UnauthorizedAccessException("Please provide jwt token");
            }
            return jwt;
        }
    }
}