﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.AspNetCore.Http;
using NetMQ;
using Storage.Models.Requests;
using UpAndRun.Gateway.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;
using Utility.Constants;

namespace UpAndRun.Gateway.Services
{
    public class FileGatewayService : ThreadSafeBaseService, IFileGatewayService
    {
        private Dictionary<Guid, AutoResetEvent> _responseIds;
        private Dictionary<Guid, UploadFileResponseMessage> _fileUploadResponses;
        private Dictionary<Guid, UpdateFileResponseMessage> _fileUpdateResponses;
        private Dictionary<Guid, GetAllFilesResponseMessage> _getAllFilesResponses;
        private Dictionary<Guid, SoftDeleteResponseMessage> _softDeleteResponses;
        private Dictionary<Guid, GetFileByIdResponseMessage> _getFileByIdResponses;
        private Dictionary<Guid, DeleteFileResponseMessage> _deleteFileResponses;
        private Dictionary<Guid, DownloadFileResponseMessage> _downloadFileResponses;
        private Dictionary<Guid, MoveFileResponseMessage> _moveFileResponses;

        private IHttpContextAccessor _httpContextAccessor;

        public FileGatewayService(IHttpContextAccessor httpContextAccessor)
        {
            _responseIds = new Dictionary<Guid, AutoResetEvent>();
            _fileUploadResponses = new Dictionary<Guid, UploadFileResponseMessage>();
            _fileUpdateResponses = new Dictionary<Guid, UpdateFileResponseMessage>();
            _getAllFilesResponses = new Dictionary<Guid, GetAllFilesResponseMessage>();
            _getFileByIdResponses = new Dictionary<Guid, GetFileByIdResponseMessage>();
            _softDeleteResponses = new Dictionary<Guid, SoftDeleteResponseMessage>();
            _deleteFileResponses = new Dictionary<Guid, DeleteFileResponseMessage>();
            _moveFileResponses = new Dictionary<Guid, MoveFileResponseMessage>();
            _downloadFileResponses = new Dictionary<Guid, DownloadFileResponseMessage>();

            Subscribe("UploadFileResponseMessage", OnFileUpload);
            Subscribe("UpdateFileResponseMessage", OnFileUpdate);
            Subscribe("MoveFileResponseMessage", OnFileMoved);
            Subscribe("GetAllFilesResponseMessage", OnGetAllFiles);
            Subscribe("SoftDeleteFileResponseMessage", OnSoftFileDeletion);
            Subscribe("DeleteFileResponseMessage", OnFileDeletion);
            Subscribe("GetFileByIdResponseMessage", OnGetFileById);
            Subscribe(MessageConstants.DownloadFileResponseTopicName, OnFileDownload);

            _httpContextAccessor = httpContextAccessor;
        }

        public void OnFileUpload(NetMQMessage message)
        {
            var uploadFileResponseMessage = new UploadFileResponseMessage(message);
            var id = uploadFileResponseMessage.Id;
            _fileUploadResponses.Add(id, uploadFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnFileDownload(NetMQMessage message)
        {
            var downloadFileResponseMessage = new DownloadFileResponseMessage(message);
            var id = downloadFileResponseMessage.Id;
            _downloadFileResponses.Add(id, downloadFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnFileUpdate(NetMQMessage message)
        {
            var updateFileResponseMessage = new UpdateFileResponseMessage(message);
            var id = updateFileResponseMessage.Id;
            _fileUpdateResponses.Add(id, updateFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnFileMoved(NetMQMessage message)
        {
            var moveFileResponseMessage = new MoveFileResponseMessage(message);
            var id = moveFileResponseMessage.Id;
            _moveFileResponses.Add(id, moveFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnGetFileById(NetMQMessage message)
        {
            var getFileByIdResponseMessage = new GetFileByIdResponseMessage(message);
            var id = getFileByIdResponseMessage.Id;
            _getFileByIdResponses.Add(id, getFileByIdResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnGetAllFiles(NetMQMessage message)
        {
            var getAllFilesResponseMessage = new GetAllFilesResponseMessage(message);
            var id = getAllFilesResponseMessage.Id;
            _getAllFilesResponses.Add(id, getAllFilesResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnSoftFileDeletion(NetMQMessage message)
        {
            var softDeleteFileResponseMessage = new SoftDeleteResponseMessage(message);
            var id = softDeleteFileResponseMessage.Id;
            _softDeleteResponses.Add(id, softDeleteFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public void OnFileDeletion(NetMQMessage message)
        {
            var deleteFileResponseMessage = new DeleteFileResponseMessage(message);
            var id = deleteFileResponseMessage.Id;
            _deleteFileResponses.Add(id, deleteFileResponseMessage);
            HandleResponseIdSetAndRemove(id, _responseIds);
        }

        public UploadFileResponseMessage SendUploadFileRequest(UploadFileRequest uploadFileRequest)
        {
            var jwt = TryGetJwt();
            var message = new UploadFileRequestMessage(uploadFileRequest, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_fileUploadResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _fileUploadResponses[message.Id];

            _fileUploadResponses.Remove(message.Id);

            return response;
        }

        public UpdateFileResponseMessage SendUpdateFileRequest(UpdateFileRequest updateFileRequest)
        {
            var jwt = TryGetJwt();
            var message = new UpdateFileRequestMessage(updateFileRequest, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_fileUpdateResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _fileUpdateResponses[message.Id];

            _fileUpdateResponses.Remove(message.Id);

            return response;
        }

        public MoveFileResponseMessage SendMoveFileRequest(MoveFileRequest moveFileRequest)
        {
            var jwt = TryGetJwt();
            var message = new MoveFileRequestMessage(moveFileRequest, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_moveFileResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _moveFileResponses[message.Id];

            _moveFileResponses.Remove(message.Id);

            return response;
        }

        public GetAllFilesResponseMessage SendGetAllFilesRequest()
        {
            var jwt = TryGetJwt();
            var message = new GetAllFilesRequestMessage(jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getAllFilesResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getAllFilesResponses[message.Id];

            _getAllFilesResponses.Remove(message.Id);

            return response;
        }

        public SoftDeleteResponseMessage SendSoftDeleteFileRequest(string fileId)
        {
            var jwt = TryGetJwt();
            var message = new SoftDeleteRequestMessage(fileId, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_softDeleteResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _softDeleteResponses[message.Id];

            _softDeleteResponses.Remove(message.Id);

            return response;
        }

        public GetFileByIdResponseMessage SendGetFileByIdRequest(string fileId)
        {
            var jwt = TryGetJwt();
            var message = new GetFileByIdRequestMessage(fileId, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_getFileByIdResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _getFileByIdResponses[message.Id];

            _getFileByIdResponses.Remove(message.Id);

            return response;
        }

        public DeleteFileResponseMessage SendDeleteFileRequest(string fileId)
        {
            var jwt = TryGetJwt();
            var message = new DeleteFileRequestMessage(fileId, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_deleteFileResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _deleteFileResponses[message.Id];

            _deleteFileResponses.Remove(message.Id);

            return response;
        }

        public DownloadFileResponseMessage SendDownloadFileRequest(string fileId)
        {
            var jwt = TryGetJwt();
            var message = new DownloadFileRequestMessage(fileId, jwt);
            Send(message);
            AutoResetEvent e = new AutoResetEvent(false);
            _responseIds.Add(message.Id, e);

            e.WaitOne();

            if (!_downloadFileResponses.ContainsKey(message.Id))
            {
                return null;
            }

            var response = _downloadFileResponses[message.Id];

            _downloadFileResponses.Remove(message.Id);

            return response;
        }

        private string TryGetJwt()
        {
            var jwt = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(jwt))
            {
                throw new UnauthorizedAccessException("Please provide jwt token");
            }
            return jwt;
        }
    }
}