﻿using NetMQ;
using System;
using UpAndRun.Models.Requests;
using UpAndRun.Models.Responses;
using URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels;

namespace UpAndRun.Gateway.Interfaces
{
    public interface IUserManagementGatewayService
    {
        void OnRegistrationResponse(NetMQMessage message);

        void OnLoginResponse(NetMQMessage message);

        DeleteResponseMessage SendDeleteRequest(Guid Id, bool hardDelete);

        EmailVerificationResponseMessage ConfirmEmail(string userId, string token);

        RegistrationResponse SendRequestForRegistration(RegistrationRequest registrationRequest);

        LoginResponseMessage SendRequestForLogin(LoginRequest loginRequest);

        GetByIdResponseMessage SendGetByIdRequest(Guid id);

        UpdateResponseMessage SendUpdateRequest(UpdateRequest updateRequest, Guid userId);
    }
}