﻿using Runtime.Models;
using System;
using Runtime.Models.Request;
using URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels;

namespace UpAndRun.Gateway.Interfaces
{
    public interface IRuntimeGatewayService
    {
        CreateLambdaResponseMessage SendCreateLambdaRequest(CreateLambdaRequest createLambdaRequest);

        DeleteLambdaResponseMessage SendDeleteLambdaRequest(Guid imageId);

        RunLambdaResponseMessage SendRunLambdaRequest(RunLambdaRequest runLambdaRequest);

        GetAllLambdasResponseMessage SendGetAllLambdasRequest(Guid userId);
    }
}