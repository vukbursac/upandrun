﻿using System;
using NetMQ;
using Storage.Models.Requests;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Gateway.Interfaces
{
    public interface IFileGatewayService
    {
        void OnFileUpload(NetMQMessage message);

        void OnFileUpdate(NetMQMessage message);

        void OnSoftFileDeletion(NetMQMessage message);

        void OnFileDeletion(NetMQMessage message);

        void OnFileDownload(NetMQMessage message);

        void OnFileMoved(NetMQMessage message);

        void OnGetFileById(NetMQMessage message);

        void OnGetAllFiles(NetMQMessage message);

        UploadFileResponseMessage SendUploadFileRequest(UploadFileRequest uploadFileRequest);

        UpdateFileResponseMessage SendUpdateFileRequest(UpdateFileRequest updateFileRequest);

        MoveFileResponseMessage SendMoveFileRequest(MoveFileRequest moveFileRequest);

        GetAllFilesResponseMessage SendGetAllFilesRequest();

        SoftDeleteResponseMessage SendSoftDeleteFileRequest(string fileId);

        GetFileByIdResponseMessage SendGetFileByIdRequest(string fileId);

        DeleteFileResponseMessage SendDeleteFileRequest(string fileId);

        DownloadFileResponseMessage SendDownloadFileRequest(string fileId);
    }
}