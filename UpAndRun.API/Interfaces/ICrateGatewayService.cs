﻿using NetMQ;
using Storage.Models.Requests;
using System;
using URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels;

namespace UpAndRun.Gateway.Interfaces
{
    public interface ICrateGatewayService
    {
        AddCrateResponseMessage SendAddCrateRequest(AddCrateRequest crateRequest, Guid userId);

        GetAllCratesResponseMessage SendGetAllCratesRequest(Guid userId);

        GetCrateByIdResponseMessage SendGetCrateByIdRequest(Guid id, Guid userId);

        SoftDeleteCrateResponseMessage SendSoftDeleteCrateRequest(Guid crateId, Guid userId);

        RenameCrateResponseMessage SendRenameCrateRequest(RenameCrateRequest updateCrateRequest, Guid userId);

        DeleteCrateResponseMessage SendDeleteCrateRequest(Guid id, Guid userId);

        MoveCrateResponseMessage SendMoveCrateRequest(MoveCrateRequest moveCrateRequest, Guid userId);

        void OnCrateAddition(NetMQMessage message);

        void OnRenameCrate(NetMQMessage msg);

        void OnSucessfulSofDelete(NetMQMessage msg);

        void OnSucessfulGetById(NetMQMessage message);

        void OnGetAllCrates(NetMQMessage message);
    }
}