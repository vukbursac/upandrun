using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using UpAndRun.Gateway.Services;

namespace UpAndRun.Gateway
{
    public class Program
    {
        public static UserManagementGatewayService GatewayService;

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}