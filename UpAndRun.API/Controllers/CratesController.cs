using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Storage.Models.Requests;
using System;
using System.Net;
using UpAndRun.Gateway.Helper;
using UpAndRun.Gateway.Interfaces;

namespace UpAndRun.Gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CratesController : ControllerBase
    {
        private readonly ICrateGatewayService _crateGatewayService;
        private readonly IUserManagementGatewayService _userManagementGatewayService;

        public CratesController(ICrateGatewayService crateGatewayService, IUserManagementGatewayService userManagementGatewayService)
        {
            _crateGatewayService = crateGatewayService;
            _userManagementGatewayService = userManagementGatewayService;
        }

        /// <summary>
        /// Method for creating a new crate
        /// </summary>
        /// <param name="addCrateRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult AddCrate(AddCrateRequest addCrateRequest)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendAddCrateRequest(addCrateRequest, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.CrateResponse);
        }

        /// <summary>
        /// Method for getting all crates
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult GetAllCrates()
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendGetAllCratesRequest(userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.Crates);
        }

        /// <summary>
        /// Method for getting crate by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(Guid id)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendGetCrateByIdRequest(id, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.CrateResponse);
        }

        /// <summary>
        /// Method for soft deleting crate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult SoftDelete(Guid id)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendSoftDeleteCrateRequest(id, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok("Successfully deleted.");
        }

        /// <summary>
        /// Method for renaming crate
        /// </summary>
        /// <param name="renameCrateRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPatch]
        [Route("rename")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult UpdateCrate(RenameCrateRequest renameCrateRequest)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendRenameCrateRequest(renameCrateRequest, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.CrateResponse);
        }

        /// <summary>
        /// Method for moving crate
        /// </summary>
        /// <param name="moveCrateRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPatch]
        [Route("move")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult UpdateParentCrate(MoveCrateRequest moveCrateRequest)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendMoveCrateRequest(moveCrateRequest, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.CrateResponse);
        }

        /// <summary>
        /// Method for hard deleting crate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Delete(Guid id)
        {
            var userId = Guid.Parse(HttpContext.GetUserId());
            var result = _crateGatewayService.SendDeleteCrateRequest(id, userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok("Successfully deleted.");
        }
    }
}