﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Storage.Models.Requests;
using Storage.Models.Responses;
using UpAndRun.Gateway.Helper;
using UpAndRun.Gateway.Interfaces;
using UpAndRun.Gateway.Models;

namespace UpAndRun.Gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFileGatewayService _fileGatewayService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FilesController(IFileGatewayService storageGatewayService, IHttpContextAccessor httpContextAccessor)
        {
            _fileGatewayService = storageGatewayService;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Mehtod for uploading a file
        /// </summary>
        /// <param name="parentCrateId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("{parentCrateId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile([FromRoute] Guid parentCrateId)
        {
            var files = _httpContextAccessor.HttpContext.Request.Form.Files;

            if (files.Count > 1)
            {
                return BadRequest("Multiple files upload not allowed.");
            }

            if (files.Count == 0)
            {
                return BadRequest("No file for upload provided.");
            }

            var uploadFileRequest = await UploadFileHelper.CreateUploadFileRequest(files, parentCrateId);

            if (uploadFileRequest.Size > 524288000)
            {
                return BadRequest("Files over 500 MB cannot be uploaded.");
            }

            var result = _fileGatewayService.SendUploadFileRequest(uploadFileRequest);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }
            else if (!result.UploadFileResponse.Equals(new UploadFileResponse()))
            {
                return Ok(result.UploadFileResponse);
            }
            else
            {
                return Ok(result.UploadBigFileResponse);
            }
        }

        /// <summary>
        /// Method for updating a file
        /// </summary>
        /// <param name="updateFileRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult UpdateFile(UpdateFileRequest updateFileRequest)
        {
            var result = _fileGatewayService.SendUpdateFileRequest(updateFileRequest);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }
            else if (result.UpdateBigFileResponse.Equals(new UpdateBigFileResponse()))
            {
                return Ok(result.UpdateFileResponse);
            }
            else
            {
                return Ok(result.UpdateBigFileResponse);
            }
        }

        /// <summary>
        /// Method for moving a file into different crate
        /// </summary>
        /// <param name="moveFileRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPatch]
        [Route("movefile")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult MoveFile(MoveFileRequest moveFileRequest)
        {
            var result = _fileGatewayService.SendMoveFileRequest(moveFileRequest);
            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }
            return Ok(result.Message);
        }

        /// <summary>
        /// Method for getting all files
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult GetAllFiles()
        {
            var result = _fileGatewayService.SendGetAllFilesRequest();
            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            var fileList = new Files
            {
                FileList = result.Files,
                BigFileList = result.BigFiles
            };
            return Ok(fileList);
        }

        /// <summary>
        /// Method for getting file by id
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("{fileId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult GetFileById([FromRoute] string fileId)
        {
            var response = _fileGatewayService.SendGetFileByIdRequest(fileId);

            if (!response.IsSuccess)
            {
                return BadRequest(response.ErrorMessage);
            }
            else if (response.FileResponse.Equals(new FileResponse()))
            {
                return Ok(response.BigFileResponse);
            }
            else
            {
                return Ok(response.FileResponse);
            }
        }

        /// <summary>
        /// Method for downloading a file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("download/{fileId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Download([FromRoute] string fileId)
        {
            var responseMessage = _fileGatewayService.SendDownloadFileRequest(fileId);

            if (!responseMessage.IsSuccess)
            {
                return BadRequest(responseMessage.Message);
            }

            var file = new FileContentResult(responseMessage.Response.FileObject, MediaTypeHeaderValue.Parse(responseMessage.Response.FileType));
            file.FileDownloadName = responseMessage.Response.FileName;

            return file;
        }

        /// <summary>
        /// Method for soft deleting a file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [Route("fileId")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult SoftDelete(string fileId)
        {
            var result = _fileGatewayService.SendSoftDeleteFileRequest(fileId);
            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(result.FileName);
        }

        /// <summary>
        /// Method for hard deleting file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{fileId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Delete([FromRoute] string fileId)
        {
            var result = _fileGatewayService.SendDeleteFileRequest(fileId);
            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }
            return Ok(result.ErrorMessage);
        }
    }
}