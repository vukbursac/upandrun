﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Runtime.Models.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UpAndRun.Gateway.Helper;
using UpAndRun.Gateway.Interfaces;
using Utility.Resources;

namespace UpAndRun.Gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RuntimesController : ControllerBase
    {
        private readonly IRuntimeGatewayService _runtimeGatewayService;
        private readonly IUserManagementGatewayService _userManagementGatewayService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RuntimesController(IRuntimeGatewayService runtimeGatewayService, IHttpContextAccessor httpContextAccessor, IUserManagementGatewayService userManagementGatewayService)
        {
            _runtimeGatewayService = runtimeGatewayService;
            _httpContextAccessor = httpContextAccessor;
            _userManagementGatewayService = userManagementGatewayService;
        }

        /// <summary>
        /// Method for Getting All Lambdas
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("byuserid/{userId}")]
        public IActionResult GetAll([FromRoute] Guid userId)
        {
            var resultUser = _userManagementGatewayService.SendGetByIdRequest(userId);

            if (!resultUser.IsSuccess)
            {
                return BadRequest(resultUser.Message);
            }

            var result = _runtimeGatewayService.SendGetAllLambdasRequest(userId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.Lambdas);
        }

        /// <summary>
        /// Method for creating new lambda
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("{userId}")]
        public async Task<IActionResult> CreateLambda([FromRoute] Guid userId)
        {
            var keys = _httpContextAccessor.HttpContext.Request.Form.Keys;
            var files = _httpContextAccessor.HttpContext.Request.Form.Files;
            var parameters = new List<string>();

            if (keys == null || !keys.Any())
            {
                return BadRequest("Key must contain username.");
            }

            foreach (var key in keys)
            {
                _httpContextAccessor.HttpContext.Request.Form.TryGetValue(key, out Microsoft.Extensions.Primitives.StringValues userName);
                parameters.Add(userName);
            }

            if (files.Count > 1)
            {
                return BadRequest("Multiple files upload not allowed.");
            }

            if (files.Count == 0)
            {
                return BadRequest("No file for upload provided.");
            }

            var uploadLambdaRequest = await UploadLambdaHelper.UploadLambdaFileRequest(files, userId, parameters[0]);
            var result = _runtimeGatewayService.SendCreateLambdaRequest(uploadLambdaRequest);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok(result.LambdaId);
        }

        /// <summary>
        /// Method for running lambda
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lambdaId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("users/{userId}/lambdas/{lambdaId}")]
        public async Task<IActionResult> RunLambda([FromRoute] Guid userId, Guid lambdaId)
        {
            var runLambdaRequest = new RunLambdaRequest();
            var keys = _httpContextAccessor.HttpContext.Request.Form.Keys;
            var parameters = new List<string>();

            foreach (var key in keys)
            {
                _httpContextAccessor.HttpContext.Request.Form.TryGetValue(key, out Microsoft.Extensions.Primitives.StringValues somevalue);
                parameters.Add(somevalue);
            }

            var files = _httpContextAccessor.HttpContext.Request.Form.Files;

            var filesList = new List<byte[]>();
            var fileNamesList = new List<string>();

            if (files == null || files.Count == 0)
            {
                runLambdaRequest.UserId = userId;
                runLambdaRequest.LambdaId = lambdaId;
                runLambdaRequest.LambdaParams = parameters;
                runLambdaRequest.FileNames = new List<string>();
                runLambdaRequest.Files = new List<byte[]>();
            }
            else
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        using var memoryStream = new MemoryStream();
                        await file.CopyToAsync(memoryStream);
                        filesList.Add(memoryStream.ToArray());
                        fileNamesList.Add(file.FileName);
                    }
                }
            }

            if (parameters == null)
            {
                return BadRequest("Docker image name must be provided.");
            }
            else if (files.Count == 0 || files == null)
            {
                runLambdaRequest.UserId = userId;
                runLambdaRequest.LambdaId = lambdaId;
                runLambdaRequest.LambdaParams = parameters;
                runLambdaRequest.FileNames = new List<string>();
                runLambdaRequest.Files = new List<byte[]>();
            }
            else
            {
                runLambdaRequest.UserId = userId;
                runLambdaRequest.LambdaId = lambdaId;
                runLambdaRequest.LambdaParams = parameters;
                runLambdaRequest.FileNames = fileNamesList;
                runLambdaRequest.Files = filesList;
            }

            var result = _runtimeGatewayService.SendRunLambdaRequest(runLambdaRequest);

            if (!result.IsSuccess)
            {
                return BadRequest(result.ErrorMessage);
            }

            if (files.Count > 0)
            {
                var net = new System.Net.WebClient();
                var data = net.DownloadData(@"C:\Users\User\Desktop\Uploads\grayscale.jpg");
                var contentType = "APPLICATION/octet-stream";
                var fileName = "grayscale.jpg";

                Directory.Delete(FilePaths.UploadsFolder.ToString(), true);
                Directory.CreateDirectory(FilePaths.UploadsFolder.ToString());

                return new FileContentResult(data, contentType)
                {
                    FileDownloadName = fileName
                };
            }
            else
            {
                return Ok(result.LambdaOutput);
            }
        }

        /// <summary>
        /// Delete Docker image
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{imageId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Delete([FromRoute] Guid imageId)
        {
            var result = _runtimeGatewayService.SendDeleteLambdaRequest(imageId);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.Message);
        }
    }
}