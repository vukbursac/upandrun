﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using UpAndRun.Gateway.Helper;
using UpAndRun.Gateway.Interfaces;
using UpAndRun.Models.Requests;
using Utility.Authentication;

namespace UpAndRun.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManagementGatewayService _gatewayService;

        public UsersController(IUserManagementGatewayService gatewayService)
        {
            _gatewayService = gatewayService;
        }

        /// <summary>
        /// Method for user login
        /// </summary>
        /// <remarks></remarks>
        /// <param name="loginRequest"></param>
        /// <returns>You logged in successfully</returns>
        /// <response code="200">Returns logged in user</response>
        [HttpPost]
        [ProducesResponseType(typeof(LoginRequest), (int)HttpStatusCode.OK)]
        [Route("Login")]
        public IActionResult Login(LoginRequest loginRequest)
        {
            var result = _gatewayService.SendRequestForLogin(loginRequest);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            var token = JwtAuthentication.Authenticate(result.LoginResponse.Id.ToString());

            result.LoginResponse.Token = token ?? throw new UnauthorizedAccessException("Unauthorized");

            return Ok(result);
        }

        /// <summary>
        /// Method for user registration
        /// </summary>
        /// <remarks></remarks>
        /// <param name="registrationRequest"></param>
        /// <returns>You reggistrated successfully</returns>
        /// <response code="200">Returns registrated user</response>
        [HttpPost]
        [ProducesResponseType(typeof(RegistrationRequest), (int)HttpStatusCode.OK)]
        [Route("Registration")]
        public IActionResult Register(RegistrationRequest registrationRequest)
        {
            var response = _gatewayService.SendRequestForRegistration(registrationRequest);

            if (!response.IsSuccess)
            {
                return BadRequest(response.Message);
            }

            return Ok(response.Message);
        }

        /// <summary>
        /// Method that is automatically sending confirm email
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Confirmemail")]
        public IActionResult ConfirmEmail(string userId, string token)
        {
            var result = _gatewayService.ConfirmEmail(userId, token);

            if (result.IsSuccess)
            {
                return RedirectPermanent("http://localhost:3000/login");
            }
            return Ok(result);
        }

        /// <summary>
        /// Method for getting user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(Guid id)
        {
            var result = _gatewayService.SendGetByIdRequest(id);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.CustomerResponse);
        }

        /// <summary>
        /// Method that hard deletes user if hard delete is checked othervise deactivates user
        /// </summary>
        /// <param name="deleteUserRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]     
        public IActionResult Delete(DeleteUserRequest deleteUserRequest)
        {
            var userId = HttpContext.GetUserId();

            if (userId != deleteUserRequest.Id.ToString())
            {
                return BadRequest("You are not allowed to delete other accounts.");
            }

            var result = _gatewayService.SendDeleteRequest(deleteUserRequest.Id, deleteUserRequest.HardDelete);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.Message);
        }

        /// <summary>
        /// Method for updating user
        /// </summary>
        /// <param name="updateRequest"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Route("{id}")]
        public IActionResult Update(Guid id, UpdateRequest updateRequest)
        {
            var userId = HttpContext.GetUserId();

            if (userId != id.ToString())
            {
                return BadRequest("You are not allowed to update other accounts.");
            }

            var result = _gatewayService.SendUpdateRequest(updateRequest, id);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.UpdateResponse);
        }
    }
}