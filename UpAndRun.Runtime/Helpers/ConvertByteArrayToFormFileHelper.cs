﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace UpAndRun.Runtime.Helpers
{
    public static class ConvertByteArrayToFormFileHelper
    {
        public static IFormFileCollection Convert(List<byte[]> filesListBytes)
        {
            FormFileCollection files = new FormFileCollection();
            foreach (var fileByteArray in filesListBytes)
            {
                var memoryStream = new MemoryStream(fileByteArray);
                FormFile file = new FormFile(memoryStream, 0, fileByteArray.Length, null, "fileName");
                files.Add(file);
            }
            return files;
        }
    }
}