﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace UpAndRun.Runtime
{
    public static class SaveFilesToHddHelper
    {
        public static List<string> SaveFiles(List<byte[]> filesAsByteArray, string rootPath, List<string> fileNames)
        {
            var pathsList = new List<string>();
            for (int i = 0; i < filesAsByteArray.Count; i++)
            {
                if (filesAsByteArray[i].Length > 0)
                {
                    File.WriteAllBytes(rootPath + @"\" + fileNames[i], filesAsByteArray[i]);
                    pathsList.Add(rootPath + @"\" + fileNames[i]);
                    //string filePath = Path.Combine(rootPath, file.FileName);
                    //using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    //{
                    //    await file.CopyToAsync(fileStream);
                    //    fileStream.Position = 0;
                }
            }
            return pathsList;
        }
    }
}