﻿using System;
using UpAndRun.Runtime.Data.Context;
using UpAndRun.Runtime.Data.Interfaces;

namespace UpAndRun.Runtime.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _context;
        private ILambdaRepository _lambdaRepository;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public ILambdaRepository LambdaRepository
        {
            get
            {
                return _lambdaRepository = _lambdaRepository ?? new LambdaRepository(_context);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}