﻿using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UpAndRun.Runtime.Data.Context;
using UpAndRun.Runtime.Data.Entities;
using UpAndRun.Runtime.Data.Interfaces;

namespace UpAndRun.Runtime.Data.Repositories
{
    public class LambdaRepository : ILambdaRepository
    {
        private readonly AppDbContext db;

        public LambdaRepository(AppDbContext db)
        {
            this.db = db;
        }

        public async Task<Maybe<LambdaEntity>> Create(LambdaEntity lambdaEntity)
        {
            await db.Lambdas.AddAsync(lambdaEntity);
            return lambdaEntity;
        }

        public void Delete(LambdaEntity lambdaEntity)
        {
            db.Lambdas.Remove(lambdaEntity);
        }

        public async Task<IEnumerable<LambdaEntity>> GetAll(Guid userId)
        {
            return await db.Lambdas.Where(lambda => lambda.UserId == userId).OrderBy(lambda => lambda.Name).ToListAsync();
        }

        public async Task<Maybe<LambdaEntity>> GetById(Guid id)
        {
            return await db.Lambdas.FindAsync(id);
        }

        public void Update(LambdaEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}