﻿using System;
using System.Collections.Generic;
using System.Text;
using UpAndRun.Runtime.Data.Entities;

namespace UpAndRun.Runtime.Data.Interfaces
{
    public interface ILambdaRepository : IRepository<LambdaEntity>
    {
    }
}