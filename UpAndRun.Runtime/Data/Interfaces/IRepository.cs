﻿using CSharpFunctionalExtensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Runtime.Data.Entities;

namespace UpAndRun.Runtime.Data.Interfaces
{
    public interface IRepository<T> where T : LambdaEntity
    {
        Task<IEnumerable<T>> GetAll(Guid userId);

        Task<Maybe<T>> GetById(Guid id);

        Task<Maybe<T>> Create(T entity);

        void Delete(T entity);

        void Update(T entity);
    }
}