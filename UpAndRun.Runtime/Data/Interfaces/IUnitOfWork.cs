﻿namespace UpAndRun.Runtime.Data.Interfaces
{
    public interface IUnitOfWork
    {
        ILambdaRepository LambdaRepository { get; }

        void Commit();
    }
}