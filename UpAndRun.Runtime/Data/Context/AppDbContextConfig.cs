﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UpAndRun.Runtime.Data.Context
{
    public class AppDbContextConfig
    {
        public string ConnectionString { get; set; }
    }
}