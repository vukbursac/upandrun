﻿using System;
using ProtoBuf;

namespace UpAndRun.Runtime.Data.Entities
{
    [ProtoContract]
    public class LambdaEntity
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        public string Name { get; set; }

        [ProtoMember(3)]
        public Guid UserId { get; set; }

        public LambdaEntity()
        {
            Id = Guid.NewGuid();
        }
    }
}