﻿using Runtime.Models;
using System;
using System.Threading.Tasks;
using Runtime.Models.Request;
using URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels;

namespace UpAndRun.Runtime.Interfaces
{
    public interface IRuntimeService
    {
        public CreateLambdaResponseMessage GetLambdaCreationResponse(CreateLambdaRequest createLambdaRequest);

        public Task<RunLambdaResponseMessage> GetLambdaRunResponse(RunLambdaRequest runLambdaRequest);

        public Task<DeleteLambdaResponseMessage> GetLambdaDeleteResponse(Guid imageId);

        Task<GetAllLambdasResponseMessage> GetAll(Guid userId);
    }
}