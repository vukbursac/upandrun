﻿using System.Threading.Tasks;
using NetMQ;
using URCommunicationProtocolLibrary;

namespace UpAndRun.Runtime.Interfaces
{
    public interface IRuntimeMessagingService : IBaseService
    {
        void HandleRequestForCreatingLambda(NetMQMessage msg);

        void HandleRequestForRunningLambda(NetMQMessage msg);

        void HandleRequestForDeletingLambda(NetMQMessage msg);

        void HandleRequestForGetAllLambdas(NetMQMessage msg);
    }
}