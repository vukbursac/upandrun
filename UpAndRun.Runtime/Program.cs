﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using UpAndRun.Runtime.Data.Context;
using UpAndRun.Runtime.Data.Interfaces;
using UpAndRun.Runtime.Data.Repositories;
using UpAndRun.Runtime.Interfaces;
using UpAndRun.Runtime.Services;

namespace UpAndRun.Runtime
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("-------------------------------Runtime-----------------------------------");

            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var serviceProvider = new ServiceCollection()
            .AddSingleton<IUnitOfWork, UnitOfWork>()
            .AddSingleton<ILambdaRepository, LambdaRepository>()
            .AddSingleton<IRuntimeMessagingService, RuntimeMessagingService>()
            .AddSingleton<IRuntimeService, RuntimeService>()
            .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
            .AddSingleton<IConfiguration>(configuration)
           .AddDbContext<AppDbContext>()
            .Configure<AppDbContextConfig>(conf => { conf.ConnectionString = configuration.GetConnectionString("UpAndRunLambdaDatabaseConn"); })

            .AddAutoMapper(Assembly.GetExecutingAssembly())
            .BuildServiceProvider();

            var messagingService = serviceProvider.GetService<IRuntimeMessagingService>();
            messagingService.Start();
        }
    }
}