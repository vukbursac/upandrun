﻿using AutoMapper;
using Runtime.Models;
using Runtime.Models.Response;
using UpAndRun.Runtime.Data.Entities;

namespace UpAndRun.Runtime.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateLambdaRequest, LambdaEntity>();
            CreateMap<LambdaEntity, LambdaResponse>();
        }
    }
}