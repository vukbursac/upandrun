﻿using NetMQ;
using System;
using UpAndRun.Runtime.Interfaces;
using URCommunicationProtocolLibrary;
using URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels;
using Utility.Authentication;
using Utility.Constants;

namespace UpAndRun.Runtime.Services
{
    public class RuntimeMessagingService : BaseService, IRuntimeMessagingService
    {
        private IRuntimeService _runtimeService;

        public RuntimeMessagingService(IRuntimeService runtimeService)
        {
            _runtimeService = runtimeService;
            Subscribe(MessageConstants.CreateLambdaRequestTopicName, HandleRequestForCreatingLambda);
            Subscribe(MessageConstants.RunLambdaRequestTopicName, HandleRequestForRunningLambda);
            Subscribe(MessageConstants.GetAllLambdasRequestTopicName, HandleRequestForGetAllLambdas);
            Subscribe(MessageConstants.DeleteLambdaRequestTopicName, HandleRequestForDeletingLambda);
        }

        public void HandleRequestForCreatingLambda(NetMQMessage msg)
        {
            var message = new CreateLambdaRequestMessage(msg);
            var response = _runtimeService.GetLambdaCreationResponse(message.CreateLambdaRequest);
            response.Id = message.Id;
            Send(response);
        }

        public async void HandleRequestForGetAllLambdas(NetMQMessage msg)
        {
            var message = new GetAllLambdasRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new GetAllLambdasResponseMessage { Id = message.Id, ErrorMessage = "Unauthorized!" });
                return;
            }
            var response = await _runtimeService.GetAll(message.UserId);
            response.Id = message.Id;
            Send(response);
        }

        public async void HandleRequestForRunningLambda(NetMQMessage msg)
        {
            var message = new RunLambdaRequestMessage(msg);
            var response = await _runtimeService.GetLambdaRunResponse(message.RunLambdaRequest);
            response.Id = message.Id;
            Send(response);
        }

        public async void HandleRequestForDeletingLambda(NetMQMessage msg)
        {
            var message = new DeleteLambdaRequestMessage(msg);
            var response = await _runtimeService.GetLambdaDeleteResponse(message.ImageId);
            response.Id = message.Id;
            Send(response);
        }
    }
}