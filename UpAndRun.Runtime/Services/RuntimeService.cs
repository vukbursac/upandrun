﻿using AutoMapper;
using Runtime.Models;
using Runtime.Models.Request;
using Runtime.Models.Response;
using SharpCompress.Archives;
using SharpCompress.Archives.Rar;
using SharpCompress.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Threading.Tasks;
using UpAndRun.Runtime.Data.Entities;
using UpAndRun.Runtime.Data.Interfaces;
using UpAndRun.Runtime.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels;
using Utility.Resources;

namespace UpAndRun.Runtime.Services
{
    public class RuntimeService : IRuntimeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RuntimeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<GetAllLambdasResponseMessage> GetAll(Guid userId)
        {
            var allLambdas = await _unitOfWork.LambdaRepository.GetAll(userId);

            if (allLambdas == null || allLambdas.Count() == 0)
            {
                return new GetAllLambdasResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "No lambdas added."
                };
            }

            var mappedLambdas = _mapper.Map<IEnumerable<LambdaResponse>>(allLambdas);
            return new GetAllLambdasResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                Lambdas = mappedLambdas
            };
        }

        public CreateLambdaResponseMessage GetLambdaCreationResponse(CreateLambdaRequest createLambdaRequest)
        {
            var pathToUnzip = FilePaths.UnzipFolder.ToString();

            if (createLambdaRequest == null)
            {
                return new CreateLambdaResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "File not provided."
                };
            }

            try
            {
                using var memoryStream = new MemoryStream();
                memoryStream.Write(createLambdaRequest.LambdaFile, 0, createLambdaRequest.LambdaFile.Length);
                memoryStream.Position = 0;
                var rarArchive = RarArchive.Open(memoryStream);

                foreach (var entry in rarArchive.Entries.Where(entry => !entry.IsDirectory))
                {
                    entry.WriteToDirectory(pathToUnzip, new ExtractionOptions()
                    {
                        ExtractFullPath = true,
                        Overwrite = true
                    });
                }
            }
            catch (InvalidFormatException)
            {
                return new CreateLambdaResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "Uploaded file isn't supported. Please, upload .rar archive!"
                };
            };

            var lambdaEntity = _mapper.Map<LambdaEntity>(createLambdaRequest);

            _unitOfWork.LambdaRepository.Create(lambdaEntity);
            _unitOfWork.Commit();

            var dockerfilePath = FilePaths._NETDockerfile.ToString();
            var copyLocation = FilePaths.UnzipFolder.ToString() + "/Dockerfile";

            File.Copy(dockerfilePath, copyLocation);

            CreateImage(createLambdaRequest.Name);

            Directory.Delete(FilePaths.UnzipFolder.ToString(), true);
            Directory.CreateDirectory(FilePaths.UnzipFolder.ToString());

            return new CreateLambdaResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                LambdaId = lambdaEntity.Id
            };
        }

        public async Task<RunLambdaResponseMessage> GetLambdaRunResponse(RunLambdaRequest runLambdaRequest)
        {
            if (runLambdaRequest == null)
            {
                return new RunLambdaResponseMessage
                {
                    IsSuccess = false,
                    ErrorMessage = "One or more requested parameters not provided."
                };
            }

            if (runLambdaRequest.Files != null)
            {
                string uploadPath = Path.Combine(@"C:\Users\User\Desktop\", "Uploads");
                SaveFilesToHddHelper.SaveFiles(runLambdaRequest.Files, uploadPath, runLambdaRequest.FileNames);
            }

            var lambdaParams = runLambdaRequest.LambdaParams;
            var parameterString = new StringBuilder(lambdaParams.Count);
            string lambdaOutput;

            for (int i = 1; i < lambdaParams.Count; i++)
            {
                parameterString.Append(lambdaParams[i] + " ");
            }

            if (runLambdaRequest.Files == null)
            {
                lambdaOutput = await RunImage(parameterString.ToString(), runLambdaRequest.LambdaParams[0]);
                runLambdaRequest.Files = null;

                if (lambdaOutput.ToString() == "" || lambdaOutput == null)
                {
                    return new RunLambdaResponseMessage
                    {
                        IsSuccess = false,
                        ErrorMessage = "Something bad happened. Please check your source code and/or required lambda parameters.",
                        LambdaOutput = "Something bad happened. Please check your source code and/or required lambda parameters."
                    };
                }
            }
            else
            {
                lambdaOutput = "";
                RunImageWithFile(runLambdaRequest.LambdaParams[0]);
            }

            return new RunLambdaResponseMessage
            {
                IsSuccess = true,
                ErrorMessage = "",
                LambdaOutput = lambdaOutput,
            };
        }

        public static async Task<string> RunImage(string parameterString, string imageName)
        {
            object consoleOutput;
            using Runspace myRunSpace = RunspaceFactory.CreateRunspace();
            myRunSpace.Open();
            using (PowerShell powershell = PowerShell.Create())
            {
                powershell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted");
                powershell.AddScript($"docker run {imageName} {parameterString}");
                powershell.AddCommand("Out-String");
                var result = await powershell.InvokeAsync();
                consoleOutput = result.ReadAll().First().BaseObject;
                powershell.Streams.ClearStreams();
                powershell.Commands.Clear();
            }
            myRunSpace.Close();

            return consoleOutput.ToString();
        }

        public static void RunImageWithFile(string imageName)
        {
            using Runspace myRunSpace = RunspaceFactory.CreateRunspace();
            myRunSpace.Open();
            using (PowerShell powershell = PowerShell.Create())
            {
                powershell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted");
                powershell.AddScript(@"cd C:\Users\User\Desktop\Uploads\");
                powershell.AddScript($"docker run -ti -v C:/Users/User/Desktop/Uploads/:/app {imageName}");
                powershell.AddCommand("Out-String");
                var result = powershell.Invoke();
                powershell.Streams.ClearStreams();
                powershell.Commands.Clear();
            }
            myRunSpace.Close();
        }

        public static void CreateImage(string imageName)
        {
            using Runspace myRunSpace = RunspaceFactory.CreateRunspace();
            myRunSpace.Open();
            using (PowerShell powershell = PowerShell.Create())
            {
                powershell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted");
                powershell.AddScript("cd C:/Users/User/Desktop/Unzip/");
                powershell.AddScript($"docker build -t {imageName} -f C:/Users/User/Desktop/Unzip/Dockerfile .");
                powershell.AddCommand("Out-String");
                var result = powershell.Invoke();
                powershell.Streams.ClearStreams();
                powershell.Commands.Clear();
            }
            myRunSpace.Close();
        }

        public async Task<DeleteLambdaResponseMessage> GetLambdaDeleteResponse(Guid imageId)
        {
            var imageToDelete = await _unitOfWork.LambdaRepository.GetById(imageId);

            if (imageToDelete == null)
            {
                return new DeleteLambdaResponseMessage
                {
                    IsSuccess = false,
                    Message = $"Lambda with id {imageId} does not exists."
                };
            }

            _unitOfWork.LambdaRepository.Delete(imageToDelete.Value);
            _unitOfWork.Commit();

            using Runspace myRunSpace = RunspaceFactory.CreateRunspace();
            myRunSpace.Open();
            using (PowerShell powershell = PowerShell.Create())
            {
                powershell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted");
                powershell.AddScript($"docker rmi {imageToDelete.Value.Name} -f");
                powershell.AddCommand("Out-String");
                var result = powershell.Invoke();
                powershell.Streams.ClearStreams();
                powershell.Commands.Clear();
            }
            myRunSpace.Close();

            return new DeleteLambdaResponseMessage
            {
                IsSuccess = true,
                Message = $"Lambda with name {imageToDelete.Value.Name} deleted.",
            };
        }
    }
}