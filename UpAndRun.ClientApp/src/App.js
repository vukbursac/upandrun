import { Switch } from "react-router";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import "./api/axiosInstance/axiosInstance";

import { authCheckState } from "./store/actions/index";

import { Register } from "./components/Register/Register";
import { Logout } from "./components/Logout/Logout";
import { Login } from "./components/Login/Login";
import { Landing } from "./components/Landing/Landing";
import { Dashboard } from "./components/Dashboard/Dashboard";
import { PrivateRoute } from "./components/Routes/PrivateRoute/PrivateRoute";
import { PublicRoute } from "./components/Routes/PublicRoute/PublicRoute";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(authCheckState());
  }, [dispatch]);

  return (
    <Switch>
      <PublicRoute restricted component={Landing} path='/' exact />
      <PublicRoute restricted component={Login} path='/login' exact />
      <PublicRoute restricted component={Register} path='/register' exact />
      <PrivateRoute component={Dashboard} path='/dashboard' />
      <PrivateRoute component={Logout} path='/logout' exact />
    </Switch>
  );
}

export default App;
