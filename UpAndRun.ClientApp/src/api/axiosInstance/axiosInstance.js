import axios from "axios";
import { store } from "../../configuration/redux";
import { logout } from "../../store/actions";

axios.interceptors.request.use(
  (request) => {
    const token = store.getState().auth.token;
    if (token) {
      request.headers.Authorization = "Bearer " + token;
    }

    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401 || error.response.status === 403) {
      store.dispatch(logout());
    }
    return Promise.reject(error);
  }
);

axios.defaults.headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};
