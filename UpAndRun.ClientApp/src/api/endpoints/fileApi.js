import axios from "axios";
import FileSaver from "file-saver";

const fetchFilesByIds = async (ids) => {
  const files = ids.map((id) => fetchFileByIdApi(id));
  return await Promise.all(files);
};

export const fetchFilesByCrateId = async (parentCrateId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates/${parentCrateId}`;

  try {
    const response = await axios.get(url);
    const fileIds = response.data.fileIds;
    const files = await fetchFilesByIds(fileIds);
    return files;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const fetchAllFilesApi = async () => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files`;
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const fetchFileByIdApi = async (fileId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files/${fileId}`;

  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const addFileApi = async (file, parentCrateId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files/${parentCrateId}`;

  try {
    const response = await axios.post(url, file);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

export const deleteFileApi = async (fileId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files/${fileId}`;

  try {
    const response = await axios.delete(url);

    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

export const updateFileApi = async (fileName, fileId, description) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files`;

  try {
    const response = await axios.put(url, { fileName, fileId, description });
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

export const moveFileIntoCrate = async (fileId, parentCrateId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files/movefile`;

  try {
    await axios.patch(url, { fileId, parentCrateId });
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const downloadFileApi = async (fileId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/files/download/${fileId}`;
  try {
    const response = await axios.get(url, { responseType: "blob" });
    const fetchFile = await fetchFileByIdApi(fileId);
    const fileName = fetchFile.fileName;
    const fileType = fetchFile.fileType;

    const blob = new Blob([response.data], {
      type: fileType,
    });
    FileSaver.saveAs(blob, fileName);
  } catch (error) {
    throw new Error(error);
  }
};
