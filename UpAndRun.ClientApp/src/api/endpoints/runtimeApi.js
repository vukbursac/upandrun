import axios from "axios";

const API_KEY_ONLINE_COMPILER = process.env.REACT_APP_API_KEY_ONLINE_COMPILER;
const BASE_URL = "https://judge0-ce.p.rapidapi.com";
const headers = {
  "content-type": "application/json",
  "x-rapidapi-key": API_KEY_ONLINE_COMPILER,
  "x-rapidapi-host": "judge0-ce.p.rapidapi.com",
};

const isAllowedLanguage = (language) => {
  const allowedLanguages = ["C#"];

  return allowedLanguages.find((allowedLanguage) =>
    language.name.includes(allowedLanguage)
  );
};

const filterOutLanguages = (languages) => {
  return languages.filter((language) => isAllowedLanguage(language));
};

export const getLanguages = async () => {
  const url = `${BASE_URL}/languages`;

  try {
    const languages = (await axios.get(url, { headers })).data;
    const chosenLanguages = filterOutLanguages(languages);

    return chosenLanguages;
  } catch (error) {
    throw new Error(error);
  }
};

export const getOutput = async (code, languageId) => {
  const url = `${BASE_URL}/submissions?base64_encoded=true`;
  const data = {
    language_id: +languageId,
    source_code: btoa(code),
    stdin: btoa(""),
  };

  try {
    const response = await axios.post(url, data, { headers });
    return await getCompilerOutput(response.data.token);
  } catch (error) {
    throw new Error(error);
  }
};

const getCompilerOutput = async (token) => {
  const url = `${BASE_URL}/submissions/${token}?base64_encoded=true`;

  try {
    const response = await axios.get(url, { headers });
    const compilationError = response.data.compile_output;
    const compilationOutput = response.data.stdout;

    return compilationError ? atob(compilationError) : atob(compilationOutput);
  } catch (error) {
    throw new Error(error);
  }
};
