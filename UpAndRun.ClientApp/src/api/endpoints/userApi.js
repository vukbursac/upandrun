import axios from "axios";

const mapResponseToUser = (response) => ({
  firstName: response.firstname,
  lastName: response.lastname,
  username: response.username,
  email: response.email,
  id: response.id,
});

// MANAGE ACCOUNT //

export const getUser = async (id) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/Users/${id}`;
  try {
    const response = await axios.get(url);
    const user = mapResponseToUser(response.data);
    return user;
  } catch (error) {
    throw new Error(error);
  }
};

export const deleteUser = async (id, hardDelete) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/Users`;
  const data = {
    id,
    hardDelete,
  };

  try {
    const response = await axios.delete(url, { data });

    return response.data;
  } catch (error) {
    console.dir(error);
    throw new Error(error);
  }
};

export const updateUser = async (id, userCredentials) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/users/${id}`;

  try {
    const response = await axios.put(url, userCredentials);
    const user = mapResponseToUser(response.data);
    return user;
  } catch (error) {
    console.dir(error);
    throw new Error(error.response.data);
  }
};

// LOGIN AND REGISTER //

export const login = async (userCredentials) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/users/login`;

  try {
    const response = await axios.post(url, userCredentials);
    const user = mapResponseToUser(response.data.loginResponse);

    return {
      token: response.data.loginResponse.token,
      user,
      message: response.data.message,
    };
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const register = async (userCredentials) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/users/registration`;

  try {
    const response = await axios.post(url, userCredentials);
  } catch (error) {
    throw new Error(error.response.data);
  }
};
