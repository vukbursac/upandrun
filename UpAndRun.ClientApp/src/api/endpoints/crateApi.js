import axios from "axios";
import { NULL_ID } from "../../shared/constants";

const fetchCratesByIds = async (ids) => {
  const crates = ids.map((id) => fetchCrateByIdApi(id));
  return await Promise.all(crates);
};

export const fetchCratesByCrateId = async (parentCrateId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates/${parentCrateId}`;

  try {
    const response = await axios.get(url);
    const crateIds = response.data.subCratesIds;
    const crates = await fetchCratesByIds(crateIds);
    return crates;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const fetchAllCratesApi = async () => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates`;
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const fetchCrateByIdApi = async (id) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates/${id}`;

  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const fetchRootCrates = async () => {
  const crates = await fetchAllCratesApi();

  const rootCrates = crates.filter((crate) => crate.parentCrateId === NULL_ID);

  return rootCrates;
};

export const addCrateApi = async (crateThunkData) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates`;

  try {
    const response = await axios.post(url, crateThunkData);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const renameCrateApi = async (name, id) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates/rename`;

  try {
    const response = await axios.patch(url, { id, name });
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const deleteCrateApi = async (id) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/crates/?id=${id}`;

  try {
    const response = await axios.delete(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data);
  }
};

export const changeParentCrateIdApi = async (id, parentCrateId) => {
  const url = `${process.env.REACT_APP_BASE_URL}/api/Crates/move`;
  await axios.patch(url, { id, parentCrateId });
};
