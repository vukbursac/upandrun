import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "../configuration/redux";
import thunk from "redux-thunk";

export const storeFactory = (initialState = {}) => {
  const createStoreMiddleware = applyMiddleware(thunk)(createStore);
  return createStoreMiddleware(rootReducer, initialState);
};

export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};
