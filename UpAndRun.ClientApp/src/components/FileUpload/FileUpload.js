import React, { useEffect } from "react";

import { useState, useReducer } from "react";
import { useDispatch } from "react-redux";
import { BsCloudUpload } from "react-icons/bs";
import classes from "./FileUpload.module.scss";

import { addFileThunk } from "../../store/actions/crateActions";
import { Button } from "../../shared/UI/Button/Button";
import { useLocation } from "react-router";

export const FileUpload = ({ isFullMode = false, handleExit }) => {
  const dispatch = useDispatch();
  const { search } = useLocation();
  const [errorMessage, setErrorMessage] = useState("");
  const [isFileAdded, setIsFileAdded] = useState(false);
  const [isHovering, setIsHovering] = useState(false);
  const [uploadedFile, setUploadedFile] = useState();
  const [crateId, setCrateId] = useState(0);

  useEffect(() => {
    const urlParams = new URLSearchParams(search);
    const crateId = urlParams.get("crateId");
    setCrateId(crateId);
  }, [search, dispatch]);

  const initialState = {
    dropDepth: 0,
    inDropZone: false,
    fileList: [],
  };

  const fileInputReducer = (state = initialState, action) => {
    switch (action.type) {
      case "SET_DROP_DEPTH":
        return { ...state, dropDepth: action.dropDepth };
      case "SET_IN_DROP_ZONE":
        return { ...state, inDropZone: action.inDropZone };
      case "ADD_FILE":
        return { ...state, fileList: state.fileList.concat(action.file) };
      default:
        return state;
    }
  };

  const [fileInputState, fileInputDispatch] = useReducer(
    fileInputReducer,
    initialState
  );

  const getDropZoneClasses = () => {
    if (isHovering && isFullMode) {
      return `${classes.fileUpload} ${classes.fullMode} ${classes.dropOver}`;
    }

    if (isHovering) {
      return `${classes.fileUpload} ${classes.dropOver}`;
    }

    if (isFullMode) {
      return `${classes.fileUpload} ${classes.fullMode} `;
    }

    return classes.fileUpload;
  };

  const renderDropBoxLoaded = () => {
    if (isFullMode) {
      return;
    }

    if (!isFileAdded) {
      return (
        <>
          <BsCloudUpload className={classes.uploadImg} />
          <div className={classes.about}>
            <div>
              <b>Drag here</b> or <b>click</b>
            </div>
            <div>
              <b>to upload files</b>
            </div>
          </div>
        </>
      );
    }

    return (
      <>
        <BsCloudUpload className={classes.uploadImg} />
        <div className={classes.about}>
          <b>Files ready for upload</b>
        </div>
      </>
    );
  };

  const handleSubmitUpload = () => {
    if (isFileAdded) {
      handleExit();
      setErrorMessage("");
      dispatch(addFileThunk(uploadedFile, crateId));
      return;
    }

    setErrorMessage("Must add a file to upload");
  };

  const handleFileInput = (e) => {
    setIsFileAdded(true);
    setErrorMessage("");

    const filePath = e.target.value;
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("file", file);

    fileInputDispatch({ type: "ADD_FILE", filePath });

    if (isFullMode) {
      dispatch(addFileThunk(formData, crateId));
      return;
    }

    setUploadedFile(formData);
  };

  const handleDragEnter = () => {
    fileInputDispatch({
      type: "SET_DROP_DEPTH",
      dropDepth: fileInputState.dropDepth + 1,
    });
  };

  const handleDragLeave = () => {
    setIsHovering(false);

    fileInputDispatch({
      type: "SET_DROP_DEPTH",
      dropDepth: fileInputState.dropDepth - 1,
    });

    if (fileInputState.dropDepth > 0) {
      return;
    }

    fileInputDispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });
  };

  const handleDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();

    setIsHovering(true);

    e.dataTransfer.dropEffect = "copy";
    fileInputDispatch({ type: "SET_IN_DROP_ZONE", inDropZone: true });
  };

  const handleDrop = (e) => {
    e.preventDefault();

    setIsFileAdded(true);
    setIsHovering(false);
    setErrorMessage("");

    const files = [...e.dataTransfer.files];
    const file = files[0];

    fileInputDispatch({ type: "ADD_FILE", files });
    fileInputDispatch({ type: "SET_DROP_DEPTH", dropDepth: 0 });
    fileInputDispatch({ type: "SET_IN_DROP_ZONE", inDropZone: false });

    const formData = new FormData();
    formData.append("file", file);

    if (isFullMode) {
      dispatch(addFileThunk(formData, crateId));
      e.dataTransfer.clearData();
      return;
    }

    setUploadedFile(formData);
    e.dataTransfer.clearData();
  };

  const renderButtons = () => {
    if (!isFullMode) {
      return (
        <>
          <div className={classes.buttons}>
            <Button
              type='button'
              variant='primary'
              classNames={[classes.button]}
              isInverted={true}
              size='small'
              click={handleExit}
              dataTestId='file-upload-cancel-button'
            >
              Cancel
            </Button>
            <Button
              type='submit'
              variant='primary'
              classNames={[classes.button]}
              size='small'
              click={handleSubmitUpload}
              dataTestId='file-upload-upload-button'
            >
              Upload
            </Button>
          </div>
          <div>
            <span className={classes.error}>{errorMessage}</span>
          </div>
        </>
      );
    }
  };

  return (
    <>
      <input
        type='file'
        multiple={false}
        id='file'
        onChange={handleFileInput}
        disabled={isFileAdded || isFullMode}
      />
      <div
        className={getDropZoneClasses()}
        onDrop={handleDrop}
        onDragOver={handleDragOver}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
      >
        <label htmlFor='file' className={classes.label}>
          {renderDropBoxLoaded()}
        </label>
      </div>
      {renderButtons()}
    </>
  );
};
