import { NavLink, useRouteMatch } from "react-router-dom";
import classes from "./Accordion.module.scss";

import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import { useIsSwitchedToMobile } from "../../../../../shared/customHooks/isSwitchedToMobile";

export const Accordion = ({
  select,
  items,
  dataTestId,
  Icon,
  selected,
  id,
  toggleAccordions,
  isCollapsed,
}) => {
  const match = useRouteMatch();
  const isSwitchedToMobile = useIsSwitchedToMobile();

  const toggleSelect = (e) => {
    const { type } = e;

    const handleInMobileView = () => {
      if (type === "click" || type === "mouseleave") {
        toggleAccordions(id);
      }
    };

    const handleInDesktopView = () => {
      const isMouseHoverEvent = type === "mouseenter" || type === "mouseleave";

      if (!isCollapsed && isMouseHoverEvent) {
        return;
      }

      toggleAccordions(id);
    };

    if (isSwitchedToMobile) {
      handleInMobileView();
      return;
    }

    handleInDesktopView();
  };

  const renderArrow = () => {
    if (selected) {
      return <MdKeyboardArrowUp className={classes.icon} />;
    }

    return <MdKeyboardArrowDown className={classes.icon} />;
  };

  const renderList = () => {
    if (selected) {
      return <ul className={classes.items}>{renderItems()}</ul>;
    }
  };

  const renderItems = () =>
    items.map((item) => (
      <li key={item.name} className={classes.item}>
        <NavLink
          data-testid={item.dataTestId}
          className={classes.link}
          to={getPathName(item.name)}
        >
          {item.name}
        </NavLink>
      </li>
    ));

  const getAccordionClasses = () => {
    if (isCollapsed) {
      return `${classes.accordion} ${classes.collapsed}`;
    }

    return classes.accordion;
  };

  const getPathName = (item) =>
    `${match.path}/${item.toLowerCase().replace(/ /g, "-")}`;

  return (
    <div
      className={getAccordionClasses()}
      onMouseEnter={toggleSelect}
      onMouseLeave={toggleSelect}
      onClick={toggleSelect}
    >
      <div className={classes.selectContainer}>
        <div className={classes.select}>
          <Icon className={classes.iconSelect} />
          <span data-testid={dataTestId} className={classes.title}>
            {select}
          </span>
        </div>
        {renderArrow()}
      </div>
      {renderList()}
    </div>
  );
};
