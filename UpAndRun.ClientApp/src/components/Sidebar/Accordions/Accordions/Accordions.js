import { GrServices } from "react-icons/gr";
import { MdAccountBox, MdPayment, MdPeople } from "react-icons/md";
import { BsInfoCircleFill } from "react-icons/bs";

import classes from "./Accordions.module.scss";

import { AccordionHandler } from "../../../../shared/hoc/AccordionHandler/AccordionHandler";

import { Accordion } from "./Accordion/Accordion";

export const Accordions = ({ isCollapsed }) => {
  const accordionItems = [
    {
      id: 1,
      select: "Services",
      dataTestId: "sidebar-services-span",
      Icon: GrServices,
      items: [
        { name: "Crates", dataTestId: "sidebar-services-crates-link" },
        { name: "Runtime", dataTestId: "sidebar-services-runtime-link" },
      ],
    },
    {
      id: 2,
      select: "Account",
      dataTestId: "sidebar-account-span",
      Icon: MdAccountBox,
      items: [
        {
          name: "Manage Account",
          dataTestId: "sidebar-services-manage-account-link",
        },
      ],
    },
    {
      id: 3,
      select: "Payment",
      dataTestId: "sidebar-payment-span",
      Icon: MdPayment,
      items: [
        {
          name: "Premium",
          dataTestId: "sidebar-services-payment-premium-link",
        },
        { name: "Free", dataTestId: "sidebar-services-payment-free-link" },
      ],
    },
    {
      id: 4,
      select: "Team Management",
      dataTestId: "sidebar-team-managment-span",
      Icon: MdPeople,
      items: [
        {
          name: "Team",
          dataTestId: "sidebar-services-team-managment-team-link",
        },
      ],
    },
    {
      id: 5,
      select: "About",
      dataTestId: "sidebar-about-span",
      Icon: BsInfoCircleFill,
      items: [
        {
          name: "Terms & Services",
          dataTestId: "sidebar-services-about-terms-and-services-link",
        },
      ],
    },
  ];

  return (
    <ul className={classes.accordions}>
      <AccordionHandler
        accordionItems={accordionItems}
        Component={Accordion}
        props={{ isCollapsed }}
      />
    </ul>
  );
};
