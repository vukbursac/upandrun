import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { BiLogOut } from "react-icons/bi";
import { AiFillHome } from "react-icons/ai";
import { RiArrowLeftRightFill } from "react-icons/ri";
import logo from "../../shared/images/logo.png";

import classes from "./Sidebar.module.scss";

import { Accordions } from "./Accordions/Accordions/Accordions";
import { ProgressBar } from "../ProgressBar/ProgressBar";

export default function Sidebar() {
  const [isSideBarCollapsed, setIsSidebarCollapsed] = useState(false);

  const toggleSidebar = () => setIsSidebarCollapsed(!isSideBarCollapsed);

  const getSideBarClasses = () => {
    if (!isSideBarCollapsed) {
      return classes.sidebar;
    }

    return `${classes.sidebar} ${classes.sidebarHide}`;
  };

  return (
    <div className={getSideBarClasses()}>
      <div className={classes.sidebarMain}>
        <div className={classes.sidebarTop}>
          <NavLink
            className={classes.linkLogo}
            to={{ pathname: "/dashboard/home" }}
          >
            <div className={classes.imgWrapper}>
              <img src={logo} className={classes.img} alt='Logo' />
            </div>
          </NavLink>

          <NavLink to={"/dashboard/home"} className={classes.link}>
            <AiFillHome className={classes.linkIcon} />
            <span className={classes.linkText}>Home</span>
          </NavLink>
          <Accordions isCollapsed={isSideBarCollapsed} />
          <div className={classes.progressBar}>
            <ProgressBar />
          </div>
        </div>
        <div className={classes.sidebarBottom}>
          <button className={classes.toggleBtn} onClick={toggleSidebar}>
            <RiArrowLeftRightFill className={classes.toggleIcon} />
            <span className={classes.toggleText}>Collapse</span>
          </button>
          <NavLink to={{ pathname: "/logout" }} className={classes.link}>
            <BiLogOut className={classes.linkIcon} />
            <span className={classes.linkText}>Sign Out</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
