import classes from "./EditFileForm.module.scss";
import { updateFileThunk } from "../../../store/actions/crateActions";
import { useDispatch } from "react-redux";
import { Button } from "../../../shared/UI/Button/Button";
import { useState } from "react";
import {
  extractFileExtension,
  extractFileName,
} from "../../../shared/Utils/file";

export const EditFileForm = ({ hideModal, file, crateId }) => {
  const dispatch = useDispatch();
  const extension = extractFileExtension(file.fileName);
  const [name, setName] = useState(() => extractFileName(file.fileName));
  const [errorMessage, setErrorMessage] = useState("");

  const renamedFileName = `${name}.${extension}`;

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!isValidInput()) {
      setErrorMessage("Must input a name");
      return;
    }

    dispatch(updateFileThunk(renamedFileName, file.id, crateId));
    exit();
  };

  const handleInputChange = (e) => setName(e.target.value);

  const isValidInput = () => {
    if (!name) {
      return false;
    }

    return true;
  };

  const exit = () => hideModal();

  const renderErrorMessage = () => {
    if (errorMessage) {
      return (
        <div className={classes.error}>
          <span>{errorMessage}</span>
        </div>
      );
    }
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <input
          value={name}
          className={classes.input}
          type='text'
          onChange={handleInputChange}
        />
      </div>
      {renderErrorMessage()}
      <div className={classes.buttons}>
        <Button
          type='button'
          variant='primary'
          classNames={[classes.button]}
          isInverted={true}
          size='small'
          click={exit}
          dataTestId='edit-file-form-cancel-button'
        >
          Cancel
        </Button>
        <Button
          type='submit'
          variant='primary'
          classNames={[classes.button]}
          size='small'
          dataTestId='edit-file-form-rename-button'
        >
          Rename
        </Button>
      </div>
    </form>
  );
};
