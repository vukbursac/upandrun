import classes from "./DeleteFileForm.module.scss";
import { useDispatch } from "react-redux";
import { deleteFileThunk } from "../../../store/actions/crateActions";
import { Button } from "../../../shared/UI/Button/Button";

export const DeleteFileForm = ({ hideModal, file, crateId }) => {
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    exit();
    dispatch(deleteFileThunk(file.id, +crateId));
  };

  const exit = () => hideModal();

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <div className={classes.buttons}>
          <Button
            type='button'
            variant='primary'
            classNames={[classes.button]}
            isInverted={true}
            size='small'
            click={exit}
            dataTestId='delete-file-form-cancel-button'
          >
            Cancel
          </Button>
          <Button
            type='submit'
            variant='primary'
            classNames={[classes.button]}
            size='small'
            dataTestId='edit-file-form-remove-button'
          >
            Remove
          </Button>
        </div>
      </div>
    </form>
  );
};
