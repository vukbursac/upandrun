import classes from "./FilesGrid.module.scss";
import { File } from "../File/File";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { filesSelector } from "../../store/reducers/crateReducer";
import { Modal } from "../../shared/UI/Modal/Modal";
import { EditFileForm } from "./EditFileForm/EditFileForm";
import { DeleteFileForm } from "./DeleteFileForm/DeleteFileForm";
import { Form } from "../Crates/Crate/Form/Form";
import { FormHeading } from "../Crates/Crate/CrateFormHeading/CrateFormHeading";
import { AddFileForm } from "./AddFileForm/AddFileForm";
import { useLocation } from "react-router";
import { FlyIn } from "../../shared/UI/FlyIn/FlyIn";
import { FileDetails } from "./FileDetails/FileDetails";
import { downloadFileThunk } from "../../store/actions/crateActions";

export const FilesGrid = ({
  showAddFileForm,
  hideAddFileForm,
  isSubCrates = false,
}) => {
  const dispatch = useDispatch();
  const { search } = useLocation();
  const files = useSelector(filesSelector);
  const [crateId, setCrateId] = useState(0);

  useEffect(() => {
    const urlParams = new URLSearchParams(search);
    const crateId = urlParams.get("crateId");
    setCrateId(crateId);
  }, [search, dispatch]);

  const [modalIsShowed, setModalIsShowed] = useState(false);
  const [isFlyInOpen, setIsFlyInOpen] = useState(false);
  const [formType, setFormType] = useState("ADD");
  const [selectedFile, setSelectedFile] = useState();

  useEffect(() => {
    if (!isSubCrates) {
      return;
    }

    if (showAddFileForm) {
      setFormType("ADD");
      setModalIsShowed(true);
    }

    hideAddFileForm();
  }, [showAddFileForm, hideAddFileForm, isSubCrates]);

  const hideModal = () => setModalIsShowed(false);

  const renderDeleteForm = () => (
    <Form>
      <FormHeading heading='Move to trash?' handleExit={hideModal} />
      <DeleteFileForm
        hideModal={hideModal}
        file={selectedFile}
        crateId={crateId}
      />
    </Form>
  );

  const renderEditForm = () => (
    <Form>
      <FormHeading heading='Rename' handleExit={hideModal} />
      <EditFileForm
        hideModal={hideModal}
        file={selectedFile}
        crateId={crateId}
      />
    </Form>
  );

  const renderAddForm = () => (
    <Form>
      <FormHeading heading='Upload' handleExit={hideModal} />
      <AddFileForm handleExit={hideModal} />
    </Form>
  );

  const forms = {
    ADD: renderAddForm,
    EDIT: renderEditForm,
    DELETE: renderDeleteForm,
  };

  const renderForm = () => forms[formType]();

  const handleOnFileClick = (file) => {
    setIsFlyInOpen(true);
    setSelectedFile(file);
  };

  const renderFiles = () =>
    files.map((file) => (
      <File
        key={file.id}
        id={file.id}
        type={file.fileType}
        name={file.fileName}
        handleClick={() => handleOnFileClick(file)}
      />
    ));

  const handleOpenDeleteForm = () => handleFormSelection("DELETE");

  const handleOpenUpdateForm = () => handleFormSelection("EDIT");

  const handleFormSelection = (type) => {
    setFormType(type);
    setModalIsShowed(true);
  };

  const handleExit = () => setIsFlyInOpen(false);

  return (
    <>
      <Modal isOpen={modalIsShowed}>{renderForm()}</Modal>
      <FlyIn
        isOpen={isFlyInOpen}
        onClickOutside={() => {
          setIsFlyInOpen(false);
        }}
      >
        <FileDetails
          {...selectedFile}
          handleDownload={() => {
            dispatch(downloadFileThunk(selectedFile.id));
          }}
          handleExit={handleExit}
          handleOpenDeleteForm={handleOpenDeleteForm}
          handleOpenUpdateForm={handleOpenUpdateForm}
        />
      </FlyIn>
      <div className={classes.container}>
        <main className={classes.main}>
          <div className={classes.grid}>{renderFiles()}</div>
        </main>
      </div>
    </>
  );
};
