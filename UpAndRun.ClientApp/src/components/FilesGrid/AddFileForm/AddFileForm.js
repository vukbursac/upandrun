import React from "react";

import { FileUpload } from "../../FileUpload/FileUpload";

import classes from "./AddFileForm.module.scss";

export const AddFileForm = ({ handleExit }) => {
  return (
    <div className={classes.addFileForm}>
      <div className={classes.formGroup}>
        <div className={classes.upload}>
          <FileUpload isFullMode={false} handleExit={handleExit} />
        </div>
      </div>
    </div>
  );
};
