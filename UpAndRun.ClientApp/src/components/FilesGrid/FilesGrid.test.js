import { CrateGrid } from "./FilesGrid";
import { shallow } from "enzyme";
import React from "react";
import Enzyme from "enzyme";
import EnzymeAdapter from "@wojtekmaj/enzyme-adapter-react-17";
Enzyme.configure({ adapter: new EnzymeAdapter() });

describe("Crate Grid", () => {
  it("Component should render", () => {
    const crateGrid = shallow(<CrateGrid shouldRender />);

    expect(crateGrid).toBeDefined();
  });

  it("Child element has specific parent element ", () => {
    const wrapper = shallow(<CrateGrid />);
    expect(wrapper.find("div").parent().is("main")).toEqual(true);
  });

  it("Component rendered", () => {
    const wrapper = shallow(<CrateGrid />);
  });
});
