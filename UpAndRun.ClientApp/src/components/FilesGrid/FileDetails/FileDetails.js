import React from "react";
import { AiFillFileText } from "react-icons/ai";
import { Button } from "../../../shared/UI/Button/Button";

import classes from "./FileDetails.module.scss";

export const FileDetails = ({
  fileName,
  fileType,
  creationDate,
  handleOpenDeleteForm,
  handleOpenUpdateForm,
  handleDownload,
  handleExit,
}) => {
  return (
    <div className={classes.container}>
      <div className={classes.exit}>
        <button onClick={handleExit} className={classes.exitBtn}>
          &#10005;
        </button>
      </div>
      <div className={classes.file}>
        <AiFillFileText className={classes.icon}></AiFillFileText>
      </div>
      <div className={classes.about}>
        <ul className={classes.details}>
          <li className={classes.detail}>
            <h4 className={classes.type}>Name</h4>
            <span className={classes.text}>{fileName}</span>
          </li>
          <li className={classes.detail}>
            <h4 className={classes.text}>Creation Date</h4>
            <span className={classes.text}>{creationDate}</span>
          </li>
          <li className={classes.detail}>
            <h4 className={classes.text}>Type</h4>
            <span className={classes.text}>{fileType}</span>
          </li>
        </ul>
      </div>
      <div className={classes.buttons}>
        <Button
          type='submit'
          variant='primary'
          classNames={[classes.button]}
          size='small'
          click={handleOpenUpdateForm}
          dataTestId='file-details-update-button'
        >
          Update
        </Button>
        <Button
          type='button'
          variant='alert'
          classNames={[classes.button]}
          isInverted={false}
          size='small'
          click={handleOpenDeleteForm}
          dataTestId='crate-details-delete-button'
        >
          Delete
        </Button>
        <Button
          type='button'
          variant='success'
          classNames={[classes.button]}
          size='small'
          click={handleDownload}
          dataTestId='crate-details-download-button'
        >
          Download
        </Button>
      </div>
    </div>
  );
};
