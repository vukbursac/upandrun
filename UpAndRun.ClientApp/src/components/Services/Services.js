import classes from "./Services.module.scss";

import { GiWoodenCrate } from "react-icons/gi";
import { FiPlay } from "react-icons/fi";
import { NavLink } from "react-router-dom";

export const Services = () => {
  return (
    <>
      <div className={classes.title}>
        <h2 className='heading-2'>Services</h2>
      </div>
      <div className={classes.options}>
        <NavLink
          className={classes.link}
          to={{ pathname: "/dashboard/crates" }}
        >
          <div className={classes.option}>
            <GiWoodenCrate className={classes.icon}></GiWoodenCrate>
          </div>
        </NavLink>
        <NavLink
          className={classes.link}
          to={{ pathname: "/dashboard/runtime" }}
        >
          <div className={classes.option}>
            <FiPlay className={classes.icon}></FiPlay>
          </div>
        </NavLink>
      </div>
    </>
  );
};
