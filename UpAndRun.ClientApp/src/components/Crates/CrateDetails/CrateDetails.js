import React from "react";
import { GiWoodenCrate } from "react-icons/gi";
import { Button } from "../../../shared/UI/Button/Button";

import classes from "./CrateDetails.module.scss";

export const CrateDetails = ({
  name,
  creationDate,
  handleOpenDeleteForm,
  handleOpenUpdateForm,
  handleExit,
}) => {
  return (
    <div className={classes.container}>
      <div className={classes.exit}>
        <button onClick={handleExit} className={classes.exitBtn}>
          &#10005;
        </button>
      </div>
      <div className={classes.crate}>
        <GiWoodenCrate className={classes.icon}></GiWoodenCrate>
      </div>
      <div className={classes.about}>
        <ul className={classes.details}>
          <li className={classes.detail}>
            <h4 className={classes.type}>Name</h4>
            <span className={classes.text}>{name}</span>
          </li>
          <li className={classes.detail}>
            <h4 className={classes.type}>Creation Date</h4>
            <span className={classes.text}>{creationDate}</span>
          </li>
        </ul>
      </div>
      <div className={classes.buttons}>
        <Button
          type='submit'
          variant='primary'
          classNames={[classes.button]}
          size='small'
          click={handleOpenUpdateForm}
          dataTestId='crate-details-update-button'
        >
          Update
        </Button>
        <Button
          type='button'
          variant='alert'
          classNames={[classes.button]}
          isInverted={false}
          size='small'
          dataTestId='crate-details-delete-button'
          click={handleOpenDeleteForm}
        >
          Delete
        </Button>
      </div>
    </div>
  );
};
