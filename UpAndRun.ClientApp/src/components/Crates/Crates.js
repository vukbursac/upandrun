import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  cratesErrorSelector,
  cratesLoadingSelector,
  cratesSelector,
} from "../../store/reducers/crateReducer";

import Spinner from "../../shared/UI/Spinner/Spinner";
import { Modal } from "../../shared/UI/Modal/Modal";
import { Crate } from "./Crate/Crate";
import { FormHeading } from "./Crate/CrateFormHeading/CrateFormHeading";
import { AddCrateForm } from "./Crate/AddCrateForm/AddCrateForm";

import classes from "./Crates.module.scss";
import {
  addCrateThunk,
  deleteCrateThunk,
  fetchCratesByIdThunk,
  fetchFilesByIdThunk,
  updateCrateThunk,
} from "../../store/actions/crateActions";
import { DeleteCrateFrom } from "./Crate/DeleteCrateForm/DeleteCrateForm";
import { UpdateCrateForm } from "./Crate/UpdateCrateForm/UpdateCrateForm";
import { AiOutlineFolderAdd } from "react-icons/ai";
import { Form } from "./Crate/Form/Form";
import { useLocation } from "react-router";
import { FlyIn } from "../../shared/UI/FlyIn/FlyIn";
import { CrateDetails } from "./CrateDetails/CrateDetails";

export const Crates = ({
  showAddCrateForm,
  hideAddCrateForm,
  isSubCrates = false,
}) => {
  const dispatch = useDispatch();
  const { search } = useLocation();
  const isLoading = useSelector(cratesLoadingSelector);
  const error = useSelector(cratesErrorSelector);
  const crates = useSelector(cratesSelector);

  const [formType, setFormType] = useState("ADD");
  const [isPopUpOpen, setIsPopUpOpen] = useState(false);
  const [isFlyInOpen, setIsFlyInOpen] = useState(false);
  const [selectedCrate, setSelectedCrate] = useState();

  const [crateId, setCrateId] = useState(0);

  useEffect(() => {
    const urlParams = new URLSearchParams(search);
    const crateId = urlParams.get("crateId");
    dispatch(fetchCratesByIdThunk(crateId));
    dispatch(fetchFilesByIdThunk(crateId));
    setCrateId(crateId);
  }, [search, dispatch]);

  useEffect(() => {
    if (!isSubCrates) {
      return;
    }

    if (showAddCrateForm) {
      openPopUpForm("ADD");
    }

    hideAddCrateForm();
  }, [showAddCrateForm, hideAddCrateForm, isSubCrates]);

  useEffect(() => {
    if (error) {
      return;
    }

    setIsPopUpOpen(false);
  }, [error]);

  const renderAddForm = () => (
    <Form>
      <FormHeading
        handleExit={() => setIsPopUpOpen(false)}
        heading='New Crate'
      />
      <AddCrateForm
        handleExit={() => setIsPopUpOpen(false)}
        handleAdd={(name) => dispatch(addCrateThunk(name, crateId))}
        error={error}
      />
    </Form>
  );

  const renderDeleteForm = () => (
    <Form>
      <FormHeading
        handleExit={() => setIsPopUpOpen(false)}
        heading='Move to trash?'
      />
      <DeleteCrateFrom
        handleExit={() => setIsPopUpOpen(false)}
        handleDelete={() =>
          dispatch(deleteCrateThunk(selectedCrate.id, crateId))
        }
      />
    </Form>
  );

  const renderUpdateForm = () => (
    <Form>
      <FormHeading
        handleExit={() => setIsPopUpOpen(false)}
        heading='Rename'
      ></FormHeading>
      <UpdateCrateForm
        handleExit={() => setIsPopUpOpen(false)}
        handleUpdate={(name) =>
          dispatch(updateCrateThunk(name, selectedCrate.id, crateId))
        }
        crate={selectedCrate}
        error={error}
      />
    </Form>
  );

  const forms = {
    ADD: renderAddForm,
    DELETE: renderDeleteForm,
    UPDATE: renderUpdateForm,
  };

  const renderForm = () => forms[formType]();

  const renderCrates = () =>
    crates.map((crate) => (
      <Crate
        key={crate.id}
        name={crate.name}
        id={crate.id}
        isSubCrate={isSubCrates}
        handleClick={() => handleOnCrateClick(crate)}
        handleExit={() => setIsFlyInOpen(false)}
      />
    ));

  const handleOnCrateClick = (crate) => {
    setSelectedCrate(crate);
    setIsFlyInOpen(true);
  };

  const openPopUpForm = (formType) => {
    setIsPopUpOpen(true);
    setFormType(formType);
  };

  const renderAddFolderButton = () => {
    if (!isSubCrates) {
      return (
        <button
          className={classes.btn}
          data-testid='crates-add-folder-button'
          onClick={() => {
            openPopUpForm("ADD");
          }}
        >
          <AiOutlineFolderAdd className={classes.folderIcon} />
        </button>
      );
    }
  };

  return (
    <>
      <div className={classes.container}>
        <Spinner isLoading={isLoading} />
        <Modal isOpen={isPopUpOpen}>{renderForm()}</Modal>
        <FlyIn
          isOpen={isFlyInOpen}
          onClickOutside={() => {
            setIsFlyInOpen(false);
          }}
        >
          <CrateDetails
            {...selectedCrate}
            handleExit={() => setIsFlyInOpen(false)}
            handleOpenDeleteForm={() => {
              openPopUpForm("DELETE");
            }}
            handleOpenUpdateForm={() => {
              openPopUpForm("UPDATE");
            }}
          />
        </FlyIn>
        <div className={classes.crates}>{renderCrates()}</div>
      </div>
      {renderAddFolderButton()}
    </>
  );
};
