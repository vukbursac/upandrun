import { useEffect, useState } from "react";
import classes from "./AddCrateForm.module.scss";

import { CrateFormButtons } from "../CrateFormButtons/CrateFormButtons";

export const AddCrateForm = ({ handleExit, handleAdd, error }) => {
  const [name, setName] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    setErrorMessage(error);
  }, [error]);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!isValidInput()) {
      setErrorMessage("Must input name");
      return;
    }

    handleAdd(name);
  };

  const handleForm = (e) => setName(e.target.value);

  const isValidInput = () => {
    if (!name) {
      return false;
    }

    return true;
  };

  const renderErrorMessage = () => {
    if (errorMessage) {
      return (
        <div className={classes.error}>
          <span>{errorMessage}</span>
        </div>
      );
    }
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <input
          name='name'
          value={name}
          className={classes.input}
          type='text'
          onChange={handleForm}
          data-testid='add-crate-form-input-field'
        />
      </div>
      {renderErrorMessage()}
      <CrateFormButtons handleExit={handleExit} submitText='Add' />
    </form>
  );
};
