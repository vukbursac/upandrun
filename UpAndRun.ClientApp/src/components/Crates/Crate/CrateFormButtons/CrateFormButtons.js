import { Button } from "../../../../shared/UI/Button/Button";

import classes from "./CrateFormButtons.module.scss";

export const CrateFormButtons = ({ handleExit, submitText }) => {
  return (
    <div className={classes.buttons}>
      <Button
        type='button'
        variant='primary'
        classNames={[classes.button]}
        isInverted={true}
        size='small'
        dataTestId='crate-form-cancel-button'
        click={handleExit}
      >
        Cancel
      </Button>
      <Button
        type='submit'
        variant='primary'
        classNames={[classes.button]}
        size='small'
        dataTestId='crate-form-add-button'
      >
        {submitText}
      </Button>
    </div>
  );
};
