import React from "react";

import classes from "./Form.module.scss";

export const Form = ({ children }) => (
  <div className={classes.form}>{children}</div>
);
