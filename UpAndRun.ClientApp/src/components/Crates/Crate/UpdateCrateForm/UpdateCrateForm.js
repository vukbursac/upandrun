import { useEffect, useState } from "react";
import classes from "./UpdateCrateForm.module.scss";

import { CrateFormButtons } from "../CrateFormButtons/CrateFormButtons";

export const UpdateCrateForm = ({ handleExit, handleUpdate, crate, error }) => {
  const [name, setName] = useState(crate.name);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    setErrorMessage(error);
  }, [error]);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!isValidInput()) {
      setErrorMessage("Must input name");
      return;
    }

    handleUpdate(name);
  };

  const handleForm = (e) => setName(e.target.value);

  const isValidInput = () => {
    if (!name) {
      return false;
    }

    return true;
  };

  const renderErrorMessage = () => {
    if (errorMessage) {
      return (
        <div className={classes.error}>
          <span>{errorMessage}</span>
        </div>
      );
    }
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <input
          name='name'
          value={name}
          className={classes.input}
          type='text'
          onChange={handleForm}
        />
      </div>
      {renderErrorMessage()}
      <CrateFormButtons handleExit={handleExit} submitText='Rename' />
    </form>
  );
};
