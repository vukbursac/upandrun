import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { GiWoodenCrate } from "react-icons/gi";
import classes from "./Crate.module.scss";
import OutsideClickHandler from "react-outside-click-handler";
import { useDispatch } from "react-redux";
import {
  moveCrateIntoCrateThunk,
  moveFileIntoCrateThunk,
} from "../../../store/actions/crateActions";
import { AiFillFolder } from "react-icons/ai";

export const Crate = ({ name, id, handleClick, isSubCrate, handleExit }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { search } = useLocation();
  const [searchParams, setSetSearchParams] = useState("");
  const [isClicked, setIsClicked] = useState(false);
  const [isHovered, setIsHovered] = useState(false);
  const [dropZoneDepth, setDropZoneDepth] = useState(0);

  useEffect(() => {
    const searchParams = new URLSearchParams(search);
    searchParams.set("crateId", id);
    setSetSearchParams(searchParams.toString());
  }, [id, search]);

  const handleOnClick = () => {
    handleClick();
    setIsClicked(true);
  };

  const handleOnDblClick = () => {
    handleExit();
    history.push({
      pathname: "/dashboard/crate-storage",
      search: searchParams,
    });
  };

  const getCrateClasses = () => {
    if (isClicked || isHovered) {
      return `${classes.container} ${classes.active}`;
    }

    return classes.container;
  };

  const getIcon = () => {
    if (isSubCrate) {
      return <AiFillFolder className={classes.icon} />;
    }

    return <GiWoodenCrate className={classes.icon}></GiWoodenCrate>;
  };

  const onDragStart = (event) => {
    event.dataTransfer.setData(
      "text/plain",
      JSON.stringify({ id, type: "CRATE" })
    );
    event.dataTransfer.effectAllowed = "move";
  };

  const onDragEnter = (event) => {
    event.preventDefault();
    setDropZoneDepth((prevState) => prevState + 1);
  };

  const onDragOver = (event) => {
    event.preventDefault();
  };

  const onDragLeave = () => {
    setDropZoneDepth((prevState) => prevState - 1);
  };

  useEffect(() => {
    if (dropZoneDepth > 0) {
      setIsHovered(true);
      return;
    }

    setIsHovered(false);
  }, [dropZoneDepth]);

  const onDrop = (event) => {
    const droppedItem = JSON.parse(event.dataTransfer.getData("text/plain"));

    if (droppedItem.type === "CRATE" && id !== droppedItem.id) {
      dispatch(moveCrateIntoCrateThunk(droppedItem.id, id));
    }

    if (droppedItem.type === "FILE") {
      dispatch(moveFileIntoCrateThunk(droppedItem.id, id));
    }

    setDropZoneDepth(0);
  };

  return (
    <div
      className={classes.crate}
      onClick={handleOnClick}
      onDoubleClick={handleOnDblClick}
      draggable={true}
      onDragStart={onDragStart}
      onDragEnter={onDragEnter}
      onDragOver={onDragOver}
      onDragLeave={onDragLeave}
      onDrop={onDrop}
    >
      <OutsideClickHandler onOutsideClick={() => setIsClicked(false)}>
        <div
          className={getCrateClasses()}
          data-testid='crate-info-drawer-container'
        >
          <div className={classes.option}>
            {getIcon()}
            <div className={classes.about}>
              <div className={classes.name}>{name}</div>
            </div>
          </div>
        </div>
      </OutsideClickHandler>
    </div>
  );
};
