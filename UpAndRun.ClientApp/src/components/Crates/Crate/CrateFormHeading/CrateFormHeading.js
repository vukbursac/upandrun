import { AiOutlineClose } from "react-icons/ai";
import classes from "./CrateFormHeading.module.scss";

export const FormHeading = ({ handleExit, heading }) => {
  return (
    <div className={classes.crateForm}>
      <div className={classes.title}>
        <h3 className={classes.heading}>{heading}</h3>
        <button className={classes.closeBtn} onClick={handleExit}>
          <AiOutlineClose className={classes.icon} />
        </button>
      </div>
    </div>
  );
};
