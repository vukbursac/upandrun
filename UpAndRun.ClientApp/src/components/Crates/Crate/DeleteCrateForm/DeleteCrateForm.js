import { CrateFormButtons } from "../CrateFormButtons/CrateFormButtons";

export const DeleteCrateFrom = ({ handleExit, handleDelete }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    handleDelete();
    handleExit();
  };

  return (
    <form onSubmit={handleSubmit}>
      <CrateFormButtons handleExit={handleExit} submitText='Remove' />
    </form>
  );
};
