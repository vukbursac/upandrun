import React from "react";

import { Runtime } from "./Runtime";
import { storeFactory, findByTestAttr } from "../../test/testUtils";
import { Provider } from "react-redux";
import { initialState } from "../../store/reducers/runtimeReducer";
import { Editor } from "./Editor/Editor";

import { mount } from "enzyme";
import {
  resetOutputAction,
  setSelectedProgrammingLanguageAction,
} from "../../store/actions/runtimeActions";

const setup = (initialState = {}) => {
  const store = storeFactory(initialState);
  const wrapper = mount(
    <Provider store={store}>
      <Runtime />
    </Provider>
  );

  return wrapper;
};

describe("render", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({ runtime: initialState });
  });

  test("is Editor rendered", () => {
    expect(wrapper.containsMatchingElement(<Editor />)).toEqual(true);
  });
});

describe("programming languages are loaded", () => {
  const programmingLanguages = [
    { name: "C#", id: 1 },
    { name: "Java", id: 2 },
  ];

  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      runtime: {
        ...initialState,
        languages: programmingLanguages,
      },
    });
  });

  test("are programming languages options rendered", () => {
    const programmingLanguageOptionNodes = findByTestAttr(
      wrapper,
      "programming-language-option"
    );
    expect(programmingLanguageOptionNodes.length).toBe(2);
  });
});

describe("programming language is selected", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      runtime: {
        ...initialState,
        selectedLanguage: { name: "C#", id: 1 },
      },
    });
  });

  test("is selected programming language name rendered in heading", () => {
    const heading = findByTestAttr(wrapper, "heading");
    expect(heading.text()).toContain("C#");
  });

  test("is selected programming language name the value of select", () => {
    const select = findByTestAttr(wrapper, "selected-language");
    expect(select.props().value).toContain("C#");
  });
});

describe("dispatch the correct action", () => {
  const getWrapper = (mockStore) =>
    mount(
      <Provider store={mockStore}>
        <Runtime />
      </Provider>
    );

  let mockStore;
  let wrapper;
  beforeEach(() => {
    mockStore = storeFactory({
      runtime: {
        ...initialState,
        languages: [{ name: "C#", id: 1 }],
      },
    });
    mockStore.dispatch = jest.fn();

    wrapper = getWrapper(mockStore);
  });

  test("should dispatch the correct action when programming languages exist", () => {
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      setSelectedProgrammingLanguageAction({ name: "C#", id: 1 })
    );
  });

  test("should dispatch the correct action on reset code click", () => {
    const button = findByTestAttr(wrapper, "reset-code-btn");
    button.simulate("click");
    expect(mockStore.dispatch).toHaveBeenCalledWith(resetOutputAction());
  });
});
