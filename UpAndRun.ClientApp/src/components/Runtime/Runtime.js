import React, { useEffect, useState } from "react";
import { Editor } from "./Editor/Editor";
import { FiSun, FiMoon } from "react-icons/fi";

import classes from "./Runtime.module.scss";

import {
  fetchProgrammingLanguagesThunk,
  fetchCompilerOutputThunk,
  resetOutputAction,
  setSelectedProgrammingLanguageAction,
} from "../../store/actions/runtimeActions";
import { useDispatch, useSelector } from "react-redux";
import {
  programmingLanguagesSelector,
  loadingCompilerOutputSelector,
  compilerOutputSelector,
  selectedProgrammingLanguageSelector,
} from "../../store/reducers/runtimeReducer";
import Spinner from "../../shared/UI/Spinner/Spinner";
import { Button } from "../../shared/UI/Button/Button";
import { ToggleButton } from "../../shared/UI/ToggleButton/ToggleButton";

export const Runtime = () => {
  const dispatch = useDispatch();
  const compilerOutput = useSelector(compilerOutputSelector);
  const isCompilerOutputLoading = useSelector(loadingCompilerOutputSelector);
  const programmingLanguages = useSelector(programmingLanguagesSelector);
  const selectedProgrammingLanguage = useSelector(
    selectedProgrammingLanguageSelector
  );

  const [code, setCode] = useState("");
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const toggleTheme = () => setIsDarkTheme(!isDarkTheme);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    dispatch(fetchProgrammingLanguagesThunk());
  }, [dispatch]);

  useEffect(() => {
    if (programmingLanguages.length > 0) {
      dispatch(setSelectedProgrammingLanguageAction(programmingLanguages[0]));
    }
  }, [programmingLanguages, dispatch]);

  const handleLanguageSelection = (event) => {
    const languageSelected = programmingLanguages.find(
      (language) => language.id === +event.target.value
    );

    dispatch(setSelectedProgrammingLanguageAction(languageSelected));
  };

  const handleSubmitCode = () => {
    if (code) {
      dispatch(fetchCompilerOutputThunk(code, selectedProgrammingLanguage.id));
      setErrorMessage("");
      return;
    }

    setErrorMessage("Cannot run without code");
  };

  const handleReset = () => {
    dispatch(resetOutputAction());
    setCode("");
  };

  const renderLanguageOptions = () =>
    programmingLanguages.map((language) => (
      <option
        data-test='programming-language-option'
        key={language.id}
        value={language.id}
        className={classes.option}
      >
        {language.name}
      </option>
    ));

  return (
    <>
      <div>
        <h2 className='heading-2'>Runtime</h2>
      </div>
      <div>
        <div className={classes.runtime}>
          <div className={classes.filesContainer}>
            <div className={classes.fileItem}>
              <Button variant='primary'>Upload file</Button>
            </div>
            <div className={classes.fileItem}>
              <span className={classes.text}>or choose files</span>
            </div>
            <div className={classes.fileItem}>
              <ul className={classes.files}>
                <li className={classes.file}>
                  <Button variant='primary' isInverted={true}>
                    File.cs
                  </Button>
                </li>
                <li className={classes.file}>
                  <Button variant='primary' isInverted={true}>
                    File.cs
                  </Button>
                </li>
                <li className={classes.file}>
                  <Button variant='primary' isInverted={true}>
                    File.cs
                  </Button>
                </li>
              </ul>
            </div>
            <div className={classes.fileItem}>
              <Button variant='primary'>More Files...</Button>
            </div>
          </div>
          <div className={classes.editor}>
            <div className={classes.configure}>
              <h3 className={`heading-3 ${classes.title}`} data-test='heading'>
                {selectedProgrammingLanguage?.name}
              </h3>
              <div className={classes.error}>
                <span>{errorMessage}</span>
              </div>
              <div className={classes.configuration}>
                <ToggleButton
                  isOn={isDarkTheme}
                  click={toggleTheme}
                  IconOn={FiSun}
                  IconOff={FiMoon}
                  className={classes.toggleBtn}
                />
                <div className={classes.selectContainer}>
                  <select
                    data-test='selected-language'
                    onChange={handleLanguageSelection}
                    value={selectedProgrammingLanguage?.name}
                    className={classes.select}
                  >
                    {renderLanguageOptions()}
                  </select>
                </div>
              </div>
            </div>
            <Editor
              language={selectedProgrammingLanguage?.name}
              value={code}
              onChange={setCode}
              isDarkTheme={isDarkTheme}
            />
          </div>
          <div className={classes.consoleContainer}>
            <div className={classes.options}>
              <Button
                variant='secondary'
                click={handleSubmitCode}
                size='medium'
                classNames={[classes.optionBtn]}
              >
                Run
              </Button>
              <Button variant='secondary' click={handleReset} size='medium'>
                Reset
              </Button>
            </div>
            <div className={classes.console}>
              <code className={classes.code}>
                <Spinner
                  loadingFinishedResult={compilerOutput}
                  isLoading={isCompilerOutputLoading}
                  size='sm'
                />
              </code>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
