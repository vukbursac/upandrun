import React from "react";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/clike/clike";
import { Controlled as ControlledEditor } from "react-codemirror2";

import classes from "./Editor.module.scss";

const clikeLanguages = ["C ", "C++", "JAVA "];

// TODO Update this function to check which programming language is chosen
const getLanguageMode = (language = "clike") => {
  const isClikeLanguage = clikeLanguages.find((clikeLanguage) =>
    language.toUpperCase().includes(clikeLanguage)
  );

  if (isClikeLanguage) {
    return "clike";
  }

  return "clike";
};

export const Editor = ({ language, value, onChange, isDarkTheme }) => {
  const getTheme = () => {
    if (isDarkTheme) {
      return "material";
    }
    return "";
  };

  const handleEditorInputChange = (editor, data, value) => {
    onChange(value);
  };

  return (
    <div className={classes.container}>
      <ControlledEditor
        onBeforeChange={handleEditorInputChange}
        value={value}
        className={classes.wrapper}
        options={{
          lineWrapping: true,
          lineNumbers: true,
          mode: getLanguageMode(language),
          tabSize: 2,
          theme: getTheme(),
        }}
      />
    </div>
  );
};
