import classes from "./File.module.scss";
import { useState } from "react";
import OutsideClickHandler from "react-outside-click-handler";

import { uploadFileIcon } from "../../shared/Utils/fileIconUpload";

export const File = ({ handleClick, type, name, id }) => {
  const [isClicked, setIsClicked] = useState(false);

  const handleOnClick = () => {
    handleClick();
    setIsClicked(true);
  };

  const getFileClasses = () => {
    if (isClicked) {
      return `${classes.container} ${classes.active}`;
    }

    return classes.container;
  };

  const onDragStart = (event) => {
    event.dataTransfer.setData(
      "text/plain",
      JSON.stringify({ id, type: "FILE" })
    );
    event.dataTransfer.effectAllowed = "move";
  };

  return (
    <div
      className={classes.file}
      onClick={handleOnClick}
      draggable={true}
      onDragStart={onDragStart}
    >
      <OutsideClickHandler onOutsideClick={() => setIsClicked(false)}>
        <div
          className={getFileClasses()}
          data-testid='file-info-drawer-container'
        >
          <div className={classes.option}>
            <div className={classes.imgWrapper}>
              <img
                className={classes.img}
                src={uploadFileIcon(name)}
                alt='File'
              />
            </div>
            <div className={classes.about}>
              <div className={classes.name}>{name}</div>
            </div>
          </div>
        </div>
      </OutsideClickHandler>
    </div>
  );
};
