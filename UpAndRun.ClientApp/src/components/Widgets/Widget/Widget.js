import classes from "./Widget.module.scss";

export const Widget = ({ name, Icon, description, colors }) => {
  const iconStyles = `${classes.icon} ${colors.icon}`;
  const descriptionStyles = `${classes.description} ${colors.desc}`;
  const calcStyles = `${classes.usedCalc} ${colors.highlight}`;

  const renderDescription = () => {
    return description.map((item, index) => {
      const renderSeparator = () => {
        if (index !== description.length - 1) {
          return <div className={classes.separator}></div>;
        }
      };

      return (
        <div className={classes.totalDesc} key={index}>
          <span className={classes.type}>{item.type}</span>
          <span className={calcStyles}>{item.calculation}</span>
          {renderSeparator()}
        </div>
      );
    });
  };

  return (
    <div className={classes.widget}>
      <div className={classes.container}>
        <div className={iconStyles}>
          <Icon className={classes.img} />
          <span className={classes.name}>{name}</span>
        </div>
        <div className={descriptionStyles}>{renderDescription()}</div>
      </div>
    </div>
  );
};
