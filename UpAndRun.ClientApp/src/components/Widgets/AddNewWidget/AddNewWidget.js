import classes from "./AddNewWidget.module.scss";
import { AiOutlineAppstoreAdd } from "react-icons/ai";

export const AddNewWidget = () => {
  return (
    <div className={classes.addNewWidget}>
      <div className={classes.container}>
        <AiOutlineAppstoreAdd className={classes.icon} />
        <p className={classes.description}>
          Add new <br />
          widget
        </p>
      </div>
    </div>
  );
};
