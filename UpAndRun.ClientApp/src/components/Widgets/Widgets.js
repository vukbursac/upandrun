import { Widget } from "./Widget/Widget";
import { FiPlay } from "react-icons/fi";
import { AiFillBug } from "react-icons/ai";
import { GiWoodenCrate } from "react-icons/gi";
import { AddNewWidget } from "./AddNewWidget/AddNewWidget";
import classes from "./Widgets.module.scss";

const cratesData = [
  {
    name: "Runtime",
    icon: FiPlay,
    description: [
      { type: "Spent", calculation: "12min" },
      { type: "Quota", calculation: "600min" },
      { type: "Cost", calculation: "25$" },
    ],

    classes: {
      icon: classes.runtimeIcon,
      desc: classes.runtimeDesc,
      highlight: classes.runtimeHighlight,
    },
  },
  {
    name: "Crates",
    icon: GiWoodenCrate,
    description: [
      { type: "Spent", calculation: "12min" },
      { type: "Quota", calculation: "600min" },
      { type: "Cost", calculation: "25$" },
    ],
    classes: {
      icon: classes.crateIcon,
      desc: classes.crateDesc,
      highlight: classes.crateHighlight,
    },
  },
  {
    name: "Crash reports",
    icon: AiFillBug,
    description: [{ type: "Total crashes: ", calculation: "23" }],
    classes: {
      icon: classes.crashIcon,
      desc: classes.crashDesc,
      highlight: classes.crashHighlight,
    },
  },
];

export const Widgets = () => {
  return (
    <div className={classes.container}>
      <div className={classes.row}>
        {cratesData.map(({ name, icon, description, classes }, index) => {
          return (
            <Widget
              key={index}
              name={name}
              Icon={icon}
              description={description}
              colors={classes}
            />
          );
        })}
        <AddNewWidget />
      </div>
    </div>
  );
};
