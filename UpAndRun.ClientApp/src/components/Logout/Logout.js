import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Redirect } from "react-router";
import { logout } from "../../store/actions/authenticationActions";

export const Logout = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(logout());
  }, [dispatch]);

  return <Redirect to='/' />;
};
