import { NavLink, useHistory } from "react-router-dom";
import Logo from "../../shared/images/logo.png";
import GoogleLogo from "../../shared/images/googleLogo.png";
import { useState, useEffect } from "react";

import { validateEmail } from "../../shared/validation/emailValidation";
import { validatePassword } from "../../shared/validation/passwordValidation";
import classes from "./Login.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { clearErrorsAction } from "../../store/actions/authenticationActions";

import {
  authenticationErrorSelector,
  loadingAuthenticationSelector,
} from "../../store/reducers/authenticationReducer";

import Spinner from "../../shared/UI/Spinner/Spinner";

import { loginUser, resetAuthentication } from "../../store/actions/index";

export const Login = (props) => {
  const dispatch = useDispatch();
  const errorMessage = useSelector(authenticationErrorSelector);
  const history = useHistory();
  const isLoadingAuthentication = useSelector(loadingAuthenticationSelector);

  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [errors, setErrors] = useState({ email: "", password: "" });
  const [formSubmitted, setFormSubmitted] = useState(false);

  const handleCredentialsChange = (event) => {
    setCredentials({ ...credentials, [event.target.name]: event.target.value });
    validateCredentials();

    if (errorMessage) {
      dispatch(clearErrorsAction());
    }
  };

  const validateCredentials = () => {
    const emailError = validateEmail(credentials.email);
    const passwordError = validatePassword(credentials.password);

    setErrors({
      email: emailError,
      password: passwordError,
    });

    if (!emailError && !passwordError) {
      return true;
    }

    return false;
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setFormSubmitted(true);
    const credentialsAreValid = validateCredentials();

    if (credentialsAreValid) {
      dispatch(loginUser(credentials));
    }
  };

  const handleRedirectToLandingPage = (e) => {
    e.preventDefault();
    history.push("/");
  };

  const renderSubmitButton = () => {
    if (isLoadingAuthentication) {
      return <></>;
    }

    return (
      <button
        data-testid='login-button-login-page'
        className={`${classes.btn} ${classes.btnLogin}`}
        type='submit'
      >
        Log in
      </button>
    );
  };
  useEffect(() => {
    dispatch(resetAuthentication());
  }, [dispatch]);

  return (
    <div className={classes.container}>
      <form className={classes.form} onSubmit={handleLogin} noValidate>
        <h1 className='heading-1'>Up 'n' Run</h1>
        <div>
          <img
            data-testid='logo-image-login-page'
            onClick={handleRedirectToLandingPage}
            className={classes.logo}
            src={Logo}
            alt='Logo'
          />
        </div>
        <div className={classes.formContainer}>
          <input
            className={
              formSubmitted && errors.email ? classes.inputError : classes.input
            }
            data-testid='email-login-input'
            onChange={handleCredentialsChange}
            type='email'
            name='email'
            value={credentials.email}
            placeholder='Email'
          />
          {formSubmitted && errors.email && (
            <span data-testid='email-input-error' className={classes.error}>
              {errors.email}
            </span>
          )}
          <input
            className={
              formSubmitted && errors.password
                ? classes.inputError
                : classes.input
            }
            data-testid='password-login-input'
            onChange={handleCredentialsChange}
            type='password'
            name='password'
            value={credentials.password}
            placeholder='Password'
          />
          {formSubmitted && errors.password && (
            <span data-testid='password-input-error' className={classes.error}>
              {errors.password}
            </span>
          )}

          <div
            data-testid='login-backend-input-error'
            className={classes.error}
          >
            {errorMessage}
          </div>

          <span className={classes.spanPassword}>Forgot your password?</span>

          <Spinner isLoading={isLoadingAuthentication} size='sm' />
          {renderSubmitButton()}

          <span className={classes.spanOr}>or</span>
          <button
            className={`${classes.btn} ${classes.btnGoogle}`}
            type='button'
          >
            <img
              className={classes.googleLogo}
              src={GoogleLogo}
              alt='GoogleLogo'
            />
            Sign in with Google
          </button>
          <div className={classes.signUp}>
            Not on Up 'n' Run yet ?
            <NavLink
              to={{ pathname: "/register" }}
              className={classes.signUpLink}
              data-testid='sign-up-link-login-page'
            >
              Sign up
            </NavLink>
          </div>
        </div>
      </form>
    </div>
  );
};
