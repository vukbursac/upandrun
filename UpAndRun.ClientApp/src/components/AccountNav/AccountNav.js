import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { currentUserSelector } from "../../store/reducers/authenticationReducer";
import classes from "./AccountNav.module.scss";

export const AccountNav = () => {
  const user = useSelector(currentUserSelector);
  const fullName = `${user.firstName} ${user.lastName}`;

  return (
    <div className={classes.accountNav}>
      <h3 className={classes.name}>{fullName}</h3>
      <span className={classes.email}>{user.email}</span>
      <div className={classes.options}>
        <NavLink to={{ pathname: "/manage-account" }} className={classes.link}>
          Manage account
        </NavLink>
        <NavLink
          data-testid='account-nav-logout'
          to={{ pathname: "/logout" }}
          className={classes.link}
        >
          Sign out
        </NavLink>
      </div>
    </div>
  );
};
