import OutsideClickHandler from "react-outside-click-handler";
import classes from "./Navigation.module.scss";

import { IoIosNotifications } from "react-icons/io";
import { MdCheckCircle } from "react-icons/md";
import Flag from "react-country-flag";
import user from "../../shared/images/user.png";
import { AccountNav } from "../AccountNav/AccountNav";
import { useState } from "react";
import { useSelector } from "react-redux";
import { CustomSelect } from "../../shared/UI/CustomSelect/CustomSelect";
import { userReactivatedMessageSelector } from "../../store/reducers/authenticationReducer";
import { SearchBar } from "./SearchBar/SearchBar";

export const Navigation = () => {
  const [isAccountSelected, setIsAccountSelected] = useState(false);
  const reactivateMessage = useSelector(userReactivatedMessageSelector);

  const toggleAccountNavDropDown = () => {
    setIsAccountSelected(!isAccountSelected);
  };

  const closeAccountNavDropDown = () => setIsAccountSelected(false);

  const renderAccount = () => {
    if (isAccountSelected) {
      return <AccountNav />;
    }
  };

  const renderAccountIsReactivatedMessage = () => {
    return reactivateMessage ? (
      <div className={classes.reactivationMessage}>
        {reactivateMessage}
        <MdCheckCircle className={classes.icon} />
      </div>
    ) : null;
  };

  return (
    <header className={classes.header}>
      {renderAccountIsReactivatedMessage()}

      <SearchBar />
      <nav className={classes.navigation}>
        <ul className={classes.items}>
          <li className={classes.item}>
            {/* // TODO Update when user selects different language */}
            <CustomSelect
              selected={<Flag countryCode='GB' svg />}
              options={[
                <Flag countryCode='GB' svg />,
                <Flag countryCode='RS' svg />,
              ]}
            />
          </li>
          <li className={classes.item}>
            <span className={classes.notification}>10</span>
            <IoIosNotifications className={classes.bellIcon} />
          </li>
          <li className={classes.item}>
            <OutsideClickHandler onOutsideClick={closeAccountNavDropDown}>
              <button
                className={classes.btn}
                onClick={toggleAccountNavDropDown}
              >
                <div className={classes.imgWrapperUser}>
                  <img
                    data-testid='avatar-image'
                    className={classes.img}
                    src={user}
                    alt='user'
                  />
                </div>
              </button>
              <div className={classes.account}>{renderAccount()}</div>
            </OutsideClickHandler>
          </li>
        </ul>
      </nav>
    </header>
  );
};
