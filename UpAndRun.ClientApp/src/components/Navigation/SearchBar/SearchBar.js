import React, { useState } from "react";
import { BsSearch } from "react-icons/bs";

import classes from "./SearchBar.module.scss";

export const SearchBar = () => {
  const [search, setSearch] = useState("");

  const onSearchChange = ({ target: { value } }) => setSearch(value);

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const renderExit = () => {
    if (search) {
      return (
        <div onClick={clearSearchInput} className={classes.exit}>
          &#10005;
        </div>
      );
    }
  };

  const clearSearchInput = () => setSearch("");

  return (
    <div className={classes.sidebar}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <div className={classes.formGroup}>
          <input
            placeholder='Search...'
            type='text'
            value={search}
            onChange={onSearchChange}
            className={classes.input}
          />
          <button className={classes.btn}>
            <BsSearch className={classes.icon} />
          </button>
          {renderExit()}
        </div>
      </form>
    </div>
  );
};
