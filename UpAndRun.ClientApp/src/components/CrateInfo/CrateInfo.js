import classes from "./CrateInfo.module.scss";

export const CrateInfo = () => (
  <div className={classes.fileInfo}>
    <div className={classes.folder}>
      <span className={classes.folderName}>Java Files</span>
    </div>
    <ul className={classes.list}>
      <li className={classes.item}>
        <span className={classes.itemName}>Name:</span> Java Files
      </li>
      <li className={classes.item}>
        <span className={classes.itemName}>Date created:</span> 10.08.2021.
      </li>
      <li className={classes.item}>
        <span className={classes.itemName}>Owner:</span> Petar Kovac
      </li>
      <li className={classes.item}>
        <span className={classes.itemName}>Size:</span> 28.2 MB
      </li>
    </ul>
  </div>
);
