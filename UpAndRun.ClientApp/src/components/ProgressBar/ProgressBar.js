import { AiOutlineCloud } from "react-icons/ai";

import classes from "./ProgressBar.module.scss";

export const ProgressBar = () => {
  return (
    <div className={classes.progressBarr}>
      <div className={classes.description}>
        <AiOutlineCloud className={classes.icon} />
        <span className={classes.text}>Storage</span>
      </div>
      <div className={classes.info}>
        <progress className={classes.bar} value='14' max='100'>
          32%
        </progress>
        <p className={classes.info}>1.1GB of 15GB</p>
      </div>
    </div>
  );
};
