import React from "react";
import classes from "./Dashboard.module.scss";

import Sidebar from "../Sidebar/Sidebar";
import { Navigation } from "../Navigation/Navigation";
import { Services } from "../Services/Services";
import { Route, Switch, useRouteMatch } from "react-router";
import { Crates } from "../Crates/Crates";
import { CrateFolder } from "../CrateFolder/CrateFolder";
import { Runtime } from "../Runtime/Runtime";
import { Home } from "../Home/Home";
import { ManageAccount } from "../ManageAccount/ManageAccount";

export const Dashboard = () => {
  const match = useRouteMatch();

  return (
    <div className={classes.mainContainer}>
      <Sidebar />
      <div className={classes.content}>
        <Navigation />
        <div className={classes.container}>
          <main className={classes.main}>
            <Switch>
              <Route path={`${match.path}/home`} component={Home} />
              <Route path={`${match.path}/services`} component={Services} />
              <Route path={`${match.path}/crates`} component={Crates} />
              <Route
                path={`${match.path}/crate-storage`}
                component={CrateFolder}
              />
              <Route
                path={`${match.path}/manage-account`}
                component={ManageAccount}
              />
              <Route path={`${match.path}/runtime`} component={Runtime} />
            </Switch>
          </main>
        </div>
      </div>
    </div>
  );
};
