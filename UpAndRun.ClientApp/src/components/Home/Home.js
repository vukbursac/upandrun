import classes from "./Home.module.scss";
import { Widgets } from "../Widgets/Widgets";
import { HomeProgressBar } from "./HomeProgressBar/HomeProgressBar";
import { Runtime } from "./Runtime/Runtime";
import { FileTrackerPieChart } from "./FileTrackerPieChart/FileTrackerPieChart";

export const Home = () => {
  return (
    <div className={classes.container}>
      <Widgets />
      <div className={classes.chartContainer}>
        <HomeProgressBar />
        <FileTrackerPieChart />
      </div>
      <Runtime />
    </div>
  );
};
