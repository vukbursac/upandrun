import { PieChart, Pie, Cell, ResponsiveContainer } from "recharts";
import classes from "./FileTrackerPieChart.module.scss";

export const FileTrackerPieChart = () => {
  const data = [
    { name: "Audio", count: 6, bgColor: "#84eba1" },
    { name: "Text", count: 4, bgColor: "#6eb6ef" },
    { name: "Pdf", count: 5, bgColor: "#1d527c" },
    { name: "Excel", count: 2, bgColor: "#8b0000" },
  ];

  const COLORS = ["#84eba1", "#6eb6ef", "#1d527c", "#8b0000"];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill='white'
        fontWeight='bold'
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline='central'
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  const renderPieChartLegend = () =>
    data.map((x, index) => (
      <div className={classes.legendWrapper} key={index}>
        <input
          className={classes.input}
          disabled
          type='color'
          id='head'
          name='head'
          value={x.bgColor}
        />
        <span className={classes.text}>{x.name}</span>
      </div>
    ));

  return (
    <div className={classes.pieChartWrapper}>
      <h3 className={classes.title}>Crates Storage</h3>
      <ResponsiveContainer width='100%' height={150}>
        <PieChart>
          <Pie
            data={data}
            cy={65}
            labelLine={false}
            label={renderCustomizedLabel}
            innerRadius={5}
            outerRadius={70}
            dataKey='count'
          >
            {data.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={COLORS[index % COLORS.length]}
              />
            ))}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
      <div className={classes.textContainer}>{renderPieChartLegend()}</div>
    </div>
  );
};
