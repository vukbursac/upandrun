import React from "react";
import { useSelector } from "react-redux";

import { AccordionHandler } from "../../../../shared/hoc/AccordionHandler/AccordionHandler";
import { recentRanFilesSelector } from "../../../../store/reducers/runtimeReducer";

import { ReportRow } from "./ReportRow/ReportRow";

export const ReportRows = () => {
  const recentRanFiles = useSelector(recentRanFilesSelector);

  const renderReports = () => (
    <AccordionHandler accordionItems={recentRanFiles} Component={ReportRow} />
  );

  return renderReports();
};
