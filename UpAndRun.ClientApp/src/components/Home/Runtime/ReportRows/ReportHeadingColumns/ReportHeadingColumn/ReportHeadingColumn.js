import React, { useState } from "react";
import { AiOutlineArrowDown, AiOutlineArrowUp } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { sortRecentRanFilesThunk } from "../../../../../../store/actions/runtimeActions";
import { recentRanFilesSelector } from "../../../../../../store/reducers/runtimeReducer";

import classes from "./ReportHeadingColumn.module.scss";

export const ReportHeadingColumn = ({ name, sort, type }) => {
  const dispatch = useDispatch();
  const recentRanFiles = useSelector(recentRanFilesSelector);
  const [isSelected, setIsSelected] = useState(false);

  const sortRows = () => {
    return recentRanFiles.sort(getSortOrder());
  };

  const getSortOrder = () => {
    if (!isSelected) {
      return (file1, file2) => sort(file1[type], file2[type]);
    } else {
      return (file2, file1) => sort(file1[type], file2[type]);
    }
  };

  const handleSortRows = () => {
    dispatch(sortRecentRanFilesThunk([...sortRows()]));
    setIsSelected(!isSelected);
  };

  const renderArrow = () => {
    if (isSelected) {
      return <AiOutlineArrowUp className={classes.icon} />;
    }

    return <AiOutlineArrowDown className={classes.icon} />;
  };

  return (
    <th className={classes.heading} onClick={handleSortRows}>
      <span className={classes.text}>
        {name}
        {renderArrow()}
      </span>
    </th>
  );
};
