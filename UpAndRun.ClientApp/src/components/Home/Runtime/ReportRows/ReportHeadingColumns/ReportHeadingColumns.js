import React from "react";

import { ReportHeadingColumn } from "./ReportHeadingColumn/ReportHeadingColumn";
import {
  sortDate,
  sortNumbers,
  sortStrings,
} from "../../../../../shared/Utils/sort";

const mockDataHeadingColumns = [
  {
    name: "File",
    sort: sortStrings,

    type: "file",
  },
  {
    name: "Program Language",
    sort: sortStrings,

    type: "programLanguage",
  },
  {
    name: "Start Date",
    type: "startDate",
    sort: sortDate,
  },
  {
    name: "Finish Date",
    type: "finishDate",
    sort: sortDate,
  },
  {
    name: "Time",
    type: "time",
    sort: sortNumbers,
  },
  {
    name: "Price",
    type: "price",
    sort: sortNumbers,
  },
];

export const ReportHeadingColumns = () => {
  const renderColumns = () =>
    mockDataHeadingColumns.map((column, index) => (
      <ReportHeadingColumn key={index} {...column} />
    ));

  return renderColumns();
};
