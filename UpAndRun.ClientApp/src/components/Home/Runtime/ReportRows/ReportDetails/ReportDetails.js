import React from "react";
import { CSSTransition } from "react-transition-group";

import classes from "./ReportDetails.module.scss";

const animationTiming = {
  enter: 500,
  exit: 500,
};

export const ReportDetails = ({
  file,
  programLanguage,
  time,
  startDate,
  finishDate,
  price,
  isOpen,
}) => {
  return (
    <CSSTransition
      in={isOpen}
      timeout={animationTiming}
      classNames={{
        enterActive: classes["showAccordion"],
        exitActive: classes["hideAccordion"],
      }}
      mountOnEnter
      unmountOnExit
    >
      <ul className={classes.reportDetails}>
        <li className={classes.item}>
          <h4 className='heading-4'>{file}</h4>
        </li>
        <li className={classes.item}>
          <span>
            You have ran your code with {programLanguage} programming language
          </span>
        </li>
        <li className={classes.item}>
          <span>Code was compiled in {time} seconds</span>
        </li>
        <li className={classes.item}>
          <span>
            Code started running at {startDate.toLocaleString()} and finished at
            {finishDate.toLocaleString()}
          </span>
        </li>
        <li className={classes.item}>
          <span>Cost was: {price} &#36;</span>
        </li>
      </ul>
    </CSSTransition>
  );
};
