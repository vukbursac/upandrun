import React from "react";

import classes from "./ReportRow.module.scss";

import { ReportDetails } from "../ReportDetails/ReportDetails";

export const ReportRow = ({
  file,
  programLanguage,
  startDate,
  finishDate,
  time,
  price,
  selected,
  id,
  toggleAccordions,
}) => {
  const toggleDetails = () => {
    toggleAccordions(id);
  };

  const renderDetails = () => (
    <tr>
      <td colSpan='6'>
        <ReportDetails
          file={file}
          programLanguage={programLanguage}
          time={time}
          startDate={startDate}
          finishDate={finishDate}
          price={price}
          isOpen={selected}
        />
      </td>
    </tr>
  );

  return (
    <>
      <tr className={classes.row} onClick={toggleDetails}>
        <td className={classes.data}>{file}</td>
        <td className={classes.data}>{programLanguage}</td>
        <td className={classes.data}>{startDate.toLocaleString()}</td>
        <td className={classes.data}>{finishDate.toLocaleString()}</td>
        <td className={classes.data}>{time}s</td>
        <td className={classes.data}>{price}</td>
      </tr>
      {renderDetails()}
    </>
  );
};
