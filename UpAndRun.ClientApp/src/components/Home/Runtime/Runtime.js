import { ReportHeadingColumns } from "./ReportRows/ReportHeadingColumns/ReportHeadingColumns";

import { ReportRows } from "./ReportRows/ReportRows";
import classes from "./Runtime.module.scss";

export const Runtime = () => {
  return (
    <div className={classes.runtime}>
      <div className={classes.title}>
        <h3>Runtime Order Reports</h3>
      </div>
      <div className={classes.tableWrapper}>
        <table className={classes.table}>
          <thead>
            <tr className={classes.rowHeading}>
              <ReportHeadingColumns />
            </tr>
          </thead>
          <tbody>
            <ReportRows />
          </tbody>
        </table>
      </div>
    </div>
  );
};
