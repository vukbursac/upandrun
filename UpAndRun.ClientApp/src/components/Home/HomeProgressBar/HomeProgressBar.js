import classes from "./HomeProgressBar.module.scss";

export const HomeProgressBar = () => {
  return (
    <div className={classes.homeProgressBarContainer}>
      <h3 className={classes.title}>Runtime Usage</h3>
      <progress
        className={`${classes.bar} ${classes.runtimeBar}`}
        value='63'
        max='100'
      ></progress>
      <h3 className={classes.title}>Create Usage</h3>
      <progress
        className={`${classes.bar} ${classes.crateBar}`}
        value='34'
        max='100'
      ></progress>
    </div>
  );
};
