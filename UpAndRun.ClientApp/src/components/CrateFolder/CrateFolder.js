import { FilesGrid } from "../FilesGrid/FilesGrid";
import { FileUpload } from "../FileUpload/FileUpload";
import classes from "../CrateFolder/CrateFolder.module.scss";
import { Crates } from "../Crates/Crates";
import { useState } from "react";
import { AiOutlineFolderAdd } from "react-icons/ai";

export const CrateFolder = () => {
  const [showAddCrateForm, setShowAddCrateForm] = useState(false);
  const [showAddFileForm, setShowAddFileForm] = useState(false);

  return (
    <>
      <div className={classes.container}>
        <Crates
          isSubCrates={true}
          showAddCrateForm={showAddCrateForm}
          hideAddCrateForm={() => setShowAddCrateForm(false)}
        />
        <FilesGrid
          isSubCrates={true}
          showAddFileForm={showAddFileForm}
          hideAddFileForm={() => setShowAddFileForm(false)}
        />
        <FileUpload isFullMode={true} />
      </div>

      <div className={classes.btn}>
        <ul className={classes.options}>
          <li className={classes.option}>
            <button
              className={classes.optionBtn}
              onClick={() => setShowAddCrateForm(true)}
              data-testid='crate-folder-add-crate-button'
            >
              Add Crate
            </button>
          </li>
          <li className={classes.option}>
            <button
              className={classes.optionBtn}
              onClick={() => setShowAddFileForm(true)}
              data-testid='crate-folder-add-file-button'
            >
              Add File
            </button>
          </li>
        </ul>
        <AiOutlineFolderAdd className={classes.icon} />
      </div>
    </>
  );
};
