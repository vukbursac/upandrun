import React, { useRef } from "react";
import Navbar from "./Navigation/Navigation";
import classes from "../Landing/Landing.module.scss";
import cloud from "../../shared/images/cloud.png";
import { Hero } from "./Hero/Hero";
import { Services } from "./Services/Services";
import { Pricing } from "./Pricing/Pricing";
import { Footer } from "./Footer/Footer";

export const Landing = () => {
  const servicesRef = useRef();
  const pricingRef = useRef();
  const footerRef = useRef();

  return (
    <>
      <div className={classes.landing}>
        <section className={classes.content}>
          <div className={classes.imageWrapper}>
            <img src={cloud} className={classes.cloud} alt='cloud' />
          </div>
          <div className={classes.main}>
            <Navbar
              servicesRef={servicesRef}
              pricingRef={pricingRef}
              footerRef={footerRef}
            />
            <Hero />
          </div>
        </section>
        <section ref={servicesRef} className={classes.services}>
          <div className={classes.container}>
            <Services />
          </div>
        </section>
        <section ref={pricingRef} className={classes.pricing}>
          <div className={classes.container}>
            <Pricing />
          </div>
        </section>
        <footer ref={footerRef}>
          <div className={classes.container}>
            <Footer />
          </div>
        </footer>
      </div>
    </>
  );
};
