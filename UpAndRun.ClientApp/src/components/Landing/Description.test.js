import React from "react";
import { Router } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import App from "../../App";
import { store } from "../../configuration/redux";
import { Provider } from "react-redux";

const renderWithRouter = (component) => {
  const history = createMemoryHistory();
  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>{component}</Router>
      </Provider>
    ),
  };
};

it("should navigate to the login page", () => {
  const { getByTestId } = renderWithRouter(<App />);
  const loginButtonLandingPage = getByTestId("login-button-landing-page");

  userEvent.click(loginButtonLandingPage);

  expect(screen.getByTestId("login-button-login-page")).toBeInTheDocument();
});

it("should navigate to the register page", () => {
  const { getByTestId } = renderWithRouter(<App />);
  const registerButtonLandingPage = getByTestId("register-button-landing-page");

  userEvent.click(registerButtonLandingPage);

  expect(screen.getByTestId("register-submit-btn")).toBeInTheDocument();
});
