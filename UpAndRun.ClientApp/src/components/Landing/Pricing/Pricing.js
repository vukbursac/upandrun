import classes from "./Pricing.module.scss";

import { AiOutlineCheck } from "react-icons/ai";

export const Pricing = () => {
  return (
    <div className={classes.pricing}>
      <div className={classes.title}>
        <h2 className='heading-2 heading-2--white'>Pricing</h2>
      </div>
      <div className={classes.content}>
        <div className={classes.payment}>
          <div className={classes.subtitle}>
            <h3 className='heading-3'>Free</h3>
          </div>
          <div className={classes.offer}>
            <span className={classes.price}>$0</span>/<span>MO</span>
          </div>
          <ul className={classes.items}>
            <li className={classes.item}>
              <p className={classes.text}>Use of 50GB of storage</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
            <li className={classes.item}>
              <p className={classes.text}>Run your code for 10h.</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
          </ul>
        </div>
        <div className={classes.payment}>
          <div className={classes.subtitle}>
            <h3 className='heading-3 '>Premium</h3>
          </div>
          <div className={classes.offer}>
            <span className={classes.price}>$50</span>/<span>MO</span>
          </div>
          <ul className={classes.items}>
            <li className={classes.item}>
              <p className={classes.text}>Use of 1000GB of storage</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
            <li className={classes.item}>
              <p className={classes.text}>Run your code for 100h.</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
          </ul>
        </div>
        <div className={classes.payment}>
          <div className={classes.subtitle}>
            <h3 className='heading-3 '>Business</h3>
          </div>
          <div className={classes.offer}>
            <span className={classes.price}>$100</span>/<span>MO</span>
          </div>
          <ul className={classes.items}>
            <li className={classes.item}>
              <p className={classes.text}>Unlimited amount of storage</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
            <li className={classes.item}>
              <p className={classes.text}>Unlimited amount of time</p>
              <AiOutlineCheck className={classes.checkIcon} />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
