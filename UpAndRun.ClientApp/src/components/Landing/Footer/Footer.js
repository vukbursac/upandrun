import classes from "./Footer.module.scss";

import { AiOutlineContacts } from "react-icons/ai";
import { GrCircleInformation, GrLocation } from "react-icons/gr";
import { BiBookAlt } from "react-icons/bi";

export const Footer = () => {
  return (
    <div className={classes.footer}>
      <div className={classes.top}>
        <ul className={classes.items}>
          <li className={classes.item}>
            <div className={classes.name}>
              Contact <AiOutlineContacts className={classes.icon} />
            </div>
            <div className={classes.address}>
              <span>Bulevar Vojvode Stepe</span> <GrLocation />
            </div>
          </li>
          <li className={classes.item}>
            <div className={classes.name}>
              About Us <GrCircleInformation className={classes.icon} />
            </div>
          </li>
          <li className={classes.item}>
            <div className={classes.name}>
              Terms and Conditions <BiBookAlt className={classes.icon} />
            </div>
          </li>
        </ul>
      </div>
      <div className={classes.bottom}>
        <span>&copy; 2021 HTEC GROUP ALL RIGHTS RESERVED</span>
      </div>
    </div>
  );
};
