import classes from "./Services.module.scss";

import { GiWoodenCrate } from "react-icons/gi";
import { FiPlay } from "react-icons/fi";

export const Services = () => {
  return (
    <div className={classes.services}>
      <div className={classes.title}>
        <h2 className='heading-2'>Services</h2>
      </div>
      <div className={classes.content}>
        <div className={classes.service}>
          <div className={classes.subtitle}>
            <h3 className='heading-3'>Crates</h3>
          </div>
          <GiWoodenCrate className={classes.icon} />
          <p className={classes.text}>
            Crates allow you to store your files of all types on the Cloud.
            Create folders, subfolders, and keep your data safe and secure.
          </p>
        </div>
        <div className={classes.service}>
          <div className={classes.subtitle}>
            <h3 className='heading-3'>Runtime</h3>
          </div>
          <FiPlay className={classes.icon} />
          <p className={classes.text}>
            Runtime lets you upload and run your code remotely, or use our code
            editor to write and execute code on the spot.
          </p>
        </div>
      </div>
    </div>
  );
};
