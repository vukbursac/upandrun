import React from "react";
import { NavLink } from "react-router-dom";
import classes from "./Hero.module.scss";

export const Hero = () => {
  return (
    <div className={classes.hero}>
      <div className={classes.title}>
        <h1 className='heading-1 heading-1--white'>Up 'n' Run </h1>
        <p className={classes.text}>
          is a cloud-based service offering users the ability to store various
          types of files and run their code remotely.
        </p>
      </div>
      <div data-testid='description-redirect-button' className={classes.links}>
        <NavLink
          data-testid='login-button-landing-page'
          className={`${classes.link} ${classes.linkLogin}`}
          to={{ pathname: "/login" }}
        >
          Login
        </NavLink>
        <span className={classes.quote}>or</span>
        <NavLink
          data-testid='register-button-landing-page'
          className={`${classes.link} ${classes.linkSignUp}`}
          to={{ pathname: "/register" }}
        >
          SignUp
        </NavLink>
      </div>
    </div>
  );
};
