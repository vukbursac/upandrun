import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import logo from "../../../shared/images/logo.png";
import classes from "./Navigation.module.scss";

import { useIsSwitchedToMobile } from "../../../shared/customHooks/isSwitchedToMobile";
import { Modal } from "../../../shared/UI/Modal/Modal";

export default function Navbar({ servicesRef, pricingRef, footerRef }) {
  const links = [
    {
      dataTestId: "navbar-services",
      text: "Services",
      ref: servicesRef,
    },
    {
      dataTestId: "navbar-pricing",
      text: "Pricing",
      ref: pricingRef,
    },
    {
      dataTestId: "navbar-contact",
      text: "Contact",
      ref: footerRef,
    },
  ];

  const isSwitchedToMobile = useIsSwitchedToMobile();
  const [isListVisible, setIsListVisible] = useState(false);

  const toggleList = () => setIsListVisible(!isListVisible);

  useEffect(() => {
    if (isSwitchedToMobile) {
      setIsListVisible(false);
      return;
    }

    setIsListVisible(true);
  }, [isSwitchedToMobile]);

  const renderList = () => {
    if (isListVisible) {
      return <ul className={classes.navList}>{renderItems()}</ul>;
    }
  };

  const renderItems = () =>
    links.map((link, index) => (
      <li key={index} className={classes.navItem}>
        <NavLink
          data-testid={link.dataTestId}
          onClick={() => scrollToView(link.ref)}
          className={`${classes.navLinks}`}
          activeClassName={classes.selected}
          to={{}}
        >
          {link.text}
        </NavLink>
      </li>
    ));

  const scrollToView = (ref) =>
    ref.current.scrollIntoView({
      behavior: "smooth",
      block: "start",
    });

  const getIconClasses = () => {
    if (isListVisible) {
      return `${classes.icon} ${classes.close}`;
    }
    return classes.icon;
  };

  const getButtonClasses = () => {
    if (isListVisible && isSwitchedToMobile) {
      return `${classes.btn} ${classes.mobile}`;
    }

    return classes.btn;
  };

  const isModalOpened = isListVisible && isSwitchedToMobile;

  return (
    <>
      <Modal isOpen={isModalOpened} />
      <div className={classes.navbar}>
        <div className={classes.logoWrapper}>
          <img src={logo} alt='logo' className={classes.logo} />
        </div>
        {renderList()}
        <button className={getButtonClasses()} onClick={toggleList}>
          <span className={getIconClasses()}></span>
        </button>
      </div>
    </>
  );
}
