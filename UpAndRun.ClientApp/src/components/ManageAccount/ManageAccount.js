import userImg from "../../shared/images/user.png";
import classes from "./ManageAccount.module.scss";
import { ManageAccountForm } from "./ManageAccountForm/ManageAccountForm";
import { DeleteAccountForm } from "./DeleteAccountForm/DeleteAccountForm";
import { Route, NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { currentUserSelector } from "../../store/reducers/authenticationReducer";
import { TiUserDelete } from "react-icons/ti";
import { RiUserSettingsLine } from "react-icons/ri";

export const ManageAccount = () => {
  const user = useSelector(currentUserSelector);
  const { firstName, lastName, email } = user;

  return (
    <div className={classes.accountContainer}>
      <div className={classes.accountOptions}>
        <div className={classes.accountInfo}>
          <img
            src={userImg}
            className={classes.manageAccImg}
            alt='manage-account'
          />
          <ul className={classes.accountInfoList}>
            <li className={classes.firstNameAndLastNameInfo}>
              {firstName} {lastName}
            </li>
            <li>{email}</li>
            <li>Subscription: Free</li>
          </ul>
        </div>

        <div className={classes.manageLinks}>
          <NavLink
            className={classes.deleteLink}
            to={"/dashboard/manage-account/delete"}
            data-testid='manage-account-deactivate-link'
          >
            Deactivate
            <TiUserDelete className={classes.deactivateLinkIcon} />
          </NavLink>
          <NavLink
            className={classes.manageLink}
            to={"/dashboard/manage-account/manage"}
            data-testid='manage-account-manage-account-link'
          >
            Manage
            <RiUserSettingsLine className={classes.manageLinkIcon} />
          </NavLink>
        </div>
        <Route
          component={ManageAccountForm}
          path='/dashboard/manage-account/manage'
        />

        <Route
          component={DeleteAccountForm}
          path='/dashboard/manage-account/delete'
        />
      </div>
    </div>
  );
};
