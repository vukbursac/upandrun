import { RiLockPasswordFill } from "react-icons/ri";
import { useState, useEffect } from "react";
import classes from "./ManageAccountForm.module.scss";
import { useSelector, useDispatch } from "react-redux";
import {
  getUserThunk,
  resetManageAccountAction,
} from "../../../store/actions/manageAccountActions";
import {
  manageAccountErrorSelector,
  manageAccountSuccessful,
  userSelector,
} from "../../../store/reducers/manageAccountReducer";
import {
  currentUserSelector,
  isAuthenticatedSelector,
} from "../../../store/reducers/authenticationReducer";
import { validateInput } from "../../Register/validation";
import { validateForm } from "../../Register/validation";
import { updateUserThunk } from "../../../store/actions/manageAccountActions";
import { NavLink } from "react-router-dom";
import { AiFillEye } from "react-icons/ai";

export const ManageAccountForm = () => {
  const dispatch = useDispatch();

  const { id: currentUserId } = useSelector(currentUserSelector);

  // INITIAL STATE //

  const updateUserInitialState = {
    firstName: "",
    lastName: "",
    oldPassword: "",
    password: "",
    confirmPassword: "",
  };

  const passwordInputTypesInitialState = {
    oldPassword: "password",
    password: "password",
    confirmPassword: "password",
  };

  // STATE //

  const [isChangePasswordClicked, setIsChangePasswordClicked] = useState(false);
  const [newUserCredentials, setNewUserCredentials] = useState(
    updateUserInitialState
  );
  const [disableInput, setDisableInput] = useState(true);
  const [errors, setErrors] = useState(newUserCredentials);
  const [showAlert, setShowAlert] = useState(false);
  const [passwordInputTypes, setPasswordInputTypes] = useState(
    passwordInputTypesInitialState
  );

  // PROPS //

  const oldPasswordErrorMessage = useSelector(manageAccountErrorSelector);
  const isAuthenticated = useSelector(isAuthenticatedSelector);
  const user = useSelector(userSelector);

  const updateNewUserCredentials = (user) => {
    setNewUserCredentials((prevUserCredentials) => ({
      ...prevUserCredentials,
      firstName: user.firstName,
      lastName: user.lastName,
    }));
  };

  // USE EFFECT //

  useEffect(() => {
    if (!user) {
      return;
    }

    updateNewUserCredentials(user);
  }, [user]);

  useEffect(() => {
    if (isAuthenticated) {
      dispatch(getUserThunk(currentUserId));
    }
  }, [dispatch, isAuthenticatedSelector, currentUserId]);

  const toggleChangePassword = () =>
    setIsChangePasswordClicked((previousState) => !previousState);

  useEffect(() => {
    if (newUserCredentials.oldPassword) {
      setDisableInput(false);
    } else {
      clearNewPasswordInputsAndErrors();
    }
  }, [newUserCredentials.oldPassword]);

  useEffect(() => {
    if (!isChangePasswordClicked) {
      clearNewPasswordInputsAndErrors();
      setNewUserCredentials({
        ...newUserCredentials,
        oldPassword: "",
      });
    }
  }, [isChangePasswordClicked]);

  const isSuccessfulUpdate = useSelector(manageAccountSuccessful);
  useEffect(() => {
    if (isSuccessfulUpdate) {
      clearAllPasswordInputsAndErrors();
      setShowAlert(true);
    }
  }, [isSuccessfulUpdate]);

  useEffect(() => {
    if (showAlert) {
      alert("User updated");
      setShowAlert(false);
    }
  }, [showAlert]);

  useEffect(() => {
    return () => dispatch(resetManageAccountAction());
  }, []);

  // STYLING FUNCTIONS //

  const togglePasswordInputDisplay = () => {
    if (isChangePasswordClicked) {
      return classes.passwordInputsShow;
    }
    return classes.passwordInputsHide;
  };

  const passwordInputsToggleDisabled = () => {
    if (disableInput) {
      return `${classes.manageUserInput} ${classes.inputDisabled}`;
    }
    return `${classes.manageUserInput}`;
  };

  const getPasswordLabelStyles = () => {
    if (disableInput) {
      return `${classes.label} ${classes.labelDisabled}`;
    }
    return `${classes.label}`;
  };

  const getPasswordInputType = (event, name) => {
    if (event.type === "mousedown") {
      setPasswordInputTypes({ ...passwordInputTypes, [name]: "text" });
    } else if (event.type === "mouseup") {
      setPasswordInputTypes({ ...passwordInputTypes, [name]: "password" });
    }
  };

  const getPasswordEyeIconStyles = () => {
    const oldPasswordInputNotEmpty = newUserCredentials.oldPassword.length > 0;

    if (oldPasswordInputNotEmpty) {
      return `${classes.passwordEye}`;
    }
    return `${classes.passwordEye} ${classes.passwordEyeFadeout}`;
  };

  const getInputBorderStyles = () => {
    return errors.firstName ? classes.inputError : classes.manageUserInput;
  };

  // HANDLE INPUTS AND SUBMIT FORM //

  const handleInputChange = ({
    target: { name: inputName, value: inputValue },
  }) => {
    setNewUserCredentials({
      ...newUserCredentials,
      [inputName]: inputValue,
    });
  };

  const handleInputBlur = (event) => {
    const { name, value } = event.target;

    if (name !== "oldPassword") {
      const validationErrorMessage = validateInput(
        name,
        value,
        newUserCredentials.password
      );

      setErrors({
        ...errors,
        [name]: validationErrorMessage,
      });
    }
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    if (!newUserCredentials.oldPassword) {
      const { firstName, lastName } = newUserCredentials;
      const credentialsWithoutPasswords = {
        firstName,
        lastName,
      };

      errorsCheckOnSubmit(credentialsWithoutPasswords);
    } else {
      errorsCheckOnSubmit(newUserCredentials);
    }
  };

  // HELPER FUNCTIONS //

  const errorsCheckOnSubmit = async (formFields) => {
    const validationErrors = validateForm(formFields);
    const noErrors = Object.values(validationErrors).every(
      (value) => value === ""
    );
    setErrors(validationErrors);

    if (noErrors) {
      dispatch(updateUserThunk(currentUserId, formFields));
    }
  };

  const clearNewPasswordInputsAndErrors = () => {
    setDisableInput(true);
    setErrors({ ...errors, password: "", confirmPassword: "" });
    setNewUserCredentials({
      ...newUserCredentials,
      password: "",
      confirmPassword: "",
    });
  };

  const clearAllPasswordInputsAndErrors = () => {
    setDisableInput(true);
    setErrors({
      ...errors,
      oldPassword: "",
      password: "",
      confirmPassword: "",
    });

    setNewUserCredentials({
      ...newUserCredentials,
      oldPassword: "",
      password: "",
      confirmPassword: "",
    });
  };

  return (
    <form className={classes.form} onSubmit={handleFormSubmit}>
      <div className={classes.formGroup}>
        <label className={classes.label} htmlFor='firstName'>
          First name:
        </label>
        <input
          className={getInputBorderStyles()}
          type='text'
          name='firstName'
          onChange={handleInputChange}
          onBlur={handleInputBlur}
          value={newUserCredentials?.firstName}
          data-testid='manage-account-form-first-name-input'
        />
      </div>

      {errors.firstName && (
        <p
          data-testid='first-name-input-error'
          className={classes.updateUserErrorMsg}
        >
          {errors.firstName}
        </p>
      )}

      <div className={classes.formGroup}>
        <label className={classes.label} htmlFor='lastName'>
          Last name:
        </label>
        <input
          className={
            errors.lastName ? classes.inputError : classes.manageUserInput
          }
          type='text'
          name='lastName'
          onChange={handleInputChange}
          onBlur={handleInputBlur}
          value={newUserCredentials?.lastName}
          data-testid='manage-account-form-last-name-input'
        />
      </div>

      {errors.lastName && (
        <p
          data-testid='last-name-input-error'
          className={classes.updateUserErrorMsg}
        >
          {errors.lastName}
        </p>
      )}
      <div className={classes.passwordWrapper}>
        <p
          className={classes.changePassword}
          onClick={toggleChangePassword}
          data-testid='manage-account-form-change-password-paragraph'
        >
          Change password
          <RiLockPasswordFill className={classes.passwordPadlock} />
        </p>
      </div>

      <div className={togglePasswordInputDisplay()}>
        <div className={classes.formGroup}>
          <label className={classes.label} htmlFor='oldPassword'>
            Old Password:
          </label>
          <input
            className={
              oldPasswordErrorMessage
                ? classes.inputError
                : classes.manageUserInput
            }
            type={passwordInputTypes.oldPassword}
            name='oldPassword'
            onChange={handleInputChange}
            onBlur={handleInputBlur}
            value={newUserCredentials?.oldPassword}
            data-testid='manage-account-form-old-password-input'
          />

          <AiFillEye
            className={getPasswordEyeIconStyles()}
            onMouseDown={(e) => getPasswordInputType(e, "oldPassword")}
            onMouseUp={(e) => getPasswordInputType(e, "oldPassword")}
          />
        </div>

        {oldPasswordErrorMessage && (
          <p
            data-testid='confirm-password-input-error'
            className={classes.updateUserErrorMsg}
          >
            {oldPasswordErrorMessage}
          </p>
        )}

        <div className={classes.formGroup}>
          <label className={getPasswordLabelStyles()} htmlFor='password'>
            New Password:
          </label>
          <input
            className={`${passwordInputsToggleDisabled()} ${
              errors.password ? classes.inputError : classes.manageUserInput
            }`}
            type={passwordInputTypes.password}
            name='password'
            disabled={disableInput}
            onChange={handleInputChange}
            onBlur={handleInputBlur}
            value={newUserCredentials?.password}
            data-testid='manage-account-form-new-password-input'
          />

          <AiFillEye
            className={getPasswordEyeIconStyles()}
            onMouseDown={(e) => getPasswordInputType(e, "password")}
            onMouseUp={(e) => getPasswordInputType(e, "password")}
          />
        </div>
        {errors.password && (
          <p
            data-testid='register-password-input-error'
            className={classes.updateUserErrorMsg}
          >
            {errors.password}
          </p>
        )}

        <div className={classes.formGroup}>
          <label className={getPasswordLabelStyles()} htmlFor='confirmPassword'>
            Confirm New Password:
          </label>
          <input
            className={`${passwordInputsToggleDisabled()} ${
              errors.confirmPassword
                ? classes.inputError
                : classes.manageUserInput
            }`}
            type={passwordInputTypes.confirmPassword}
            name='confirmPassword'
            disabled={disableInput}
            onChange={handleInputChange}
            onBlur={handleInputBlur}
            value={newUserCredentials?.confirmPassword}
            data-testid='manage-account-form-confirm-new-password-input'
          />

          <AiFillEye
            className={getPasswordEyeIconStyles()}
            onMouseDown={(e) => getPasswordInputType(e, "confirmPassword")}
            onMouseUp={(e) => getPasswordInputType(e, "confirmPassword")}
          />
        </div>

        {errors.confirmPassword && (
          <p
            data-testid='confirm-password-input-error'
            className={classes.updateUserErrorMsg}
          >
            {errors.confirmPassword}
          </p>
        )}
      </div>

      <div className={classes.formButtons}>
        <NavLink
          className={classes.cancelBtn}
          to={"/dashboard/manage-account"}
          data-testid='manage-account-cancel-button'
        >
          Cancel
        </NavLink>
        <button
          className={classes.updateBtn}
          data-testid='manage-account-update-button'
        >
          Update
        </button>
      </div>
    </form>
  );
};
