import classes from "./DeleteAccountForm.module.scss";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { currentUserSelector } from "../../../store/reducers/authenticationReducer";
import { RiErrorWarningLine } from "react-icons/ri";
import { deleteUserThunk } from "../../../store/actions/manageAccountActions";
import { NavLink } from "react-router-dom";
import { useCallback } from "react";

export const DeleteAccountForm = () => {
  const dispatch = useDispatch();
  const { id: currentUserId } = useSelector(currentUserSelector);

  const [hardDelete, setHardDelete] = useState(false);
  const [deleteBtnContent, setDeleteBtnContent] = useState();
  const [isDeleteBtnClickedOnce, setIsDeleteBtnClickedOnce] = useState(false);

  const toggleCheckbox = () => {
    setHardDelete((previousState) => !previousState);
  };

  const changeBtnContent = useCallback(() => {
    if (isDeleteBtnClickedOnce) {
      setDeleteBtnContent("Proceed");
      return;
    }

    if (hardDelete) {
      setDeleteBtnContent("Delete");
      return;
    }
    setDeleteBtnContent("Deactivate");
  }, [hardDelete, isDeleteBtnClickedOnce]);

  useEffect(() => {
    changeBtnContent();
  }, [hardDelete, isDeleteBtnClickedOnce, changeBtnContent]);

  const getWarningMessageBeforeDelete = () => {
    if (hardDelete) {
      return "Are you sure you want to delete your account?";
    }
    return "Are you sure you want to deactivate your account?";
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    changeBtnContent();
    setIsDeleteBtnClickedOnce(true);

    if (isDeleteBtnClickedOnce) {
      dispatch(deleteUserThunk(currentUserId, hardDelete));
      if (hardDelete) {
        alert("User deleted");
      } else {
        alert("User deactivated");
      }
      setIsDeleteBtnClickedOnce(false);
    }
  };

  const renderDeleteContainer = () => {
    if (isDeleteBtnClickedOnce) {
      return (
        <div
          className={classes.deleteWarningContainer}
          data-testid='delete-account-form-deactivate-warning'
        >
          <RiErrorWarningLine className={classes.deleteWarningImg} />
          <p className={classes.deleteWarning}>
            {getWarningMessageBeforeDelete()}
          </p>
        </div>
      );
    }

    return null;
  };

  return (
    <form className={classes.deleteAccountForm} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <input
          className={classes.checkbox}
          type='checkbox'
          id='dataCheckbox'
          onChange={toggleCheckbox}
          data-testid='delete-account-form-checkbox'
        />

        <label className={classes.label} htmlFor='dataCheckbox'>
          Permanently delete account
        </label>
      </div>

      <p className={classes.dataWarning}>
        Deactivation can be undone at any time by logging in. <br />
        By checking <b>"Permanently Delete Account"</b>, you agree that all
        related data will be removed within a month. This cannot be reverted.
      </p>
      {renderDeleteContainer()}
      <div className={classes.formButtons}>
        <NavLink
          data-testid='delete-account-form-cancel-button'
          className={classes.cancelBtn}
          to={"/dashboard/manage-account"}
        >
          Cancel
        </NavLink>
        <button
          data-testid='delete-account-form-deactivate-button'
          className={classes.deleteBtn}
        >
          {deleteBtnContent}
        </button>
      </div>
    </form>
  );
};
