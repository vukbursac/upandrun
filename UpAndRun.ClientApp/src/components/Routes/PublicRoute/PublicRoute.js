import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  isAuthenticatedSelector,
  isSessionResolvedSelector,
} from "../../../store/reducers/authenticationReducer";

export const PublicRoute = ({ component: Component, restricted, ...rest }) => {
  const isAuthenticated = useSelector(isAuthenticatedSelector);
  const isSessionResolved = useSelector(isSessionResolvedSelector);

  return (
    <Route
      {...rest}
      render={(props) =>
        isSessionResolved ? (
          isAuthenticated && restricted ? (
            <>
              <Redirect to={`/dashboard/home`} />
            </>
          ) : (
            <Component {...props} />
          )
        ) : null
      }
    />
  );
};
