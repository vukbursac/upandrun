import React from "react";
import { Route, Redirect, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  isAuthenticatedSelector,
  isSessionResolvedSelector,
} from "../../../store/reducers/authenticationReducer";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const isAuthenticated = useSelector(isAuthenticatedSelector);
  const isSessionResolved = useSelector(isSessionResolvedSelector);
  const location = useLocation();
  return (
    <Route
      {...rest}
      render={(props) =>
        isSessionResolved ? (
          isAuthenticated ? (
            <Component {...props} />
          ) : (
            <Redirect to={{ pathname: "/login", state: location }} />
          )
        ) : null
      }
    />
  );
};
