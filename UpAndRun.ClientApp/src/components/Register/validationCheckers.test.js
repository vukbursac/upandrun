import { isFirstCharNumber } from "./validationCheckers";

describe("isFirstCharNumber", () => {
  it("Should return true if first character is number", () => {
    const mockString = "1ABC";

    expect(isFirstCharNumber(mockString)).toEqual(true);
  });

  it("Should return false if first character is not a number", () => {
    const mockString = "ABC1";

    expect(isFirstCharNumber(mockString)).toEqual(false);
  });
});
