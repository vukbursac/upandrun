function hasWhiteSpace(input) {
  return /\s/g.test(input);
}

function hasNumber(string) {
  return /\d/.test(string);
}

function noUpperCase(string) {
  return string === string.toLowerCase();
}

function hasAllUpperCase(string) {
  return string === string.toUpperCase();
}

function isCapitalized(string) {
  return string[0] === string[0].toUpperCase();
}

function containsOnlyLetters(string) {
  const onlyLetterRegex = "^[a-zA-Z]+$";

  return string.match(onlyLetterRegex);
}

function isFirstCharNumber(string) {
  return !isNaN(string[0]);
}

function isFirstCharSpecialChar(string) {
  const format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  return format.test(string[0]);
}

function isStringLongerThenFiftyChar(string) {
  return string.length > 50;
}

function isStringLongerThenThirtyTwoChar(string) {
  return string.length > 32;
}

export {
  hasWhiteSpace,
  hasNumber,
  noUpperCase,
  hasAllUpperCase,
  isCapitalized,
  containsOnlyLetters,
  isFirstCharNumber,
  isFirstCharSpecialChar,
  isStringLongerThenFiftyChar,
  isStringLongerThenThirtyTwoChar,
};
