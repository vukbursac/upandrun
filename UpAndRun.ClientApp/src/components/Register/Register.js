import { NavLink } from "react-router-dom";
import classes from "../Register/Register.module.scss";
import Logo from "../../shared/images/logo.png";
import googleLogo from "../../shared/images/googleLogo.png";
import { AiFillEye } from "react-icons/ai";
import { validateInput } from "./validation";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  isAuthenticationSuccessfulSelector,
  authenticationErrorSelector,
  loadingAuthenticationSelector,
} from "../../store/reducers/authenticationReducer";

import {
  clearErrorsAction,
  registerRedirect,
  resetAuthentication,
} from "../../store/actions/authenticationActions";
import { validateForm } from "./validation";
import { registerUser } from "../../store/actions/authenticationActions";
import Spinner from "../../shared/UI/Spinner/Spinner";

const registerFormFields = {
  firstName: "",
  lastName: "",
  username: "",
  email: "",
  password: "",
  confirmPassword: "",
};

export const Register = () => {
  const dispatch = useDispatch();
  const registrationSuccess = useSelector(isAuthenticationSuccessfulSelector);
  const errorMessage = useSelector(authenticationErrorSelector);
  const isLoadingAuthentication = useSelector(loadingAuthenticationSelector);
  const history = useHistory();

  const [credentials, setCredentials] = useState(registerFormFields);
  const [errors, setErrors] = useState(registerFormFields);

  useEffect(() => {
    dispatch(resetAuthentication());
  }, [dispatch]);

  useEffect(() => {
    if (registrationSuccess) {
      alert("Registration successful, please confirm your email before login");
      history.push("/login");
      dispatch(registerRedirect());
    }
  }, [registrationSuccess, dispatch, history]);

  const handleInputValue = (event) => {
    setCredentials({
      ...credentials,
      [event.target.name]: event.target.value,
    });

    if (errorMessage) {
      dispatch(clearErrorsAction());
    }
  };

  const handleInputBlur = (event) => {
    const { name, value } = event.target;
    const validationErrorMessage = validateInput(
      name,
      value,
      credentials.password
    );

    setErrors({
      ...errors,
      [name]: validationErrorMessage,
    });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const validationErrors = validateForm(credentials);
    const noErrors = Object.values(validationErrors).every((x) => x === "");
    setErrors(validationErrors);

    if (noErrors) {
      dispatch(registerUser(credentials));
    }
  };

  const handleRedirectToLandingPage = (e) => {
    e.preventDefault();
    history.push("/");
  };
  const renderSubmitButton = () => {
    if (isLoadingAuthentication) {
      return <></>;
    }

    return (
      <button
        type='submit'
        data-testid='register-submit-btn'
        className={`${classes.btn} ${classes.btnSignUp}`}
      >
        Sign up
      </button>
    );
  };
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const togglePasswordInputType = () => setIsPasswordShown(!isPasswordShown);

  return (
    <div className={classes.container}>
      <form className={classes.form} onSubmit={handleFormSubmit}>
        <h1 className='heading-1'>Up 'n' Run</h1>
        <img
          data-testid='logo-image-register-page'
          onClick={handleRedirectToLandingPage}
          className={classes.logo}
          src={Logo}
          alt='Logo'
        />
        <div className={classes.formContainer}>
          <div className={classes.formGroup}>
            <input
              type='text'
              name='firstName'
              data-testid='register-first-name'
              className={errors.firstName ? classes.inputError : classes.input}
              placeholder='First Name'
              value={credentials.firstName}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
          </div>
          {errors.firstName && (
            <p data-testid='first-name-input-error' className={classes.error}>
              {errors.firstName}
            </p>
          )}

          <div className={classes.formGroup}>
            <input
              type='text'
              name='lastName'
              data-testid='register-last-name'
              className={errors.lastName ? classes.inputError : classes.input}
              placeholder='Last Name'
              value={credentials.lastName}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
          </div>
          {errors.lastName && (
            <p data-testid='last-name-input-error' className={classes.error}>
              {errors.lastName}
            </p>
          )}

          <div className={classes.formGroup}>
            <input
              type='text'
              name='username'
              data-testid='register-username'
              className={errors.username ? classes.inputError : classes.input}
              placeholder='Username'
              value={credentials.username}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
          </div>
          {errors.username && (
            <p data-testid='username-input-error' className={classes.error}>
              {errors.username}
            </p>
          )}

          <div className={classes.formGroup}>
            <input
              type='email'
              name='email'
              data-testid='register-email'
              className={errors.email ? classes.inputError : classes.input}
              placeholder='Email'
              value={credentials.email}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
          </div>
          {errors.email && (
            <p
              data-testid='register-email-input-error'
              className={classes.error}
            >
              {errors.email}
            </p>
          )}

          <div className={classes.formGroup}>
            <input
              type={isPasswordShown ? "text" : "password"}
              name='password'
              data-testid='register-password'
              className={errors.password ? classes.inputError : classes.input}
              placeholder='Password'
              value={credentials.password}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
            <AiFillEye
              className={classes.passwordIcon}
              onClick={togglePasswordInputType}
            />
          </div>
          {errors.password && (
            <p
              data-testid='register-password-input-error'
              className={classes.error}
            >
              {errors.password}
            </p>
          )}

          <div className={classes.formGroup}>
            <input
              type='password'
              name='confirmPassword'
              data-testid='register-confirm-password'
              className={
                errors.confirmPassword ? classes.inputError : classes.input
              }
              placeholder='Confirm Your Password'
              value={credentials.confirmPassword}
              onChange={handleInputValue}
              onBlur={handleInputBlur}
            />
          </div>
          {errors.confirmPassword && (
            <p
              data-testid='confirm-password-input-error'
              className={classes.error}
            >
              {errors.confirmPassword}
            </p>
          )}
          <div
            data-testid='register-backend-input-error'
            className={classes.error}
          >
            {errorMessage}
          </div>

          <Spinner isLoading={isLoadingAuthentication} size='sm' />
          {renderSubmitButton()}

          <div className={classes.spanOr}>OR</div>
          <div className={classes.googleBtnWrapper}>
            <button
              type='button'
              className={`${classes.btn} ${classes.btnGoogle}`}
            >
              <img
                className={classes.googleLogo}
                src={googleLogo}
                alt='google-logo'
              />
              Sign up with Google
            </button>
          </div>
          <span className={classes.login}>
            Already on Up 'n' Run ?
            <NavLink
              to={{ pathname: "/login" }}
              className={classes.logInLink}
              data-testid='login-link-register-page'
            >
              Log in
            </NavLink>
          </span>
        </div>
      </form>
    </div>
  );
};
