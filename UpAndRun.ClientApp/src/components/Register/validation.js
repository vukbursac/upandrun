import * as validation from "./validationCheckers";

const validateFirstName = (value) => {
  if (
    !value ||
    !validation.isCapitalized(value) ||
    validation.isFirstCharNumber(value) ||
    validation.isFirstCharSpecialChar(value)
  ) {
    return "First name must start with an uppercase letter";
  } else if (validation.isStringLongerThenFiftyChar(value)) {
    return "First name cannot be longer then 50 characters";
  } else {
    return "";
  }
};

const validateLastName = (value) => {
  if (
    !value ||
    !validation.isCapitalized(value) ||
    validation.isFirstCharNumber(value) ||
    validation.isFirstCharSpecialChar(value)
  ) {
    return "Last name must start with an uppercase letter";
  } else if (validation.isStringLongerThenFiftyChar(value)) {
    return "Last name cannot be longer then 50 characters";
  } else {
    return "";
  }
};

const validateUsername = (value) => {
  if (!value || !/^([a-zA-Z0-9 _-]+)$/.test(value)) {
    return "Username must contain only letters, numbers, and special characters (only: _ and -) with no white space";
  } else if (validation.isStringLongerThenThirtyTwoChar(value)) {
    return "Username cannot be longer then 32 characters";
  } else {
    return "";
  }
};

const validateEmail = (value) => {
  if (!value) {
    return "Email is required";
  } else if (!/\S+@\S+\.\S+/.test(value)) {
    return "Email is invalid";
  } else {
    return "";
  }
};

const validatePassword = (value) => {
  if (
    !value ||
    value.length <= 5 ||
    validation.noUpperCase(value) ||
    validation.hasAllUpperCase(value) ||
    !validation.hasNumber(value)
  ) {
    return "Enter a combination of at least six characters, with at least one uppercase, one lowercase, and one number";
  } else {
    return "";
  }
};

const validateConfirmPassword = (value, password) => {
  if (!value) {
    return "Confirm password is required";
  } else if (value !== password) {
    return "Passwords do not match";
  } else {
    return "";
  }
};

export const validateInput = (name, value, password) => {
  switch (name) {
    case "firstName":
      return validateFirstName(value);
    case "lastName":
      return validateLastName(value);
    case "username":
      return validateUsername(value);
    case "email":
      return validateEmail(value);
    case "password":
      return validatePassword(value);
    case "confirmPassword":
      return validateConfirmPassword(value, password);
    default:
      return "";
  }
};

export const validateForm = (credentials) => {
  const registrationFormErrors = Object.keys(credentials).reduce((acc, cur) => {
    acc[cur] = validateInput(cur, credentials[cur], credentials.password);
    return acc;
  }, {});
  return registrationFormErrors;
};
