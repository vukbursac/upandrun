import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { authenticationReducer } from "../store/reducers/authenticationReducer";
import { runtimeReducer } from "../store/reducers/runtimeReducer";
import thunk from "redux-thunk";
import { crateReducer } from "../store/reducers/crateReducer";
import { manageAccountReducer } from "../store/reducers/manageAccountReducer";
export const rootReducer = combineReducers({
  auth: authenticationReducer,
  runtime: runtimeReducer,
  crate: crateReducer,
  manageAccount: manageAccountReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);
