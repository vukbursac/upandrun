import * as actionTypes from "./actionTypes/runtimeActionTypes";

import { getLanguages, getOutput } from "../../api/endpoints/runtimeApi";

export const setSelectedProgrammingLanguageAction = (language) => ({
  type: actionTypes.SET_SELECTED_PROGRAMMING_LANGUAGE,
  payload: language,
});

const fetchProgrammingLanguagesAction = (languages) => ({
  type: actionTypes.FETCH_PROGRAMMING_LANGUAGES,
  payload: languages,
});

const fetchProgrammingLanguagesFailedAction = (error) => ({
  type: actionTypes.FETCH_PROGRAMMING_LANGUAGES_FAILED,
  payload: error,
});

const fetchCompilerStartAction = () => ({
  type: actionTypes.FETCH_COMPILER_OUTPUT_START,
});

const fetchCompilerOutputSuccessfulAction = (output) => ({
  type: actionTypes.FETCH_COMPILER_OUTPUT_SUCCESSFUL,
  payload: output,
});

const fetchCompilerOutputFailedAction = (error) => ({
  type: actionTypes.FETCH_COMPILER_OUTPUT_FAILED,
  payload: error,
});

export const resetOutputAction = () => ({
  type: actionTypes.RESET_COMPILER_OUTPUT,
});

export const fetchRecentRanFilesAction = (files) => ({
  type: actionTypes.FETCH_RECENT_RAN_FILES,
  payload: files,
});

export const sortRecentRanFilesAction = (files) => ({
  type: actionTypes.SORT_RECENT_RAN_FILES,
  payload: files,
});

export const fetchRecentRanFilesThunk = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchRecentRanFilesAction([]));
    } catch (error) {
      dispatch(fetchRecentRanFilesAction([]));
    }
  };
};

export const sortRecentRanFilesThunk = (files) => {
  return async (dispatch) => {
    try {
      dispatch(sortRecentRanFilesAction(files));
    } catch (error) {
      dispatch(sortRecentRanFilesAction(files));
    }
  };
};

export const fetchProgrammingLanguagesThunk = () => {
  return async (dispatch) => {
    try {
      const languages = await getLanguages();

      dispatch(fetchProgrammingLanguagesAction(languages));
    } catch (error) {
      dispatch(fetchProgrammingLanguagesFailedAction(error));
    }
  };
};

export const fetchCompilerOutputThunk = (code, languageId) => {
  return async (dispatch) => {
    dispatch(fetchCompilerStartAction());

    try {
      const output = await getOutput(code, languageId);

      dispatch(fetchCompilerOutputSuccessfulAction(output));
    } catch (error) {
      dispatch(fetchCompilerOutputFailedAction(error));
    }
  };
};
