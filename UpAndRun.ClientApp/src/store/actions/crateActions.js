import * as actionTypes from "./actionTypes/crateActionTypes";
import {
  fetchFilesByCrateId,
  addFileApi,
  updateFileApi,
  deleteFileApi,
  moveFileIntoCrate,
  downloadFileApi,
} from "../../api/endpoints/fileApi";
import {
  fetchRootCrates,
  addCrateApi,
  renameCrateApi,
  deleteCrateApi,
  fetchCratesByCrateId,
  changeParentCrateIdApi,
} from "../../api/endpoints/crateApi";

// FILES ACTION CREATORS //

export const fetchFilesStartAction = () => ({
  type: actionTypes.FETCH_FILES_START,
});

export const fetchFilesSuccessAction = (entities) => ({
  type: actionTypes.FETCH_FILES_SUCCESSFUL,
  payload: entities,
});

export const fetchFilesFailedAction = (error) => ({
  type: actionTypes.FETCH_FILES_FAILED,
  payload: error,
});

export const addFileSuccessAction = (files) => ({
  type: actionTypes.ADD_FILE_SUCCESSFUL,
  payload: files,
});

export const addFileFailedAction = (error) => ({
  type: actionTypes.ADD_FILE_FAILED,
  payload: error,
});

export const updateFileSuccessAction = (file) => ({
  type: actionTypes.UPDATE_FILE_SUCCESSFUL,
  payload: file,
});

export const updateFileFailedAction = (error) => ({
  type: actionTypes.UPDATE_FILE_FAILED,
  payload: error,
});

export const deleteFileSuccessAction = (file) => ({
  type: actionTypes.DELETE_FILE_SUCCESSFUL,
  payload: file,
});

export const deleteFileFailedAction = (error) => ({
  type: actionTypes.DELETE_FILE_FAILED,
  payload: error,
});

export const downloadFileFailed = (error) => ({
  type: actionTypes.DOWNLOAD_FILE_FAILED,
  payload: error,
});

// CRATES ACTION CREATORS //

export const fetchCratesStartAction = () => ({
  type: actionTypes.FETCH_CRATES_START,
});

const fetchCratesSuccessfulAction = (crates) => ({
  type: actionTypes.FETCH_CRATES_SUCCESSFUL,
  payload: crates,
});

const fetchCratesFailedAction = (error) => ({
  type: actionTypes.FETCH_CRATES_FAILED,
  payload: error,
});

export const addCrateSuccessfulAction = (crate) => ({
  type: actionTypes.ADD_CRATE_SUCCESSFUL,
  payload: crate,
});

export const addCrateFailedAction = (error) => ({
  type: actionTypes.ADD_CRATE_FAILED,
  payload: error,
});

export const deleteCrateSuccessfulAction = (id) => ({
  type: actionTypes.DELETE_CRATE_SUCCESSFUL,
  payload: id,
});

export const deleteCrateFailedAction = (error) => ({
  type: actionTypes.DELETE_CRATE_FAILED,
  payload: error,
});
const renameCrateSuccessfulAction = (crate) => ({
  type: actionTypes.RENAME_CRATE_SUCCESSFUL,
  payload: crate,
});

const renameCrateFailedAction = (error) => ({
  type: actionTypes.RENAME_CRATE_FAILED,
  payload: error,
});

export const fetchCratesByIdThunk = (parentCrateId) => {
  return async (dispatch) => {
    dispatch(fetchCratesStartAction());

    try {
      let crates = [];
      if (parentCrateId) {
        crates = await fetchCratesByCrateId(parentCrateId);
      } else {
        crates = await fetchRootCrates();
      }
      dispatch(fetchCratesSuccessfulAction(crates));
    } catch (error) {
      dispatch(fetchCratesFailedAction(error));
    }
  };
};

export const addCrateThunk = (name, parentCrateId = null) => {
  const crateThunkData = { name, parentCrateId };

  return async (dispatch) => {
    try {
      const crates = await addCrateApi(crateThunkData);
      dispatch(addCrateSuccessfulAction(crates));
    } catch (error) {
      dispatch(addCrateFailedAction(error.message));
    }
  };
};

export const deleteCrateThunk = (id, parentCrateId) => {
  return async (dispatch) => {
    try {
      await deleteCrateApi(id, parentCrateId);
      dispatch(deleteCrateSuccessfulAction(id));
    } catch (error) {
      dispatch(deleteCrateFailedAction(error));
    }
  };
};

export const updateCrateThunk = (name, id, parentCrateId) => {
  return async (dispatch) => {
    try {
      const crate = await renameCrateApi(name, id, parentCrateId);
      dispatch(renameCrateSuccessfulAction(crate));
    } catch (error) {
      dispatch(renameCrateFailedAction(error.message));
    }
  };
};

export const moveCrateIntoCrateThunk = (id, parentCrateId) => {
  return async (dispatch) => {
    try {
      await changeParentCrateIdApi(id, parentCrateId);
      dispatch(deleteCrateSuccessfulAction(id));
    } catch (error) {}
  };
};

export const moveFileIntoCrateThunk = (id, parentCrateId) => {
  return async (dispatch) => {
    try {
      await moveFileIntoCrate(id, parentCrateId);
      dispatch(deleteFileSuccessAction(id));
    } catch (error) {}
  };
};

// FILES THUNKS //

export const fetchFilesByIdThunk = (parentCrateId) => {
  return async (dispatch) => {
    dispatch(fetchFilesStartAction());

    try {
      let files = [];
      if (parentCrateId) {
        files = await fetchFilesByCrateId(parentCrateId);
      }
      dispatch(fetchFilesSuccessAction(files));
    } catch (error) {
      dispatch(fetchFilesFailedAction(error));
    }
  };
};

export const addFileThunk = (file, parentCrateId) => {
  return async (dispatch) => {
    try {
      const addedFile = await addFileApi(file, parentCrateId);
      dispatch(addFileSuccessAction(addedFile));
    } catch (error) {
      dispatch(addFileFailedAction(error));
    }
  };
};

export const updateFileThunk = (fileName, fileId, parentCrateId) => {
  return async (dispatch) => {
    try {
      const file = await updateFileApi(fileName, fileId, parentCrateId);
      dispatch(updateFileSuccessAction(file));
    } catch (error) {
      dispatch(updateFileFailedAction(error));
    }
  };
};

export const deleteFileThunk = (id, parentCrateId) => {
  return async (dispatch) => {
    try {
      await deleteFileApi(id, parentCrateId);
      dispatch(deleteFileSuccessAction(id));
    } catch (error) {
      dispatch(deleteFileFailedAction(error));
    }
  };
};

export const downloadFileThunk = (fileId) => {
  return async (dispatch) => {
    try {
      await downloadFileApi(fileId);
    } catch (error) {
      dispatch(downloadFileFailed(error));
    }
  };
};
