import * as actionTypes from "./actionTypes/authenticationActionTypes";

import { login, register } from "../../api/endpoints/userApi";

export const updateUserAction = (user) => ({
  type: actionTypes.UPDATE_USER,
  payload: user,
});

export const clearErrorsAction = () => ({
  type: actionTypes.CLEAR_ERROR,
});

const loginStart = () => ({
  type: actionTypes.LOGIN_START,
});

const registerStart = () => ({
  type: actionTypes.REGISTRATION_START,
});

const loginSuccess = (authData) => ({
  type: actionTypes.LOGIN_SUCCESS,
  payload: authData,
});

const registerSuccess = (authData) => ({
  type: actionTypes.REGISTRATION_SUCCESS,
  payload: authData,
});

const loginFail = (error) => ({
  type: actionTypes.LOGIN_FAIL,
  payload: error,
});

const registerFail = (error) => ({
  type: actionTypes.REGISTRATION_FAIL,
  payload: error,
});

export const registerRedirect = () => {
  return {
    type: actionTypes.REGISTRATION_REDIRECT,
  };
};

export const resetAuthentication = () => ({
  type: actionTypes.RESET_AUTHENTICATION,
});

export const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("user");

  return {
    type: actionTypes.LOGOUT,
  };
};

export const authCheckState = () => {
  return (dispatch) => {
    const token = localStorage.getItem("token");
    const user = JSON.parse(localStorage.getItem("user"));

    if (!token) {
      dispatch(logout());
      return;
    }

    dispatch(
      loginSuccess({
        token,
        user,
        message: null,
      })
    );
  };
};

export const loginUser = (userCredentials) => {
  return async (dispatch) => {
    dispatch(loginStart());

    try {
      const authData = await login(userCredentials);

      localStorage.setItem("token", authData.token);
      localStorage.setItem("user", JSON.stringify(authData.user));

      dispatch(loginSuccess(authData));
    } catch (error) {
      dispatch(loginFail(error.message));
    }
  };
};

export const registerUser = (userCredentials) => {
  return async (dispatch) => {
    dispatch(registerStart());

    try {
      const authData = await register(userCredentials);

      dispatch(registerSuccess(authData));
    } catch (error) {
      dispatch(registerFail(error.message));
    }
  };
};
