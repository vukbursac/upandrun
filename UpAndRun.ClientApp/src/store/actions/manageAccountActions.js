import * as actionTypes from "./actionTypes/manageAccountActionTypes";
import { getUser, deleteUser, updateUser } from "../../api/endpoints/userApi";
import { logout, updateUserAction } from "./authenticationActions";

export const resetManageAccountAction = () => ({
  type: actionTypes.RESET_MANAGE_ACCOUNT,
});

export const getUserSuccessfulAction = (user) => ({
  type: actionTypes.GET_USER_SUCCESS,
  payload: user,
});

export const getUserFailedAction = (error) => ({
  type: actionTypes.GET_USER_FAILED,
  payload: error,
});

export const deleteUserSuccesfulAction = (user) => ({
  type: actionTypes.DELETE_USER_SUCCESS,
  payload: user,
});

export const deleteUserFailedAction = (error) => ({
  type: actionTypes.DELETE_USER_FAILED,
  payload: error,
});

export const updateUserSuccesfulAction = (user) => ({
  type: actionTypes.UPDATE_USER_SUCCESS,
  payload: user,
});

export const updateUserFailedAction = (error) => ({
  type: actionTypes.UPDATE_USER_FAILED,
  payload: error,
});

export const updateUserFinishedAction = () => ({
  type: actionTypes.UPDATE_USER_FINISH,
});

// THUNKS //

export const getUserThunk = (id) => {
  return async (dispatch) => {
    try {
      const user = await getUser(id);

      dispatch(getUserSuccessfulAction(user));
    } catch (error) {
      dispatch(getUserFailedAction(error));
    }
  };
};

export const deleteUserThunk = (id, hardReset) => {
  return async (dispatch) => {
    try {
      const user = await deleteUser(id, hardReset);
      dispatch(deleteUserSuccesfulAction(user));
      dispatch(logout());
    } catch (error) {
      dispatch(deleteUserFailedAction(error));
    }
  };
};

export const updateUserThunk = (id, userCredentials) => {
  return async (dispatch) => {
    try {
      const user = await updateUser(id, userCredentials);
      localStorage.setItem("user", JSON.stringify(user));
      dispatch(updateUserSuccesfulAction(user));
      dispatch(updateUserAction(user));
      dispatch(updateUserFinishedAction());
    } catch (error) {
      dispatch(updateUserFailedAction(error.message));
    }
  };
};
