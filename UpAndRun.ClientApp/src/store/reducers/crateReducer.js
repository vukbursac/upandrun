import * as actionTypes from "../actions/actionTypes/crateActionTypes";

const initialState = {
  entities: [],
  crates: [],
  loading: false,
  error: null,
};

export const updateCrates = (state, updatedCrate) => {
  const crates = [...state.crates];
  const crateIndex = crates.findIndex(({ id }) => id === updatedCrate.id);
  const updatedCrateCopy = { ...crates[crateIndex] };
  updatedCrateCopy.name = updatedCrate.name;
  crates[crateIndex] = updatedCrateCopy;
  return crates;
};

export const deleteCrates = (state, id) => {
  const crates = [...state.crates];
  const deleteCratesIndex = crates.findIndex((crate) => crate.id === id);
  crates.splice(deleteCratesIndex, 1);
  return crates;
};

export const deleteFiles = (state, id) => {
  const files = [...state.entities];
  const deleteFilesIndex = files.findIndex((file) => file.id === id);
  files.splice(deleteFilesIndex, 1);
  return files;
};

export const updateFiles = (state, updatedFile) => {
  const files = [...state.entities];
  const fileIndex = files.findIndex(({ id }) => id === updatedFile.fileId);
  const updatedFileCopy = { ...files[fileIndex] };
  updatedFileCopy.fileName = updatedFile.fileName;
  files[fileIndex] = updatedFileCopy;
  return files;
};

export const crateReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_FILES_START:
      return { ...state, loading: true };
    case actionTypes.FETCH_FILES_SUCCESSFUL:
      return { ...state, entities: action.payload, loading: false };
    case actionTypes.FETCH_FILES_FAILED:
      return { ...state, error: true, loading: false };
    case actionTypes.ADD_FILE_START:
      return { ...state, loading: true };
    case actionTypes.ADD_FILE_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        entities: [...state.entities, action.payload],
      };
    case actionTypes.ADD_FILE_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.UPDATE_FILE_START:
      return { ...state, loading: true };
    case actionTypes.UPDATE_FILE_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        entities: updateFiles(state, action.payload),
      };
    case actionTypes.UPDATE_FILE_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.DELETE_FILE_START:
      return { ...state, loading: true };

    case actionTypes.DELETE_FILE_SUCCESSFUL:
      return {
        ...state,
        loading: false,
        entities: deleteFiles(state, action.payload),
      };

    case actionTypes.DELETE_FILE_FAILED:
      return { ...state, loading: false, error: action.payload };

    // CRATES CASES //
    case actionTypes.FETCH_CRATES_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        crates: action.payload,
      };
    case actionTypes.FETCH_CRATES_START:
      return { ...state, loading: true };
    case actionTypes.FETCH_CRATES_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.ADD_CRATE_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        crates: [...state.crates, action.payload],
      };
    case actionTypes.ADD_CRATE_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.DELETE_CRATE_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        crates: deleteCrates(state, action.payload),
      };
    case actionTypes.DELETE_CRATE_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.RENAME_CRATE_SUCCESSFUL:
      return {
        ...state,
        error: null,
        loading: false,
        crates: updateCrates(state, action.payload),
      };
    case actionTypes.RENAME_CRATE_FAILED:
      return { ...state, error: action.payload, loading: false };
    default:
      return state;
  }
};

export const filesSelector = (state) => state.crate.entities;
export const cratesSelector = (state) => state.crate.crates;
export const cratesLoadingSelector = (state) => state.crate.loading;
export const cratesErrorSelector = (state) => state.crate.error;
