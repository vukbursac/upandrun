import * as actionTypes from "../actions/actionTypes/manageAccountActionTypes";

const initialState = {
  user: null,
  loading: false,
  error: null,
  isSuccessful: false,
};

export const manageAccountReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_USER_START:
      return { ...state, loading: true };
    case actionTypes.GET_USER_SUCCESS:
      return { ...state, user: action.payload, loading: false };
    case actionTypes.GET_USER_FAILED:
      return { ...state, error: action.payload, loading: false };
    case actionTypes.DELETE_USER_START:
      return { ...state, loading: true };
    case actionTypes.DELETE_USER_SUCCESS:
      return { ...state, error: null };
    case actionTypes.DELETE_USER_FAILED:
      return { ...state, error: action.payload };
    case actionTypes.UPDATE_USER_START:
      return { ...state, loading: true };
    case actionTypes.UPDATE_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        isSuccessful: true,
        loading: false,
        error: null,
      };
    case actionTypes.UPDATE_USER_FINISH:
      return { ...state, isSuccessful: false };
    case actionTypes.UPDATE_USER_FAILED:
      return { ...state, error: action.payload, isSuccessful: false };
    case actionTypes.RESET_MANAGE_ACCOUNT:
      return { ...initialState };
    default:
      return state;
  }
};

export const userSelector = (state) => state.manageAccount.user;
export const manageAccountErrorSelector = (state) => state.manageAccount.error;
export const manageAccountSuccessful = (state) =>
  state.manageAccount.isSuccessful;
