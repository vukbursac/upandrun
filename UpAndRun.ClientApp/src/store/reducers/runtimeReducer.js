import * as actionTypes from "../actions/actionTypes/runtimeActionTypes";

const getDate = (days) => {
  const date = new Date();
  date.setDate(date.getDate() + days);
  return date;
};

export const initialState = {
  languages: [],
  selectedLanguage: null,
  output: null,
  loading: false,
  error: null,
  recentRanFiles: [
    {
      id: 1,
      file: "Person.cs",
      programLanguage: "C#",
      startDate: getDate(1),
      finishDate: getDate(2),
      time: new Date().getMinutes() + 1,
      price: 10,
    },
    {
      id: 2,
      file: "Car.cs",
      programLanguage: "Javascript",
      startDate: getDate(2),
      finishDate: getDate(3),
      time: new Date().getMinutes(),
      price: 8,
    },
    {
      id: 3,
      file: "Name.js",
      programLanguage: "Java",
      startDate: getDate(3),
      finishDate: getDate(4),
      time: new Date().getMinutes() + 2,
      price: 6,
    },
  ],
};

export const runtimeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_COMPILER_OUTPUT_START:
      return { ...state, loading: true };
    case actionTypes.FETCH_COMPILER_OUTPUT_SUCCESSFUL:
      return { ...state, loading: false, output: action.payload };
    case actionTypes.FETCH_COMPILER_OUTPUT_FAILED:
      return { ...state, loading: false, error: action.payload };
    case actionTypes.RESET_COMPILER_OUTPUT:
      return { ...state, output: null };
    case actionTypes.FETCH_PROGRAMMING_LANGUAGES:
      return { ...state, languages: [...action.payload] };
    case actionTypes.FETCH_PROGRAMMING_LANGUAGES_FAILED:
      return { ...state, error: action.payload };
    case actionTypes.SET_SELECTED_PROGRAMMING_LANGUAGE:
      return { ...state, selectedLanguage: action.payload };
    case actionTypes.FETCH_RECENT_RAN_FILES:
      return { ...state };
    case actionTypes.SORT_RECENT_RAN_FILES:
      return {
        ...state,
        recentRanFiles: action.payload,
      };
    default:
      return state;
  }
};

export const programmingLanguagesSelector = (state) => state.runtime.languages;
export const compilerOutputSelector = (state) => state.runtime.output;
export const loadingCompilerOutputSelector = (state) => state.runtime.loading;
export const selectedProgrammingLanguageSelector = (state) =>
  state.runtime.selectedLanguage;
export const recentRanFilesSelector = (state) => state.runtime.recentRanFiles;
