import * as actionTypes from "../actions/actionTypes/authenticationActionTypes";

import { updateObject } from "./utility";

const initialState = {
  token: null,
  isSessionResolved: false,
  user: null,
  loading: false,
  error: null,
  success: false,
  reactivateMessage: null,
};

const loginStart = (state, action) =>
  updateObject(state, { loading: true, error: null });

const registerStart = (state, action) =>
  updateObject(state, { loading: true, error: null });

const loginSuccess = (state, action) =>
  updateObject(state, {
    token: action.payload.token,
    isSessionResolved: true,
    loading: false,
    error: null,
    user: action.payload.user,
    reactivateMessage: action.payload.message,
  });

const registerSuccess = (state, actions) =>
  updateObject(state, { error: null, loading: false, success: true });

const registerRedirect = (state, actions) =>
  updateObject(state, { error: null, loading: false, success: false });

const loginFail = (state, action) =>
  updateObject(state, { loading: false, error: action.payload });

const registerFail = (state, action) =>
  updateObject(state, {
    loading: false,
    error: action.payload,
    success: false,
  });

const logout = (state, action) =>
  updateObject(state, { token: null, user: null, isSessionResolved: true });

const resetAuthentication = (state, action) =>
  updateObject(initialState, { isSessionResolved: true });

// TODO Refactor so we don't use the util function
export const authenticationReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_START:
      return loginStart(state, action);
    case actionTypes.LOGIN_SUCCESS:
      return loginSuccess(state, action);
    case actionTypes.LOGIN_FAIL:
      return loginFail(state, action);
    case actionTypes.REGISTRATION_SUCCESS:
      return registerSuccess(state, action);
    case actionTypes.REGISTRATION_START:
      return registerStart(state, action);
    case actionTypes.REGISTRATION_FAIL:
      return registerFail(state, action);
    case actionTypes.REGISTRATION_REDIRECT:
      return registerRedirect(state, action);
    case actionTypes.LOGOUT:
      return logout(state, action);
    case actionTypes.RESET_AUTHENTICATION:
      return resetAuthentication(state, action);
    case actionTypes.UPDATE_USER:
      return { ...state, user: action.payload };
    case actionTypes.CLEAR_ERROR:
      return { ...state, error: null };
    default:
      return state;
  }
};

export const isAuthenticatedSelector = (state) => state.auth.token;
export const isAuthenticationSuccessfulSelector = (state) => state.auth.success;
export const authenticationErrorSelector = (state) => state.auth.error;
export const currentUserSelector = (state) => state.auth.user;
export const loadingAuthenticationSelector = (state) => state.auth.loading;
export const isSessionResolvedSelector = (state) =>
  state.auth.isSessionResolved;
export const userReactivatedMessageSelector = (state) =>
  state.auth.reactivateMessage;
