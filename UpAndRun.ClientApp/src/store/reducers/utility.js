export const updateObject = (prevState, updatedValues) => {
  return { ...prevState, ...updatedValues };
};
