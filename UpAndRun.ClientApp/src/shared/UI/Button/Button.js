import classes from "./Button.module.scss";

export const Button = ({
  variant,
  children,
  click,
  classNames = [],
  size,
  isInverted = false,
  dataTestId,
  type = "button",
}) => {
  const getClassVariant = () => {
    if (isInverted) {
      return classes[`${variant}--invert`];
    }

    return classes[variant];
  };

  const getButtonClasses = () => {
    const variantClass = getClassVariant();
    const sizeClass = classes[size];
    const passedClassNames = classNames.join(" ");

    return `${variantClass} ${sizeClass} ${passedClassNames}`;
  };

  return (
    <button
      type={type}
      data-testid={dataTestId}
      onClick={click}
      className={getButtonClasses()}
    >
      {children}
    </button>
  );
};
