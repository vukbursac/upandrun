import { NavLink } from "react-router-dom";

import classes from "./Link.module.scss";

export const Link = ({
  Icon,
  text,
  pathname,
  variant,
  size,
  isRounded,
  isInverted,
  classNames = [],
}) => {
  const getRoundClassName = () => {
    if (isRounded) {
      return classes["round"];
    }
  };

  const getClassVariantName = () => {
    if (isInverted) {
      return classes[`${variant}--invert`];
    }

    return classes[variant];
  };

  const getLinkClasses = () => {
    const variantClassName = getClassVariantName();
    const sizeClassName = classes[size];
    const passedClassesName = classNames.join(" ");
    const roundClassName = getRoundClassName();

    return `${variantClassName} ${sizeClassName} ${passedClassesName} ${roundClassName}`;
  };

  const renderIcon = () => {
    if (Icon) {
      return <Icon className={classes.icon} />;
    }
  };

  return (
    <NavLink to={{ pathname }} className={getLinkClasses()}>
      {renderIcon()} {text}
    </NavLink>
  );
};
