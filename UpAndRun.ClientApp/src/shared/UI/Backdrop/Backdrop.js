import classes from "./Backdrop.module.scss";

export const Backdrop = ({ isOpen }) => {
  const renderBackdrop = () => {
    if (isOpen) {
      return <div className={classes.backdrop}></div>;
    }

    return null;
  };

  return renderBackdrop();
};
