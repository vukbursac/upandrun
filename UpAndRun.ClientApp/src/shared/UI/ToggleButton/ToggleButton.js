import classes from "./ToggleButton.module.scss";

export const ToggleButton = ({ click, isOn, IconOn, IconOff, className }) => {
  const getSwitchThemeClasses = () => {
    if (isOn) {
      return `${classes.switch} ${classes.switchOn}`;
    }

    return classes.switch;
  };

  const buttonClasses = `${classes.btn} ${className}`;

  return (
    <button className={buttonClasses} onClick={click}>
      <div className={getSwitchThemeClasses()}></div>
      <div className={classes.icons}>
        <IconOn className={classes.icon} />
        <IconOff className={classes.icon} />
      </div>
    </button>
  );
};
