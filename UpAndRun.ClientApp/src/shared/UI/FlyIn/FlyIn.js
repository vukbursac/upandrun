import React from "react";
import OutsideClickHandler from "react-outside-click-handler";
import { CSSTransition } from "react-transition-group";

import classes from "./FlyIn.module.scss";

const animationTiming = {
  enter: 600,
  exit: 400,
};

export const FlyIn = ({ children, isOpen, onClickOutside }) => {
  return (
    <OutsideClickHandler onOutsideClick={onClickOutside}>
      <CSSTransition
        in={isOpen}
        timeout={animationTiming}
        classNames={{
          enterActive: classes["showFlyIn"],
          enterDone: classes["show"],
          exitActive: classes["hideFlyIn"],
        }}
        mountOnEnter
        unmountOnExit
      >
        <div className={classes.flyIn}>{children}</div>
      </CSSTransition>
    </OutsideClickHandler>
  );
};
