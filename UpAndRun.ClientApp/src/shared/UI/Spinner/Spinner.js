import React from "react";

import classes from "./Spinner.module.scss";

export default function Spinner({ loadingFinishedResult, isLoading, size }) {
  if (isLoading) {
    return (
      <div className={classes.container}>
        <div className={`${classes.spinner} ${classes[size]}`}></div>
      </div>
    );
  }

  return <>{loadingFinishedResult}</>;
}
