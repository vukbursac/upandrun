import { CSSTransition } from "react-transition-group";
import { Backdrop } from "../Backdrop/Backdrop";

import classes from "./Modal.module.scss";

export const Modal = ({ children, isOpen }) => {
  const animationTiming = {
    enter: 500,
    exit: 500,
  };

  return (
    <>
      <Backdrop isOpen={isOpen} />
      <CSSTransition
        in={isOpen}
        timeout={animationTiming}
        classNames={{
          enterActive: classes["showModal"],
          exitActive: classes["hideModal"],
        }}
        mountOnEnter
        unmountOnExit
      >
        <div className={classes.modal}>{children}</div>
      </CSSTransition>
    </>
  );
};
