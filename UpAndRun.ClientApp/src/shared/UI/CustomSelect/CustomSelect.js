import classes from "./CustomSelect.module.scss";

import { MdKeyboardArrowDown } from "react-icons/md";
import { useState } from "react";
import OutsideClickHandler from "react-outside-click-handler";

export const CustomSelect = ({ selected, options }) => {
  const [currentSelected, setCurrentSelected] = useState(selected);
  const [isListOpened, setIsListOpened] = useState(false);

  const renderList = () => {
    if (isListOpened) {
      return (
        <ul className={classes.items} onClick={handleChosenOption}>
          {options.map((option, index) => (
            <li
              key={index}
              onClick={() => setCurrentSelected(option)}
              className={classes.item}
            >
              {option}
            </li>
          ))}
        </ul>
      );
    }
  };

  const handleChosenOption = () => {
    setIsListOpened(false);
  };

  const toggleList = () => setIsListOpened(!isListOpened);

  const handleOutsideClick = () => setIsListOpened(false);

  return (
    <OutsideClickHandler onOutsideClick={handleOutsideClick}>
      <div className={classes.customSelect}>
        <div className={classes.selected} onClick={toggleList}>
          {currentSelected}
          <MdKeyboardArrowDown className={classes.icon} />
        </div>
        {renderList()}
      </div>
    </OutsideClickHandler>
  );
};
