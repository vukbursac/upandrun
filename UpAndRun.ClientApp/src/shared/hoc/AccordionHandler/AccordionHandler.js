import React, { useEffect, useState } from "react";

export const AccordionHandler = ({ accordionItems, Component, props }) => {
  const [accordions, setAccordions] = useState(accordionItems);

  useEffect(() => {
    setAccordions(accordionItems);
  }, [accordionItems]);

  const hideAllAccordions = () => {
    const accordionsCopy = [...accordions];
    accordionsCopy.forEach((accordion) => (accordion.selected = false));
    setAccordions(accordionsCopy);
  };

  const isAccordionAlreadySelected = (accordionId) => {
    const accordion = accordions.find(
      (accordion) => accordion.id === accordionId
    );

    return accordion.selected;
  };

  const getSelectedAccordionIndex = (accordionId) => {
    return accordionItems.findIndex(
      (accordion) => accordion.id === accordionId
    );
  };

  const toggleAccordions = (accordionId) => {
    const accordionCopy = [...accordions];
    const isAccordionPreviouslySelected =
      isAccordionAlreadySelected(accordionId);
    const selectedAccordionIndex = getSelectedAccordionIndex(accordionId);
    hideAllAccordions();

    const selectedAccordion = { ...accordionCopy[selectedAccordionIndex] };
    if (isAccordionPreviouslySelected) {
      selectedAccordion.selected = false;
    } else {
      selectedAccordion.selected = true;
    }

    accordionCopy[selectedAccordionIndex] = selectedAccordion;

    setAccordions(accordionCopy);
  };

  const renderAccordions = () =>
    accordions.map((accordion) => (
      <Component
        key={accordion.id}
        {...accordion}
        {...props}
        toggleAccordions={toggleAccordions}
      />
    ));

  return renderAccordions();
};
