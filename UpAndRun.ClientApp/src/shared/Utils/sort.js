export const sortStrings = (value1, value2) => {
  const string1 = value1.toUpperCase();
  const string2 = value2.toUpperCase();

  if (string1 > string2) {
    return 1;
  }

  if (string1 < string2) {
    return -1;
  }

  return 0;
};

export const sortDate = (date1, date2) => {
  if (date1 < date2) {
    return 1;
  }

  if (date1 > date2) {
    return -1;
  }

  return 0;
};

export const sortNumbers = (number1, number2) => {
  if (number1 < number2) {
    return 1;
  }

  if (number1 > number2) {
    return -1;
  }

  return 0;
};
