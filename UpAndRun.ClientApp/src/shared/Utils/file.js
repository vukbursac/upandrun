export const extractFileName = (fileName) => {
  const indexOfPoint = fileName.lastIndexOf(".");
  const name = fileName.substring(0, indexOfPoint);
  return name;
};

export const extractFileExtension = (fileName) => {
  const indexOfPoint = fileName.lastIndexOf(".");
  const extension = fileName.substring(indexOfPoint + 1);
  return extension;
};
