import pdf from "../images/pdffile-icon.png";
import txt from "../images/textfile-icon.png";
import mp3 from "../images/mp3-icon.png";
import mp4 from "../images/mp4-icon.png";
import docx from "../images/word-icon.png";
import pptx from "../images/powerpoint-icon.png";
import xlsx from "../images/excel-icon.png";
import jpg from "../images/jpg-icon.jpg";
import png from "../images/png-icon.png";
import defaultFileImage from "../images/defaultFileImage.png";

export const uploadFileIcon = (name) => {
  const indexOfPoint = name.lastIndexOf(".");
  const fileExtension = name.substring(indexOfPoint + 1);
  switch (fileExtension) {
    case "jpg":
      return jpg;
    case "png":
      return png;
    case "txt":
      return txt;
    case "pdf":
      return pdf;
    case "mp3":
      return mp3;
    case "mp4":
      return mp4;
    case "docx":
      return docx;
    case "xlsx":
      return xlsx;
    case "pptx":
      return pptx;
    default:
      return defaultFileImage;
  }
};
