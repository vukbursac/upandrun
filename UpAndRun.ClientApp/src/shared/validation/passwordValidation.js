export const validatePassword = (password) => {
  if (!password) {
    return "Please enter your password!";
  } else if (password.length < 6) {
    return "Password must have at least 6 characters!";
  }

  return null;
};
