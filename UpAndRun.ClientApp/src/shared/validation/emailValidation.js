export const validateEmail = (email) => {
  const regexEmailValidation = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
  if (!email) {
    return "Please enter your email!";
  } else if (!regexEmailValidation.test(email)) {
    return "Invaild email, missing characters!";
  }
  return null;
};
