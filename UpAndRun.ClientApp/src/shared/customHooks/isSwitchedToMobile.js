import { useEffect, useState } from "react";

export const useIsSwitchedToMobile = () => {
  const [isSwitchedToMobile, setIsSwitchedToMobile] = useState(false);

  const handleWindowSizeChange = () => {
    if (window.innerWidth <= 768) {
      setIsSwitchedToMobile(true);
    }

    if (window.innerWidth > 768) {
      setIsSwitchedToMobile(false);
    }
  };

  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    handleWindowSizeChange();

    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  return isSwitchedToMobile;
};
