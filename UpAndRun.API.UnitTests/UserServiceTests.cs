using AutoMapper;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Models.Entities;
using UpAndRun.UserManagement.Mappings;
using UpAndRun.UserManagement.Services;
using UpAndRun.UserManagement.UpAndRun.Data.Interfaces;
using Utility.Exceptions;
using Utility.SendGrid;
using Xunit;

namespace UpAndRun.Gateway.UnitTests
{
    public class UserServiceTests
    {
        private readonly UserService _userService;
        private readonly IUnitOfWork _unitOfWork = Substitute.For<IUnitOfWork>();
        private readonly UserManager<IdentityUser> _userManager = Substitute.For<UserManager<IdentityUser>>(Substitute.For<IUserStore<IdentityUser>>(), null, null, null, null, null, null, null, null);
        private readonly IConfiguration _configuration = Substitute.For<IConfiguration>();
        private readonly IEmailService _emailService = Substitute.For<IEmailService>();
        private readonly IMapper _mapper;

        private bool _doSomethingVariable;

        private readonly Customer _customer = new Customer("Ivan", "Ivanovic", "ivaN21", "ivan1", "ivan@test.com");

        public UserServiceTests()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(typeof(CustomerResponseMapping));
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }

            _userService = new UserService(_unitOfWork, _userManager, _configuration, _emailService, _mapper);
            _customer.IsActive = true;
        }

        [Fact]
        public async Task GetById_ReturnCustomer()
        {
            //Arrange
            var customerId = Guid.Parse(_customer.Id);
            Maybe<Customer> customer = _customer;
            _unitOfWork.CustomerRepository.GetById(customerId).Returns(customer);

            //Act
            var result = await _userService.GetById(customerId);

            //Assert
            Assert.Equal(customer.Value.Id, result.CustomerResponse.Id);
        }

        [Fact]
        public async Task GetAll_ReturnsCustomers()
        {
            List<Customer> customers = new List<Customer>
            {
                new Customer("Ivan", "Ivanovic", "ivaN21", "ivan1", "ivan@test.com"),
                new Customer("Jovan", "Jovanovic", "jovaN21", "jovan1", "jovan@test.com"),
                new Customer("Marija", "Jovanovic", "marijA21", "marija1", "marija@test.com"),
            };

            _unitOfWork.CustomerRepository.GetAll().Returns(customers);

            var result = await _userService.GetAll();

            Assert.Equal(customers, result);
            ;
        }

        [Fact]
        public async Task GetAll_ThrowsNotFoundException()
        {
            List<Customer> customers = new List<Customer>();
            _unitOfWork.CustomerRepository.GetAll().Returns(customers);

            await Assert.ThrowsAsync<NotFoundException>(() => _userService.GetAll());
        }
    }
}