package apiTesting;

import apiTesting.response.CrateResponse;
import apiTesting.response.InvalidCrateResponseErrors;
import basePackage.TestDataGenerator;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CratesTests extends BaseAPI {

    String bearerToken;

    @BeforeClass
    public void beforeAllTests () {

        RestAssured.basePath = "api/Crates";
        bearerToken = getTokenByLoggingIn(loginRequestObject);

    }

    @Test (priority = 1)
    public void verifyThatGetAllCratesWorksWithStatusCode200() {

        log.info("***  VERIFY THAT GET ALL CRATES API CALL WORKS TEST START  ***");
        cratesRequest.sendGetAllCratesRequest(bearerToken);
    }

    @Test (priority = 5)
    public void verifyThatCreatingCrateWorks () {

        log.info("***  VERIFY THAT CREATING A CRATE API CALL WORKS TEST START  ***");
        CrateResponse createCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CrateResponse getCrateResponse = cratesRequest.fromJson(cratesRequest.sendGetCrateByIdRequest(bearerToken, createCrateResponse.getId(), 200));
        assertIfTwoCrateResponsesAreEqualInNameAndAllIds(createCrateResponse, getCrateResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, createCrateResponse.getId());

    }

    @Test (priority = 10)
    public void verifyThatRenameCrateByIdWorks () {

        log.info("***  VERIFY THAT RENAMING A CRATE API CALL WORKS TEST START  ***");
        CrateResponse createCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        renameCrateModel.setId(createCrateResponse.getId());
        renameCrateModel.setName(TestDataGenerator.removeFirstLetterOfAString(createCrateResponse.getName()));
        CrateResponse updateCrateResponse = cratesRequest.fromJson(cratesRequest.sendRenameCrateRequest(bearerToken, renameCrateModel));
        CrateResponse getCrateResponse = cratesRequest.fromJson(cratesRequest.sendGetCrateByIdRequest(bearerToken, renameCrateModel.getId(), 200));
        assertIfTwoCrateResponsesAreEqualInNameAndAllIds(updateCrateResponse, getCrateResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, createCrateResponse.getId());

    }

    @Test (priority = 15)
    public void verifyThatGetCrateByIdWorks () {

        log.info("***  VERIFY THAT GET CRATE BY ID API CALL WORKS TEST START  ***");
        CrateResponse createCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CrateResponse getCrateResponse = cratesRequest.fromJson(cratesRequest.sendGetCrateByIdRequest(bearerToken, createCrateResponse.getId(), 200));
        assertIfTwoCrateResponsesAreEqualInNameAndAllIds(createCrateResponse, getCrateResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, createCrateResponse.getId());

    }

    @Test (priority = 20)
    public void verifyThatDeleteCrateByIdWorks () {

        log.info("***  VERIFY THAT DELETE CRATE API CALL WORKS TEST START  ***");
        crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());
        Response response = cratesRequest.sendGetCrateByIdRequest(bearerToken, crateResponse.getId(), 400);
        Assert.assertEquals(response.asString(), "Crate with id - " + crateResponse.getId() + " does not exist.");

    }

    @Test (priority = 23)
    public void verifyThatMovingACrateWorks() {

        CrateResponse parentCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        crateRequestObject.setName(TestDataGenerator.removeFirstLetterOfAString(crateRequestObject.getName()));
        CrateResponse childCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        moveCrateModel.setId(childCrateResponse.getId());
        moveCrateModel.setParentCrateId(childCrateResponse.getParentCrateId());
        CrateResponse moveCrateResponse = cratesRequest.fromJson(cratesRequest.sendMoveCrateRequest(bearerToken, moveCrateModel));
        CrateResponse getCrateResponse = cratesRequest.fromJson(cratesRequest.sendGetCrateByIdRequest(bearerToken, childCrateResponse.getId(), 200));
        assertIfTwoCrateResponsesAreEqualInNameAndAllIds(moveCrateResponse, getCrateResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, parentCrateResponse.getId());

    }

    @Test (priority = 25)
    public void verifyThatCreatingTwoCratesWithTheSameNameIsImpossible () {

        log.info("***  VERIFY THAT CREATING TWO CRATES WITH THE SAME NAME IS IMPOSSIBLE TEST START  ***");
        crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        Response response = cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 400);
        Assert.assertEquals(response.asString(), "Crate name already exists. Crate not created.");
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 30)
    public void verifyThatDeletingACrateDeletesItsSubcrates () {

        log.info("***  VERIFY THAT DELETING A CRATE DELETES ITS SUBCRATES TEST START  ***");
        CrateResponse parentCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        crateRequestObject.setParentCrateId(parentCrateResponse.getId());
        CrateResponse childCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, parentCrateResponse.getId());
        Response response = cratesRequest.sendGetCrateByIdRequest(bearerToken, childCrateResponse.getId(), 400);
        Assert.assertEquals(response.asString(), "Crate with id - " + childCrateResponse.getId() + " does not exist.");

    }

    @Test (priority = 35)
    public void verifyThatCrateCantHaveABlankName () {

        log.info("***  VERIFY THAT CRATE CAN'T HAVE A BLANK NAME TEST START  ***");
        crateRequestObject.setName("");
        InvalidCrateResponseErrors invalidCrateResponseErrors =
                cratesRequest.fromJsonInvalidCrateResponse(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 400)).getErrors();
        Assert.assertEquals(invalidCrateResponseErrors.getName().get(0),
                "A crate name can't be empty or contain any of the following characters: < > : / |  \\ \" ? *");

    }

    @Test (priority = 40)
    public void verifyCratesCantHaveForbiddenCharactersInName () {

        log.info("***  VERIFY THAT CRATEs CAN'T HAVE A FORBIDDEN CHARACTER IN NAME TEST START  ***");
        crateRequestObject.setName("</З");
        InvalidCrateResponseErrors invalidCrateResponseErrors =
                cratesRequest.fromJsonInvalidCrateResponse(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 400)).getErrors();
        Assert.assertEquals(invalidCrateResponseErrors.getName().get(0),
                "A crate name can't be empty or contain any of the following characters: < > : / |  \\ \" ? *");

    }

    @Test (priority = 45)
    public void verifyCratesCantBeUsedWithoutAuthorization () {

        log.info("***  VERIFY THAT CRATES CAN'T BE USED WITHOUT AUTHORIZATION TEST START  ***");
        cratesRequest.sendCreateCrateRequestBody("", crateRequestObject, 401);
    }

}
