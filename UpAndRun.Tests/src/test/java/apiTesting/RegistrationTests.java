package apiTesting;
import apiTesting.requests.RegistrationRequest;
import basePackage.Constants;
import basePackage.TestDataGenerator;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RegistrationTests extends BaseAPI {

    @BeforeClass
    public void beforeEveryTest() {
        RestAssured.basePath = "api/Users/Registration";
        registrationRequest = new RegistrationRequest();

    }

    @Test(priority = 1)
    public void validRegistration() {
        log.info("***  VALID REGISTRATION TEST START  ***");
        String username = TestDataGenerator.getUsername();
        String email = TestDataGenerator.getEmail();
        registrationRequestObject.setUsername(username);
        registrationRequestObject.setEmail(email);
        Response res = registrationRequest.sendRegistrationRequestBodyAndStatusCode(registrationRequestObject, 200);
        Assert.assertEquals(res.asString(), "Registration successful " + username + ". Please confirm your email.");
    }

    //Email address

    @Test(priority = 5)
    public void invalidRegistrationInvalidEmailFormat() {
        registrationRequestObject.setEmail("janedoeexample.com");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getEmail().get(0), "Invalid e-mail address.");
    }

    @Test (priority = 10)
    public void invalidRegistrationRegisteredEmail() {
        log.info("***  INVALID REGISTRATION ALREADY REGISTERED EMAIL TEST START  ***");
        registrationRequestObject.setUsername(Constants.USERNAME_JANEDOEE);
        Response res = registrationRequest.sendRegistrationRequestBodyAndStatusCode(registrationRequestObject, 400);
        Assert.assertEquals(res.asString(), "User with email: " + Constants.EMAIL_JANEDOE_EXAMPLE_COM + " already exists.");
    }

    @Test (priority = 15)
    public void invalidRegistrationBlankEmail() {
        log.info("***  INVALID REGISTRATION BLANK EMAIL TEST START  ***");
        registrationRequestObject.setEmail(null);
        invalidAuthenticationResponse =  registrationRequest.fromJson(registrationRequest.sendRegistrationRequestBodyAndStatusCode(registrationRequestObject, 400));
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getEmail().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_EMAIL);
    }

//    //Username

    @Test (priority = 20)
    public void invalidRegistrationExceededUsernameCharacterLimit() {
        log.info("***  INVALID REGISTRATION EXCEEDED USERNAME CHARACTERS LIMIT TEST START  ***");
        registrationRequestObject.setUsername("janejanejanejanejanejanejanejanee");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getUsername().get(0), Constants.API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_USERNAME);
    }

    @Test (priority = 25)
    public void invalidRegistrationUsernameContainingForbiddenSpecialCharacters() {
        log.info("***  INVALID REGISTRATION USERNAME CONTAINING FORBIDDEN CHARACTERS TEST START  ***");
        registrationRequestObject.setUsername("jane doe");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getUsername().get(0), Constants.API_ERROR_MESSAGE_INVALID_USERNAME_FORMAT);
    }

    @Test (priority = 30)
    public void invalidRegistrationRegisteredUsername() {
        log.info("***  INVALID REGISTRATION ALREADY REGISTERED USERNAME TEST START  ***");
        Response res = registrationRequest.sendRegistrationRequestBodyAndStatusCode(registrationRequestObject, 400);
        Assert.assertEquals(res.asString(), "User with username: " + Constants.USERNAME_JANEDOE + " already exists.");
    }

    @Test (priority = 35)
    public void invalidRegistrationBlankUsername() {
        log.info("***  INVALID REGISTRATION BLANK USERNAME TEST START  ***");
        registrationRequestObject.setUsername(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getUsername().get(0), "Username is required.");
    }

//    //Password & Confirm password

    @Test (priority = 40)
    public void invalidRegistrationBlankPasswordAndConfirmationPassword(){
        log.info("***  INVALID REGISTRATION BLANK PASSWORD AND CONFIRMATION PASSWORD TEST START  ***");
        registrationRequestObject.setPassword(null);
        registrationRequestObject.setConfirmPassword(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_PASSWORD);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getConfirmPassword().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_CONFIRM_PASSWORD);
    }


    @Test (priority = 45)
    public void invalidRegistrationBlankConfirmationPassword(){
        log.info("***  INVALID REGISTRATION CONFIRMATION PASSWORD TEST START  ***");
        registrationRequestObject.setConfirmPassword(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getConfirmPassword().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_CONFIRM_PASSWORD);
    }

    @Test (priority = 50)
    public void invalidRegistrationBlankPassword(){
        log.info("***  INVALID REGISTRATION BLANK PASSWORD TEST START  ***");
        registrationRequestObject.setPassword(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_PASSWORD);
    }

    @Test (priority = 55)
    public void invalidRegistrationPasswordBellowMinimumCharacters(){
        log.info("***  INVALID REGISTRATION PASSWORD BELLOW MINIMUM CHARACTERS TEST START  ***");
        registrationRequestObject.setPassword("Aa123");
        registrationRequestObject.setConfirmPassword("Aa123");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_PASSWORD_BELLOW_MINIMUM_CHAR_LIMIT);
    }

    @Test (priority = 56)
    public void invalidRegistrationNoUpperCaseCharactersPassword(){
        log.info("***  INVALID REGISTRATION NO UPPERCASE CHARACTERS PASSWORD TEST START  ***");
        registrationRequestObject.setPassword("aaa123");
        registrationRequestObject.setConfirmPassword("aaa123");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);
    }

    @Test (priority = 57)
    public void invalidRegistrationNoLowerCaseCharactersPassword(){
        log.info("***  INVALID REGISTRATION NO LOWERCASE CHARACTERS PASSWORD TEST START  ***");
        registrationRequestObject.setPassword("AAA123");
        registrationRequestObject.setConfirmPassword("AAA123");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);
    }

    @Test (priority = 58)
    public void invalidRegistrationNoNumericCharactersPassword(){
        log.info("***  INVALID REGISTRATION NO NUMERIC CHARACTERS PASSWORD TEST START  ***");
        registrationRequestObject.setPassword("Aaaaaa");
        registrationRequestObject.setConfirmPassword("Aaaaaa");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);
    }

    @Test (priority = 58)
    public void invalidRegistrationNoLettersPassword() {
        log.info("***  INVALID REGISTRATION NO LETTERS PASSWORD TEST START  ***");
        registrationRequestObject.setPassword("111111");
        registrationRequestObject.setConfirmPassword("111111");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);
    }

    @Test (priority = 60)
    public void invalidRegistrationUnmatchingPasswordAndConfirmationPassword(){
        log.info("***  INVALID REGISTRATION UNMATCHING PASSWORD AND CONFIRMATION PASSWORD TEST START  ***");
        registrationRequestObject.setConfirmPassword("aaa123");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getConfirmPassword().get(0), Constants.API_ERROR_MESSAGE_UNMATCHING_CONFIRM_PASSWORD);
    }

//    //First name

    @Test (priority = 62)
    public void invalidRegistrationBlankFirstName() {
        log.info("***  INVALID REGISTRATION BLANK FIRST NAME TEST START  ***");
        registrationRequestObject.setFirstName(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getFirstName().get(0), "The FirstName field is required.");
    }

    @Test (priority = 65)
    public void invalidRegistrationLowerCaseFirstName() {
        log.info("***  INVALID REGISTRATION LOWERCASE FIRST NAME TEST START  ***");
        registrationRequestObject.setFirstName("jane");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getFirstName().get(0), Constants.API_ERROR_MESSAGE_LOWERCASE_FIRST_NAME);
    }

    @Test (priority = 66)
    public void invalidRegistrationCharNumberExceededFirstName() {
        log.info("***  INVALID REGISTRATION EXCEEDED FIRST NAME CHARACTERS LIMIT TEST START  ***");
        registrationRequestObject.setFirstName("Jane Jane Jane Jane Jane Jane Jane Jane Jane Jane .");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getFirstName().get(0), Constants.API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_FIRST_NAME);
    }

//    //Last name

    @Test (priority = 67)
    public void invalidRegistrationBlankLastName() {
        log.info("***  INVALID REGISTRATION BLANK LAST NAME TEST START  ***");
        registrationRequestObject.setLastName(null);
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getLastName().get(0), "The LastName field is required.");
    }

    @Test (priority = 70)
    public void invalidRegistrationLowerCaseLastName() {
        log.info("***  INVALID REGISTRATION LOWERCASE LAST NAME TEST START  ***");
        registrationRequestObject.setLastName("doe");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getLastName().get(0), Constants.API_ERROR_MESSAGE_LOWERCASE_LAST_NAME);
    }

    @Test (priority = 66)
    public void invalidRegistrationCharNumberExceededLastName() {
        log.info("***  INVALID REGISTRATION EXCEEDED FIRST NAME CHARACTERS LIMIT TEST START  ***");
        registrationRequestObject.setLastName("Doee Doee Doee Doee Doee Doee Doee Doee Doee Doee .");
        invalidAuthenticationResponse =  registrationRequest.registration(registrationRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getLastName().get(0), Constants.API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_LAST_NAME);
    }

    @AfterMethod
    public void afterEveryClass(){
        log.info("***  REGISTRATION TEST END  ***");
    }

}