package apiTesting.requests;

import apiTesting.models.CrateModel;
import apiTesting.models.MoveCrateModel;
import apiTesting.models.RenameCrateModel;
import apiTesting.response.CrateResponse;
import apiTesting.response.InvalidCrateResponse;
import basePackage.ICustomMapper;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.baseURI;

public class CratesRequest implements ICustomMapper {

    public RequestSpecification baseRequest(String bearerToken) {

        return RestAssured.given()
                .headers(
                        "Authorization",
                        "Bearer " + bearerToken)
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .when();

    }

    public Response sendCreateCrateRequestBody(String bearerToken, CrateModel crateModel, int statusCode) {
        return baseRequest(bearerToken)
                .body(crateModel)
                .post(baseURI + "api/Crates")
                .then()
                .statusCode(statusCode)
                .extract().response();
    }

    public Response sendGetAllCratesRequest (String bearerToken) {

        return baseRequest(bearerToken)
                .get()
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendGetCrateByIdRequest (String bearerToken, String id, int statusCode) {

        return baseRequest(bearerToken)
                .get(baseURI + "api/Crates/" + id)
                .then()
                .statusCode(statusCode)
                .extract().response();

    }

    public Response sendDeleteCrateByIdRequest (String bearerToken, String id) {

        return baseRequest(bearerToken)
                .delete(baseURI + "api/Crates?id=" + id)
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendRenameCrateRequest(String bearerToken, RenameCrateModel renameCrateModel){

        return baseRequest(bearerToken)
                .body(renameCrateModel)
                .patch(baseURI + "api/Crates/rename")
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendMoveCrateRequest (String bearerToken, MoveCrateModel moveCrateModel) {

        return baseRequest(bearerToken)
                .body(moveCrateModel)
                .patch(baseURI + "api/Crates/move")
                .then()
                .statusCode(200)
                .extract().response();

    }

    @Override
    public CrateResponse fromJson(Response response) {

        return new Gson().fromJson(response.asString(), CrateResponse.class);

    }

    public InvalidCrateResponse fromJsonInvalidCrateResponse (Response response) {

        return new Gson().fromJson(response.asString(), InvalidCrateResponse.class);

    }
}
