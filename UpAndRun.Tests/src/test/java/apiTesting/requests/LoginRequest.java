package apiTesting.requests;

import apiTesting.models.LoginModel;
import apiTesting.response.InvalidAuthenticationResponse;
import apiTesting.response.ValidLoginResponse;
import basePackage.ICustomMapper;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class LoginRequest implements ICustomMapper {


    public ValidLoginResponse fromJson (Response response) {
        return new Gson().fromJson(response.asString(), ValidLoginResponse.class);
    }

    public Response sendLoginRequestBodyAndStatusCode(LoginModel loginModel, int statusCode) {
        return given()
                .contentType(ContentType.JSON)
                .body(loginModel)
                .relaxedHTTPSValidation()
                .when()
                .post()
                .then()
                .statusCode(statusCode)
                .extract().response();
    }


    public ValidLoginResponse login(LoginModel loginModel, int statusCode) {
        return fromJson(sendLoginRequestBodyAndStatusCode(loginModel, statusCode));
    }

    public ValidLoginResponse getValidLoginResponse (LoginModel loginModel) {
        Response res = given()
                .contentType(ContentType.JSON)
                .body(loginModel)
                .relaxedHTTPSValidation()
                .when()
                .post("https://localhost:44309/api/Users/Login")
                .then()
                .extract().response();

        return new Gson().fromJson(res.asString(), ValidLoginResponse.class);

    }

    public InvalidAuthenticationResponse invalidLogin(LoginModel loginModel, int statusCode) {
        return new Gson().fromJson(sendLoginRequestBodyAndStatusCode(loginModel, statusCode).asString(), InvalidAuthenticationResponse.class);
    }

}