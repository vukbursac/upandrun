package apiTesting.requests;

import apiTesting.models.MoveFileModel;
import apiTesting.models.RenameFileModel;
import apiTesting.response.CreateFileResponse;
import apiTesting.response.GetFileResponse;
import basePackage.ICustomMapper;
import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.File;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;


public class FilesRequest implements ICustomMapper {

    public RequestSpecification enterRequestHeaderAndContentType (String bearerToken) {

        return given()
                .headers(
                        "Authorization",
                        "Bearer " + bearerToken)
                .contentType(ContentType.JSON)
                .relaxedHTTPSValidation()
                .when();

    }

    public Response sendGetAllFilesRequest (String bearerToken) {

        return enterRequestHeaderAndContentType(bearerToken)
                .get()
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendGetFileByIdRequest (String bearerToken, String fileId, int statusCode) {

        return enterRequestHeaderAndContentType(bearerToken)
                .get(baseURI + "api/Files/" + fileId)
                .then()
                .statusCode(statusCode)
                .extract().response();

    }

    public Response sendUploadAFileRequest (String bearerToken, String parentCrateId, File file, int statusCode) {

        return given()
                .headers(
                        "Authorization",
                        "Bearer " + bearerToken)
                .multiPart(file)
                .relaxedHTTPSValidation()
                .when()
                .post(baseURI + "api/Files/" + parentCrateId)
                .then()
                .statusCode(statusCode)
                .extract().response();

    }

    public Response sendDeleteFileRequest (String bearerToken, String fileId) {

        return enterRequestHeaderAndContentType(bearerToken)
                .delete(baseURI + "api/Files/" + fileId)
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendRenameFileByIdRequest(String bearerToken, RenameFileModel renameFileModel) {

        return enterRequestHeaderAndContentType(bearerToken)
                .body(renameFileModel)
                .put()
                .then()
                .statusCode(200)
                .extract().response();

    }

    public Response sendMoveFileRequest (String bearerToken, MoveFileModel moveFileModel, int statusCode) {

        return enterRequestHeaderAndContentType(bearerToken)
                .body(moveFileModel)
                .patch(baseURI + "api/Files/movefile")
                .then()
                .statusCode(statusCode)
                .extract().response();

    }

    public CreateFileResponse fromJson (Response response) {

        return new Gson().fromJson(response.asString(), CreateFileResponse.class);

    }

    public GetFileResponse fromJsonGetFileResponse(Response response) {

        return new Gson().fromJson(response.asString(), GetFileResponse.class);

    }

}
