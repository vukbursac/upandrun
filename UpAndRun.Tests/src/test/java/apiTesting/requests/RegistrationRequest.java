package apiTesting.requests;

import apiTesting.models.RegistrationModel;
import apiTesting.response.InvalidAuthenticationResponse;
import basePackage.ICustomMapper;
import com.google.gson.*;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class RegistrationRequest implements ICustomMapper {



    public Response sendRegistrationRequestBodyAndStatusCode(RegistrationModel registrationModel, int statusCode) {
        return given()
                .contentType(ContentType.JSON)
                .body(registrationModel)
                .relaxedHTTPSValidation()
                .when()
                .post()
                .then()
                .statusCode(statusCode)
                .extract().response();
    }


    public InvalidAuthenticationResponse registration(RegistrationModel registrationModel, int statusCode) {
        return fromJson(sendRegistrationRequestBodyAndStatusCode(registrationModel, statusCode));
    }

    public InvalidAuthenticationResponse fromJson(Response response) {
        return new Gson().fromJson(response.asString(), InvalidAuthenticationResponse.class);
    }
}