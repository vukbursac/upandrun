package apiTesting;

import apiTesting.models.CrateModel;
import apiTesting.models.LoginModel;
import apiTesting.models.MoveCrateModel;
import apiTesting.models.MoveFileModel;
import apiTesting.models.RegistrationModel;
import apiTesting.models.RenameCrateModel;
import apiTesting.models.RenameFileModel;
import apiTesting.requests.CratesRequest;
import apiTesting.requests.FilesRequest;
import apiTesting.requests.LoginRequest;
import apiTesting.requests.RegistrationRequest;
import apiTesting.response.CrateResponse;
import apiTesting.response.CreateFileResponse;
import apiTesting.response.GetFileResponse;
import apiTesting.response.InvalidAuthenticationResponse;
import apiTesting.response.ValidLoginResponse;
import basePackage.Constants;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

import java.io.File;

import static io.restassured.RestAssured.baseURI;


public class BaseAPI {

    static Logger log = Logger.getLogger(BaseAPI.class);
    public LoginRequest loginRequest = new LoginRequest();
    public InvalidAuthenticationResponse invalidAuthenticationResponse;
    public RegistrationRequest registrationRequest;
    public CratesRequest cratesRequest = new CratesRequest();
    public FilesRequest filesRequest = new FilesRequest();
    public LoginModel loginRequestObject = new LoginModel(Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123);
    public RegistrationModel registrationRequestObject = new RegistrationModel(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOE, Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
    public CrateModel crateRequestObject = new CrateModel("testCrate");
    public ValidLoginResponse validLoginResponse = new ValidLoginResponse();
    public CrateResponse crateResponse = new CrateResponse();
    public RenameCrateModel renameCrateModel = new RenameCrateModel();
    public MoveCrateModel moveCrateModel = new MoveCrateModel();
    public RenameFileModel renameFileModel = new RenameFileModel();
    public MoveFileModel moveFileModel = new MoveFileModel();
    public GetFileResponse getFileResponse = new GetFileResponse();
    public CreateFileResponse createFileResponse = new CreateFileResponse();
    File file = new File(Constants.FILE_PATH);

    @BeforeSuite
    public void beforeAll() {
        PropertyConfigurator.configure("src/log4j.properties");
        baseURI = "https://localhost:44309/";
        log.info("### API UP`N`RUN TESTING START ###");
        invalidAuthenticationResponse = new InvalidAuthenticationResponse();
    }

    @AfterClass
    public void afterAllTest() {
        log.info("### API UP`N`RUN TESTING END ###");
        log.info("=================================================================================");
        log.info("                                                                                 ");
    }

    public String getTokenByLoggingIn (LoginModel loginModel) {

        return loginRequest.getValidLoginResponse(loginModel).getLoginResponse().getToken();

    }

    public void assertIfTwoCrateResponsesAreEqualInNameAndAllIds (CrateResponse crateResponse1,CrateResponse crateResponse2) {

        Assert.assertTrue(crateResponse1.getName().equals(crateResponse2.getName()) &&
                crateResponse1.getSubCratesIds().equals(crateResponse2.getSubCratesIds()) &&
                crateResponse1.getFileIds().equals(crateResponse2.getFileIds()) &&
                crateResponse1.getId().equals(crateResponse2.getId()) &&
                crateResponse1.getParentCrateId().equals(crateResponse2.getParentCrateId()));

    }

    public void assertIfTwoFileResponsesAreEqual (CreateFileResponse createFileResponse, GetFileResponse getFileResponse) {

        Assert.assertTrue(createFileResponse.getFileName().equals(getFileResponse.getFileName()) &&
                createFileResponse.getFileType().equals(getFileResponse.getFileType()) &&
                createFileResponse.getSize() == getFileResponse.getSize() &&
                createFileResponse.getId().equals(getFileResponse.getId()) &&
                createFileResponse.getParentCrateId().equals(getFileResponse.getParentCrateId()));

    }
}