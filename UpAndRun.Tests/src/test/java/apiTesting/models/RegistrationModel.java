package apiTesting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegistrationModel {

    public String firstName;
    public String lastName;
    public String username;
    public String email;
    public String password;
    public String confirmPassword;


}
