package apiTesting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CrateModel {

    public CrateModel (String name) {

        this.name = name;

    }

    @Required private String name;
    private String parentCrateId;

}
