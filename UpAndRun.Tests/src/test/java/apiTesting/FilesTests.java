package apiTesting;

import apiTesting.response.CrateResponse;
import apiTesting.response.CreateFileResponse;
import apiTesting.response.GetFileResponse;
import basePackage.TestDataGenerator;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FilesTests extends BaseAPI {

    String bearerToken;

    @BeforeClass
    public void beforeAllTests () {

        RestAssured.basePath = "api/Files";
        bearerToken = getTokenByLoggingIn(loginRequestObject);

    }

    @Test (priority = 1)
    public void verifyThatGetAllFilesWorks() {

        log.info("***  VERIFY THAT GET ALL FILES API CALL WORKS TEST START  ***");
        filesRequest.sendGetAllFilesRequest(bearerToken);

    }

    @Test (priority = 5)
    public void verifyThatUploadingAFileWorks() {

        log.info("***  VERIFY THAT UPLOAD A FILE API CALL WORKS TEST START  ***");
        CrateResponse crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CreateFileResponse requestCreateFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 200));
        GetFileResponse getCreateFileResponse = filesRequest.fromJsonGetFileResponse(filesRequest.sendGetFileByIdRequest(bearerToken, requestCreateFileResponse.getId(), 200));
        assertIfTwoFileResponsesAreEqual(requestCreateFileResponse, getCreateFileResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 10)
    public void verifyThatGetFileByIdWorks() {

        log.info("***  VERIFY THAT GET FILE BY ID API CALL WORKS TEST START  ***");
        CrateResponse crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CreateFileResponse requestCreateFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 200));
        GetFileResponse getCreateFileResponse = filesRequest.fromJsonGetFileResponse(filesRequest.sendGetFileByIdRequest(bearerToken, requestCreateFileResponse.getId(), 200));
        assertIfTwoFileResponsesAreEqual(requestCreateFileResponse, getCreateFileResponse);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 15)
    public void verifyThatDeletingAFileWorks () {

        log.info("***  VERIFY THAT DELETE FILE BY ID API CALL WORKS TEST START  ***");
        CrateResponse crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CreateFileResponse requestCreateFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 200));
        filesRequest.sendDeleteFileRequest(bearerToken, requestCreateFileResponse.getId());
        Response response = filesRequest.sendGetFileByIdRequest(bearerToken, requestCreateFileResponse.getId(), 400);
        Assert.assertEquals(response.asString(),"File not found.");
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 20)
    public void verifyThatRenamingAFileByIdWorks() {

        log.info("***  VERIFY THAT RENAME FILE BY ID API CALL WORKS TEST START  ***");
        CrateResponse crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        CreateFileResponse createFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 200));
        renameFileModel.setFileId(createFileResponse.getId());
        renameFileModel.setFileName(TestDataGenerator.removeFirstLetterOfAString(createFileResponse.getFileName()));
        renameFileModel.setDescription(createFileResponse.getDescription());
        Response res = filesRequest.sendRenameFileByIdRequest(bearerToken, renameFileModel);
        getFileResponse = filesRequest.fromJsonGetFileResponse(filesRequest.sendGetFileByIdRequest(bearerToken, createFileResponse.getId(), 200));
        Assert.assertTrue(res.asString().equals("\"" + createFileResponse.getId() + "\"") &&
                renameFileModel.getFileName().equals(getFileResponse.getFileName()));
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 25)
    public void verifyThatMovingAFileWorks () {

        log.info("***  VERIFY THAT MOVING FILE API CALL WORKS TEST START  ***");
        CrateResponse firstCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        crateRequestObject.setName(TestDataGenerator.removeFirstLetterOfAString(crateRequestObject.getName()));
        CrateResponse secondCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject, 200));
        createFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, firstCrateResponse.getId(), file, 200));
        moveFileModel.setFileId(createFileResponse.getId());
        moveFileModel.setParentCrateId(secondCrateResponse.getId());
        Response response = filesRequest.sendMoveFileRequest(bearerToken, moveFileModel, 200);
        getFileResponse = filesRequest.fromJsonGetFileResponse(filesRequest.sendGetFileByIdRequest(bearerToken, createFileResponse.getId(), 200));
        Assert.assertTrue(response.asString().equals("File " + createFileResponse.getFileName() + " successfully moved.") &&
                getFileResponse.getParentCrateId().equals(secondCrateResponse.getId()));
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, firstCrateResponse.getId());
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, secondCrateResponse.getId());

    }

    @Test (priority = 30)
    public void verifyThatCreatingTwoFilesWithTheSameNameIsImpossible () {

        log.info("***  VERIFY THAT CREATING TWO FILES WITH THE SAME NAME IS IMPOSSIBLE TEST START  ***");
        crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject,  200));
        filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 200);
        Response response = filesRequest.sendUploadAFileRequest(bearerToken, crateResponse.getId(), file, 400);
        Assert.assertEquals(response.asString(), "Cannot upload two files with the same name in provided crate.");
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 35)
    public void verifyFilesCantBeCreatedWithoutAuthorization () {

        log.info("***  VERIFY THAT FILES CAN'T BE CREATED WITHOUT AUTHORIZATION TEST START  ***");
        crateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject,  200));
        filesRequest.sendUploadAFileRequest("", crateResponse.getId(), file, 401);
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, crateResponse.getId());

    }

    @Test (priority = 40)
    public void verifyMovingAFileToACrateWithAFileWithTheSameNameIsImpossible () {

        log.info("***  VERIFY THAT MOVING A FILE TO A CRATE WITH A FILE WITH THE SAME NAME IS IMPOSSIBLE TEST START  ***");
        CrateResponse firstCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject,  200));
        CreateFileResponse firstFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, firstCrateResponse.getId(), file, 200));
        crateRequestObject.setName(TestDataGenerator.removeFirstLetterOfAString(crateRequestObject.getName()));
        CrateResponse secondCrateResponse = cratesRequest.fromJson(cratesRequest.sendCreateCrateRequestBody(bearerToken, crateRequestObject,  200));
        CreateFileResponse secondFileResponse = filesRequest.fromJson(filesRequest.sendUploadAFileRequest(bearerToken, secondCrateResponse.getId(), file, 200));
        moveFileModel.setFileId(firstFileResponse.getId());
        moveFileModel.setParentCrateId(secondCrateResponse.getId());
        Response response = filesRequest.sendMoveFileRequest(bearerToken, moveFileModel, 400);
        Assert.assertEquals(response.asString(), "The file " + firstFileResponse.getFileName() + " already exists in destination crate.");
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, firstCrateResponse.getId());
        cratesRequest.sendDeleteCrateByIdRequest(bearerToken, secondCrateResponse.getId());

    }

}
