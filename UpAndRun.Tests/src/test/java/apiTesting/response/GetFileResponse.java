package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class GetFileResponse {

    @SerializedName("fileName")
    private String fileName;
    @SerializedName("fileType")
    private String fileType;
    @SerializedName("description")
    private String description;
    @SerializedName("size")
    private int size;
    @SerializedName("id")
    private String id;
    @SerializedName("parentCrateId")
    private String parentCrateId;
    @SerializedName("creationDate")
    private String creationDate;

}
