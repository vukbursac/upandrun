package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class LoginResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("token")
    private String token;


}
