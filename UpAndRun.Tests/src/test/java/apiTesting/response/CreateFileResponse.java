package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateFileResponse {

    @SerializedName("fileName")
    private String fileName;
    @SerializedName("fileType")
    private String fileType;
    @SerializedName("description")
    private String description;
    @SerializedName("size")
    private int size;
    @SerializedName("fileObject")
    private String fileObject;
    @SerializedName("id")
    private String id;
    @SerializedName("parentCrateId")
    private String parentCrateId;
    @SerializedName("creationDate")
    private String creationDate;

}
