package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public  class InvalidAuthenticationResponse {

   @SerializedName("type")
   private String type;
   @SerializedName("title")
   private String title;
   @SerializedName("status")
   private int status;
   @SerializedName("traceId")
   private String traceId;
   @SerializedName("errors")
   private Errors errors;


}
