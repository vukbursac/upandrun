package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class InvalidCrateResponse {

    @SerializedName("type")
    private String type;
    @SerializedName("title")
    private String title;
    @SerializedName("status")
    private int status;
    @SerializedName("traceId")
    private String traceId;
    @SerializedName("errors")
    private InvalidCrateResponseErrors errors;

}
