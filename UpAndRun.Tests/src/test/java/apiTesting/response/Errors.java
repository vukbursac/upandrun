package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class Errors {

    @SerializedName("Email")
    private List<String> Email;
    @SerializedName("Username")
    private List<String> Username;
    @SerializedName("Password")
    private List<String> Password;
    @SerializedName("ConfirmPassword")
    private List<String> ConfirmPassword;
    @SerializedName("FirstName")
    private List<String> FirstName;
    @SerializedName("LastName")
    private List<String> LastName;

}
