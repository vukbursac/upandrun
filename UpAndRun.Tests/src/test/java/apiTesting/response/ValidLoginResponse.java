package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class ValidLoginResponse {

    @SerializedName("topic")
    private String topic;
    @SerializedName("id")
    private String id;
    @SerializedName("isSuccess")
    private String isSuccess;
    @SerializedName("message")
    private String message;
    @SerializedName("loginResponse")
    private LoginResponse loginResponse;


}
