package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class InvalidCrateResponseErrors {

    @SerializedName("Name")
    private List<String> Name;

}
