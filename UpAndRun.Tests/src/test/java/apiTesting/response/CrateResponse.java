package apiTesting.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class CrateResponse {

    @SerializedName("name")
    private String name;
    @SerializedName("subCratesIds")
    private List<String> subCratesIds;
    @SerializedName("fileIds")
    private List<String> fileIds;
    @SerializedName("id")
    private String id;
    @SerializedName("parentCrateId")
    private String parentCrateId;
    @SerializedName("creationDate")
    private String creationDate;

}
