package apiTesting;
import apiTesting.requests.LoginRequest;
import basePackage.Constants;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTests extends BaseAPI {

    @BeforeClass
    public void beforeEveryTest() {
        RestAssured.basePath = "api/Users/Login";
        loginRequest = new LoginRequest();
    }

    @Test(priority = 1)
    public void validLogin() {

        log.info("***  VALID LOGIN TEST START  ***");
        validLoginResponse = loginRequest.login(loginRequestObject, 200);
        Assert.assertEquals(validLoginResponse.getLoginResponse().getUsername(), Constants.USERNAME_JANEDOE);

    }

    @Test(priority = 5)
    public void invalidLoginWrongPassword() {

        log.info("***  INVALID LOGIN WRONG PASSWORD TEST START  ***");
        loginRequestObject.setPassword("aaa123");
        Response res = loginRequest.sendLoginRequestBodyAndStatusCode(loginRequestObject, 400);
        Assert.assertEquals(res.asString(), "Incorrect password.");

    }

    @Test(priority = 10)
    public void invalidLoginWrongEmail() {
        log.info("***  INVALID LOGIN WRONG EMAIL TEST START  ***");
        loginRequestObject.setEmail(Constants.EMAIL_JANEDOE1_EXAMPLE_COM);
        Response res = loginRequest.sendLoginRequestBodyAndStatusCode(loginRequestObject, 400);
        Assert.assertEquals(res.asString(), "Email: " + Constants.EMAIL_JANEDOE1_EXAMPLE_COM + " does not exists.");
    }

    @Test(priority = 11)
    public void invalidLoginUnconfirmedEmail() {
        log.info("***  INVALID LOGIN UNCONFIRMED EMAIL TEST START  ***");
        loginRequestObject.setEmail("janedoe2@example.com");
        loginRequestObject.setPassword(Constants.PASSWORD_AAA123);
        Response res = loginRequest.sendLoginRequestBodyAndStatusCode(loginRequestObject, 400);
        Assert.assertEquals(res.asString(), "Email not confirmed.");
    }

    @Test(priority = 15)
    public void invalidLoginBlankEmailAndPassword() {
        log.info("***  INVALID LOGIN BLANK EMAIL AND PASSWORD TEST START  ***");
        loginRequestObject.setEmail(null);
        loginRequestObject.setPassword(null);
        invalidAuthenticationResponse = loginRequest.invalidLogin(loginRequestObject, 400);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getEmail().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_EMAIL);
        Assert.assertEquals(invalidAuthenticationResponse.getErrors().getPassword().get(0), Constants.API_ERROR_MESSAGE_REQUIRED_PASSWORD);
    }

    @AfterMethod
    public void afterEveryTest() {
        log.info("***  LOGIN TEST END  ***");
    }

}