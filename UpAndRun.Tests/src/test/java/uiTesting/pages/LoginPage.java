package uiTesting.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{

    public LoginPage (WebDriver driver) {

        super(driver);

    }

    @FindBy (css = "[data-testid=\"email-login-input\"]")
    WebElement emailField;

    @FindBy (css = "[data-testid=\"password-login-input\"]")
    WebElement passwordField;

    @FindBy (css = "[data-testid=\"login-button-login-page\"]")
    WebElement loginButton;

    @FindBy (css = "[data-testid=\"sign-up-link-login-page\"]")
    WebElement signupButton;

    @FindBy (css = "[data-testid=\"login-backend-input-error\"]")
    WebElement loginError;

    @FindBy (css= "[data-testid=\"email-input-error\"]")
    WebElement invalidEmailError;

    @FindBy (css = "[data-testid=\"password-input-error\"]")
    WebElement invalidPasswordError;

    public LoginPage enterDataToEmailField(String input) {

        enterDataToField(emailField, input);
        return this;

    }

    public LoginPage enterDataToPasswordField(String input) {

        enterDataToField(passwordField, input);
        return this;

    }

    public LoginPage clickLoginButton() {

        clickFunction(loginButton);
        return this;

    }

    public LoginPage clickSignupButton() {

        clickFunction(signupButton);
        return this;

    }

    public String getTextFromLoginError() {

        return getTextFromWebElement(loginError);

    }

    public String getTextFromInvalidEmailError() {

        return getTextFromWebElement(invalidEmailError);
    }

    public String getTextFromInvalidPasswordError () {

        return getTextFromWebElement(invalidPasswordError);

    }

}
