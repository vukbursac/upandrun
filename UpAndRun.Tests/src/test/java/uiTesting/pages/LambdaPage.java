package uiTesting.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class LambdaPage extends BasePage {

    public LambdaPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "file")
    WebElement uploadFileInput;
    @FindBy(xpath = "//button[text()='Upload']")
    WebElement uploadLambdaButton;
    @FindBy(css = "[data-testid=\"link-open-lambda-form\"]")
    List<WebElement> lambdaNameLinkList;
    @FindBy(css = "[data-testid=\"lambda-get-link\"]")
    WebElement getMethodButton;
    @FindBy(css = "[data-testid=\"lambda-post-link\"]")
    WebElement postMethodButton;
    @FindBy(xpath = "//button[text()='Run']")
    WebElement runGetLambdaButton;
    WebElement lambdaInputWindow;
    @FindBy(xpath = "//*[@id=\"root\"]/div[1]/div[2]/div/main/div[2]/div[1]/form/div[2]/div/code")
    WebElement lambdaOutputWindow;
    @FindBy(css = "[data-testid=\"delete-lambda-icon\"]")
    List<WebElement> deleteLambdaIconList;
    @FindBy(css = "[data-testid=\"spinner\"]")
    WebElement lambdaUploadSpinner;

    public void clickOnUploadLambdaButton() {
        wait.until(ExpectedConditions.elementToBeClickable(uploadLambdaButton));
        uploadLambdaButton.click();
    }

    public void uploadLambda(String absolutePathForFile) {
        uploadFileInput.sendKeys(absolutePathForFile);
        clickOnUploadLambdaButton();
        waitForLambdaUpload();
    }

    public void clickOnLambdaLink(String lambdaName){
        for (int i = 0; i< lambdaNameLinkList.size(); i++){
            if(lambdaNameLinkList.get(i).getText().equals(lambdaName)){
                wait.until(ExpectedConditions.elementToBeClickable(lambdaNameLinkList.get(i)));
                clickFunction(lambdaNameLinkList.get(i));
            } else {
              break;
            }
        }
    }

    public void clickOnDeleteLambdaIcon(String lambdaName){
        for (int i = 0; i< lambdaNameLinkList.size(); i++){
            if(lambdaNameLinkList.get(i).getText().equals(lambdaName)){
                wait.until(ExpectedConditions.elementToBeClickable(deleteLambdaIconList.get(i)));
                clickFunction(deleteLambdaIconList.get(i));
            } else {
                break;
            }
        }
    }

    public void clickOnGetMethodButton() {
        clickFunction(getMethodButton);
    }

    public void clickOnPostMethodButton() {
        clickFunction(postMethodButton);
    }

    public void clickOnRunLambdaButton(){
        wait.until(ExpectedConditions.elementToBeClickable(runGetLambdaButton));
        clickFunction(runGetLambdaButton);
    }

    public void fillLambdaInputWindow(String json){
       enterDataToField(lambdaInputWindow, json);
    }

    public String getOutputWindowText(){
        return getTextFromWebElement(lambdaOutputWindow);
    }

    public boolean outputWindowTextPresent(){
        if(getOutputWindowText().length() > 0){
            return true;
        } else {
            return false;
        }
    }

    public String getLambdaName(int lambdaIndex){
       return getTextFromWebElement(lambdaNameLinkList.get(lambdaIndex));
    }

    public boolean lambdaNameMatches(String expectedLambdaName){
        boolean lambdaNameMatches = false;
        for (int i = 0; i< lambdaNameLinkList.size(); i++){
            if(lambdaNameLinkList.get(i).getText().equals(expectedLambdaName)){
                lambdaNameMatches = true;
            } else {
                lambdaNameMatches = false;
            }
        }
        return lambdaNameMatches;
    }

    public void waitForLambdaUpload(){
        wait.until(ExpectedConditions.visibilityOf(uploadLambdaButton));
    }


}
