package uiTesting.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class NavigationSideMenu extends BasePage {


    public NavigationSideMenu(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "[data-testid=\"sidebar-services-span\"]")
    WebElement servicesDropdown;
    @FindBy(css = "[data-testid=\"sidebar-services-runtime-link\"]")
    WebElement runtimeService;

    public NavigationSideMenu clickOnServicesDropdown() {
        wait.until(ExpectedConditions.elementToBeClickable(servicesDropdown));
        clickFunction(servicesDropdown);
        return this;
    }

    public void clickOnRuntimeService() {
        wait.until(ExpectedConditions.elementToBeClickable(runtimeService));
        clickFunction(runtimeService);
    }

}
