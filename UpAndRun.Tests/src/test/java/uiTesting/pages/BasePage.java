package uiTesting.pages;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

import static basePackage.Constants.LOGGER_ELEMENT_NOT_INTERACTABLE;
import static basePackage.Constants.LOGGER_INVALID_SELECTOR;
import static basePackage.Constants.LOGGER_NO_ELEMENT;


public class BasePage {

    static Logger log = Logger.getLogger(BasePage.class);

    WebDriver driver;
    Wait<WebDriver> wait;
    Actions action;

    public BasePage (WebDriver driver) {

        this.driver = driver;
        wait = new FluentWait <WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .withMessage("Waiter Exception");
        PageFactory.initElements(driver, this);
        PropertyConfigurator.configure("src/log4j.properties");
        action = new Actions(driver);

    }

    public BasePage clickFunction(WebElement clickableElement){
        try {
            clickableElement.click();
        }catch(ElementNotInteractableException exception){
            log.error(LOGGER_ELEMENT_NOT_INTERACTABLE);
        }catch(InvalidSelectorException e1){
            log.error(LOGGER_INVALID_SELECTOR);
        }catch(NoSuchElementException e2){
            log.error("Clickable " + LOGGER_NO_ELEMENT);
        }

        return this;
    }

    public BasePage enterDataToField (WebElement field, String input){
        try {
            field.sendKeys(input);
        }catch(ElementNotInteractableException exception){
            log.error(LOGGER_ELEMENT_NOT_INTERACTABLE);
        }catch(InvalidSelectorException e1){
            log.error(LOGGER_INVALID_SELECTOR);
        }catch(NoSuchElementException e2){
            log.error("Writable " + LOGGER_NO_ELEMENT);
        }

        return this;
    }

    public String getTextFromWebElement(WebElement element) {
        try {
            element.getText();
        } catch (InvalidSelectorException e1) {
            log.error(LOGGER_INVALID_SELECTOR);
        } catch (NoSuchElementException e2) {
            log.error("Readable " + LOGGER_NO_ELEMENT);
        }

        return wait.until(ExpectedConditions.visibilityOf(element)).getText();

    }


}
