package uiTesting.pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage{

    public RegistrationPage(WebDriver driver) {

        super(driver);

    }

    @FindBy(css = "[data-testid=\"register-first-name\"]")
    WebElement firstNameField;

    @FindBy(css = "[data-testid=\"register-last-name\"]")
    WebElement lastNameField;

    @FindBy(css = "[data-testid=\"register-username\"]")
    WebElement usernameField;

    @FindBy(css = "[data-testid=\"register-email\"]")
    WebElement emailField;

    @FindBy(css = "[data-testid=\"register-password\"]")
    WebElement passwordField;

    @FindBy(css = "[data-testid=\"register-confirm-password\"]")
    WebElement confirmationPasswordField;

    @FindBy(css = "[data-testid=\"register-submit-btn\"]")
    WebElement signupButton;

    @FindBy(css = "[data-testid=\"login-link-register-page\"")
    WebElement loginButton;

    @FindBy(css = "[data-testid=\"first-name-input-error\"]")
    @Getter WebElement firstNameErrorMessage;

    @FindBy(css = "[data-testid=\"last-name-input-error\"]")
    @Getter WebElement lastNameErrorMessage;

    @FindBy(css = "[data-testid=\"username-input-error\"]")
    @Getter WebElement usernameErrorMessage;

    @FindBy(css = "[data-testid=\"register-email-input-error\"]")
    @Getter WebElement emailErrorMessage;

    @FindBy(css = "[data-testid=\"register-password-input-error\"]")
    @Getter WebElement passwordErrorMessage;

    @FindBy(css = "[data-testid=\"confirm-password-input-error\"]")
    @Getter WebElement confirmPasswordErrorMessage;

    @FindBy(css = "[data-testid=\"register-backend-input-error\"]")
    @Getter WebElement registrationErrorMessage;

    public RegistrationPage enterDataToFirstNameField(String input) {

        enterDataToField(firstNameField, input);
        return this;
    }

    public RegistrationPage enterDataToLastNameField(String input) {

        enterDataToField(lastNameField, input);
        return this;

    }

    public RegistrationPage enterDataToUsernameField(String input) {

        enterDataToField(usernameField, input);
        return this;
    }

    public RegistrationPage enterDataToEmailField(String input) {

        enterDataToField(emailField, input);
        return this;
    }

    public RegistrationPage enterDataToPasswordField(String input) {

        enterDataToField(passwordField, input);
        return this;
    }

    public RegistrationPage enterDataToConfirmationPasswordField(String input) {

        enterDataToField(confirmationPasswordField, input);
        return this;
    }

    public void clickSignupButton() {

        clickFunction(signupButton);

    }

    public void clickLoginButton() {

        clickFunction(loginButton);

    }

    public String getTextFromFirstNameErrorMessage() {

        return getTextFromWebElement(firstNameErrorMessage);

    }

    public String getTextFromLastNameErrorMessage() {

        return getTextFromWebElement(lastNameErrorMessage);

    }

    public String getTextFromUsernameErrorMessage() {

        return getTextFromWebElement(usernameErrorMessage);

    }

    public String getTextFromEmailErrorMessage() {

        return getTextFromWebElement(emailErrorMessage);

    }

    public String getTextFromPasswordErrorMessage() {

        return getTextFromWebElement(passwordErrorMessage);

    }

    public String getTextFromConfirmPasswordErrorMessage() {

        return getTextFromWebElement(confirmPasswordErrorMessage);

    }

    public String getTextFromRegistrationErrorMessage() {

        return getTextFromWebElement(registrationErrorMessage);

    }

}
