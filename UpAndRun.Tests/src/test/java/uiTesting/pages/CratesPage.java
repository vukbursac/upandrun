package uiTesting.pages;

import basePackage.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uiTesting.base.IWaitAndActionBuilder;

import java.util.List;

public class CratesPage extends BasePage implements IWaitAndActionBuilder{

    public CratesPage (WebDriver driver) {

        super(driver);
    }

    @FindBy (css = "[data-testid=\"crates-add-folder-button\"]")
    public WebElement addNewCrateIcon;

    @FindBy (css = "[data-testid=\"add-crate-form-input-field\"]")
    public WebElement newCrateNameField;

    @FindBy (css = "[data-testid=\"crate-form-add-button\"]")
    public WebElement addNewCrateButton;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_CRATE_NAME + "']")
    public WebElement testCrate1;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_CRATE_NAME + "']")
    public List<WebElement> testCrate1List;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_CRATE_NAME_2 + "']")
    public WebElement testCrate2;

    @FindBy (xpath = "//*[text()='" + Constants.INVALID_CHARACTERS_FOR_NAMING + "']")
    public List<WebElement> invalidTestCrateList;

    @FindBy (xpath = "//div[@class='FlyIn_flyIn__1NA5m FlyIn_show__1aF3k']")
    public WebElement flyInSection;

    @FindBy (xpath = "//div[@class='Backdrop_backdrop__1e0xY']")
    public WebElement backgroundSection;

    @FindBy (css = "[data-testid=\"crate-details-delete-button\"]")
    public WebElement deleteCrateButton;

    @FindBy (css = "[data-testid=\"crate-form-add-button\"]")
    public WebElement removeCrateButton;

    @FindBy (css = "[data-testid=\"edit-file-form-remove-button\"]")
    public WebElement removeFileButton;

    @FindBy (css = "[data-testid=\"crate-details-update-button\"]")
    public WebElement updateCrateNameButton;

    @FindBy (css = "[data-testid=\"file-details-update-button\"]")
    public WebElement updateFileNameButton;

    @FindBy (css = "[class=\"UpdateCrateForm_input__36mTj\"]")
    public WebElement renameCrateNameField;

    @FindBy (css = "[class=\"EditFileForm_input__1SsVU\"]")
    public WebElement renameFileNameField;

    @FindBy (css = "[data-testid=\"crate-form-add-button\"]")
    public WebElement renameCrateNameButton;

    @FindBy (css = "[data-testid=\"edit-file-form-rename-button\"]")
    public WebElement renameFileNameButton;

    @FindBy (css = "[class=\"CrateFolder_btn__2IacJ\"]")
    public WebElement addNewSubcrate_FileIcon;

    @FindBy (css = "[data-testid=\"crate-folder-add-file-button\"]")
    public WebElement addNewFileButton;

    @FindBy (css = "[data-testid=\"crate-folder-add-crate-button\"]")
    public WebElement addNewSubcrateButton;

    @FindBy (css = "[class=\"FileUpload_label__3i-6H\"]")
    public WebElement uploadFileSection;

    @FindBy (id = "file")
    public WebElement uploadFileInWindow;

    @FindBy (css = "[data-testid=\"file-upload-upload-button\"]")
    public WebElement uploadNewFileButton;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_FILE_NAME + "']")
    public WebElement testFile;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_RENAMED_FILE_NAME + ".txt']")
    public WebElement testRenamedFile;

    @FindBy (xpath = "//*[text()='" + Constants.VALID_TEST_FILE_NAME + "']")
    public List<WebElement> testFileList;

    @FindBy (xpath = "//*[text()='" + Constants.INVALID_SIZE_FILE_NAME + "']")
    public List<WebElement> invalidSizeTestFileList;

    @FindBy (css = "[data-testid=\"crate-details-download-button\"]")
    public WebElement downloadFileButton;


    public CratesPage clickAddNewCrateIcon() {

        clickFunction(addNewCrateIcon);
        return this;
    }

    public CratesPage clickBackgroundSection() {

        clickFunction(backgroundSection);
        return this;
    }

    public CratesPage enterPathForFileDownloadAndClickOpen(String path) {

        uploadFileInWindow.sendKeys(path);
        return this;
    }

    @Override
    public void waitForWebElementToBeClickable(WebElement element) {

        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    @Override
    public void waitAndClickWebElement(WebElement element) {

        wait.until(ExpectedConditions.elementToBeClickable(element));
        clickFunction(element);
    }

    /**
     * This method is used to click on crate or file, then on fly-in section which opens on the right side of the screen.
     * It is mandatory to make buttons on fly-in section interactable.
     * @param element1
     * @param element2
     */
    @Override
    public void waitAndClickFlyInSectionAndClickWebElement(WebElement element1, WebElement element2) {

        wait.until(ExpectedConditions.elementToBeClickable(element1));
        clickFunction(element1);
        wait.until(ExpectedConditions.elementToBeClickable(element2));
        clickFunction(element2);
    }

    @Override
    public void waitAndDeleteAndRemove(WebElement element1, WebElement element2) {

        wait.until(ExpectedConditions.elementToBeClickable(element1));
        clickFunction(element1);
        wait.until(ExpectedConditions.elementToBeClickable(element2));
        clickFunction(element2);
    }

    @Override
    public void waitAndClickWebElementAndEnterDataAndClickButton(WebElement element1, String string, WebElement element2) {

        wait.until(ExpectedConditions.elementToBeClickable(element1));
        (element1).clear();
        enterDataToField(element1, string);
        clickBackgroundSection();
        wait.until(ExpectedConditions.elementToBeClickable(element2));
        clickFunction(element2);
    }

    @Override
    public void waitAndDoubleClickWebElement(WebElement element) {

        wait.until(ExpectedConditions.elementToBeClickable(element));
        action.doubleClick(element).perform();
    }

    @Override
    public void waitAndHoverAndClickWebElement(WebElement element1, WebElement element2) {

        wait.until(ExpectedConditions.elementToBeClickable(element1));
        action.moveToElement(element1).perform();
        wait.until(ExpectedConditions.elementToBeClickable(element2));
        clickFunction(element2);
    }

    @Override
    public int numberOfWebElements(List<WebElement> elementList) {

        return elementList.size();
    }

    @Override
    public String waitAndGetNameFromWebElement(WebElement element) {

        wait.until(ExpectedConditions.elementToBeClickable(element));
        return getTextFromWebElement(element);
    }
}
