package uiTesting.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class UserDashboardPage extends BasePage{

    public UserDashboardPage (WebDriver driver) {

        super(driver);

    }

    @FindBy (css = "[data-testid=\"avatar-image\"]")
    public WebElement myAccountIcon;

    @FindBy (css = "[data-testid=\"account-nav-logout\"]")
    public WebElement signoutButton;

    @FindBy (css = "[data-testid=\"sidebar-services-span\"]")
    public WebElement servicesDropdownMenuButton;

    @FindBy(css = "[data-testid=\"sidebar-services-crates-link\"]")
    public WebElement cratesServicesButton;


    public WebElement waitForMyAccountIconToAppearAndGetText() {

        return wait.until(ExpectedConditions.visibilityOf(myAccountIcon));

    }

    public WebElement waitForServiceDropdownMenuButtonToBeClickable() {

        return wait.until(ExpectedConditions.elementToBeClickable(servicesDropdownMenuButton));

    }

    public UserDashboardPage clickMyAccountIcon() {

        clickFunction(myAccountIcon);
        return this;

    }

    public UserDashboardPage clickSignOutButton() {

        clickFunction(signoutButton);
        return this;

    }

    public UserDashboardPage clickServicesDropdownMenuButton() {

        clickFunction(servicesDropdownMenuButton);
        return this;

    }

    public UserDashboardPage clickCratesServicesButton() {

        clickFunction(cratesServicesButton);
        return this;

    }



}
