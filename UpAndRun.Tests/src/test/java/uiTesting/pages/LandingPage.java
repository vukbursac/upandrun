package uiTesting.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LandingPage extends BasePage {

    public LandingPage(WebDriver driver) {

        super(driver);

    }

    @FindBy (css = "[data-testid=\"login-button-landing-page\"]")
    WebElement loginButton;

    @FindBy (css = "[data-testid=\"register-button-landing-page\"]")
    WebElement signupButton;




    public LandingPage clickLoginButton() {

        clickFunction(loginButton);
        return this;

    }

    public LandingPage clickSignupButton() {

        clickFunction(signupButton);
        return this;

    }

}