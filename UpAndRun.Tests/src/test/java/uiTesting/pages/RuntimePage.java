package uiTesting.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RuntimePage extends BasePage {

    public RuntimePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[@href='/dashboard/runtime/lambda']")
    WebElement lambdaButton;


    public void clickOnLambdaButton() {
        wait.until(ExpectedConditions.elementToBeClickable(lambdaButton));
        clickFunction(lambdaButton);
    }
}
