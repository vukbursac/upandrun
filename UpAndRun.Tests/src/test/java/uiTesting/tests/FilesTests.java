package uiTesting.tests;

import basePackage.Constants;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;


public class FilesTests extends BaseTest{

    @BeforeMethod
    public void beforeEveryTest() throws InterruptedException {
        loginAndNavigateToCratesService();
    }

    @Test (priority = 1)

    public void uploadFile() throws InterruptedException {

        log.info("***  UPLOAD FILE TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testFile), Constants.VALID_TEST_FILE_NAME);
    }

    @Test (priority = 2)

    public void uploadTwoFilesWithTheSameName() throws InterruptedException {

        log.info("***  UPLOAD TWO FILES WITH THE SAME NAME TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        cratesPage.waitAndHoverAndClickWebElement(cratesPage.addNewSubcrate_FileIcon, cratesPage.addNewFileButton);
        cratesPage.waitForWebElementToBeClickable(cratesPage.uploadFileSection);
        cratesPage.enterPathForFileDownloadAndClickOpen(getFilePath(Constants.UPLOAD_FILE_PATH));
        cratesPage.waitAndClickWebElement(cratesPage.uploadNewFileButton);
        cratesPage.waitForWebElementToBeClickable(cratesPage.testFile);
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.testFileList) == 1));
    }

    @Test (priority = 3)

    public void uploadTooLargeFile() throws InterruptedException {

        log.info("***  UPLOAD TOO LARGE FILE TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_LARGE_FILE_PATH));
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.invalidSizeTestFileList) == 0));
    }

    @Test (priority = 5)

    public void deleteUploadedFile() throws InterruptedException {

        log.info("***  DELETE FILE TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        deleteFile();
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.testFileList) == 0));
    }

    @Test (priority = 10)

    public void renameFile() throws InterruptedException {

        log.info("***  RENAME FILE TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testFile, cratesPage.flyInSection);
        cratesPage.waitAndClickWebElement(cratesPage.updateFileNameButton);
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.renameFileNameField, Constants.VALID_TEST_RENAMED_FILE_NAME, cratesPage.renameFileNameButton);
        cratesPage.clickBackgroundSection();
        cratesPage.waitAndClickWebElement(cratesPage.renameFileNameButton);
        driverInitUtil.driver.navigate().refresh();
        cratesPage.waitAndClickWebElement(cratesPage.testRenamedFile);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testRenamedFile), Constants.VALID_TEST_RENAMED_FILE_NAME + ".txt");
    }

    @Test (priority = 11)

    public void renameFileWithInvalidCharacters() throws InterruptedException {

        log.info("***  RENAME FILE WITH INVALID CHARACTERS TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testFile, cratesPage.flyInSection);
        cratesPage.waitAndClickWebElement(cratesPage.updateFileNameButton);
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.renameFileNameField,
                Constants.INVALID_CHARACTERS_FOR_NAMING, cratesPage.renameFileNameButton);
        driverInitUtil.driver.navigate().refresh();
        cratesPage.waitAndClickWebElement(cratesPage.testFile);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testFile),
                Constants.VALID_TEST_FILE_NAME);
    }

    @Test (priority = 15)

    public void downloadFile() throws InterruptedException, IOException {

        log.info("***  DOWNLOAD FILE TEST START  ***");
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH));
        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testFile, cratesPage.flyInSection);
        cratesPage.waitAndClickWebElement(cratesPage.downloadFileButton);
        Thread.sleep(2000);
        driverInitUtil.driver.navigate().back();
        deleteTestCrate();
        createNewTestFile(getFilePath(Constants.UPLOAD_FILE_PATH_FOR_DOWNLOAD_CONFIRM));
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testFile), Constants.VALID_TEST_FILE_NAME);
    }


    @AfterMethod

    public void afterEveryTest(){
        driverInitUtil.driver.navigate().back();
        deleteTestCrate();
        log.info("*** FILES TEST END  ***");
    }

}
