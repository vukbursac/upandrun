package uiTesting.tests;

import basePackage.Constants;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CratesTests extends BaseTest {

    @BeforeMethod
    public void beforeEveryTest() throws InterruptedException {
        loginAndNavigateToCratesService();
    }

    @Test(priority = 1)

    public void createCrate() {

        log.info("***  CREATE NEW CRATE TEST START  ***");
        log.info("Click create crate icon, enter valid crate name and click add button");
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testCrate1), Constants.VALID_TEST_CRATE_NAME);
    }

    @Test(priority = 2)

    public void createSubcrate() {

        log.info("***  CREATE SUBCRATE TEST START  ***");
        log.info("Click create crate icon, enter valid crate name and click add button");
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        cratesPage.waitAndDoubleClickWebElement(cratesPage.testCrate1);
        cratesPage.waitAndHoverAndClickWebElement(cratesPage.addNewSubcrate_FileIcon, cratesPage.addNewSubcrateButton);
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.newCrateNameField, Constants.VALID_TEST_CRATE_NAME,
                cratesPage.addNewCrateButton);
        cratesPage.waitForWebElementToBeClickable(cratesPage.testCrate1);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testCrate1), Constants.VALID_TEST_CRATE_NAME);
    }

    @Test(priority = 3)

    public void createCrateWithInvalidCharactersName() {

        log.info("***  CREATE NEW CRATE WITH INVALID CHARCTERS TEST START  ***");
        log.info("Click create crate icon, enter valid crate name and click add button");
        cratesPage.clickAddNewCrateIcon();
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.newCrateNameField,
                Constants.INVALID_CHARACTERS_FOR_NAMING, cratesPage.addNewCrateButton);
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.invalidTestCrateList) == 0));
    }

    @Test(priority = 4)

    public void createTwoCratesWithTheSameName() {

        log.info("***  CREATE TWO CRATES WITH THE SAME NAME TEST START  ***");
        log.info("Click create crate icon, enter valid crate name and click add button");
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.testCrate1List) == 1));
    }

    @Test(priority = 5)

    public void deleteCrate() {

        log.info("***  DELETE CRATE TEST START  ***");
        log.info("Click crate, and click delete button");
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        deleteTestCrate();
        Assert.assertTrue((cratesPage.numberOfWebElements(cratesPage.testCrate1List) == 0));
    }

    @Test(priority = 10)

    public void renameCrate() {

        log.info("***  RENAME CRATE TEST START  ***");
        log.info("Create new crate with valid name, rename it with valid characters, and click rename button");
        createSubcrate();
        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testCrate1, cratesPage.flyInSection);
        cratesPage.waitAndClickWebElement(cratesPage.updateCrateNameButton);
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.renameCrateNameField,
                Constants.VALID_TEST_CRATE_NAME_2, cratesPage.renameCrateNameButton);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testCrate2), Constants.VALID_TEST_CRATE_NAME_2);
    }

    @Test(priority = 10)

    public void renameCrateWithInvalidCharacters() {

        log.info("***  RENAME CRATE WITH INVALID CHARACTERS TEST START  ***");
        log.info("Create new crate with valid name, rename it with invalid characters, and click rename button");
        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testCrate1, cratesPage.flyInSection);
        cratesPage.waitAndClickWebElement(cratesPage.updateCrateNameButton);
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.renameCrateNameField,
                Constants.INVALID_CHARACTERS_FOR_NAMING, cratesPage.renameCrateNameButton);
        Assert.assertEquals(cratesPage.waitAndGetNameFromWebElement(cratesPage.testCrate1), Constants.VALID_TEST_CRATE_NAME);
    }

    @AfterMethod

    public void afterEveryTest() {


        cratesPage.waitAndClickWebElement(userDashboardPage.servicesDropdownMenuButton);
        cratesPage.waitAndClickWebElement(userDashboardPage.cratesServicesButton);
        if(cratesPage.numberOfWebElements(cratesPage.testCrate1List) > 0){
            deleteTestCrate();
        }
            log.info("*** CRATES TEST END  ***");
        }
    }

