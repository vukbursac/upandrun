package uiTesting.tests;

import basePackage.Constants;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RuntimeTests extends BaseTest {

    @BeforeMethod
    public void beforeEachTest() {
        loginBeforeMethodSteps();
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123);
        navigationSideMenu.clickOnServicesDropdown()
                .clickOnRuntimeService();
        runtimePage.clickOnLambdaButton();
    }

    @Test(priority = 1)
    public void uploadNewLambdaTest() {
        lambdaPage.uploadLambda(getSumLambdaAbsolutePath());
        Assert.assertTrue(lambdaPage.lambdaNameMatches(Constants.TESTING_LAMBDA_NAME));
    }

    @Test(priority = 2)
    public void runGetMethodLambdaTest() throws InterruptedException {
        lambdaPage.uploadLambda(getSumLambdaAbsolutePath());
        lambdaPage.clickOnLambdaLink(Constants.TESTING_LAMBDA_NAME);
        lambdaPage.clickOnGetMethodButton();
        lambdaPage.clickOnRunLambdaButton();
        Thread.sleep(3000);
        Assert.assertTrue(lambdaPage.outputWindowTextPresent());
    }


    @Test(priority = 100)
    public void deleteLambdaTest() {
        lambdaPage.uploadLambda(getSumLambdaAbsolutePath());
        lambdaPage.clickOnDeleteLambdaIcon(Constants.TESTING_LAMBDA_NAME);
        driverInitUtil.driver.navigate().refresh();
        Assert.assertFalse(lambdaPage.lambdaNameMatches(Constants.TESTING_LAMBDA_NAME));
    }

    @AfterMethod
    public void afterEachTest(){
        lambdaPage.clickOnDeleteLambdaIcon(Constants.TESTING_LAMBDA_NAME);
    }

}
