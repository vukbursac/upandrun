package uiTesting.tests;

import basePackage.Constants;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class LoginTests extends BaseTest {

    @BeforeMethod
    public void beforeEveryTest() {
        navigateToLoginPage();
    }

    @Test (priority = 1)
    public void validLogin() {

        log.info("***  VALID LOGIN TEST START  ***");
        log.info("Enter correct email and password and click login button");
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123);
        Assert.assertTrue(userDashboardPage.waitForMyAccountIconToAppearAndGetText().isDisplayed());
        log.info("Signout");
        signout();

    }

    @Test (priority = 5)

    public void loginWithIncorrectPassword () {

        log.info("***  INVALID LOGIN WITH INCORRECT PASSWORD TEST START  ***");
        log.info("Enter correct email and incorrect password and click login button");
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM, "aaaaaa");
        Assert.assertEquals(loginPage.getTextFromLoginError(), "Incorrect password.");

    }

    @Test (priority = 10)

    public void loginWithIncorrectEmail () {

        log.info("***  INVALID LOGIN WITH INCORRECT EMAIL TEST START  ***");
        log.info("Enter incorrect email and correct password and click login button");
        login(Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123);
        Assert.assertEquals(loginPage.getTextFromLoginError(), "Email: janedoe1@example.com does not exists.");

    }

    @Test (priority = 15)

    public void loginWithInvalidEmailFormat () {

        log.info("***  INVALID LOGIN WITH INVALID EMAIL FORMAT TEST START  ***");
        log.info("Enter invalid email and correct password and click login button");
        login("janedoeexamplecom", Constants.PASSWORD_AAA123);
        Assert.assertEquals(loginPage.getTextFromInvalidEmailError(), "Invaild email, missing characters!");

    }

    @Test (priority = 20)

    public void loginWithIncorrectEmailAndPassword () {

        log.info("***  INVALID LOGIN WITH INCORRECT EMAIL AND PASSWORD TEST START  ***");
        log.info("Enter incorrect email and incorrect password and click login button");
        login(Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "aaaaaa");
        Assert.assertEquals(loginPage.getTextFromLoginError(), "Email: janedoe1@example.com does not exists.");

    }

    @Test (priority = 25)

    public void loginWithBlankEmail () {

        log.info("***  INVALID LOGIN WITH BLANK EMAIL TEST START  ***");
        log.info("Enter blank email and correct password and click login button");
        login("", Constants.PASSWORD_AAA123);
        Assert.assertEquals(loginPage.getTextFromInvalidEmailError(), "Please enter your email!");

    }

    @Test (priority = 30)

    public void loginWithBlankPassword () {

        log.info("***  INVALID LOGIN WITH BLANK PASSWORD TEST START  ***");
        log.info("Enter correct email and blank password and click login button");
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM, "");
        Assert.assertEquals(loginPage.getTextFromInvalidPasswordError(), "Please enter your password!");

    }

    @Test (priority = 35)

    public void loginWithEmailAddressInUpperCase() {

        log.info("***  VALID LOGIN WITH ALL UPPERCASE LETTERS IN EMAIL TEST START  ***");
        log.info("Enter email with all uppercase letters and correct password and click login button");
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM.toUpperCase(), Constants.PASSWORD_AAA123);
        Assert.assertTrue(userDashboardPage.waitForMyAccountIconToAppearAndGetText().isDisplayed());

    }

    @AfterMethod

     public void afterEveryTest(){

        log.info("***  LOGIN TEST END   ***");
    }

}




