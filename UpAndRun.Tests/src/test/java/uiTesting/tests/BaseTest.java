package uiTesting.tests;

import basePackage.Constants;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import uiTesting.base.DriverInitUtil;
import uiTesting.pages.CratesPage;
import uiTesting.pages.LoginPage;
import uiTesting.pages.RegistrationPage;
import uiTesting.pages.LandingPage;
import uiTesting.pages.UserDashboardPage;
import uiTesting.pages.NavigationSideMenu;
import uiTesting.pages.LambdaPage;
import uiTesting.pages.RuntimePage;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;




public class BaseTest {

    static Logger log = Logger.getLogger(BaseTest.class);

    DriverInitUtil driverInitUtil;
    LandingPage landingPage;
    LoginPage loginPage;
    RegistrationPage registrationPage;
    UserDashboardPage userDashboardPage;
    RuntimePage runtimePage;
    LambdaPage lambdaPage;
    CratesPage cratesPage;
    String baseUrl;
    Wait<WebDriver> wait;
    NavigationSideMenu navigationSideMenu;



    @BeforeClass

    public void beforeAllTests () throws IOException {

        PropertyConfigurator.configure("src/log4j.properties");
        java.io.InputStream is = this.getClass().getResourceAsStream("maven.properties");
        java.util.Properties props = new Properties();
        props.load(BaseTest.class.getClassLoader().getResourceAsStream("maven.properties"));
        String browser = props.getProperty("browser");
        driverInitUtil = new DriverInitUtil();
        driverInitUtil.webDriverSetup(browser);
        wait= new FluentWait<WebDriver>(driverInitUtil.driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .withMessage("Waiter Exception");
        landingPage = new LandingPage(driverInitUtil.driver);
        loginPage = new LoginPage(driverInitUtil.driver);
        registrationPage = new RegistrationPage(driverInitUtil.driver);
        userDashboardPage = new UserDashboardPage(driverInitUtil.driver);
        runtimePage = new RuntimePage(driverInitUtil.driver);
        lambdaPage = new LambdaPage(driverInitUtil.driver);
        navigationSideMenu = new NavigationSideMenu(driverInitUtil.driver);
        cratesPage = new CratesPage(driverInitUtil.driver);
        baseUrl = "http://localhost:3000/";
        log.info("### UI UP`N`RUN TESTING START ###");
    }

    public void login (String email, String password) {

        loginPage.enterDataToEmailField(email)
                .enterDataToPasswordField(password)
                .clickLoginButton();
    }

    public void registration(String firstName, String lastName, String username, String emailAddress, String password, String confirmPassword){
        registrationPage.enterDataToFirstNameField(firstName)
                .enterDataToLastNameField(lastName)
                .enterDataToUsernameField(username)
                .enterDataToEmailField(emailAddress)
                .enterDataToPasswordField(password)
                .enterDataToConfirmationPasswordField(confirmPassword)
                .clickSignupButton();
    }

    public void acceptPopupMessage () {

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            driverInitUtil.driver.switchTo().alert().accept();
            log.info("Popup message clicked");
        }catch(Exception e){
            log.error("Popup message didn't appear");
        }

    }

    public void signout(){

        userDashboardPage.clickMyAccountIcon()
                         .clickSignOutButton();
    }

    public void createNewTestCrate(String crateName){

        cratesPage.clickAddNewCrateIcon();
        cratesPage.waitAndClickWebElementAndEnterDataAndClickButton(cratesPage.newCrateNameField, crateName,
                cratesPage.addNewCrateButton);
        driverInitUtil.driver.navigate().refresh();
        cratesPage.waitForWebElementToBeClickable(cratesPage.testCrate1);
    }

    public void deleteTestCrate(){

        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testCrate1, cratesPage.flyInSection);
        cratesPage.waitAndDeleteAndRemove(cratesPage.deleteCrateButton, cratesPage.removeCrateButton);
        driverInitUtil.driver.navigate().refresh();
    }

    public void createNewTestFile(String path) throws InterruptedException {

        createNewTestCrate(Constants.VALID_TEST_CRATE_NAME);
        cratesPage.waitAndDoubleClickWebElement(cratesPage.testCrate1);
        cratesPage.waitAndHoverAndClickWebElement(cratesPage.addNewSubcrate_FileIcon, cratesPage.addNewFileButton);
        cratesPage.waitForWebElementToBeClickable(cratesPage.uploadFileSection);
        cratesPage.enterPathForFileDownloadAndClickOpen(path);
        cratesPage.waitAndClickWebElement(cratesPage.uploadNewFileButton);
        driverInitUtil.driver.navigate().refresh();
    }

    public void deleteFile() throws InterruptedException {

        cratesPage.waitAndClickFlyInSectionAndClickWebElement(cratesPage.testFile, cratesPage.flyInSection);
        cratesPage.waitAndDeleteAndRemove(cratesPage.deleteCrateButton, cratesPage.removeFileButton);
        driverInitUtil.driver.navigate().refresh();
    }

    public String getFilePath(String path){
        File file = new File(path);
        return file.getAbsolutePath();
    }

    public void checkIfFirstNameErrorMessageIsDisplayedAndVerifyIt (){

        Assert.assertTrue(registrationPage.getFirstNameErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromFirstNameErrorMessage(), Constants.UI_ERROR_MESSAGE_LOWERCASE_FIRST_NAME);

    }

    public void checkIfLastNameErrorMessageIsDisplayedAndVerifyIt () {

        Assert.assertTrue(registrationPage.getLastNameErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromLastNameErrorMessage(), Constants.UI_ERROR_MESSAGE_LOWERCASE_LAST_NAME);

    }

    public void checkIfUsernameErrorMessageIsDisplayedAndVerifyIt () {

        Assert.assertTrue(registrationPage.getUsernameErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromUsernameErrorMessage(),
                "Username must contain only letters, numbers, and special characters (only: _ and -) with no white space");

    }

    public void checkIfPasswordErrorMessageIsDisplayedAndVerifyIt () {

        Assert.assertTrue(registrationPage.getPasswordErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromPasswordErrorMessage(), Constants.UI_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);

    }

    public void beforeMethodInitialSteps(){

        driverInitUtil.driver.navigate().to(baseUrl);
        log.info("Open Up`N`Run landing page");
        driverInitUtil.driver.manage().window().maximize();
        log.info("Maximize window size");
    }

    public void navigateToLoginPage(){

        beforeMethodInitialSteps();
        landingPage.clickLoginButton();
        log.info("Navigate to login page");
    }

    public void navigateToRegistrationPage(){

        beforeMethodInitialSteps();
        landingPage.clickSignupButton();
        log.info("Navigate to registration page");
    }

    public void loginAndNavigateToCratesService() throws InterruptedException {

        beforeMethodInitialSteps();
        landingPage.clickLoginButton();
        login(Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123);
        userDashboardPage.waitForServiceDropdownMenuButtonToBeClickable();
        userDashboardPage.clickServicesDropdownMenuButton();
        userDashboardPage.clickCratesServicesButton();
        log.info("Navigate to crates service page");
    }

    public String getSumLambdaAbsolutePath(){
        File sumLambda = new File(Constants.SUM_LAMBDA_PATH);
        return sumLambda.getAbsolutePath();
    }

    @AfterClass

    public void afterAllTest() throws IOException {

        FileUtils.deleteDirectory(new File(getFilePath(Constants.DOWNLOADED_FILE_PATH)));

        try {
            driverInitUtil.driver.quit();
            log.info("Close window");
        }catch(Exception e){
            log.error("Window failed to close");
        }
        log.info("### UI UP`N`RUN TESTING END ###");
        log.info("=================================================================================");
        log.info("                                                                                 ");

    }

}