package uiTesting.tests;

import basePackage.Constants;
import basePackage.TestDataGenerator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegistrationTests extends BaseTest {

    @BeforeMethod
    public void beforeEachTest() {

        navigateToRegistrationPage();
    }

    @Test(priority = 1)
    public void validRegistration() {
        log.info("***  VALID REGISTRATION TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, TestDataGenerator.getUsername(), TestDataGenerator.getEmail(), Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        acceptPopupMessage();
        Assert.assertEquals(driverInitUtil.driver.getCurrentUrl(), "http://localhost:3000/login");

    }

    @Test(priority = 5)
    public void invalidRegistrationInvalidEmailFormat() {
        log.info("***  INVALID REGISTRATION WITH INVALID EMAIL TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, invalid email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOE, "janedoeexample.com", Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        Assert.assertTrue(registrationPage.getEmailErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromEmailErrorMessage(), "Email is invalid");
    }


    @Test(priority = 10)
    public void invalidRegistrationRegisteredEmail() {
        log.info("***  INVALID REGISTRATION WITH ALREADY REGISTERED EMAIL TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, already registered email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        Assert.assertTrue(registrationPage.getRegistrationErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromRegistrationErrorMessage(), "User with email: " + Constants.EMAIL_JANEDOE_EXAMPLE_COM + " already exists.");
    }

    @Test(priority = 15)
    public void invalidRegistrationBlankEmail() {
        log.info("***  INVALID REGISTRATION WITH BLANK EMAIL TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, blank email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, "", Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        Assert.assertTrue(registrationPage.getEmailErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromEmailErrorMessage(), "Email is required");
    }

    @Test(priority = 20)
    public void invalidRegistrationInvalidUsernameFormatUnauthorizedSpecialCharacter() {
        log.info("***  INVALID REGISTRATION WITH INVALID USERNAME WITH UNAUTHORIZED SPECIAL CHARACTER TEST START  ***");
        log.info("Enter correct first name, correct last name, invalid username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, "jane.doe", Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfUsernameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 30)
    public void invalidRegistrationRegisteredUsername() {
        log.info("***  INVALID REGISTRATION WITH ALREADY REGISTERED USERNAME TEST START  ***");
        log.info("Enter correct first name, correct last name, already registered username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOE, Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        Assert.assertTrue(registrationPage.getRegistrationErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromRegistrationErrorMessage(), "User with username: " + Constants.USERNAME_JANEDOE + " already exists.");
    }

    @Test(priority = 35)
    public void invalidRegistrationBlankUsername() {
        log.info("***  INVALID REGISTRATION WITH BLANK USERNAME TEST START  ***");
        log.info("Enter correct first name, correct last name, blank username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, "", Constants.EMAIL_JANEDOE_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfUsernameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 40)
    public void invalidRegistrationBlankPasswordAndConfirmationPassword() {
        log.info("***  INVALID REGISTRATION WITH BLANK PASSWORD AND CONFIRMATION PASSWORD TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, blank password, and blank confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "", "");
        Assert.assertTrue(registrationPage.getPasswordErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromPasswordErrorMessage(), Constants.UI_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT);
        Assert.assertEquals(registrationPage.getTextFromConfirmPasswordErrorMessage(), "Confirm password is required");
    }

    @Test(priority = 45)
    public void invalidRegistrationBlankConfirmationPassword() {
        log.info("***  INVALID REGISTRATION WITH BLANK CONFIRMATION PASSWORD TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, correct password, and blank confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, "");
        Assert.assertTrue(registrationPage.getConfirmPasswordErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromConfirmPasswordErrorMessage(), "Confirm password is required");
    }

    @Test(priority = 50)
    public void invalidRegistrationBlankPassword() {
        log.info("***  INVALID REGISTRATION WITH BLANK PASSWORD TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, blank password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "", Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 55)
    public void invalidRegistrationFiveCharactersPassword() {
        log.info("***  INVALID REGISTRATION WITH FIVE CHARACTERS PASSWORD AND CONFIRMATION PASSWORD TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, five characters password, and five characters confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "Aa123", "Aa123");
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }


    @Test(priority = 60)
    public void invalidRegistrationUnmatchingPasswordAndConfirmationPassword() {
        log.info("***  INVALID REGISTRATION WITH UNMATCHING PASSWORD AND CONFIRMATION PASSWORD TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, correct password, and unmatching confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, "aaa123");
        Assert.assertTrue(registrationPage.getConfirmPasswordErrorMessage().isDisplayed());
        Assert.assertEquals(registrationPage.getTextFromConfirmPasswordErrorMessage(), "Passwords do not match");

    }

    @Test(priority = 62)
    public void invalidRegistrationBlankFirstName() {
        log.info("***  INVALID REGISTRATION WITH BLANK FIRST NAME TEST START  ***");
        log.info("Enter blank first name, correct last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration("", Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfFirstNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 64)
    public void invalidRegistrationLowerCaseFirstName() {
        log.info("***  INVALID REGISTRATION WITH FIRST NAME WITH ALL LOWER CASE LETTERS TEST START  ***");
        log.info("Enter first name with all lower case letters, correct last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration("jane", Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfFirstNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 65) //Test fails because error message isn't implemented yet
    public void invalidRegistrationSpecialCharactersStartFirstName() {
        log.info("***  INVALID REGISTRATION WITH FIRST NAME STARTED WITH SPECIAL CHARACTER TEST START  ***");
        log.info("Enter first name started with special character, correct last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration("!Jane", Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfFirstNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 66) //Test fails because error message isn't implemented yet
    public void invalidRegistrationNumericCharactersStartFirstName() {
        log.info("***  INVALID REGISTRATION WITH FIRST NAME STARTED WITH NUMERIC CHARACTER TEST START  ***");
        log.info("Enter first name started with numeric character, correct last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration("6Jane", Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfFirstNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 67)
    public void invalidRegistrationBlankLastName() {
        log.info("***  INVALID REGISTRATION WITH BLANK LAST NAME TEST START  ***");
        log.info("Enter correct first name, blank last name, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, "", Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfLastNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 70)
    public void invalidRegistrationLowerCaseLastName() {
        log.info("***  INVALID REGISTRATION WITH LAST NAME WITH ALL LOWER CASE LETTERS TEST START  ***");
        log.info("Enter correct first name, last name with all lower case letters, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, "doe", Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfLastNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 71) //Test fails because error message isn't implemented yet
    public void invalidRegistrationSpecialCharactersStartLastName() {
        log.info("***  INVALID REGISTRATION WITH LAST NAME STARTED WITH SPECIAL CHARACTER TEST START  ***");
        log.info("Enter correct first name, last name started with special character, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, "!Doe", Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfLastNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 72) //Test fails because error message isn't implemented yet
    public void invalidRegistrationNumericCharactersStartLastName() {
        log.info("***  INVALID REGISTRATION WITH LAST NAME STARTED WITH NUMERIC CHARACTER TEST START  ***");
        log.info("Enter correct first name, last name started with numeric character, correct username, correct email, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, "6Doe", Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        checkIfLastNameErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 75)
    public void registrationWithEmailAddressInUpperCase() throws InterruptedException {
        log.info("***  VALID REGISTRATION WITH EMAIL WITH ALL UPPER CASE CASE LETTERS TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, email with all upper case letters, correct password, and correct confirmation password and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, TestDataGenerator.getUsername(), TestDataGenerator.getEmail().toUpperCase(), Constants.PASSWORD_AAA123, Constants.CONFIRMATION_PASSWORD_AAA123);
        acceptPopupMessage();
        Thread.sleep(2000);
        Assert.assertEquals(driverInitUtil.driver.getCurrentUrl(), "http://localhost:3000/login");

    }

    @Test(priority = 80)
    public void invalidRegistrationPasswordLowerCaseLettersOnly() {
        log.info("***  INVALID REGISTRATION WITH PASSWORD AND CONFIRMATION PASSWORD WITH ALL LOWER CASE LETTERS TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, password with all lower case letters, confirmation password with all lower case letters and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "aaaaaa", "aaaaaa");
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 85)
    public void invalidRegistrationPasswordUpperCaseLettersOnly() {
        log.info("***  INVALID REGISTRATION WITH PASSWORD AND CONFIRMATION PASSWORD WITH ALL UPPER CASE LETTERS TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, password with all upper case letters, confirmation password with all upper case letters and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "AAAAAA", "AAAAAA");
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 90)
    public void invalidRegistrationPasswordNumbersOnly() {
        log.info("***  INVALID REGISTRATION WITH PASSWORD AND CONFIRMATION PASSWORD WITH ALL NUMBERS TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, password with all numbers, confirmation password with all numbers and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "111111", "111111");
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }

    @Test(priority = 95)
    public void invalidRegistrationPasswordSpecialCharactersOnly() {
        log.info("***  INVALID REGISTRATION WITH PASSWORD AND CONFIRMATION PASSWORD WITH ALL SPECIAL CHARACTERS TEST START  ***");
        log.info("Enter correct first name, correct last name, correct username, correct email, password with all special characters, confirmation password with special characters and click sign up button");
        registration(Constants.FIRST_NAME_JANE, Constants.LAST_NAME_DOE, Constants.USERNAME_JANEDOEE, Constants.EMAIL_JANEDOE1_EXAMPLE_COM, "!!!!!!", "!!!!!!");
        checkIfPasswordErrorMessageIsDisplayedAndVerifyIt();
    }

    @AfterMethod
    public void afterEveryClass() {
        log.info("***  REGISTRATION TEST END   ***");
    }
}

