package uiTesting.base;

import basePackage.Constants;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class DriverInitUtil {

    public WebDriver driver;

    public DriverInitUtil() {


    }

    public String getFilePath(String path){
        File file = new File(path);
        return file.getAbsolutePath();
    }

    public WebDriver webDriverSetup(String browser) {

        browser = browser.toLowerCase();

        switch (browser) {
            case "chrome":

                WebDriverManager.chromedriver().setup();
                String downloadFilepath = getFilePath(Constants.DOWNLOADED_FILE_PATH);
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", downloadFilepath);
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                driver = new ChromeDriver(options);
                break;

            case "firefox":

                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;

            case "edge":

                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;

            case "internet-explorer":

                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;

            default:

                System.out.println("Invalid input for browser type, please try again.");
                break;
        }

        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        return driver;

    }

}