package uiTesting.base;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface IWaitAndActionBuilder {

    void waitForWebElementToBeClickable(WebElement element);

    void waitAndClickWebElement(WebElement element);

    void waitAndClickFlyInSectionAndClickWebElement(WebElement element1, WebElement element2);

    void waitAndDeleteAndRemove(WebElement element1, WebElement element2);

    void waitAndClickWebElementAndEnterDataAndClickButton(WebElement element1, String string, WebElement element2);

    void waitAndDoubleClickWebElement(WebElement element);

    void waitAndHoverAndClickWebElement(WebElement element1, WebElement element2);

    int numberOfWebElements(List<WebElement> elementList);

    String waitAndGetNameFromWebElement(WebElement element);
}
