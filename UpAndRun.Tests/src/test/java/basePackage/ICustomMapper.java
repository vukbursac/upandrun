package basePackage;

import io.restassured.response.Response;

public interface ICustomMapper {

    Object fromJson(Response response);

}
