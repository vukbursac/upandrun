package basePackage;

import org.apache.commons.lang3.RandomStringUtils;

public class TestDataGenerator {

    public static String getUsername(){
        String generatedString = RandomStringUtils.randomAlphabetic(3);
        return (Constants.USERNAME_JANEDOE + generatedString);
    }

    public static String getEmail(){
        String generatedString = RandomStringUtils.randomNumeric(3);
        return ("janedoe" + generatedString + "@example.com");
    }

    public static String removeFirstLetterOfAString(String stringToRemoveLetter) {

        return stringToRemoveLetter.substring(1);
    }

}
