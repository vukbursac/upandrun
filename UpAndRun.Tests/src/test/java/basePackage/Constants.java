package basePackage;

public class Constants {

    //Input data
    public static final String FIRST_NAME_JANE = "Jane";
    public static final String LAST_NAME_DOE = "Doe";
    public static final String USERNAME_JANEDOE = "janedoe";
    public static final String USERNAME_JANEDOEE = "janedoee";
    public static final String EMAIL_JANEDOE_EXAMPLE_COM = "janedoe@example.com";
    public static final String EMAIL_JANEDOE1_EXAMPLE_COM = "janedoe1@example.com";
    public static final String PASSWORD_AAA123 = "Aaa123";
    public static final String CONFIRMATION_PASSWORD_AAA123 = "Aaa123";
    public static final String VALID_TEST_CRATE_NAME = "Test Crate";
    public static final String VALID_TEST_CRATE_NAME_2 = "Second Test Crate";
    public static final String VALID_TEST_FILE_NAME = "Test file.txt";
    public static final String VALID_TEST_RENAMED_FILE_NAME = "Test file 2";
    public static final String INVALID_CHARACTERS_FOR_NAMING = "<>";
    public static final String INVALID_SIZE_FILE_NAME = "50MB.txt";



    //Properties
    public static final String RESPONSE_ERROR = "error";
    public static final String ERRORS_EMAIL = "errors.Email";
    public static final String ERRORS_EMAIL_0 = "errors.Email[0]";
    public static final String ERRORS_PASSWORD = "errors.Password";
    public static final String ERRORS_USERNAME = "errors.Username[0]";
    public static final String ERRORS_CONFIRM_PASSWORD = "errors.ConfirmPassword";
    public static final String ERRORS_CONFIRM_PASSWORD_0 = "errors.ConfirmPassword[0]";
    public static final String ERRORS_FIRST_NAME_0 = "errors.FirstName[0]";
    public static final String ERRORS_LAST_NAME_0 = "errors.LastName[0]";

    //UI error messages
    public static final String UI_ERROR_MESSAGE_LOWERCASE_FIRST_NAME = "First name must start with an uppercase letter";
    public static final String UI_ERROR_MESSAGE_LOWERCASE_LAST_NAME = "Last name must start with an uppercase letter";
    public static final String UI_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT = "Enter a combination of at least six characters, with at least one uppercase, one lowercase, and one number";


    //API error messages
    public static final String API_ERROR_MESSAGE_REQUIRED_EMAIL = "E-mail is required.";
    public static final String API_ERROR_MESSAGE_REQUIRED_PASSWORD = "Password is required.";
    public static final String API_ERROR_MESSAGE_REQUIRED_CONFIRM_PASSWORD = "Confirm password is required.";
    public static final String API_ERROR_MESSAGE_UNMATCHING_CONFIRM_PASSWORD = "Password and confirmation password do not match";
    public static final String API_ERROR_MESSAGE_INVALID_USERNAME_FORMAT = "Only letters, numbers, - and _ are allowed.";
    public static final String API_ERROR_MESSAGE_INVALID_PASSWORD_FORMAT = "Password must contain at least one lowercase letter, one uppercase letter and one number.";
    public static final String API_ERROR_MESSAGE_PASSWORD_BELLOW_MINIMUM_CHAR_LIMIT = "Password must contain at least 6 characters.";
    public static final String API_ERROR_MESSAGE_LOWERCASE_FIRST_NAME = "First name must start with an uppercase letter.";
    public static final String API_ERROR_MESSAGE_LOWERCASE_LAST_NAME = "Last name must start with an uppercase letter.";
    public static final String API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_FIRST_NAME = "The field FirstName must be a string or array type with a maximum length of '50'.";
    public static final String API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_LAST_NAME = "The field LastName must be a string or array type with a maximum length of '50'.";
    public static final String API_ERROR_MESSAGE_EXCEEDED_CHAR_NUMBER_USERNAME = "Username must not be longer than 32 characters.";

    //Logger Messages

    public static final String LOGGER_ELEMENT_NOT_INTERACTABLE = "It is impossible to interact with WebElement.";
    public static final String LOGGER_INVALID_SELECTOR = "Selector used to find an element does not return a WebElement.";
    public static final String LOGGER_NO_ELEMENT= "WebElement could not be found.";

    //Files
    public static final String FILE_PATH = "src\\test\\resources\\Files\\TestFile.txt";
    public static final String UPLOAD_FILE_PATH = "src\\test\\resources\\Files\\Test file.txt";
    public static final String UPLOAD_FILE_PATH_FOR_DOWNLOAD_CONFIRM = "src\\test\\resources\\DownloadedFiles\\Test file.txt";
    public static final String UPLOAD_LARGE_FILE_PATH = "src\\test\\resources\\Files\\50MB.txt";

    //Paths
    public static final String DOWNLOADED_FILE_PATH = "src\\test\\resources\\DownloadedFiles";
    public static final String TESTING_LAMBDA_NAME = "janedoe/sum.rar";
    public static final String SUM_LAMBDA_PATH = "src/test/resources/Files/Sum.rar";





}