package com.requests

import com.config.ConstantsData.ENDPOINTS.API_REGISTRATION
import com.config.ConstantsData._
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object RegistrationRequest {

    def userRegistrationRequest() = {
            feed(REGISTRATION_USER_DATA.registrationRequestBodyFeeder)
              .exec(http("Register New User")
                .post(API_BASE_URL + API_REGISTRATION)
                .body(ElFileBody("bodies/ValidUserAuthenticationData.json")).asJson
                .check(status.is(200)))
        }

}