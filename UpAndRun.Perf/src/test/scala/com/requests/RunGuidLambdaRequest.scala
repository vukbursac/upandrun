package com.requests

import com.config.ConstantsData.ENDPOINTS.{API_GUID_LAMBDA_ID, API_LOGIN, API_RUN_LAMBDA, API_USER_ID}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import com.config.ConstantsData._
import com.requests.LoginRequest.loginBodyFeeder


object RunGuidLambdaRequest {

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(ElFileBody("bodies/TestUserLoginData.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))
  }

  def runGuidLambdaRequest() = {
    exec(http("Run a Guid Lambda")
      .post(API_BASE_URL + API_RUN_LAMBDA + API_USER_ID + "/lambdas" + API_GUID_LAMBDA_ID)
      .headers(Map("Authorization" -> "Bearer ${token}"))
      .formParam("imageName", "janedoe/guid.rar")
      .check(status.is(200)))

  }

}
