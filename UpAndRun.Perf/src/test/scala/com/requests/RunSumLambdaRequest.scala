package com.requests

import com.config.ConstantsData.ENDPOINTS.{API_LOGIN, API_RUN_LAMBDA, API_SUM_LAMBDA_ID, API_USER_ID}
import com.config.ConstantsData._
import com.requests.LoginRequest.loginBodyFeeder
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object RunSumLambdaRequest {

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(ElFileBody("bodies/TestUserLoginData.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))
  }

  def runSumLambdaRequest() = {
    exec(http("Run a Sum Lambda")
      .post(API_BASE_URL + API_RUN_LAMBDA + API_USER_ID + "/lambdas" + API_SUM_LAMBDA_ID)
      .headers(Map("Authorization" -> "Bearer ${token}"))
      .formParamMap(Map("imageName" -> "janedoe/sum.rar", "param1" -> "43", "param2" -> "26"))
      .check(status.is(200)))

  }

}
