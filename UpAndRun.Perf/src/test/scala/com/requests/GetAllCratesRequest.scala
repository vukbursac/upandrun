package com.requests

import com.config.ConstantsData.API_BASE_URL
import com.config.ConstantsData.ENDPOINTS.{API_GET_ALL_CRATES, API_LOGIN}
import com.requests.LoginRequest.loginBodyFeeder
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object GetAllCratesRequest {

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(StringBody("""{"email": "${email}", "password": "${password}"}""")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))
  }

  def getAllCrates() = {
      exec(http("Get All Crates")
       .get(API_BASE_URL + API_GET_ALL_CRATES)
       .headers(Map("Authorization" -> "Bearer ${token}"))
       .check(status.is(200)))
  }

}