package com.requests

import com.config.ConstantsData.CRATE_NAME.createCrateRequestBodyFeeder
import com.config.ConstantsData.ENDPOINTS.{API_CREATE_CRATE, API_LOGIN}
import com.config.ConstantsData._
import com.requests.LoginRequest.loginBodyFeeder
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CreateCrateRequest{

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(ElFileBody("bodies/TestUserLoginData.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))

  }

  def userCreateCrateRequest() = {
    feed(createCrateRequestBodyFeeder)
      .exec(http("Create New Crate")
        .post(API_BASE_URL + API_CREATE_CRATE)
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .body(ElFileBody("bodies/CreateCrate.json")).asJson
        .check(status.is(200)))

  }

}