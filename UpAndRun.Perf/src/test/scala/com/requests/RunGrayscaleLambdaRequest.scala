package com.requests

import com.config.ConstantsData.ENDPOINTS.{API_GRAYSCALE_LAMBDA_ID, API_LOGIN, API_RUN_LAMBDA, API_USER_ID}
import com.config.ConstantsData._
import com.requests.LoginRequest.loginBodyFeeder
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object RunGrayscaleLambdaRequest {

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(ElFileBody("bodies/TestUserLoginData.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))
  }

  def runGrayscaleLambdaRequest() = {
    exec(http("Run a Grayscale Lambda")
      .post(API_BASE_URL + API_RUN_LAMBDA + API_USER_ID + "/lambdas" + API_GRAYSCALE_LAMBDA_ID)
      .headers(Map("Authorization" -> "Bearer ${token}"))
      .formParam("imageName", "janedoe/grayscale.rar")
      .formUpload("", "files/picture.jpg")
      .check(status.is(200)))

  }

}
