package com.requests

import com.config.ConstantsData.API_BASE_URL
import com.config.ConstantsData.CRATE_NAME.createCrateRequestBodyFeeder
import com.config.ConstantsData.ENDPOINTS.{API_CREATE_CRATE, API_LOGIN, API_UPLOAD_FILE}
import com.requests.LoginRequest.loginBodyFeeder
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object UploadFileRequest {

  def getTokenRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Get Token")
        .post(API_BASE_URL + API_LOGIN)
        .body(ElFileBody("bodies/TestUserLoginData.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.loginResponse.token").saveAs("token")))
  }

  def userCreateCrateRequest() = {
    feed(createCrateRequestBodyFeeder)
      .exec(http("Create New Crate")
        .post(API_BASE_URL + API_CREATE_CRATE)
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .body(ElFileBody("bodies/CreateCrate.json")).asJson
        .check(status.is(200))
      .check(jsonPath("$.id").saveAs("id")))
  }

  val fileNames200kBFeeder = csv("data/fileNames200kB.csv").circular
  val fileNames500kBFeeder = csv("data/fileNames500kB.csv").circular
  val fileNames1MBFeeder = csv("data/fileNames1MB.csv").circular
  val fileNames2MBFeeder = csv("data/fileNames2MB.csv").circular
  val fileNames15MBFeeder = csv("data/fileNames15MB.csv").circular


  def userUploadFile200kBRequest() = {
    feed(fileNames200kBFeeder)
    .exec(http("Upload file 200 kB")
      .post(API_BASE_URL + API_UPLOAD_FILE + "${id}")
      .headers(Map("Authorization" -> "Bearer ${token}"))
      .formUpload("files", "files/${fileName}")
    .check(status.is(200)))

  }

  def userUploadFile500kBRequest() = {
    feed(fileNames500kBFeeder)
      .exec(http("Upload file 500 kB")
        .post(API_BASE_URL + API_UPLOAD_FILE + "${id}")
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .formUpload("files", "files/${fileName}")
        .check(status.is(200)))

  }

  def userUploadFile1MBRequest() = {
    feed(fileNames1MBFeeder)
      .exec(http("Upload file 1 MB")
        .post(API_BASE_URL + API_UPLOAD_FILE + "${id}")
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .formUpload("files", "files/${fileName}")
        .check(status.is(200)))

  }

  def userUploadFile2MBRequest() = {
    feed(fileNames2MBFeeder)
      .exec(http("Upload file 2 MB")
        .post(API_BASE_URL + API_UPLOAD_FILE + "${id}")
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .formUpload("files", "files/${fileName}")
        .check(status.is(200)))

  }

  def userUploadFile15MBRequest() = {
    feed(fileNames15MBFeeder)
      .exec(http("Upload file 15 MB")
        .post(API_BASE_URL + API_UPLOAD_FILE + "${id}")
        .headers(Map("Authorization" -> "Bearer ${token}"))
        .formUpload("files", "files/${fileName}")
        .check(status.is(200)))

  }

}