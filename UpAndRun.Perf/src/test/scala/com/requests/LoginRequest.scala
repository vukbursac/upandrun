package com.requests

import com.config.ConstantsData.ENDPOINTS.API_LOGIN
import com.config.ConstantsData._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object LoginRequest {

  val loginBodyFeeder = csv("bodies/UserData.csv").circular

  def userLoginRequest() = {
    feed(loginBodyFeeder)
      .exec(http("Login New User")
        .post(API_BASE_URL + API_LOGIN)
        .body(StringBody("""{"email": "${email}", "password": "${password}"}""")).asJson
        .check(status.is(200)))

  }

}