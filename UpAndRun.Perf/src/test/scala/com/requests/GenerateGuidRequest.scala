package com.requests

import com.config.ConstantsData.ENDPOINTS.API_GENERATE_GUID
import com.config.ConstantsData._
import io.gatling.core.Predef._
import io.gatling.http.Predef._


object GenerateGuidRequest {

  def generateGuidRequest() = {
    exec(http("Generate Guid")
      .get(API_BASE_URL + API_GENERATE_GUID)
      .check(status.is(200)))
  }

}