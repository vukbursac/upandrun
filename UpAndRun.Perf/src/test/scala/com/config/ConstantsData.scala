package com.config

import scala.util.Random

object ConstantsData {

  val API_BASE_URL = "https://localhost:44309"
  val CONTENT_TYPE_HEADER = "application/json"

  object ENDPOINTS {
    val API_REGISTRATION = "/api/Users/Registration"
    val API_LOGIN = "/api/Users/Login"
    val API_GENERATE_GUID = "/api/Runtimes/Generateguid"
    val API_CREATE_CRATE = "/api/Crates"
    val API_GET_ALL_CRATES = "/api/Crates"
    val API_UPLOAD_FILE = "/api/Files/"
    val API_UPLOAD_LAMBDA = "/api/Runtimes"
    val API_RUN_LAMBDA = "/api/Runtimes/users"
    val API_USER_ID = "/9c872b63-2afe-418e-9300-835b95c3eea6"
    val API_GUID_LAMBDA_ID = "/468bce85-e9eb-4a52-a6fa-7948025c8a78"
    val API_SUM_LAMBDA_ID = "/4943953f-8676-4a53-bf84-b7cbf3488e18"
    val API_GRAYSCALE_LAMBDA_ID = "/14617556-c87a-4c61-adc3-8d10e55a748b"

  }

  object REGISTRATION_USER_DATA {
    val registrationRequestBodyFeeder = Iterator.continually(Map(
      "firstName" -> "Jane",
      "lastName" -> "Doe",
      "username" -> (Random.alphanumeric.take(7).mkString),
      "email" -> (Random.alphanumeric.take(6).mkString + "@example.com"),
      "password" -> "Aaa123",
      "confirmPassword" -> "Aaa123"
    ))
  }

  object CRATE_NAME {
    val createCrateRequestBodyFeeder = Iterator.continually(Map(
      "name" -> (Random.alphanumeric.take(6).mkString)
    ))
  }

}



