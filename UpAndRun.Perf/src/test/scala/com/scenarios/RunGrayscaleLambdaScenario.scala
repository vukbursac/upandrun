package com.scenarios

import com.requests.RunGrayscaleLambdaRequest
import io.gatling.core.Predef.scenario

object RunGrayscaleLambdaScenario {

  val runLambda = scenario("Run a Grayscale Lambda")
    .exec(RunGrayscaleLambdaRequest.getTokenRequest())
    .exec(RunGrayscaleLambdaRequest.runGrayscaleLambdaRequest())

}
