package com.scenarios

import com.requests.GetAllCratesRequest
import io.gatling.core.Predef.{exec, scenario}

object GetAllCratesScenario {

  val getAllCrates = scenario("Get All Crates")
    .exec(GetAllCratesRequest.getTokenRequest())
    .exec(GetAllCratesRequest.getAllCrates())



}

