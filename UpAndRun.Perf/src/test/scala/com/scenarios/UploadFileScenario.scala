package com.scenarios

import com.requests.{UploadFileRequest}
import io.gatling.core.Predef.{scenario}

object UploadFileScenario {

  val uploadFile = scenario("Upload file scenario")
    .exec(UploadFileRequest.getTokenRequest())
    .exec(UploadFileRequest.userCreateCrateRequest())
    .exec(UploadFileRequest.userUploadFile200kBRequest())
    .exec(UploadFileRequest.userCreateCrateRequest())
    .exec(UploadFileRequest.userUploadFile500kBRequest())
    .exec(UploadFileRequest.userCreateCrateRequest())
    .exec(UploadFileRequest.userUploadFile1MBRequest())
    .exec(UploadFileRequest.userCreateCrateRequest())
    .exec(UploadFileRequest.userUploadFile2MBRequest())
    .exec(UploadFileRequest.userCreateCrateRequest())
    .exec(UploadFileRequest.userUploadFile15MBRequest())
}

