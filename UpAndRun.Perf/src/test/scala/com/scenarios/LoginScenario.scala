package com.scenarios

import com.requests.LoginRequest
import io.gatling.core.Predef.scenario

object LoginScenario {

  val login = scenario("User Login")
    .exec(LoginRequest.userLoginRequest())
}