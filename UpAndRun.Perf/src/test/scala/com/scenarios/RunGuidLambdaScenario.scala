package com.scenarios

import com.requests.{RunGuidLambdaRequest, RunSumLambdaRequest}
import io.gatling.core.Predef.scenario

object RunGuidLambdaScenario {

  val runLambda = scenario("Run a Guid Lambda")
    .exec(RunGuidLambdaRequest.getTokenRequest())
    .exec(RunGuidLambdaRequest.runGuidLambdaRequest())
}