package com.scenarios

import com.requests.CreateCrateRequest
import io.gatling.core.Predef._

object CreateCrateScenario {

  val createCrate = scenario("Create New Crate")
    .exec(CreateCrateRequest.getTokenRequest())
    .exec(CreateCrateRequest.userCreateCrateRequest())

}