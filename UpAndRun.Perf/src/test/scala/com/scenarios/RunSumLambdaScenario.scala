package com.scenarios

import com.requests.{RunGuidLambdaRequest, RunSumLambdaRequest}
import io.gatling.core.Predef.scenario

object RunSumLambdaScenario {

  val runLambda = scenario("Run a Sum Lambda")
    .exec(RunGuidLambdaRequest.getTokenRequest())
    .exec(RunSumLambdaRequest.runSumLambdaRequest())

}
