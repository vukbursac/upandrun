package com.scenarios

import com.requests.RegistrationRequest
import io.gatling.core.Predef.scenario

object RegisterScenario {

  val registration = scenario("User Registration")
    .exec(RegistrationRequest.userRegistrationRequest())
}