package com.scenarios

import com.requests.GenerateGuidRequest
import io.gatling.core.Predef.scenario

object GenerateGuidScenario {

  val generateGuid = scenario("Generate Guid")
    .exec(GenerateGuidRequest.generateGuidRequest())
}