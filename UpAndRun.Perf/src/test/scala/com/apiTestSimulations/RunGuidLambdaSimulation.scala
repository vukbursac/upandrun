package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.RunGuidLambdaScenario
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt

class RunGuidLambdaSimulation extends Simulation{

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("ERROR"))

  private val runLambdaExec = RunGuidLambdaScenario.runLambda
    .inject(atOnceUsers(1))
//  .inject(rampUsers(75, 60 seconds))

  setUp(runLambdaExec)

}
