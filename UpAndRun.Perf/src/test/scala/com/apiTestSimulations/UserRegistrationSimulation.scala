package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.RegisterScenario
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt

class UserRegistrationSimulation extends Simulation {

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))

  private val userRegistrationExec = RegisterScenario.registration
  .inject(atOnceUsers(1))

  setUp(userRegistrationExec)

}



