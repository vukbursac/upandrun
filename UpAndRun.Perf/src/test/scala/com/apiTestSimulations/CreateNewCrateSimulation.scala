package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.CreateCrateScenario
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt

class CreateNewCrateSimulation extends Simulation{

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))


  private val userCreateCrateExec = CreateCrateScenario.createCrate
    .inject(atOnceUsers(2))

  setUp(userCreateCrateExec)

}
