package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt


class GetAllCratesSimulation extends Simulation {

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))

  private val userGetAllCratesExec = GetAllCratesScenario.getAllCrates
    .inject(atOnceUsers(1))

  setUp(userGetAllCratesExec)


}
