package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.UploadFileScenario
import io.gatling.core.Predef.{openInjectionProfileFactory, rampUsers}
import io.gatling.core.scenario.Simulation
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt

class UploadFileSimulation extends Simulation {

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("ERROR"))

  private val uploadFileExec = UploadFileScenario.uploadFile
  .inject(rampUsers(5)during(10 minutes))

  setUp(uploadFileExec)

}
