package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.RunGrayscaleLambdaScenario
import io.gatling.core.Predef._
import org.slf4j.LoggerFactory
import io.gatling.core.scenario.Simulation

class RunGrayscaleLambdaSimulation extends Simulation {

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("ERROR"))

  private val runLambdaExec = RunGrayscaleLambdaScenario.runLambda
    .inject(atOnceUsers(1))
  //    .inject(rampUsers(56)during(60 seconds))

  setUp(runLambdaExec)

}
