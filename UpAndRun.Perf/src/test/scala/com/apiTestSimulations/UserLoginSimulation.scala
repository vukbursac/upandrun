package com.apiTestSimulations

import ch.qos.logback.classic.{Level, LoggerContext}
import com.scenarios.LoginScenario
import io.gatling.core.Predef._
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt

class UserLoginSimulation extends Simulation{


  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))

  private val userLoginExec = LoginScenario.login
    .inject(rampUsers(300) during (60 seconds))

  setUp(userLoginExec)

}
