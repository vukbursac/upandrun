﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace URCommunicationProtocolLibrary
{
    public class BaseService : IBaseService
    {
        private DealerSocket _dealer;
        private SubscriberSocket _subscriber;
        private NetMQPoller _poller;
        private ConcurrentDictionary<string, Action<NetMQMessage>> _subscriptionsAndHandlers = new ConcurrentDictionary<string, Action<NetMQMessage>>();

        public BaseService()
        {
            _dealer = new DealerSocket();
            _subscriber = new SubscriberSocket();
            _poller = new NetMQPoller() { _subscriber };

            _subscriber.ReceiveReady += OnMessageReceived;
        }

        public void HandleResponseIdSetAndRemove(Guid id, Dictionary<Guid, AutoResetEvent> responseIds)
        {
            if (!responseIds.ContainsKey(id))
            {
                return;
            }

            responseIds[id].Set();
            responseIds.Remove(id);
        }
        private void OnMessageReceived(object sender, NetMQSocketEventArgs e)
        {
            var message = e.Socket.ReceiveMultipartMessage();
            Console.WriteLine(message);
            var topic = message.First.ConvertToString();

            if (!_subscriptionsAndHandlers.ContainsKey(topic))
            {
                return;
            }

            _subscriptionsAndHandlers[topic].Invoke(message);
        }

        public void Send(IMessage message)
        {
            _dealer.SendMultipartMessage(message.ConvertToMQMessage());
        }

        public void StartAsync()
        {
            _dealer.Connect("tcp://127.0.0.1:9000");
            _subscriber.Connect("tcp://127.0.0.1:9001");

            _poller.RunAsync();
        }

        public void Start()
        {
            _dealer.Connect("tcp://127.0.0.1:9000");
            _subscriber.Connect("tcp://127.0.0.1:9001");

            _poller.Run();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void Subscribe(string topic, Action<NetMQMessage> handler)
        {
            if (string.IsNullOrEmpty(topic))
            {
                _subscriber.SubscribeToAnyTopic();
            }
            else
            {
                _subscriber.Subscribe(topic);
            }
            _subscriptionsAndHandlers.TryAdd(topic, handler);
        }

        public async Task SubscribeAsync(string topic, Task<Action<NetMQMessage>> handler)
        {
            if (string.IsNullOrEmpty(topic))
            {
                _subscriber.SubscribeToAnyTopic();
            }
            else
            {
                _subscriber.Subscribe(topic);
            }
            _subscriptionsAndHandlers.TryAdd(topic, await handler);
        }

        public void Unsubscribe(string topic)
        {
            _subscriber.Unsubscribe(topic);
        }
    }
}