﻿using NetMQ;
using System;
using UpAndRun.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class RegistrationResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.RegistrationResponseTopicName;
        public Guid Id { get; set; }
        public RegistrationResponse RegistrationResponse { get; set; }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());

            var serializedObject = Serialization.Serialize(RegistrationResponse);
            message.Append(serializedObject);
            return message;
        }
    }
}