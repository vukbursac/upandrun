﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class DeleteRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.DeleteRequestTopicName;
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public bool HardDelete { get; set; }
        public string Jwt { get; set; }

        public DeleteRequestMessage(Guid userId, bool hardDelete, string jwt)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            HardDelete = hardDelete;
            Jwt = jwt;
        }

        public DeleteRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            HardDelete = bool.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(HardDelete.ToString());
            message.Append(Jwt.ToString());

            return message;
        }
    }
}