﻿using NetMQ;
using System;
using UpAndRun.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class GetByIdResponseMessage : IMessage
    {
        public string Topic => MessageConstants.GetByIdResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public CustomerResponse CustomerResponse { get; set; }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            var serializedObject = Serialization.Serialize(CustomerResponse);
            message.Append(serializedObject);
            return message;
        }
    }
}