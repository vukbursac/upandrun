﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class DeleteResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.DeleteResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            return message;
        }
    }
}