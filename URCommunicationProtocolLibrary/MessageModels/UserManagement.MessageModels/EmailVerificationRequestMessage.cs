﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class EmailVerificationRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.RequestForEmailVerificationTopicName;
        public string Token { get; set; }
        public string UserId { get; set; }
        public Guid Id { get; set; }

        public EmailVerificationRequestMessage(string token, string userId)
        {
            Id = Guid.NewGuid();
            Token = token;
            UserId = userId;
        }

        public EmailVerificationRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = message.Pop().ConvertToString();
            Token = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId);
            message.Append(Token);

            return message;
        }
    }
}