﻿using NetMQ;
using System;
using UpAndRun.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class UpdateResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.UpdateResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public UpdateResponse UpdateResponse { get; set; }

        public UpdateResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            Message = message.Pop().ConvertToString();
            UpdateResponse = Deserialization.Deserialize<UpdateResponse>(message.Pop().ToByteArray());
        }

        public UpdateResponseMessage()
        {
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);
            message.Append(Serialization.Serialize(UpdateResponse));

            return message;
        }
    }
}