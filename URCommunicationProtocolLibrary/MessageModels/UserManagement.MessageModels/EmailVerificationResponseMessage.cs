﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class EmailVerificationResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.ResponseForEmailVerificationTopicName;
        public Guid Id { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }

        public EmailVerificationResponseMessage()
        {
        }

        public EmailVerificationResponseMessage(string message, bool isSuccess)
        {
            Message = message;
            IsSuccess = isSuccess;
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Message);
            message.Append(IsSuccess.ToString());

            return message;
        }
    }
}
