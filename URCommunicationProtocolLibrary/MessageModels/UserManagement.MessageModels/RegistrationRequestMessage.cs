﻿using NetMQ;
using System;
using UpAndRun.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class RegistrationRequestMessage : IMessage
    { 
        public string Topic { get; } = MessageConstants.RegistrationRequestTopicName;
        public Guid Id { get; set; }
        public RegistrationRequest RegistrationRequest { get; set; }

        public RegistrationRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public RegistrationRequestMessage(RegistrationRequest registrationRequest)
        {
            Id = Guid.NewGuid();
            RegistrationRequest = registrationRequest;
        }

        public RegistrationRequestMessage(NetMQMessage msg)
        {
            msg.Pop();
            Id = Guid.Parse(msg.Pop().ConvertToString());
            RegistrationRequest = Deserialization.Deserialize<RegistrationRequest>(msg.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());

            var serializedObject = Serialization.Serialize(RegistrationRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}