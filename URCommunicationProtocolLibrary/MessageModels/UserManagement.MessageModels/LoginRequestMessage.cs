﻿using NetMQ;
using System;
using UpAndRun.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class LoginRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.LoginRequestTopicName;
        public Guid Id { get; set; }
        public LoginRequest LoginRequest { get; set; }

        public LoginRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public LoginRequestMessage(LoginRequest loginRequest)
        {
            Id = Guid.NewGuid();
            LoginRequest = loginRequest;
        }

        public LoginRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            LoginRequest = Deserialization.Deserialize<LoginRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());

            var serializedObject = Serialization.Serialize(LoginRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}