﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class GetByIdRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.GetByIdRequestTopicName;
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string JwtToken { get; set; }

        public GetByIdRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public GetByIdRequestMessage(Guid customerId, string jwt)
        {
            Id = Guid.NewGuid();
            CustomerId = customerId;
            JwtToken = jwt;
        }

        public GetByIdRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            CustomerId = Guid.Parse(message.Pop().ConvertToString());
            JwtToken = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(CustomerId.ToString());
            message.Append(JwtToken.ToString());

            return message;
        }
    }
}