﻿using NetMQ;
using System;
using UpAndRun.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class UpdateRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.UpdateRequestTopicName;
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }
        public UpdateRequest UpdateRequest { get; set; }

        public UpdateRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            UpdateRequest = Deserialization.Deserialize<UpdateRequest>(message.Pop().ToByteArray());
        }

        public UpdateRequestMessage(UpdateRequest updateRequest, Guid userId, string jwt)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            UpdateRequest = updateRequest;
            Jwt = jwt;
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());
            message.Append(Serialization.Serialize(UpdateRequest));

            return message;
        }
    }
}