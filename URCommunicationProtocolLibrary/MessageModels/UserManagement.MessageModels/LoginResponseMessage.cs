﻿using NetMQ;
using System;
using UpAndRun.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels
{
    public class LoginResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.LoginResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public LoginResponse LoginResponse { get; set; }

        public LoginResponseMessage()
        {           
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            var serializedObject = Serialization.Serialize(LoginResponse);
            message.Append(serializedObject);
            return message;
        }
    }
}