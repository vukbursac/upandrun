﻿using System;
using NetMQ;
using Runtime.Models.Request;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class RunLambdaRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.RunLambdaRequestTopicName;
        public Guid Id { get; set; }
        public RunLambdaRequest RunLambdaRequest { get; set; }

        public RunLambdaRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public RunLambdaRequestMessage(RunLambdaRequest runLambdaRequest)
        {
            Id = Guid.NewGuid();
            RunLambdaRequest = runLambdaRequest;
        }

        public RunLambdaRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            RunLambdaRequest = Deserialization.Deserialize<RunLambdaRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            var serializedObject = Serialization.Serialize(RunLambdaRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}