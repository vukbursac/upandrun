﻿using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class DeleteLambdaRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.DeleteLambdaRequestTopicName;
        public Guid Id { get; set; }
        public Guid ImageId { get; set; }

        public DeleteLambdaRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public DeleteLambdaRequestMessage(Guid imageId)
        {
            Id = Guid.NewGuid();
            ImageId = imageId;
        }

        public DeleteLambdaRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            ImageId = Guid.Parse(message.Pop().ConvertToString());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(ImageId.ToString());

            return message;
        }
    }
}