﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class CreateLambdaResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.CreateLambdaResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public Guid LambdaId { get; set; }

        public CreateLambdaResponseMessage()
        {
        }

        public CreateLambdaResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            LambdaId = Guid.Parse(message.Pop().ConvertToString());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            message.Append(LambdaId.ToString());
            return message;
        }
    }
}