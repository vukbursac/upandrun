﻿using NetMQ;
using Runtime.Models.Response;
using System;
using System.Collections.Generic;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class GetAllLambdasResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.GetAllLambdasResponseTopicName;

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public IEnumerable<LambdaResponse> Lambdas { get; set; }

        public GetAllLambdasResponseMessage()
        {
        }

        public GetAllLambdasResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            Lambdas = Deserialization.DeserializeCollection<LambdaResponse>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var serializedLambdas = Serialization.Serialize(Lambdas);
            message.Append(serializedLambdas);
            return message;
        }
    }
}