﻿using System;
using NetMQ;
using Runtime.Models;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class CreateLambdaRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.CreateLambdaRequestTopicName;
        public Guid Id { get; set; }
        public CreateLambdaRequest CreateLambdaRequest { get; set; }

        public CreateLambdaRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public CreateLambdaRequestMessage(CreateLambdaRequest createLambdaRequest)
        {
            Id = Guid.NewGuid();
            CreateLambdaRequest = createLambdaRequest;
        }

        public CreateLambdaRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            CreateLambdaRequest = Deserialization.Deserialize<CreateLambdaRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            var serializedObject = Serialization.Serialize(CreateLambdaRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}