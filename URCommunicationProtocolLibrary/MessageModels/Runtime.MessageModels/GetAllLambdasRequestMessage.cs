﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class GetAllLambdasRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.GetAllLambdasRequestTopicName;
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }

        public GetAllLambdasRequestMessage(string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            Jwt = jwt;
            UserId = userId;
        }

        public GetAllLambdasRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());
            return message;
        }
    }
}