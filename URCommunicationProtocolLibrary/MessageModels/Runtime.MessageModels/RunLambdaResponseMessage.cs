﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class RunLambdaResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.RunLambdaResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string LambdaOutput { get; set; }

        public RunLambdaResponseMessage()
        {
        }

        public RunLambdaResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            LambdaOutput = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            message.Append(LambdaOutput);

            return message;
        }
    }
}