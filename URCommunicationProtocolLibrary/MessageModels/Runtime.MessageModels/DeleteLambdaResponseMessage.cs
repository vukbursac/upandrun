﻿using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Runtime.MessageModels
{
    public class DeleteLambdaResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.DeleteLambdaResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public DeleteLambdaResponseMessage()
        {
        }

        public DeleteLambdaResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            Message = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            return message;
        }
    }
}