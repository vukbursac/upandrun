﻿using NetMQ;
using Storage.Models.Requests;
using System;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class RenameCrateRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.RenameCrateRequestTopicName;
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }
        public RenameCrateRequest UpdateCrateRequest { get; set; }

        public RenameCrateRequestMessage(RenameCrateRequest updateCrateRequest, string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            UpdateCrateRequest = updateCrateRequest;
            Jwt = jwt;
            UserId = userId;
        }

        public RenameCrateRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            UpdateCrateRequest = Deserialization.Deserialize<RenameCrateRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(UpdateCrateRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}