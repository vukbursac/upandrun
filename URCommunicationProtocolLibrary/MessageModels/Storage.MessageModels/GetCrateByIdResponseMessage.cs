﻿using NetMQ;
using Storage.Models.Responses;
using System;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetCrateByIdResponseMessage : IMessage
    {
        public string Topic => MessageConstants.GetCrateByIdResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public CrateResponse CrateResponse { get; set; }

        public GetCrateByIdResponseMessage(Guid id)
        {
            Id = id;
        }

        public GetCrateByIdResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            var response = message.Pop().ToByteArray();
            if (response.Length == 0)
            {
                CrateResponse = null;
            }
            else
            {
                CrateResponse = Deserialization.Deserialize<CrateResponse>(response);
            }
        }

        public GetCrateByIdResponseMessage()
        {
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var serializedObject = Serialization.Serialize(CrateResponse);
            message.Append(serializedObject);
            return message;
        }
    }
}