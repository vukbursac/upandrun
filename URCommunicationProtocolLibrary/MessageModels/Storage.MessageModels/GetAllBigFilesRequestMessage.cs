﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllBigFilesRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.GetAllBigFilesRequestTopicName;

        public Guid Id { get; set; }
        public string Jwt { get; set; }

        public GetAllBigFilesRequestMessage(string jwt)
        {
            Id = Guid.NewGuid();
            Jwt = jwt;
        }

        public GetAllBigFilesRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            return message;
        }
    }
}