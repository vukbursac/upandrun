﻿using NetMQ;
using Storage.Models.ValidationAttributes;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetBigFileByIdRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.GetBigFileByIdRequestTopicName;

        public Guid Id { get; set; }

        public string FileId { get; set; }

        public string Jwt { get; set; }

        public GetBigFileByIdRequestMessage(string fileId, string jwt)
        {
            Id = Guid.NewGuid();
            FileId = fileId;
            Jwt = jwt;
        }

        public GetBigFileByIdRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            FileId = message.Pop().ConvertToString();
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            if (string.IsNullOrWhiteSpace(FileId))
            {
                message.Append("");
            }
            else
            {
                message.Append(FileId);
            }

            message.Append(Jwt.ToString());
            return message;
        }
    }
}