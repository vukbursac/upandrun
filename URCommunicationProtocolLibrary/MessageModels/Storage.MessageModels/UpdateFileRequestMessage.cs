﻿using NetMQ;
using Storage.Models.Requests;
using System;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class UpdateFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = "UpdateFileRequestMessage";
        public Guid Id { get; set; }
        public UpdateFileRequest UpdateFileRequest { get; set; }
         public string Jwt { get; set; }

        public UpdateFileRequestMessage(UpdateFileRequest updateFileRequest, string jwt)
        {
            Id = Guid.NewGuid();
            UpdateFileRequest = updateFileRequest;
            Jwt = jwt;
        }

        public UpdateFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            UpdateFileRequest = Deserialization.Deserialize<UpdateFileRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(UpdateFileRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}