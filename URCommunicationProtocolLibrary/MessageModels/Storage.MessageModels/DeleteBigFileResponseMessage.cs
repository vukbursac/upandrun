﻿using System;
using System.Collections.Generic;
using System.Text;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class DeleteBigFileResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.DeleteBigFileResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string FileName { get; set; }

        public DeleteBigFileResponseMessage()
        {
        }

        public DeleteBigFileResponseMessage(NetMQMessage msg)
        {
            msg.Pop();
            Id = Guid.Parse(msg.Pop().ConvertToString());
            IsSuccess = bool.Parse(msg.Pop().ConvertToString());
            Message = msg.Pop().ConvertToString();
            FileName = msg.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);
            if (FileName == null)
            {
                message.Append("");
            }
            else message.Append(FileName);
            return message;
        }
    }
}