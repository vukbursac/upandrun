﻿using System;
using NetMQ;
using Storage.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class MoveFileRequestMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.MoveFileRequestTopicName;
        public Guid Id { get; set; }
        public MoveFileRequest MoveFileRequest { get; set; }
        public string Jwt { get; set; }

        public MoveFileRequestMessage(MoveFileRequest moveFileRequest, string jwt)
        {
            Id = Guid.NewGuid();
            MoveFileRequest = moveFileRequest;
            Jwt = jwt;
        }

        public MoveFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            MoveFileRequest = Deserialization.Deserialize<MoveFileRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(MoveFileRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}