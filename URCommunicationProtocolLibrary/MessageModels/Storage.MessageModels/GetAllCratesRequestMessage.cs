﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllCratesRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.GetAllCratesRequestTopicName;
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }

        public GetAllCratesRequestMessage(string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            Jwt = jwt;
            UserId = userId;
        }

        public GetAllCratesRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());
            return message;
        }
    }
}