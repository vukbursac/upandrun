﻿using NetMQ;
using System;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class SoftDeleteResponseMessage : IMessage

    {
        public string Topic { get; } = "SoftDeleteFileResponseMessage";
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string FileName { get; set; }

        public SoftDeleteResponseMessage()
        {
        }

        public SoftDeleteResponseMessage(NetMQMessage msg)
        {
            msg.Pop();
            Id = Guid.Parse(msg.Pop().ConvertToString());
            IsSuccess = bool.Parse(msg.Pop().ConvertToString());
            ErrorMessage = msg.Pop().ConvertToString();
            FileName = msg.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            message.Append(FileName);
            return message;
        }
    }
}