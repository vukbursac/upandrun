﻿using System;
using System.Collections.Generic;
using NetMQ;
using Storage.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllBigFilesResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.GetAllBigFilesResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public IEnumerable<BigFileResponse> Files { get; set; }

        public GetAllBigFilesResponseMessage()
        {
        }

        public GetAllBigFilesResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            Files = Deserialization.DeserializeCollection<BigFileResponse>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            var serializedObject = Serialization.Serialize(Files);
            message.Append(serializedObject);
            return message;
        }
    }
}