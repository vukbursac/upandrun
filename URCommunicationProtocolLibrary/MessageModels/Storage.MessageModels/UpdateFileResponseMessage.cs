﻿using NetMQ;
using Storage.Models.Responses;
using System;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class UpdateFileResponseMessage : IMessage
    {
        public string Topic { get; } = "UpdateFileResponseMessage";

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string FileName { get; set; }

        public FileResponse UpdateFileResponse { get; set; }

        public BigFileResponse UpdateBigFileResponse { get; set; }

        public UpdateFileResponseMessage()
        {
        }

        public UpdateFileResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            Message = message.Pop().ConvertToString();
            UpdateFileResponse = Deserialization.Deserialize<FileResponse>(message.Pop().ToByteArray());
            UpdateBigFileResponse = Deserialization.Deserialize<BigFileResponse>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            var updateFileResponse = Serialization.Serialize(UpdateFileResponse);
            message.Append(updateFileResponse);

            var updateBigFileResponse = Serialization.Serialize(UpdateBigFileResponse);
            message.Append(updateBigFileResponse);

            return message;
        }
    }
}