﻿using System;
using System.Collections.Generic;
using System.Text;
using NetMQ;
using Storage.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetBigFileByIdResponseMessage:IMessage
    {
        public string Topic { get; } = MessageConstants.GetBigFileByIdResponseTopicName;

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public BigFileResponse BigFileResponse { get; set; }

        public GetBigFileByIdResponseMessage()
        {
        }

        public GetBigFileByIdResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();

            var response = message.Pop().ToByteArray();
            if (response.Length == 0)
            {
                BigFileResponse = null;
            }
            else
            {
                BigFileResponse = Deserialization.Deserialize<BigFileResponse>(response);
            }
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            var serializedObject = Serialization.Serialize(BigFileResponse);
            message.Append(serializedObject);
            return message;
        }
    }
}