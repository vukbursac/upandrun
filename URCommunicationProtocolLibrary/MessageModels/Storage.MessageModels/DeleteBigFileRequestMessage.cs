﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class DeleteBigFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.DeleteBigFileRequestTopicName;
        public Guid Id { get; set; }
        public string BigFileId { get; set; }
        public string Jwt { get; set; }

        public DeleteBigFileRequestMessage(string fileId, string jwt)
        {
            Id = Guid.NewGuid();
            BigFileId = fileId;
            Jwt = jwt;
        }

        public DeleteBigFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            BigFileId = message.Pop().ConvertToString();
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(BigFileId);
            message.Append(Jwt.ToString());

            return message;
        }
    }
}