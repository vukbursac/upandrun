﻿using NetMQ;
using System;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class SoftDeleteRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = "SoftDeleteFileRequestMessage";
        public Guid Id { get; set; }
        public string FileId { get; set; }
        public string Jwt { get; set; }

        public SoftDeleteRequestMessage()
        {
            Id = Guid.NewGuid();
        }

        public SoftDeleteRequestMessage(string fileId, string jwt)
        {
            Id = Guid.NewGuid();
            FileId = fileId;
            Jwt = jwt;
        }

        public SoftDeleteRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            FileId = message.Pop().ConvertToString();
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(FileId);
            message.Append(Jwt.ToString());
            return message;
        }
    }
}