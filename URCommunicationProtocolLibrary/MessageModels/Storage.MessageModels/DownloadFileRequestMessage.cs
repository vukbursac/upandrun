﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class DownloadFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.DownloadFileRequestTopicName;

        public Guid Id { get; set; }
        public string FileId { get; set; }
        public string Jwt { get; set; }

        public DownloadFileRequestMessage()
        {
        }

        public DownloadFileRequestMessage(string fileId, string jwt)
        {
            Id = Guid.NewGuid();
            FileId = fileId;
            Jwt = jwt;
        }

        public DownloadFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            FileId = message.Pop().ConvertToString();
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(FileId);
            message.Append(Jwt.ToString());
            return message;
        }
    }
}