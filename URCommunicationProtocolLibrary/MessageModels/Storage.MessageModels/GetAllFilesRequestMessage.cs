﻿using NetMQ;
using System;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllFilesRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = "GetAllFilesRequestMessage";

        public Guid Id { get; set; }
        public string Jwt { get; set; }

        public GetAllFilesRequestMessage(string jwt)
        {
            Id = Guid.NewGuid();
            Jwt = jwt;
        }

        public GetAllFilesRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            return message;
        }
    }
}