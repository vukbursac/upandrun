﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class SoftDeleteCrateResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.SoftDeleteCrateResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public string CrateName { get; set; }

        public SoftDeleteCrateResponseMessage()
        {
        }

        public SoftDeleteCrateResponseMessage(NetMQMessage msg)
        {
            msg.Pop();
            Id = Guid.Parse(msg.Pop().ConvertToString());
            IsSuccess = bool.Parse(msg.Pop().ConvertToString());
            ErrorMessage = msg.Pop().ConvertToString();
            CrateName = msg.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            if (CrateName == null)
            {
                message.Append("");
            }
            else message.Append(CrateName);
            return message;
        }
    }
}