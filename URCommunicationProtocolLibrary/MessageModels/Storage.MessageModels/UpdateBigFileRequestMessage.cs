﻿using System;
using NetMQ;
using Storage.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class UpdateBigFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.UpdateBigFileRequestTopicName;

        public Guid Id { get; set; }
        public string Jwt { get; set; }

        public UpdateBigFileRequest UpdateBigFileRequest;

        public UpdateBigFileRequestMessage(UpdateBigFileRequest updateBigFileRequest, string jwt)
        {
            Id = Guid.NewGuid();
            UpdateBigFileRequest = updateBigFileRequest;
            Jwt = jwt;
        }

        public UpdateBigFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            UpdateBigFileRequest = Deserialization.Deserialize<UpdateBigFileRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(UpdateBigFileRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}