﻿using NetMQ;
using Storage.Models.Responses;
using System;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class UploadFileResponseMessage : IMessage
    {
        public string Topic { get; } = "UploadFileResponseMessage";
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public UploadFileResponse UploadFileResponse { get; set; }

        public UploadBigFileResponse UploadBigFileResponse { get; set; }

        public UploadFileResponseMessage()
        {
        }

        public UploadFileResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            var fileResponse = message.Pop().ToByteArray();
            if (fileResponse.Length == 0)
            {
                UploadFileResponse = null;
            }
            else
            {
                UploadFileResponse = Deserialization.Deserialize<UploadFileResponse>(fileResponse);
            }
            var bigFileResponse=message.Pop().ToByteArray();
            if (bigFileResponse.Length == 0)
            {
                UploadBigFileResponse = null;
            }
            else
            {
                UploadBigFileResponse = Deserialization.Deserialize<UploadBigFileResponse>(bigFileResponse);
            }
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var uploadFileResponse = Serialization.Serialize(UploadFileResponse);
            message.Append(uploadFileResponse);
            var uploadBigFileResponse = Serialization.Serialize(UploadBigFileResponse);
            message.Append(uploadBigFileResponse);

            return message;
        }
    }
}