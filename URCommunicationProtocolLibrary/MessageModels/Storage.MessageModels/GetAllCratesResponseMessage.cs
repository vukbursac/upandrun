﻿using NetMQ;
using ProtoBuf;
using Storage.Models.Responses;
using System;
using System.Collections.Generic;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllCratesResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.GetAllCratesResponseTopicName;

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        [ProtoMember(1)]
        public IEnumerable<CrateResponse> Crates { get; set; }

        public GetAllCratesResponseMessage()
        {
        }

        public GetAllCratesResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            Crates = Deserialization.DeserializeCollection<CrateResponse>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var serializedCrates = Serialization.Serialize(Crates);
            message.Append(serializedCrates);
            return message;
        }
    }
}