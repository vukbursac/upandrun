﻿using System;
using System.Collections.Generic;
using System.Text;
using NetMQ;
using Storage.Models.Responses;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class DownloadFileResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.DownloadFileResponseTopicName;

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public DownloadFileResponse Response { get; set; }

        public DownloadFileResponseMessage()
        {
        }

        public DownloadFileResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            Message = message.Pop().ConvertToString();

            var downloadFileResponse = message.Pop().ToByteArray();

            if (downloadFileResponse.Length != 0)
            {
                Response = Deserialization.Deserialize<DownloadFileResponse>(downloadFileResponse);
            }
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);

            var downloadFileResponse = Serialization.Serialize(Response);
            message.Append(downloadFileResponse);

            return message;
        }
    }
}