﻿using NetMQ;
using System;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class SoftDeleteCrateRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.SoftDeleteCrateRequestTopicName;
        public Guid Id { get; set; }
        public Guid CrateId { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }

        public SoftDeleteCrateRequestMessage(Guid crateId, string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            CrateId = crateId;
            Jwt = jwt;
            UserId = userId;
        }

        public SoftDeleteCrateRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            CrateId = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(CrateId.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());
            return message;
        }
    }
}