﻿using NetMQ;
using Storage.Models.Requests;
using System;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class UploadFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = "UploadFileRequestMessage";

        public Guid Id { get; set; }
        public string Jwt { get; set; }

        public UploadFileRequest UploadFileRequest;

        public UploadFileRequestMessage(UploadFileRequest uploadFileRequest, string jwt)
        {
            Id = Guid.NewGuid();
            UploadFileRequest = uploadFileRequest;
            Jwt = jwt;
        }

        public UploadFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            UploadFileRequest = Deserialization.Deserialize<UploadFileRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(UploadFileRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}