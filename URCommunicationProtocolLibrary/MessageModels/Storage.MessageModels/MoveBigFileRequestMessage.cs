﻿using System;
using NetMQ;
using Storage.Models.Requests;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class MoveBigFileRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.MoveBigFileRequestTopicName;
        public Guid Id { get; set; }
        public MoveBigFileRequest MoveBigFileRequest { get; set; }
        public string Jwt { get; set; }

        public MoveBigFileRequestMessage(MoveBigFileRequest moveFileRequest, string jwt)
        {
            Id = Guid.NewGuid();
            MoveBigFileRequest = moveFileRequest;
            Jwt = jwt;
        }

        public MoveBigFileRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            MoveBigFileRequest = Deserialization.Deserialize<MoveBigFileRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(Jwt.ToString());
            var serializedObject = Serialization.Serialize(MoveBigFileRequest);
            message.Append(serializedObject);
            return message;
        }
    }
}