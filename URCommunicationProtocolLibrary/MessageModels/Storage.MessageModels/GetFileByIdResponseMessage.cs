﻿using NetMQ;
using Storage.Models.Responses;
using System;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetFileByIdResponseMessage : IMessage
    {
        public string Topic { get; } = "GetFileByIdResponseMessage";

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public FileResponse FileResponse { get; set; }

        public BigFileResponse BigFileResponse { get; set; }

        public GetFileByIdResponseMessage()
        {
        }

        public GetFileByIdResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();

            var fileResponse = message.Pop().ToByteArray();
            if (fileResponse.Length == 0)
            {
                FileResponse = null;
            }
            else
            {
                FileResponse = Deserialization.Deserialize<FileResponse>(fileResponse);
            }
            var bigFileResponse = message.Pop().ToByteArray();
            if (bigFileResponse.Length == 0)
            {
                BigFileResponse = null;
            }
            else
            {
                BigFileResponse = Deserialization.Deserialize<BigFileResponse>(bigFileResponse);
            }
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);
            var fileResponse = Serialization.Serialize(FileResponse);
            message.Append(fileResponse);
            var bigFileResponse = Serialization.Serialize(BigFileResponse);
            message.Append(bigFileResponse);

            return message;
        }
    }
}