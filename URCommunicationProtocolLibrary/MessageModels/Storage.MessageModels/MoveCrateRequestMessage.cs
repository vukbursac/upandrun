﻿using NetMQ;
using Storage.Models.Requests;
using System;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class MoveCrateRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.MoveCrateRequestTopicName;
        public Guid Id { get; set; }
        public MoveCrateRequest MoveCrateRequest { get; set; }
        public Guid UserId { get; set; }
        public string Jwt { get; set; }

        public MoveCrateRequestMessage(MoveCrateRequest moveCrateRequest, string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            MoveCrateRequest = moveCrateRequest;
            Jwt = jwt;
            UserId = userId;
        }

        public MoveCrateRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            MoveCrateRequest = Deserialization.Deserialize<MoveCrateRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());

            var serializedObject = Serialization.Serialize(MoveCrateRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}