﻿using NetMQ;
using Storage.Models.Responses;
using System;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class MoveCrateResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.MoveCrateResponseTopicName;
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public CrateResponse CrateResponse { get; set; }

        public MoveCrateResponseMessage()
        {
        }

        public MoveCrateResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            var response = message.Pop().ToByteArray();

            if (response.Length == 0)
            {
                CrateResponse = null;
            }
            else
            {
                CrateResponse = Deserialization.Deserialize<CrateResponse>(response);
            }
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();

            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var serializedObject = Serialization.Serialize(CrateResponse);
            message.Append(serializedObject);

            return message;
        }
    }
}