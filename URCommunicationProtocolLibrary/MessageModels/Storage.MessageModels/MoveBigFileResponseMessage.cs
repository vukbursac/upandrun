﻿using System;
using NetMQ;
using Utility.Constants;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class MoveBigFileResponseMessage : IMessage
    {
        public string Topic { get; } = MessageConstants.MoveBigFileResponseTopicName;

        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string FileName { get; set; }
        public string FileId { get; set; }

        public MoveBigFileResponseMessage()
        {
        }

        public MoveBigFileResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            Message = message.Pop().ConvertToString();
            FileName = message.Pop().ConvertToString();
            FileId = message.Pop().ConvertToString();
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var noName = "";
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(Message);
            if (FileName == null)
            {
                message.Append(noName);
            }
            else message.Append(FileName);
            message.Append(FileId);
            return message;
        }
    }
}