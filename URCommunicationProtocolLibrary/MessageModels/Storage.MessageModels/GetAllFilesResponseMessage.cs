﻿using NetMQ;
using Storage.Models.Responses;
using System;
using System.Collections.Generic;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class GetAllFilesResponseMessage : IMessage
    {
        public string Topic { get; } = "GetAllFilesResponseMessage";
        public Guid Id { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public IEnumerable<FileResponse> Files { get; set; }
        public IEnumerable<BigFileResponse> BigFiles { get; set; }

        public GetAllFilesResponseMessage()
        {
        }

        public GetAllFilesResponseMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            IsSuccess = bool.Parse(message.Pop().ConvertToString());
            ErrorMessage = message.Pop().ConvertToString();
            Files = Deserialization.DeserializeCollection<FileResponse>(message.Pop().ToByteArray());
            BigFiles = Deserialization.DeserializeCollection<BigFileResponse>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(IsSuccess.ToString());
            message.Append(ErrorMessage);

            var files = Serialization.Serialize(Files);
            message.Append(files);

            var bigFiles = Serialization.Serialize(BigFiles);
            message.Append(bigFiles);

            return message;
        }
    }
}