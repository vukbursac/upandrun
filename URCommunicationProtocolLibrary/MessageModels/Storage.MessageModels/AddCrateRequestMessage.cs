﻿using NetMQ;
using Storage.Models.Requests;
using System;
using Utility.Constants;
using Utility.Protobuf;

namespace URCommunicationProtocolLibrary.MessageModels.Storage.MessageModels
{
    public class AddCrateRequestMessage : IMessage, IAuthorizedMessage
    {
        public string Topic { get; } = MessageConstants.AddCrateRequestMessage;
        public Guid Id { get; set; }
        public AddCrateRequest AddCrateRequest { get; set; }
        public string Jwt { get; set; }
        public Guid UserId { get; set; }

        public AddCrateRequestMessage(AddCrateRequest addCrateRequest, string jwt, Guid userId)
        {
            Id = Guid.NewGuid();
            AddCrateRequest = addCrateRequest;
            Jwt = jwt;
            UserId = userId;
        }

        public AddCrateRequestMessage(NetMQMessage message)
        {
            message.Pop();
            Id = Guid.Parse(message.Pop().ConvertToString());
            UserId = Guid.Parse(message.Pop().ConvertToString());
            Jwt = message.Pop().ConvertToString();
            AddCrateRequest = Deserialization.Deserialize<AddCrateRequest>(message.Pop().ToByteArray());
        }

        public NetMQMessage ConvertToMQMessage()
        {
            var message = new NetMQMessage();
            message.Append(Topic);
            message.Append(Id.ToString());
            message.Append(UserId.ToString());
            message.Append(Jwt.ToString());

            var serializedObject = Serialization.Serialize(AddCrateRequest);
            message.Append(serializedObject);

            return message;
        }
    }
}