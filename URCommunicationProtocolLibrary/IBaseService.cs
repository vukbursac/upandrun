﻿using NetMQ;
using System;

namespace URCommunicationProtocolLibrary
{
    public interface IBaseService
    {
        void Start();

        void StartAsync();

        void Stop();

        void Send(IMessage message);

        void Subscribe(string topic, Action<NetMQMessage> handler);

        void Unsubscribe(string topic);
    }
}