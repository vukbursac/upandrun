﻿using System;
using System.Collections.Generic;
using System.Text;

namespace URCommunicationProtocolLibrary
{
    public interface IAuthorizedMessage
    {
        public string Jwt { get; }
    }
}
