﻿using NetMQ;
using System;

namespace URCommunicationProtocolLibrary
{
    public interface IMessage
    {
        public string Topic { get; }
        public Guid Id { get; }

        NetMQMessage ConvertToMQMessage();
    }
}