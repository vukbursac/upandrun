﻿using System.Threading.Tasks;
using UpAndRun.Models.Entities.SendGrid;

namespace Utility.SendGrid
{
    public interface IEmailService
    {
        public Task SendRegistrationEmailAsync(string toEmail, CustomerEmailData customer);
    }
}