﻿using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;
using UpAndRun.Models.Entities.SendGrid;
using Utility.Exceptions;

namespace Utility.SendGrid
{
    public class EmailService : IEmailService
    {
        private IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendRegistrationEmailAsync(string toEmail, CustomerEmailData customerEmailData)
        {
            var apiKey = _configuration["SendGridAPIKey"];
            var client = new SendGridClient(apiKey);
            var senderEmail1 = "upandrun.noreply@gmail.com";
            var senderEmail2 = "upandrun.noreplyservice@gmail.com";
            var emailSubject = "Up and Run";
            var from = new EmailAddress(senderEmail1, emailSubject);
            var to = new EmailAddress(toEmail);
            var templateId1 = "d-edd52b9ffac744caa7d68fb41f638fd4";
            var templateId2 = "d-4baab9fb4ea1447293577596d796d1fe";
            var msg = MailHelper.CreateSingleTemplateEmail(from, to, templateId1, customerEmailData);
            var response = await client.SendEmailAsync(msg);

            if (response == null)
            {
                throw new BadRequestException("Oops! Something went wrong :( ");
            }

            if (!response.IsSuccessStatusCode)
            {
                var errorMessage = await response.Body.ReadAsStringAsync();
                Console.WriteLine(errorMessage);
                throw new BadRequestException("Oops! E-mail can not be sent ! :( ");
            }
        }
    }
}