﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProtoBuf;

namespace Utility.Protobuf
{
    public static class Deserialization
    {
        public static T Deserialize<T>(byte[] messageToDeserialize)
        {
            using var memoryStream = new MemoryStream(messageToDeserialize);
            return Serializer.Deserialize<T>(memoryStream);
        }

        public static IEnumerable<T> DeserializeCollection<T>(byte[] messageToDeserialize)
        {
            using var memoryStream = new MemoryStream(messageToDeserialize);
            return Serializer.Deserialize<IEnumerable<T>>(memoryStream);
        }
    }
}