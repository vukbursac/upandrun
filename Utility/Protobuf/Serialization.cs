﻿using ProtoBuf;
using System.IO;

namespace Utility.Protobuf
{
    public static class Serialization
    {
        public static byte[] Serialize(object objectToSerialize)
        {
            var memoryStream = new MemoryStream();
            using (memoryStream)
            {
                Serializer.Serialize(memoryStream, objectToSerialize);
            }
            byte[] serialized = memoryStream.ToArray();
            return serialized;
        }
    }
}