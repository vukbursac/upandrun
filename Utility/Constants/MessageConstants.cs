﻿namespace Utility.Constants
{
    public static class MessageConstants
    {
        //---------------------------------------------- User Management topics ----------------------------------------------

        public const string RegistrationRequestTopicName = "RegistrationRequestMessage";
        public const string RegistrationResponseTopicName = "RegistrationResponseMessage";
        public const string UnsuccessfulRegistrationTopicName = "UnsuccessfulRegistration";
        public const string LoginRequestTopicName = "LoginRequestMessage";
        public const string LoginResponseTopicName = "LoginResponseMessage";
        public const string GetByIdRequestTopicName = "GetByIdMessageRequestMessage";
        public const string GetByIdResponseTopicName = "GetByIdResponseMessage";
        public const string DeleteRequestTopicName = "DeleteRequestMessage";
        public const string DeleteResponseTopicName = "DeleteResponseMessage";
        public const string UpdateRequestTopicName = "UpdateRequestMessage";
        public const string UpdateResponseTopicName = "UpdateResponseMessage";       
        public const string RequestForEmailVerificationTopicName = "VerificationEmailRequestMessage";
        public const string ResponseForEmailVerificationTopicName = "VerificationEmailResponseMessage";

        //---------------------------------------------- Storage crates topics ----------------------------------------------

        public const string AddCrateResponseTopicName = "AddCrateResponseMessage";

        public const string AddCrateRequestMessage = "AddCrateRequestMessage";
        public const string GetCrateByIdRequestTopicName = "RequestForCrateGetByIdMessage";
        public const string GetCrateByIdResponseTopicName = "SuccessfulForCrateGetByIdMessage";
        public const string GetAllCratesRequestTopicName = "GetAllCratesRequestMessage";
        public const string GetAllCratesResponseTopicName = "GetAllCratesResponseMessage";
        public const string SoftDeleteCrateRequestTopicName = "SoftDeleteCrateRequestMessage";
        public const string SoftDeleteCrateResponseTopicName = "SoftDeleteCrateResponseMessage";
        public const string RenameCrateRequestTopicName = "RenameCrateRequestMessage";
        public const string RenameCrateResponseTopicName = "RenameCrateResponseMessage";
        public const string MoveCrateRequestTopicName = "MoveCrateRequestMessage";
        public const string MoveCrateResponseTopicName = "MoveCrateResponseMessage";
        public const string DeleteCrateResponseTopicName = "DeleteCrateResponseMessage";
        public const string DeleteCrateRequestTopicName = "DeleteCrateRequestMessage";

        //---------------------------------------------- Storage files topics ----------------------------------------------

        public const string UploadFileRequestMessageTopicName = "UploadFileRequestMessage";

        public const string UpdateFileRequestTopicName = "UpdateFileRequestMessage";
        public const string MoveFileRequestTopicName = "MoveFileRequestMessage";
        public const string MoveFileResponseTopicName = "MoveFileResponseMessage";
        public const string GetFileByIdRequestTopicName = "GetFileByIdRequestMessage";
        public const string SoftDeleteFileRequestTopicName = "SoftDeleteFileRequestMessage";
        public const string DeleteFileRequestTopicName = "DeleteFileRequestMessage";
        public const string DownloadFileRequestTopicName = "DownloadFileRequestMessage";
        public const string DownloadFileResponseTopicName = "DownloadFileResponseMessage";
        public const string GetAllBigFilesRequestTopicName = "GetAllBigFilesRequestMessage";
        public const string GetAllBigFilesResponseTopicName = "GetAllBigFilesResponseMessage";
        public const string GetBigFileByIdRequestTopicName = "GetBigFileByIdRequestMessage";
        public const string GetBigFileByIdResponseTopicName = "GetBigFileByIdResponseMessage";
        public const string DownloadBigFileRequestTopicName = "DownloadBigFileRequestMessage";
        public const string DownloadBigFileResponseTopicName = "DownloadBigFileResponseMessage";
        public const string DeleteBigFileRequestTopicName = "DeleteBigFileRequestMessage";
        public const string DeleteBigFileResponseTopicName = "DeleteBigFileResponseMessage";
        public const string UpdateBigFileRequestTopicName = "UpdateBigFileRequestMessage";
        public const string UpdateBigFileResponseTopicName = "UpdateBigFileResponseMessage";
        public const string MoveBigFileRequestTopicName = "MoveBigFileRequestMessage";
        public const string MoveBigFileResponseTopicName = "MoveBigFileResponseMessage";

        //---------------------------------------------- Runtime topics ----------------------------------------------------

        public const string CreateLambdaRequestTopicName = "CreateLambdaRequestMessage";

        public const string CreateLambdaResponseTopicName = "CreateLambdaResponseMessage";
        public const string RunLambdaRequestTopicName = "RunLambdaRequestMessage";
        public const string RunLambdaResponseTopicName = "RunLambdaResponseMessage";
        public const string GetAllLambdasRequestTopicName = "GetAllLambdasRequestMessage";
        public const string GetAllLambdasResponseTopicName = "GetAllLambdasResponseMessage";
        public const string DeleteLambdaRequestTopicName = "DeleteLambdaRequestMessage";
        public const string DeleteLambdaResponseTopicName = "DeleteLambdaResponseMessage";
    }
}