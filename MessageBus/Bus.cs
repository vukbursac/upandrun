﻿using NetMQ;
using NetMQ.Sockets;

namespace MessageBus
{
    public class Bus
    {
        private DealerSocket _dealer;
        private PublisherSocket _publisher;
        private NetMQPoller _poller;

        public Bus()
        {
            _dealer = new DealerSocket();
            _publisher = new PublisherSocket();
            _poller = new NetMQPoller() { _dealer };

            _dealer.ReceiveReady += OnMessageReceived;
        }

        private void OnMessageReceived(object sender, NetMQSocketEventArgs e)
        {
            var message = e.Socket.ReceiveMultipartMessage();
            _publisher.SendMultipartMessage(message);
        }

        public void Start()
        {
            _publisher.Bind("tcp://127.0.0.1:9001");
            _dealer.Bind("tcp://127.0.0.1:9000");

            _poller.Run();
        }

        public void Stop()
        {
            _poller.Stop();

            _poller.Remove(_dealer);
            _dealer.Unbind("tcp://127.0.0.1:9000");
            _dealer.Close();

            _publisher.Unbind("tcp://127.0.0.1:9001");
            _publisher.Close();
        }
    }
}