﻿using System;

namespace MessageBus
{
    internal class Program
    {
        private static Bus messageBus;

        private static void Main(string[] args)
        {
            Console.WriteLine("-------------------------------Message bus-----------------------------------");
            messageBus = new Bus();
            messageBus.Start();
        }
    }
}