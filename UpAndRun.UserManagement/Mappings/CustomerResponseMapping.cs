﻿using UpAndRun.Models.Entities;
using UpAndRun.Models.Responses;

namespace UpAndRun.UserManagement.Mappings
{
    public class CustomerResponseMapping : AutoMapper.Profile
    {
        public CustomerResponseMapping() => CreateMap<Customer, CustomerResponse>();
    }
}
