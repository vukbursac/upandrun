﻿using AutoMapper;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpAndRun.Models.Entities;
using UpAndRun.Models.Entities.SendGrid;
using UpAndRun.Models.Requests;
using UpAndRun.Models.Responses;
using UpAndRun.UserManagement.Interfaces;
using UpAndRun.UserManagement.UpAndRun.Data.Interfaces;
using URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels;
using Utility.Exceptions;
using Utility.SendGrid;

namespace UpAndRun.UserManagement.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private IEmailService _emailService;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager, IConfiguration configuration, IEmailService emailService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _configuration = configuration;
            _emailService = emailService;
            _mapper = mapper;
        }

        public async Task<Maybe<LoginResponseMessage>> Login(LoginRequest loginRequest)
        {
            var existingCustomer = await _unitOfWork.CustomerRepository.GetByEmail(loginRequest.Email);

            if (existingCustomer.HasNoValue)
            {
                return new LoginResponseMessage() { IsSuccess = false, Message = $"Email: {loginRequest.Email} does not exists." };
            }

            var loginResponse = new LoginResponse(existingCustomer.Value.Id, existingCustomer.Value.Email, existingCustomer.Value.UserName, existingCustomer.Value.FirstName, existingCustomer.Value.LastName, "");

            if (!existingCustomer.Value.IsActive)
            {
                existingCustomer.Value.IsActive = true;
                existingCustomer.Value.DeleteDate = null;

                _unitOfWork.CustomerRepository.Update(existingCustomer.Value);
                _unitOfWork.Commit();
                return new LoginResponseMessage() { IsSuccess = true, Message = $"Welcome back {existingCustomer.Value.UserName}, your account has been reactivated.", LoginResponse = loginResponse };
            }

            var verified = BCrypt.Net.BCrypt.Verify(loginRequest.Password, existingCustomer.Value.Password);

            if (!verified)
            {
                return new LoginResponseMessage() { IsSuccess = false, Message = $"Incorrect password." };
            }

            if (!existingCustomer.Value.EmailConfirmed)
            {
                return new LoginResponseMessage() { IsSuccess = false, Message = $"Email not confirmed." };
            }

            return new LoginResponseMessage() { IsSuccess = true, Message = "", LoginResponse = loginResponse };
        }

        public async Task<Maybe<RegistrationResponse>> Register(RegistrationRequest registrationRequest)
        {
            var existingCustomerUsername = await _unitOfWork.CustomerRepository.GetByUsername(registrationRequest.Username);
            var registerResponse = new RegistrationResponse($"Registration successful {registrationRequest.Username}. Please confirm your email.", true);

            if (existingCustomerUsername.HasValue)
            {
                registerResponse.Message = $"User with username: {existingCustomerUsername.Value.UserName} already exists.";
                registerResponse.IsSuccess = false;

                return registerResponse;
            }

            var existingCustomer = await _unitOfWork.CustomerRepository.GetByEmail(registrationRequest.Email);

            if (existingCustomer.HasValue)
            {
                registerResponse.Message = $"User with email: {existingCustomer.Value.Email} already exists.";
                registerResponse.IsSuccess = false;

                return registerResponse;
            }

            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(registrationRequest.Password);

            Customer newCustomer = new Customer(
                userName: registrationRequest.Username,
                email: registrationRequest.Email,
                password: hashedPassword,
                firstName: registrationRequest.FirstName,
                lastName: registrationRequest.LastName
                );

            await _unitOfWork.CustomerRepository.Create(newCustomer);
            _unitOfWork.Commit();

            var confirmationEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(newCustomer);
            var encodedEmailToken = Encoding.UTF8.GetBytes(confirmationEmailToken);
            var validEmailToken = WebEncoders.Base64UrlEncode(encodedEmailToken);

            string url = $"{_configuration["AppUrl"]}/api/users/confirmemail?userid={newCustomer.Id}&token={validEmailToken}";
            var customer = new CustomerEmailData(url, registrationRequest.FirstName, registrationRequest.LastName);

            _ = _emailService.SendRegistrationEmailAsync(newCustomer.Email, customer);

            return registerResponse;
        }

        public async Task<Maybe<EmailVerificationResponseMessage>> ConfirmEmail(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return new EmailVerificationResponseMessage("User not found", false);
            }

            var decodedToken = WebEncoders.Base64UrlDecode(token);
            var normalToken = Encoding.UTF8.GetString(decodedToken);

            var result = await _userManager.ConfirmEmailAsync(user, normalToken);
            if (result.Succeeded)
            {
                return new EmailVerificationResponseMessage("Email confirmed succesfully!", true);
            }

            return new EmailVerificationResponseMessage("Email not confirmed", false);
        }

        public async Task<GetByIdResponseMessage> GetById(Guid id)
        {
            var customer = await _unitOfWork.CustomerRepository.GetById(id);

            if (customer.HasNoValue || !customer.Value.IsActive)
            {
                return new GetByIdResponseMessage() { Message = $"User with id - {id} does not exist." };
            }

            var customerResponse = _mapper.Map<CustomerResponse>(customer.Value);

            return new GetByIdResponseMessage() { IsSuccess = true, Message = string.Empty, CustomerResponse = customerResponse };
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            var customers = await _unitOfWork.CustomerRepository.GetAll();

            if (customers.Count() < 1)
            {
                throw new NotFoundException("There are no users in database");
            }

            return customers;
        }

        public async Task<DeleteResponseMessage> DeleteUser(Guid id, bool hardDelete)
        {

            var user = await _unitOfWork.CustomerRepository.GetById(id);
            if (user == null)
            {
                return new DeleteResponseMessage() { IsSuccess = false, Message = "User does not exists." };
            }

            if (!user.Value.IsActive)
            {
                return new DeleteResponseMessage() { IsSuccess = false, Message = "User already deactivated." };
            }

            if (hardDelete)
            {
                await _unitOfWork.CustomerRepository.Delete(user.Value);
                _unitOfWork.Commit();
                return new DeleteResponseMessage() { IsSuccess = true, Message = "Successfully deleted." };
            }
            else
            {
                user.Value.IsActive = false;
                _unitOfWork.CustomerRepository.Update(user.Value);
                _unitOfWork.Commit();
                return new DeleteResponseMessage() { IsSuccess = true, Message = "User deactivated." };
            }
        }

        public async Task<UpdateResponseMessage> UpdateUser(Guid id, UpdateRequest updateRequest)
        {
            var user = await _unitOfWork.CustomerRepository.GetById(id);

            var updateResponse = new UpdateResponse() { Firstname = updateRequest.FirstName, Id = user.Value.Id, Lastname = updateRequest.LastName, Email = user.Value.Email, Username = user.Value.UserName };
            var updateResponseMessage = new UpdateResponseMessage() { Message = "Successfully updated", IsSuccess = true, UpdateResponse = updateResponse };

            if (user.HasNoValue)
            {
                return new UpdateResponseMessage() { IsSuccess = false, Message = "User not found." };
            }

            if (updateRequest.OldPassword == null && (updateRequest.Password != null || updateRequest.ConfirmPassword != null))
            {
                updateResponseMessage.IsSuccess = false;
                updateResponseMessage.Message = "Old password must be provided in order to change it.";
                return updateResponseMessage;
            }

            user.Value.FirstName = updateRequest.FirstName;
            user.Value.LastName = updateRequest.LastName;

            if (updateRequest.Password == null || updateRequest.OldPassword == null)
            {
                _unitOfWork.CustomerRepository.Update(user.Value);
                _unitOfWork.Commit();

                return updateResponseMessage;
            }
            else
            {
                var verified = BCrypt.Net.BCrypt.Verify(updateRequest.OldPassword, user.Value.Password);

                if (!verified)
                {
                    return new UpdateResponseMessage() { IsSuccess = false, Message = $"Old password is not correct." };
                }

                var hashedPassword = BCrypt.Net.BCrypt.HashPassword(updateRequest.Password);

                user.Value.Password = hashedPassword;

                _unitOfWork.CustomerRepository.Update(user.Value);
                _unitOfWork.Commit();

                return updateResponseMessage;
            }
        }
    }
}