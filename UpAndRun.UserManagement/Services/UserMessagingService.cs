﻿using NetMQ;
using UpAndRun.UserManagement.Interfaces;
using URCommunicationProtocolLibrary;
using URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels;
using Utility.Authentication;
using Utility.Constants;

namespace UpAndRun.UserManagement.Services
{
    public class UserMessagingService : BaseService, IUserMessagingService
    {
        private readonly IUserService _userService;

        public UserMessagingService(IUserService userService)
        {
            _userService = userService;
            Subscribe(MessageConstants.RegistrationRequestTopicName, HandleRequestForRegistration);
            Subscribe(MessageConstants.LoginRequestTopicName, HandleRequestForLogin);
            Subscribe(MessageConstants.RequestForEmailVerificationTopicName, HandleRequestForEmailConfirmation);
            Subscribe(MessageConstants.GetByIdRequestTopicName, HandleRequestForGetById);
            Subscribe(MessageConstants.DeleteRequestTopicName, HandleRequestForDeletion);
            Subscribe(MessageConstants.UpdateRequestTopicName, HandleRequestForUpdate);
        }

        private async void HandleRequestForDeletion(NetMQMessage msg)
        {
            var message = new DeleteRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new DeleteResponseMessage { Id = message.Id, Message = "Unauthorized!" });
                return;
            }

            var response = await _userService.DeleteUser(message.UserId, message.HardDelete);
            response.Id = message.Id;
            Send(response);
        }

        private async void HandleRequestForUpdate(NetMQMessage msg)
        {
            var message = new UpdateRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.Jwt))
            {
                Send(new UpdateResponseMessage { Id = message.Id, Message = "Unauthorized!" });
                return;
            }

            var response = await _userService.UpdateUser(message.UserId, message.UpdateRequest);
            response.Id = message.Id;
            Send(response);
        }

        public async void HandleRequestForRegistration(NetMQMessage msg)
        {
            var message = new RegistrationRequestMessage(msg);

            var response = await _userService.Register(message.RegistrationRequest);

            Send(new RegistrationResponseMessage() { Id = message.Id, RegistrationResponse = response.Value });
        }

        public async void HandleRequestForLogin(NetMQMessage msg)
        {
            var message = new LoginRequestMessage(msg);

            var response = await _userService.Login(message.LoginRequest);
            var loginResponseMessage = new LoginResponseMessage();

            if (response.HasValue)
            {
                loginResponseMessage.LoginResponse = response.Value.LoginResponse;
                loginResponseMessage.Id = message.Id;
                loginResponseMessage.Message = response.Value.Message;
                loginResponseMessage.IsSuccess = response.Value.IsSuccess;

                Send(loginResponseMessage);
            }
        }

        public async void HandleRequestForEmailConfirmation(NetMQMessage msg)
        {
            var message = new EmailVerificationRequestMessage(msg);

            var response = await _userService.ConfirmEmail(message.UserId, message.Token);
            response.Value.Id = message.Id;

            Send(response.Value);
        }

        public async void HandleRequestForGetById(NetMQMessage msg)
        {
            var message = new GetByIdRequestMessage(msg);

            if (!JwtAuthentication.IsTokenValid(message.JwtToken))
            {
               Send(new GetByIdResponseMessage { Id = message.Id, Message = "Unauthorized to access user management service!" });
               return;
            }

            var response = await _userService.GetById(message.CustomerId);
            response.Id = message.Id;

            Send(response);
        }
    }
}