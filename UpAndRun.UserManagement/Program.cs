﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using UpAndRun.UserManagement.Interfaces;
using UpAndRun.UserManagement.Mappings;
using UpAndRun.UserManagement.Services;
using UpAndRun.UserManagement.UpAndRun.Data;
using UpAndRun.UserManagement.UpAndRun.Data.Context;
using UpAndRun.UserManagement.UpAndRun.Data.Interfaces;
using Utility.SendGrid;

namespace UpAndRun.UserManagement
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("-------------------------------User management service-----------------------------------");

            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var host = new HostBuilder()

            .ConfigureHostConfiguration(configHost =>
            {
            })
            .ConfigureServices((hostContext, services) =>
            {               
                services.AddSingleton<IUserMessagingService, UserMessagingService>();
                services.AddSingleton<IUnitOfWork, UnitOfWork>();
                services.AddSingleton<IUserService, UserService>();
                services.AddSingleton<IConfiguration>(configuration);
                services.AddTransient<UserManager<IdentityUser>>();
                services.AddSingleton<IEmailService, EmailService>();
                services.AddScoped<IdentityDbContext, AppDBContext>();
                services.AddLogging(config =>
            {
                config.AddDebug();
                config.AddConsole();
            });
                services.AddDbContext<AppDBContext>();
                services.Configure<AppDBContextConfig>(conf => { conf.ConnectionString = configuration.GetConnectionString("UpAndRunDatabaseConn"); });
                services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredLength = 6;
            })
            .AddEntityFrameworkStores<AppDBContext>()
            .AddDefaultTokenProviders()
            .Services           
            .AddAutoMapper(typeof(CustomerResponseMapping))
            .BuildServiceProvider();
            })
           .UseConsoleLifetime()
           .Build();

            host.Services.GetService<IUserMessagingService>().StartAsync();
            host.Run();
        }
    }
}