﻿using CSharpFunctionalExtensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UpAndRun.Models.Entities;
using UpAndRun.Models.Requests;
using UpAndRun.Models.Responses;
using URCommunicationProtocolLibrary.MessageModels.UserManagement.MessageModels;

namespace UpAndRun.UserManagement.Interfaces
{
    public interface IUserService
    {
        public Task<Maybe<LoginResponseMessage>> Login(LoginRequest loginRequest);

        public Task<Maybe<RegistrationResponse>> Register(RegistrationRequest registrationRequest);

        public Task<Maybe<EmailVerificationResponseMessage>> ConfirmEmail(string userId, string token);

        public Task<GetByIdResponseMessage> GetById(Guid guid);

        public Task<IEnumerable<Customer>> GetAll();

        public Task<DeleteResponseMessage> DeleteUser(Guid id, bool hardDelete);

        public Task<UpdateResponseMessage> UpdateUser(Guid id, UpdateRequest updateRequest);
    }
}