﻿using NetMQ;
using URCommunicationProtocolLibrary;

namespace UpAndRun.UserManagement.Interfaces
{
    public interface IUserMessagingService : IBaseService
    {
        public void HandleRequestForRegistration(NetMQMessage msg);
    }
}