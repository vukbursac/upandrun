﻿namespace UpAndRun.UserManagement.UpAndRun.Data.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }

        void Commit();
    }
}