﻿using CSharpFunctionalExtensions;
using System.Threading.Tasks;
using UpAndRun.Models.Entities;

namespace UpAndRun.UserManagement.UpAndRun.Data.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Maybe<Customer>> GetByEmail(string email);

        Task<Maybe<Customer>> GetByUsername(string email);

        void DeleteInactiveUsers();
    }
}