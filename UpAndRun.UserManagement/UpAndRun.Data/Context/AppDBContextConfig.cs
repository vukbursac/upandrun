﻿namespace UpAndRun.UserManagement.UpAndRun.Data.Context
{
    public class AppDBContextConfig
    {
        public string ConnectionString { get; set; }
    }
}