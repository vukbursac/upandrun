﻿using System;
using UpAndRun.UserManagement.UpAndRun.Data.Context;
using UpAndRun.UserManagement.UpAndRun.Data.Interfaces;
using UpAndRun.UserManagement.UpAndRun.Data.Repositories;

namespace UpAndRun.UserManagement.UpAndRun.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDBContext _context;
        private ICustomerRepository _customerRepository;

        public UnitOfWork(AppDBContext context)
        {
            _context = context;
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                return _customerRepository = _customerRepository ?? new CustomerRepository(_context);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}