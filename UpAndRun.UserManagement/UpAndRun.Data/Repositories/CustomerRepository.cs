﻿using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UpAndRun.Models.Entities;
using UpAndRun.UserManagement.UpAndRun.Data.Context;
using UpAndRun.UserManagement.UpAndRun.Data.Interfaces;

namespace UpAndRun.UserManagement.UpAndRun.Data.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly AppDBContext db;

        public CustomerRepository(AppDBContext db)
        {
            this.db = db;
        }

        public async Task<Maybe<Customer>> Create(Customer customer)
        {
            await db.Customers.AddAsync(customer);

            return customer;
        }

        public void DeleteInactiveUsers()
        {
            var users = db.Customers.Where(user => user.DeleteDate <= DateTime.Now);
            db.Customers.RemoveRange(users);
        }

        public async Task Delete(Customer customer)
        {
            await Task.Run(() => db.Customers.Remove(customer));
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            return await db.Customers.OrderBy(customer => customer.LastName).ToListAsync();
        }

        public async Task<Maybe<Customer>> GetByEmail(string email)
        {
            return await db.Customers.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<Maybe<Customer>> GetById(Guid id)
        {
            return await db.Customers.FindAsync(id.ToString());
        }

        public async Task<Maybe<Customer>> GetByUsername(string username)
        {
            return await db.Customers.FirstOrDefaultAsync(customer => customer.UserName == username);
        }

        public void Update(Customer customer)
        {
            db.Entry(customer).State = EntityState.Modified;
        }
    }
}