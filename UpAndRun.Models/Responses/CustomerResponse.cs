﻿using ProtoBuf;

namespace UpAndRun.Models.Responses
{
    [ProtoContract]
    public class CustomerResponse
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string Email { get; set; }

        [ProtoMember(3)]
        public string Username { get; set; }

        [ProtoMember(4)]
        public string Firstname { get; set; }

        [ProtoMember(5)]
        public string Lastname { get; set; }
    }
}
