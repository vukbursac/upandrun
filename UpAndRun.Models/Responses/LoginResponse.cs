﻿using ProtoBuf;

namespace UpAndRun.Models.Responses
{
    [ProtoContract]
    public class LoginResponse
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string Email { get; set; }

        [ProtoMember(3)]
        public string Username { get; set; }

        [ProtoMember(4)]
        public string Firstname { get; set; }

        [ProtoMember(5)]
        public string Lastname { get; set; }

        [ProtoMember(6)]
        public string Token { get; set; }

        public LoginResponse(string id, string email, string username, string firstname, string lastname, string token)
        {
            Id = id;
            Email = email;
            Username = username;
            Firstname = firstname;
            Lastname = lastname;
            Token = token;
        }

        public LoginResponse()
        {
        }
    }
}