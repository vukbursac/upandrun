﻿using ProtoBuf;

namespace UpAndRun.Models.Responses
{
    [ProtoContract]
    public class UpdateResponse
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string Firstname { get; set; }

        [ProtoMember(3)]
        public string Lastname { get; set; }

        [ProtoMember(4)]
        public string Email { get; set; }

        [ProtoMember(5)]
        public string Username { get; set; }

        public UpdateResponse(string id, string firstname, string lastname, string email, string username)
        {
            Id = id;
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Username = username;
        }

        public UpdateResponse()
        {
        }
    }
}
