﻿using ProtoBuf;
using System.Collections.Generic;

namespace UpAndRun.Models.Responses
{
    [ProtoContract]
    public class RegistrationResponse
    {
        [ProtoMember(1)]
        public string Message { get; set; }

        [ProtoMember(2)]
        public bool IsSuccess { get; set; }

        [ProtoMember(3)]
        public IEnumerable<string> Errors { get; set; }

        public RegistrationResponse(string message, bool isSuccess, IEnumerable<string> errors)
        {
            Message = message;
            IsSuccess = isSuccess;
            Errors = errors;
        }

        public RegistrationResponse(string message, bool isSuccess)
        {
            Message = message;
            IsSuccess = isSuccess;
        }

        public RegistrationResponse()
        {
        }
    }
}