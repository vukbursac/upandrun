﻿using System;

namespace UpAndRun.Models.Requests
{
    public class DeleteUserRequest
    {
        public Guid Id { get; set; }
        public bool HardDelete { get; set; }

        public DeleteUserRequest(Guid id, bool hardDelete)
        {
            Id = id;
            HardDelete = hardDelete;
        }

        public DeleteUserRequest()
        {
        }
    }
}
