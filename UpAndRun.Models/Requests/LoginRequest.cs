﻿using ProtoBuf;
using System.ComponentModel.DataAnnotations;

namespace UpAndRun.Models.Requests
{
    [ProtoContract]
    public class LoginRequest
    {
        [ProtoMember(1)]
        [Required(ErrorMessage = "E-mail is required.")]
        [EmailAddress]
        public string Email { get; set; }

        [ProtoMember(2)]
        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}