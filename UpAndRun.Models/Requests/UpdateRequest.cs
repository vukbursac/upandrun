﻿using ProtoBuf;
using System.ComponentModel.DataAnnotations;
using UpAndRun.Models.ValidationAttributes;

namespace UpAndRun.Models.Requests
{
    [ProtoContract]
    public class UpdateRequest
    {
        [ProtoMember(1)]
        [Required]
        [MaxLength(50)]
        [UpperCaseFirst(ErrorMessage = "First name must start with an uppercase letter.")]
        public string FirstName { get; set; }

        [ProtoMember(2)]
        [Required]
        [MaxLength(50)]
        [UpperCaseFirst(ErrorMessage = "Last name must start with an uppercase letter.")]
        public string LastName { get; set; }

        [ProtoMember(3)]
        public string OldPassword { get; set; }

        [ProtoMember(4)]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Password must contain at least 6 characters.")]
        [UpdatePasswordValidation(ErrorMessage = "Password must contain at least one lowercase letter, one uppercase letter and one number.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }
    }
}
