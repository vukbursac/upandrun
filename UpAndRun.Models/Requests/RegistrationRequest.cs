﻿using ProtoBuf;
using System.ComponentModel.DataAnnotations;
using UpAndRun.Models.ValidationAttributes;

namespace UpAndRun.Models.Requests
{
    [ProtoContract]
    public class RegistrationRequest
    {
        [ProtoMember(1)]
        [Required]
        [MaxLength(50)]
        [UpperCaseFirst(ErrorMessage = "First name must start with an uppercase letter.")]
        public string FirstName { get; set; }

        [ProtoMember(2)]
        [Required]
        [MaxLength(50)]
        [UpperCaseFirst(ErrorMessage = "Last name must start with an uppercase letter.")]
        public string LastName { get; set; }

        [ProtoMember(3)]
        [Required(ErrorMessage = "Username is required.")]
        [MaxLength(32, ErrorMessage = "Username must not be longer than 32 characters.")]
        [RegularExpression(@"^[a-zA-Z0-9_\\-]+$", ErrorMessage = "Only letters, numbers, - and _ are allowed.")]
        [NoWhiteSpaces(ErrorMessage = "No white spaces allowed.")]
        public string Username { get; set; }

        [ProtoMember(4)]
        [Required(ErrorMessage = "E-mail is required.")]
        [EmailValidation(ErrorMessage = "Invalid e-mail address.")]
        public string Email { get; set; }

        [ProtoMember(5)]
        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Password must contain at least 6 characters.")]
        [PasswordValidation(ErrorMessage = "Password must contain at least one lowercase letter, one uppercase letter and one number.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }

        public RegistrationRequest(string firstName, string lastName, string username, string email, string password, string confirmPassword)
        {
            FirstName = firstName;
            Email = email;
            LastName = lastName;
            Username = username;
            Password = password;
            ConfirmPassword = confirmPassword;
        }

        public RegistrationRequest()
        {
        }
    }
}