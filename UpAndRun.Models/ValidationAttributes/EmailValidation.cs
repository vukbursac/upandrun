﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace UpAndRun.Models.ValidationAttributes
{
    internal class EmailValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            string expression = "^(?!\\.)(?!.*\\.$)(?!.*?\\.\\.)[a-zA-Z0-9.]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            string email = value?.ToString();

            if (Regex.IsMatch(email, expression) && !email.Contains(".@"))
            {
                return true;
            }

            return false;
        }
    }
}