﻿using System.ComponentModel.DataAnnotations;

namespace UpAndRun.Models.ValidationAttributes
{
    internal class NoWhiteSpaces : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            return !string.IsNullOrWhiteSpace(value.ToString());
        }
    }
}