﻿using System.ComponentModel.DataAnnotations;

namespace UpAndRun.Models.ValidationAttributes
{
    internal class PasswordValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            const int MIN_LENGTH = 6;
            bool meetsLengthRequirements = value.ToString().Length >= MIN_LENGTH;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in value.ToString())
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            bool isValid = hasUpperCaseLetter
                        && hasLowerCaseLetter
                        && hasDecimalDigit;

            return isValid;
        }
    }
}