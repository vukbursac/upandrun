﻿using System.ComponentModel.DataAnnotations;

namespace UpAndRun.Models.ValidationAttributes
{
    public class UpperCaseFirst : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            return !string.IsNullOrEmpty(value.ToString()) && char.IsUpper(value.ToString()[0]);
        }
    }
}