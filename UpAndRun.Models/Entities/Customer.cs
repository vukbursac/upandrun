﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace UpAndRun.Models.Entities
{
    public class Customer : IdentityUser
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public string Password { get; set; }

        public bool IsActive { get; set; }
        public DateTime? DeleteDate { get; set; }

        public Customer(string firstName, string lastName, string password, string userName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Password = password;
            UserName = userName;
            Email = email;
            IsActive = true;
            DeleteDate = null;
        }
    }
}