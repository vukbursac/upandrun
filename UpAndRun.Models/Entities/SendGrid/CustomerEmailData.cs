﻿using Newtonsoft.Json;

namespace UpAndRun.Models.Entities.SendGrid
{
    public class CustomerEmailData
    {
        [JsonProperty(PropertyName = "urlToRedirect")]
        public string UrlToRedirect { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        public CustomerEmailData(string urlToRedirect, string firstName, string lastName)
        {
            UrlToRedirect = urlToRedirect;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}