﻿using ProtoBuf;
using System.Collections.Generic;

namespace UpAndRun.Models.Helpers
{
    [ProtoContract]
    public class UnsuccessfulResponse
    {
        [ProtoMember(1)]
        public string Message { get; set; }

        [ProtoMember(2)]
        public bool IsSuccess { get; set; }

        [ProtoMember(3)]
        public IEnumerable<string> Errors { get; set; }

        public UnsuccessfulResponse(string message, bool isSuccess)
        {
            Message = message;
            IsSuccess = isSuccess;
        }

        public UnsuccessfulResponse()
        {
        }
    }
}